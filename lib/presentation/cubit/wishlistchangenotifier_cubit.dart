import 'package:bloc/bloc.dart';
import 'package:grobay/api/model/product.dart';

class WishlistchangenotifierCubit extends Cubit<WishListStatusChange> {
  WishlistchangenotifierCubit() : super(null);

  productWishListStatusChanged(
    Product product,
    bool wishListStatus
  ) {
    product.isFavorite=wishListStatus;
    emit(WishListStatusChange(productId: product.id,product: product));
  }
}


class WishListStatusChange {
  Product product;
  String productId;
  WishListStatusChange({this.productId,this.product});
}