import 'package:get/get.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/model/cart.dart';
import 'package:grobay/api/response/order_fetch_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:queue/queue.dart';

class CartCubit extends HydratedCubit<CartCubitState> {
  final Queue updateQueue = Queue();
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  CartCubit()
      : super(CartCubitState(count: Map<String, int>(), variantIds: []));

  // Clear all data from this cubit
  clearAll() {
    emit(CartCubitState(count: Map<String, int>(), variantIds: []));
  }

  // Add all Variant/Count to Server Database
  addAllVariant(String userId) async {
    List<String> qty = [];
    this.getVaraintNonDuplicateArray().forEach((vid) {
      qty.add(this.getVariantsCount(vid).toString());
    });
    if (this.getVaraintNonDuplicateArray().length > 0 &&
        this.state.isAlreadyUpdated == false)
      return await this
          .apiBridge
          .saveItemsToCart(pvid: this.getVaraintNonDuplicateArray(), qty: qty);
  }

  // Add Products in a Order to Cart (Server database)
  reorder(Order order) async {
    try {
      List<String> pvids = [];
      List<String> qty = [];
      order.items.forEach((element) {
        pvids.add(element.variantId);
        qty.add(element.quantity);
        this
            .state
            .count
            .addAll({element.variantId: int.parse(element.quantity)});
      });
      emit(CartCubitState(
          isFromCart: this.state.isFromCart,
          isLoading: true,
          count: this.state.count,
          lastUpdatedVariantId: null,
          variantIds: this.state.variantIds,
          isAlreadyUpdated: false));
      await this.apiBridge.saveItemsToCart(pvid: pvids, qty: qty);
      emit(CartCubitState(
          isFromCart: this.state.isFromCart,
          isReorderSuccess: true,
          count: this.state.count,
          lastUpdatedVariantId: null,
          variantIds: this.state.variantIds,
          isAlreadyUpdated: false));
    } catch (e) {
      emit(CartCubitState(
          isFromCart: this.state.isFromCart,
          isReorderFailed: true,
          count: this.state.count,
          lastUpdatedVariantId: null,
          variantIds: this.state.variantIds,
          isAlreadyUpdated: false));
    }
  }

  // Sync offline cart data and server data
  syncCartUserData(List<CartItem> cartItems) {
    this.clearAll();
    cartItems.forEach((ci) {
      this.addProductVariant(ci.productId, ci.productVariantId,
          double.parse(ci.item[0].stock), double.parse(ci.item[0].measurement),
          isFromCart: true, qty: int.parse(ci.qty));
    });

    emit(CartCubitState(
        isFromCart: this.state.isFromCart,
        count: this.state.count,
        lastUpdatedVariantId: null,
        variantIds: this.state.variantIds,
        isAlreadyUpdated: true));
  }

  // Add a product to cart (Server Database)
  addProductVariant(String pid, String vid, double stock, double measurement,
      {int qty, String userId, bool isFromCart = false}) {
    CartCubitState previousState = this.state;
    int productCount = previousState.count[pid] ?? 0;
    int variantCount = this.getVariantsCount(vid) ?? 0;

    if (variantCount < AppConfig.MAXIMUM_ITEMS_ALLOWED_IN_CART) {
      if ((variantCount.toDouble() * measurement) < stock &&
          variantCount < 10) {
        if (qty != null) {
          productCount += qty;
          while (qty > 0) {
            previousState.variantIds.add(vid);
            qty--;
          }
        } else {
          productCount++;
          previousState.variantIds.add(vid);
        }
        previousState.count[pid] = productCount;
      }

      if (userId != null) {
        updateQueue.add(() => this.apiBridge.saveItemToCart(
              pid: pid,
              pvid: vid,
              qty: this.getVariantsCount(vid).toString(),
            ));
      }
      emit(CartCubitState(
          isFromCart: isFromCart,
          count: previousState.count,
          lastUpdatedVariantId: vid,
          variantIds: previousState.variantIds));
    } else {
      Get.closeAllSnackbars();
      Get.showSnackbar(GetSnackBar(
        duration: Duration(seconds: 2),
        message: "Apologies maximum count reached",
      ));
    }
  }

  // Decrease count for a product from cart (Server Database)
  removeProductVariant(String pid, String vid,
      {String userId, bool isFromCart = false}) {
    CartCubitState previousState = this.state;
    previousState.variantIds.remove(vid);
    int productCount = previousState.count[pid] ?? 0;
    if (this.getVariantsCount(vid) != 0) {
      productCount--;
      previousState.count[pid] = productCount;
    } else {
      previousState.count.remove(pid);
    }

    emit(CartCubitState(
      count: previousState.count,
      variantIds: previousState.variantIds,
      lastUpdatedVariantId: vid,
      isFromCart: isFromCart,
    ));
    if (userId != null) {
      updateQueue.add(() => this.apiBridge.saveItemToCart(
            pid: pid,
            pvid: vid,
            qty: this.getVariantsCount(vid).toString(),
          ));
    }
  }

  // Delete a product from cart (Server Database)
  deleteProductVariant(String pid, String vid,
      {String userId, bool isFromCart = false}) {
    CartCubitState previousState = this.state;
    int productCount = previousState.count[pid];
    previousState.variantIds.forEach((element) {
      if (element == vid) {
        productCount--;
      }
    });
    if (productCount != null) {
      previousState.count[pid] = productCount;
      previousState.variantIds.removeWhere((element) => element == vid);
      if (userId != null) {
        this.apiBridge.saveItemToCart(pid: pid, pvid: vid, qty: "0");
      }
    } else {
      previousState.count.remove(pid);
    }
    emit(CartCubitState(
        isFromCart: isFromCart,
        count: previousState.count,
        lastUpdatedVariantId: vid,
        variantIds: previousState.variantIds));
  }

  // Get a List of all product varaint id
  List<String> getVaraintNonDuplicateArray() {
    return this.state.variantIds.toSet().toList();
  }

  int getVariantsCount(String vid) {
    int count = 0;
    this.state.variantIds.forEach((element) {
      if (element == vid) {
        count++;
      }
    });
    return count;
  }

  // Check if a product is available in selected pincode
  bool checkProductAvailabelInPincode(
      String productPincodeIds, String selectedPincodeId) {
    List<String> pincodeList = productPincodeIds.trim().split(",");
    if (pincodeList.length == 0 ||
        pincodeList[0] == "" ||
        pincodeList.contains(selectedPincodeId)) {
      return true;
    }
    return false;
  }

  @override
  CartCubitState fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> temp = json['count'];
    Map<String, int> tempCount = Map();
    temp.forEach((key, val) {
      tempCount[key] = val;
    });
    return CartCubitState(
        variantIds: json['variantIds'],
        count: tempCount,
        isAlreadyUpdated: json['isAlreadyUpdated']);
  }

  @override
  Map<String, dynamic> toJson(CartCubitState state) {
    return state.toJson();
  }
}

class CartCubitState {
  // Contains the product varaint id ( Product Variant Id May Be In Multiples )
  List<String> variantIds;
  // Product varaint id as key and count as value
  Map<String, int> count;

  // This will be true if cart and database in server is in sync
  final bool isAlreadyUpdated;

  // Used For Reorder
  final bool isReorderSuccess;
  final bool isReorderFailed;

  // Error Message from API fails etc.
  final String errMsg;

  // Used to signal that some async function is running
  final bool isLoading;
  // Contains the last updated Product Variant Id
  final lastUpdatedVariantId;
  // This is to handle toast behaviour in productdetailspage
  // if this is true toast will not be shown
  // Used when updating product count from cart page;
  bool isFromCart;
  CartCubitState({
    this.variantIds,
    this.isLoading = false,
    this.isReorderSuccess = false,
    this.isReorderFailed = false,
    this.isFromCart = false,
    this.isAlreadyUpdated = false,
    this.errMsg,
    this.lastUpdatedVariantId,
    this.count,
  });

  Map<String, dynamic> toJson() {
    return {
      "variantIds": variantIds,
      "count": count,
      "isAlreadyUpdated": this.isAlreadyUpdated
    };
  }
}
