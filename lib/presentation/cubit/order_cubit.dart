import 'package:bloc/bloc.dart';
import 'package:grobay/api/response/order_fetch_response.dart';
import 'package:grobay/core/constant/app_constants.dart';
import 'package:intl/intl.dart';

List<String> ORDER_LEVELS = [
  AppConstants.ORDER_RECIEVED,
  AppConstants.ORDER_PROCESSED,
  AppConstants.ORDER_SHIPPED,
  AppConstants.ORDER_DELIVERED,
  AppConstants.ORDER_CANCELLED,
  AppConstants.ORDER_RETURNED,
];

class OrderCubit extends Cubit<String> {
  OrderCubit() : super("");

  bool checkIfCancellable(Order order, OrderedProduct orderedProduct) {
    int activeIndex = 0, tillIndex = 0;
    activeIndex = ORDER_LEVELS.indexOf(orderedProduct.activeStatus);
    tillIndex = ORDER_LEVELS.indexOf(orderedProduct.tillStatus);
    if (activeIndex < tillIndex && orderedProduct.cancelableStatus == '1') {
      return true;
    }
    return false;
  }

  checkIfReturnable(Order order, OrderedProduct orderedProduct) {
    var inputFormat = DateFormat('dd-MM-yyyy hh:mm:ss:aa');
    String date =
        orderedProduct.status[orderedProduct.status.length - 1][1];
    String lastUpdatedDate = date.substring(0,date.length-2);
    String amPm = date.substring(date.length-2,date.length);
    String finalDateString = lastUpdatedDate+":"+amPm.toUpperCase();
    DateTime deliveredDate = inputFormat.parse(finalDateString);
    DateTime today = DateTime.now();
    if (orderedProduct.returnStatus == "1" &&
        orderedProduct.activeStatus == AppConstants.ORDER_DELIVERED &&
        deliveredDate.difference(today).inDays <=
            double.parse(orderedProduct.returnDays)) {
      return true;
    }
    return false;
  }

  checkIfAlreadyCancelled(OrderedProduct orderedProduct) {
    if (orderedProduct.activeStatus == AppConstants.ORDER_CANCELLED) {
      return true;
    }
    return false;
  }

  checkIfAlreadyreturned(OrderedProduct orderedProduct) {
    if (orderedProduct.activeStatus == AppConstants.ORDER_RETURNED) {
      return true;
    }
    return false;
  }
}
