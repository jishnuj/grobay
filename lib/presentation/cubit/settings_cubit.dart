import 'package:bloc/bloc.dart';
import 'package:grobay/api/model/time_slots.dart';
import 'package:grobay/api/response/payment_settings_response.dart';
import 'package:grobay/core/constant/payment_methods.dart';

class SettingsCubit extends Cubit<SettingsCubitState> {
  SettingsCubit() : super(SettingsCubitState(dateIndex: 1));

  reset() {
    emit(SettingsCubitState(
      dateIndex: 1,
    ));
  }

  setNote(String note) {
    emit(SettingsCubitState(
      note: note,
      paymentSettingsResponse: this.state.paymentSettingsResponse,
      paymentMethod: this.state.paymentMethod,
      date: this.state.date,
      dateIndex: this.state.dateIndex,
      timeSlots: this.state.timeSlots,
    ));
  }

  setPaymentResponse(PaymentSettingsResponse paymentSettingsResponse) {
    emit(SettingsCubitState(
      note: this.state.note,
      paymentSettingsResponse: paymentSettingsResponse,
      paymentMethod: this.state.paymentMethod,
      date: this.state.date,
      dateIndex: this.state.dateIndex,
      timeSlots: this.state.timeSlots,
    ));
  }

  setPaymentMethod(PAYMENT_METHODS method) {
    emit(SettingsCubitState(
      note: this.state.note,
      paymentSettingsResponse: this.state.paymentSettingsResponse,
      paymentMethod: method,
      date: this.state.date,
      dateIndex: this.state.dateIndex,
      timeSlots: this.state.timeSlots,
    ));
  }



  setTime(TimeSlots timeSlot) {
    emit(SettingsCubitState(
      note: this.state.note,
      paymentSettingsResponse: this.state.paymentSettingsResponse,
      paymentMethod: this.state.paymentMethod,
      date: this.state.date,
      dateIndex: this.state.dateIndex,
      timeSlots: timeSlot,
    ));
  }

  setDate(String date, int dateIndex) {
    emit(SettingsCubitState(
      note: this.state.note,
      paymentSettingsResponse: this.state.paymentSettingsResponse,
      paymentMethod: this.state.paymentMethod,
      date: date,
      dateIndex: dateIndex,
      timeSlots: this.state.timeSlots,
    ));
  }
}

class SettingsCubitState {
  final PaymentSettingsResponse paymentSettingsResponse;
  final PAYMENT_METHODS paymentMethod;
  final TimeSlots timeSlots;
  final String date;
  final int dateIndex;
  final String note;

  SettingsCubitState({
    this.paymentMethod,
    this.dateIndex,
    this.paymentSettingsResponse,
    this.date,
    this.timeSlots,
    this.note,
  });
}
