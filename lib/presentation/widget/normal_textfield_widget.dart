import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:grobay/core/constant/palette.dart';

class NormalTextField extends StatelessWidget {
  final formKey;
  final Function(String) validator;
  final FORM_VALIDATORS defaultValidator;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final String hintText;
  final IconData icon;
  final bool enabled;
  const NormalTextField(
      {Key key,
      this.formKey,
      this.enabled = true,
      this.icon,
      this.controller,
      this.defaultValidator,
      this.keyboardType,
      this.validator,
      this.hintText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Palette.teritaryColor))),
      child: TextFormField(
        onChanged: (value) {
          // formKey.currentState.validate();
        },
        validator: validator != null
            ? validator
            : (this.defaultValidator != null)
                ? getValidators(this.defaultValidator)
                : null,
        controller: controller ?? null,
        keyboardType: this.keyboardType ?? TextInputType.text,
        decoration: _buildInputDecoration(),
      ),
    );
  }

  InputDecoration _buildInputDecoration() {
    return InputDecoration(
        enabled: this.enabled,
        prefixIcon: this.icon != null ? Icon(this.icon) : null,
        hintText: hintText,
        hintStyle: TextStyle(color: Palette.disabledColor),
        border: InputBorder.none);
  }
}

enum FORM_VALIDATORS { NAME, PHONE, EMAIL, PASSWORD, REFERRALCODE }

extension StorageKeysExtension on FORM_VALIDATORS {
  String get key {
    switch (this) {
      case FORM_VALIDATORS.NAME:
        return "name";
      case FORM_VALIDATORS.PHONE:
        return "phone";
      case FORM_VALIDATORS.EMAIL:
        return "email";
      case FORM_VALIDATORS.PASSWORD:
        return "password";
      case FORM_VALIDATORS.REFERRALCODE:
        return "refferalCode";
      default:
        return null;
    }
  }
}

Function(String) getValidators(FORM_VALIDATORS formValidators) {
  if (formValidators.key == "name") {
    return nameValidator;
  } else if (formValidators.key == "email") {
    return emailValidator;
  } else if (formValidators.key == "refferalCode") {
    return referralCodeValidator;
  } else if (formValidators.key == "password") {
    return passwordValidator;
  }
  return null;
}

Function(String) nameValidator = (value) {
  if (value == null || value.isEmpty) {
    return tr('pleaseEnterYourName');
  } else if (!RegExp(r"^\s*([A-Za-z]{1,}([\.,] |[-']| ))+[A-Za-z]+\.?\s*$")
      .hasMatch(value)) {
    return tr('pleaseEnterValidName');
  }
  return null;
};

Function(String) emailValidator = (value) {
  if (value == null || value.isEmpty) {
    return tr('pleaseEnterYourEmail');
  } else if (!RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(value)) {
    return tr('pleaseEnterValidEmailAddress');
  }
  return null;
};

Function(String) referralCodeValidator = (value) {
  if (value.isNotEmpty && value.length < 3){
    return tr('pleaseEnterValidReferralCode');
  }
  return null;
};

Function(String) passwordValidator = (value) {
  if (value == null || value.isEmpty) {
    return tr('pleaseEnterYourPassword');
  } else if (value.length < 8) {
    return tr('passwordShouldBe8CharactersLong');
  }
  return null;
};
