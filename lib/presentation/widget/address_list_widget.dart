import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/cities/cities_bloc.dart';
import 'package:grobay/presentation/bloc/user_address/useraddress_bloc.dart';
import 'package:grobay/presentation/page/address/add_address_page.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/skelton_loaders.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class AddressListWidget extends StatelessWidget {
  AddressListWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UseraddressBloc, UseraddressState>(
      builder: (context, addressState) {
        if (addressState is UseraddressSuccess) {
          return SliverPadding(
            padding: EdgeInsets.all(10.0),
            sliver: SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
              return _buildAddressCard(addressState, index, context);
            }, childCount: addressState.userAddressResponse.data.length)),
          );
        }
        if (addressState is UseraddressFailed) {
          return _buildErrorBox(context, addressState);
        }
        return _buildLoader(context);
      },
    );
  }

  SliverToBoxAdapter _buildLoader(BuildContext context) {
    return SliverToBoxAdapter(
      child:AddressListLoader()
    );
  }

  SliverToBoxAdapter _buildErrorBox(
      BuildContext context, UseraddressFailed addressState) {
    return SliverToBoxAdapter(
        child: Container(
      height: MediaQuery.of(context).size.height / 1.5,
      child: CustomErrorWidget(
        msg: addressState.message,
        onNetworkError: () {},
      ),
    ));
  }

  InkWell _buildAddressCard(
      UseraddressSuccess addressState, int index, BuildContext context) {
    return InkWell(
      onTap: () {
        _onAddressSelect(context, addressState, index);
      },
      child: Container(
          clipBehavior: Clip.hardEdge,
          margin: EdgeInsets.symmetric(vertical: 5.0),
          decoration: _buildAddressCardDecoration(addressState, index, context),
          // color: Theme.of(context).accentColor,
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
          child: Row(
            children: [
              _buildAddressSelectIcon(addressState, index, context),
              UIUtilWidget.horizontalSpaceMedium(),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          "${addressState.userAddressResponse.data[index].name}",
                          style: Theme.of(context)
                              .primaryTextTheme
                              .headline6
                              .copyWith(
                                color: Palette.onSecondary,
                              ),
                        ),
                        UIUtilWidget.horizontalSpaceMedium(),
                        Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 5.0, vertical: 5.0),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                color: Palette.teritaryColor),
                            child: Text(
                              "${addressState.userAddressResponse.data[index].type}",
                              style:
                                  Theme.of(context).textTheme.subtitle2.copyWith(),
                            )),
                      ],
                    ),
                    UIUtilWidget.verticalSpaceSmall(),
                    Text(
                        "${addressState.userAddressResponse.data[index].address} ${addressState.userAddressResponse.data[index].city} ${addressState.userAddressResponse.data[index].state}   ${addressState.userAddressResponse.data[index].country}  ${addressState.userAddressResponse.data[index].pincode}",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                                fontWeight: FontWeight.w200,
                                color: Colors.grey[900])),
                    UIUtilWidget.verticalSpaceCustom(8.0),
                    Row(
                      children: [
                        Icon(
                          Icons.phone_android,
                          size: 14.0,
                        ),
                        UIUtilWidget.horizontalSpaceSmall(),
                        Text(
                            "${addressState.userAddressResponse.data[index].mobile}",
                            style: Theme.of(context)
                                .primaryTextTheme
                                .subtitle2
                                .copyWith(
                                  color: Palette.onSecondary,
                                )),
                      ],
                    ),
                  ],
                ),
              ),
              _buildAddressControls(context, addressState, index)
            ],
          )),
    );
  }

  Column _buildAddressControls(
      BuildContext context, UseraddressSuccess addressState, int index) {
    return Column(
      children: [
        IconButton(
          color: Colors.grey,
          icon: Icon(Icons.delete),
          onPressed: () {
            BlocProvider.of<UseraddressBloc>(context).add(
                DeleteUserAddressEvent(
                    addressId:
                        addressState.userAddressResponse.data[index].id));
          },
        ),
        IconButton(
          color: Colors.grey,
          icon: Icon(Icons.edit),
          onPressed: () {
            BlocProvider.of<CitiesBloc>(context).add(SelectCityByIdevent(
                cityId: addressState.userAddressResponse.data[index].cityId));
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return AddAddressPage(
                updateAddress: addressState.userAddressResponse.data[index],
                isUpdate: true,
              );
            }));
          },
        ),
      ],
    );
  }

  Container _buildAddressSelectIcon(
      UseraddressSuccess addressState, int index, BuildContext context) {
    return Container(
        width: 50,
        child: Icon(
          addressState.selectedAddress.id ==
                  addressState.userAddressResponse.data[index].id
              ? Icons.check_circle_sharp
              : Icons.location_pin,
          color: addressState.selectedAddress.id ==
                  addressState.userAddressResponse.data[index].id
              ? Palette.addressSelectedColor
              : Theme.of(context).highlightColor,
          size: addressState.selectedAddress.id ==
                  addressState.userAddressResponse.data[index].id
              ? 50.0
              : 30.0,
        ));
  }

  BoxDecoration _buildAddressCardDecoration(
      UseraddressSuccess addressState, int index, BuildContext context) {
    return BoxDecoration(
        color: Colors.white,
        boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 5.0)],
        borderRadius: BorderRadius.circular(20.0),
        border: Border.all(
            width: addressState.selectedAddress.id ==
                    addressState.userAddressResponse.data[index].id
                ? 3.0
                : 1.0,
            color: addressState.selectedAddress.id ==
                    addressState.userAddressResponse.data[index].id
                ? Palette.addressSelectedColor
                :Palette.onPrimary));
  }

  void _onAddressSelect(
      BuildContext context, UseraddressSuccess addressState, int index) {
    BlocProvider.of<UseraddressBloc>(context).add(SelectUserAddressEvent(
        address: addressState.userAddressResponse.data[index]));
  }
}
