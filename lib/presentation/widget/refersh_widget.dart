import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CustomRefreshWidget extends StatelessWidget {
  final Widget child;
  final Function onRefresh;
  CustomRefreshWidget({Key key, this.child, this.onRefresh}) : super(key: key);

  final RefreshController _refreshController = RefreshController(
      initialRefresh: false, initialRefreshStatus: RefreshStatus.idle);

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      controller: _refreshController,
      enablePullDown: true,
      header: WaterDropHeader(),
      child: this.child,
      onRefresh: () async {
        await this.onRefresh();
        await Future.delayed(Duration(milliseconds: 1000));
        this._refreshController.refreshCompleted();
      },
    );
  }
}
