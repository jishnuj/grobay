import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/location.dart';
import 'package:grobay/presentation/bloc/location/location_bloc.dart';
import 'package:grobay/presentation/bloc/startup/startup_bloc.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class PincodeList extends StatelessWidget {
  const PincodeList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _resetLocationBloc(context);
    return BottomSheet(
      enableDrag: false,
      onClosing: () {},
      builder: (context) {
        return BlocConsumer<LocationBloc, LocationState>(
          listener: (context, locationState) {
            if (locationState is LocationSuccess) {
              _reloadStartupData(context);
            }
          },
          builder: (context, locationState) {
            if (locationState is LocationSuccess) {
              return Container(
                padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
                height: MediaQuery.of(context).size.height / 1.5,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            tr('defaultDeliveryLocation'),
                            style: Theme.of(context).textTheme.headline5,
                          ),
                          InkWell(child: Icon(Icons.arrow_drop_down),onTap: (){
                            Navigator.pop(context);
                          },)
                        ],
                      ),
                      Text(tr('selectAndOnlyDelProWillBeShown')),
                      UIUtilWidget.verticalSpaceCustom(15.0),
                      _buildPincodeSearch(context),
                      UIUtilWidget.verticalSpaceCustom(25.0),
                      _buildPincodeList(context, locationState),
                    ],
                  ),
                ),
              );
            } else if (locationState is LocationFailed) {
              return _buildCategoryErrorBox(locationState, context);
            }
            return CircularProgressIndicator();
          },
        );
      },
    );
  }

  Widget _buildCategoryErrorBox(
      LocationFailed locationFailed, BuildContext context) {
    return Container(
      child: CustomErrorWidget(
        callerName: "pincode_list",
        msg: locationFailed.message,
        // onNetworkError: () {
        //   BlocProvider.of<StartupBloc>(context).add(FetchStartupEvent());
        // },
      ),
    );
  }

  Container _buildPincodeList(
      BuildContext context, LocationSuccess locationState) {
    return Container(
      height: (MediaQuery.of(context).size.height / 1.5 - 167),
      child: ListView(
        children: locationState.locationResposne.locations
            .map((e) => _buildTileButton(context, e))
            .toList(),
        shrinkWrap: true,
      ),
    );
  }

  InkWell _buildTileButton(BuildContext context, Location e) {
    return InkWell(
      onTap: () {
        _onSelectLocation(context, e);
      },
      child: Container(
          margin: EdgeInsets.symmetric(vertical: 5.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              border: Border.all(color: Colors.black)),
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
          child: Text(
            e.pincode,
            style: Theme.of(context).textTheme.headline5,
          )),
    );
  }

  void _onSelectLocation(BuildContext context, Location e) {
    BlocProvider.of<LocationBloc>(context)
        .add(SelectLocationEvent(location: e));
    Navigator.pop(context);
  }

  Row _buildPincodeSearch(BuildContext context) {
    return Row(
      children: [
        Expanded(child: TextField(
          onChanged: (value) {
            BlocProvider.of<LocationBloc>(context)
                .add(SearchLocationEvent(searchTerm: value));
          },
        )),
      ],
    );
  }

  void _reloadStartupData(BuildContext context) {
    BlocProvider.of<StartupBloc>(context).add(FetchStartupEvent());
  }

  void _resetLocationBloc(BuildContext context) {
    if (BlocProvider.of<LocationBloc>(context).originalStateBeforeSearch !=
        null) {
      BlocProvider.of<LocationBloc>(context).add(ResetLocationSearchEvent());
    }
  }
}
