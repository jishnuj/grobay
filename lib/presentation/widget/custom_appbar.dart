import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/constant/assets.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/location/location_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/widget/banners.dart';
import 'package:grobay/presentation/widget/pincode_list_widget.dart';

class CustomAppbar extends StatefulWidget {
  final bool showBanner;
  final bool showLocation;
  final String title;
  final double elevation;
  final bool showAction;
  final bool showSearchField;
  final bool showSearchIcon;
  final bool showTitle;
  final bool showLogo;
  CustomAppbar(
      {Key key,
      this.showBanner = true,
      this.showLocation = false,
      this.showLogo = true,
      this.showAction = true,
      this.showSearchField = true,
      this.showSearchIcon = false,
      this.showTitle = false,
      this.title,
      this.elevation})
      : super(key: key);

  @override
  _CustomAppbarState createState() => _CustomAppbarState();
}

class _CustomAppbarState extends State<CustomAppbar> {
  @override
  Widget build(BuildContext context) {
    return SliverLayoutBuilder(builder: (context, snapshot) {
      return SliverAppBar(
          backgroundColor: Palette.secondary,
          elevation: widget.elevation ?? 5.0,
          
          title: widget.showTitle
              ? Text(
                  widget.title,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2
                      .copyWith(color: Palette.onSecondary),
                )
              : widget.showLogo
                  ? Image.asset(
                      Assets.LOGO,
                      width: 50,
                    )
                  : SizedBox(),
          pinned: true,
          expandedHeight: widget.showBanner ? 394.0 : 70.0,
          // centerTitle: widget.showTitle ? false : true,
          flexibleSpace: FlexibleSpaceBar(
            stretchModes: [StretchMode.zoomBackground],
            collapseMode: CollapseMode.parallax,
            background: Container(
              child: Column(
                children: [
                  SizedBox(
                    height: 50,
                  ),
                  _buildLocationText(context),
                  SizedBox(
                    height: 20,
                  ),
                  if (widget.showSearchField)
                    InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, "search");
                      },
                      child: _buildSearchBox(),
                    ),
                  if (widget.showBanner) Banners()
                ],
              ),
            ),
          ),
          actions: widget.showAction
              ? <Widget>[
                  if ((snapshot.scrollOffset > 205 || widget.showSearchIcon))
                    IconButton(
                      icon: const Icon(Icons.search),
                      tooltip: tr('searchProducts'),
                      onPressed: () {
                        Navigator.pushNamed(context, "search");
                      },
                    ),
                  Stack(
                    children: [
                      IconButton(
                        icon: const Icon(Icons.shopping_cart_outlined),
                        tooltip: tr('cart'),
                        onPressed: () {
                          /* ... */
                          Navigator.pushNamed(context, "cart");
                        },
                      ),
                      Positioned(
                          right: 5.0,
                          bottom: 5.0,
                          child: Container(
                            padding: EdgeInsets.all(2.0),
                            alignment: Alignment.center,
                            width: 22,
                            height: 22,
                            decoration: BoxDecoration(
                                color: Palette.buttonBg,
                                borderRadius: BorderRadius.circular(50.0)),
                            child: BlocBuilder<CartCubit, CartCubitState>(
                              builder: (context, cartCubitState) {
                                return Text(
                                  BlocProvider.of<CartCubit>(context)
                                      .getVaraintNonDuplicateArray()
                                      .length
                                      .toString(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 10.0),
                                );
                              },
                            ),
                          ))
                    ],
                  ),
                ]
              : []);
    });
  }

  Container _buildSearchBox() {
    return Container(
        height: 70,
        color: Palette.teritaryColor,
        padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
        child: TextField(
          autofocus: false,
          enabled: false,
          decoration: InputDecoration(
              isDense: true,
              hintText: tr('searchProducts'),
              contentPadding:
                  EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
              fillColor: Colors.white,
              suffixIcon: Padding(
                padding: EdgeInsets.zero,
                child: Container(
                  decoration: BoxDecoration(
                      color: Palette.buttonBg,
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(15.0),
                          topRight: Radius.circular(15.0))),
                  child: Icon(
                    Icons.search,
                    color: Palette.buttonText,
                  ),
                ),
              ),
              filled: true,
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15.0),
                  borderSide: BorderSide(color: Colors.black))),
        ));
  }

  Widget _buildLocationText(BuildContext context) {
    if (widget.showLocation)
      return BlocBuilder<LocationBloc, LocationState>(
        builder: (context, locationState) {
          if (locationState is LocationSuccess) {
            return InkWell(
              onTap: () {
                Scaffold.of(context).showBottomSheet(
                  (context) => PincodeList(),
                );
              },
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 13.0),
                child: Row(
                  children: [
                    Icon(
                      Icons.location_pin,
                      color: Palette.buttonBg,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(locationState.selectedLocation.pincode,
                        style: Theme.of(context)
                            .textTheme
                            .subtitle2
                            .copyWith(color: Palette.onSecondary))
                  ],
                ),
              ),
            );
          }
          return SizedBox();
        },
      );
    return SizedBox();
  }
}
