import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/core/platform/network_connection_service.dart';
import 'package:grobay/presentation/widget/network_connection_error.dart';

class NetworkConnectivityAdapter extends StatefulWidget {
  final Widget child;
  final Function onNetworkAvailable;
  final callerName;
  NetworkConnectivityAdapter(
      {Key key, @required this.child, this.onNetworkAvailable, this.callerName})
      : super(key: key);

  @override
  _NetworkConnectivityAdapterState createState() => _NetworkConnectivityAdapterState();
}

class _NetworkConnectivityAdapterState extends State<NetworkConnectivityAdapter> {
  static Map<String, bool> previousNetworkValueByInstance = {};
  NetworkConnectionService _networkConnectionService;
  bool isOnline = true;
  StreamSubscription _streamSubscription;

  @override
  void initState() {
    super.initState();
    this._networkConnectionService = GetIt.I.get<NetworkConnectionService>();
    this._networkConnectionService.initialize();
    this._streamSubscription =
        this._networkConnectionService.connectionChange.listen((event) {
      if (_NetworkConnectivityAdapterState
              .previousNetworkValueByInstance[widget.callerName] !=
          event) {
        _NetworkConnectivityAdapterState
            .previousNetworkValueByInstance[widget.callerName] = event;
        if (event) {
          setState(() {
            isOnline = true;
            if (widget.onNetworkAvailable != null) {
              widget.onNetworkAvailable();
            }
          });
        } else {
          setState(() {
            isOnline = false;
             _NetworkConnectivityAdapterState
            .previousNetworkValueByInstance[widget.callerName] = null;
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isOnline) {
      return widget.child;
    } else {
      return NetworkConnectionError();
    }
  }

  @override
  void dispose() {
    super.dispose();
    this._streamSubscription.cancel();
  }
}
