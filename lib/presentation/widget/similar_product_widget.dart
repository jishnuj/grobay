import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/product.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/similar/similarproducts_bloc.dart';
import 'package:grobay/presentation/page/product/product_detail_page.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class SimilarProductWidget extends StatelessWidget {
  final String title;
  final String description;
  final String categoryId;
  final String subCategoryId;
  final String productId;
  final SimilarproductsBloc similarproductsBloc;

  const SimilarProductWidget({
    Key key,
    this.title,
    this.similarproductsBloc,
    this.productId,
    this.categoryId,
    this.subCategoryId,
    this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (context, index) {
          if (index == 0) {
            return Container(
              margin: EdgeInsets.only(top: 10.0),
              // color: Theme.of(context).highlightColor.withOpacity(0.1),
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _buildTitle(context),
                ],
              ),
            );
          } else {
            return BlocBuilder<SimilarproductsBloc, SimilarproductsState>(
              bloc: similarproductsBloc,
              builder: (context, similarProductState) {
                if (similarProductState is SimilarproductsSuccess) {
                  return GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: similarProductState
                          .similarproductsResposne.products.length,
                      shrinkWrap: true,
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      itemBuilder: (context, index) {
                        return _buildProductCard(
                            similarProductState
                                .similarproductsResposne.products[index],
                            context);
                      },
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2));
                } else if (similarProductState is SimilarproductsFailed) {
                  return Container(
                    height: 100,
                    child: CustomErrorWidget(
                      showIcon: false,
                      msg: tr('noProducts'),
                      
                    ),
                  );
                }
                return UIUtilWidget.buildCircularProgressIndicator();
              },
            );
          }
        },
        childCount: 2,
      ),
    );
  }

  Column _buildTitle(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          this.title,
          style: Theme.of(context).textTheme.headline6.copyWith(
              color: Palette.buttonBg,
              fontWeight: FontWeight.w800),
        ),
        Text(this.description ?? "",
            style: Theme.of(context).textTheme.bodyText1),
      ],
    );
  }

  Widget _buildProductCard(Product product, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return ProductDetailsPage(
            productId: product.id,
            product: product,
          );
        }));
      },
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Expanded(
                  child: CachedNetworkImage(
                width: 100,
                fit: BoxFit.fitHeight,
                imageUrl: product.image,
                placeholder: (context, url) => Container(
                  color: Palette.imageCacheColor,
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              )),
              SizedBox(
                height: 30,
              ),
              Text(product.name, style: Theme.of(context).textTheme.subtitle2),
              Text(
                  '${CurrencyWidget.getFixedPriceCurrencyString(double.parse(product.variants[0].discountedPrice))}  ',
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2
                      .copyWith(color: Theme.of(context).colorScheme.secondary))
            ],
          ),
        ),
      ),
    );
  }
}