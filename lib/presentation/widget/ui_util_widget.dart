import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grobay/core/constant/palette.dart';

class UIUtilWidget extends StatelessWidget {
  const UIUtilWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  static void showSnackBar(String content) {
    Get.showSnackbar(GetSnackBar(
        duration: Duration(seconds: 2),
        messageText: Text(
          content,
          style: TextStyle(color: Palette.secondary),
        )));
  }

  static void showSnackBarWidget(Widget content, {int seconds}) {
    Get.showSnackbar(GetSnackBar(
        duration: Duration(seconds: seconds ?? 2), messageText: content));
  }

  static Widget buildRetryBox(String text, Function onRetry) {
    return Container(
      alignment: Alignment.center,
      color: Colors.transparent,
      height: 100,
      child: InkWell(
        onTap: onRetry,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Icon(Icons.refresh),
            Text(
              text ?? tr('retry'),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }

  static void clearAllSnackBars() {
    Get.closeAllSnackbars();
  }

  static Widget verticalSpaceMedium() {
    return SizedBox(
      height: 10,
    );
  }

  static Widget horizontalSpaceMedium() {
    return SizedBox(
      width: 10,
    );
  }

  static Widget verticalSpaceSmall() {
    return SizedBox(
      height: 5,
    );
  }

  static Widget horizontalSpaceSmall() {
    return SizedBox(
      width: 5,
    );
  }

  static Widget verticalSpaceCustom(double size) {
    return SizedBox(
      height: size,
    );
  }

  static Widget horizontalSpaceCustom(double size) {
    return SizedBox(
      width: size,
    );
  }

  static Widget verticalSpaceLarge() {
    return SizedBox(
      height: 20,
    );
  }

  static Widget horizontalSpaceLarge() {
    return SizedBox(
      width: 20,
    );
  }

  static Widget buildBorderedContainer(
      {Widget child, EdgeInsets padding, Color borderColor}) {
    return Container(
      padding: padding ?? EdgeInsets.all(10.0),
      decoration:
          BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(color: borderColor ?? Colors.black)),
      child: child,
    );
  }

  static Widget buildCircularProgressIndicator() {
    return Container(
        child: Center(
      child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Palette.highlightColor)),
    ));
  }

  static Widget buildCircularProgressIndicatorWithText(String text) {
    return Container(
        width: Get.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(
                valueColor:
                    AlwaysStoppedAnimation<Color>(Palette.highlightColor)),
            UIUtilWidget.verticalSpaceMedium(),
            Text(text)
          ],
        ));
  }
}
