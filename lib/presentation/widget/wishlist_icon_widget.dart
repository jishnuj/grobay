import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/product.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/favourite/favourite_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/wishlistchangenotifier_cubit.dart';

// ignore: must_be_immutable
class WishListIconWidget extends StatefulWidget {
  Product product;
  final bool isMiniType;
  WishListIconWidget({Key key, this.product, this.isMiniType = false})
      : super(key: key);

  @override
  _WishListIconWidgetState createState() => _WishListIconWidgetState();
}

class _WishListIconWidgetState extends State<WishListIconWidget> {
  @override
  Widget build(BuildContext context) {
    if (widget.isMiniType) {
      return _buildIconMethod();
    }
    return Container(
      child: Row(
        children: [
          _buildIconMethod(),
          Text(
            tr('wishlist'),
            style: Theme.of(context)
                .textTheme
                .subtitle2
                .copyWith(color: Palette.onSecondary),
          )
        ],
      ),
    );
  }

  Widget _buildIconMethod() {
    UserloginState userloginState =
        BlocProvider.of<UserloginBloc>(context).state;
    return BlocBuilder<WishlistchangenotifierCubit, WishListStatusChange>(
      builder: (context, wishListChangeState) {
        if (wishListChangeState != null &&
            wishListChangeState.productId == widget.product.id) {
          widget.product.isFavorite = wishListChangeState.product.isFavorite;
        }
        return BlocConsumer<FavouriteBloc, FavouriteState>(
          listener: (context, state) {},
          builder: (context, state) {
            return IconButton(
              padding: EdgeInsets.zero,
              icon: (widget.product.isFavorite ||
                      (state is FavouriteSuccessGuest &&
                          state.products
                                  .where((element) =>
                                      element.id == widget.product.id)
                                  .length !=
                              0))
                  ? Icon(
                      Icons.favorite_sharp,
                      color: Colors.redAccent,
                    )
                  : Icon(Icons.favorite_border_outlined,
                      color: Colors.redAccent),
              onPressed: () {
                if (widget.product.isFavorite ||
                    (state is FavouriteSaveSuccess &&
                        state.productIds.contains(widget.product.id))) {
                  BlocProvider.of<FavouriteBloc>(context).add(
                      RemoveFavouriteEvent(
                          isGuest: !(userloginState is UserLoginSuccess),
                          product: widget.product,
                          productId: widget.product.id));
                  BlocProvider.of<WishlistchangenotifierCubit>(context)
                      .productWishListStatusChanged(widget.product, false);
                  setState(() {
                    widget.product.isFavorite = false;
                  });
                } else {
                  BlocProvider.of<FavouriteBloc>(context).add(
                      SaveFavouriteEvent(
                          isGuest: !(userloginState is UserLoginSuccess),
                          product: widget.product,
                          productId: widget.product.id));
                  BlocProvider.of<WishlistchangenotifierCubit>(context)
                      .productWishListStatusChanged(widget.product, true);

                  setState(() {
                    widget.product.isFavorite = true;
                  });
                }
              },
            );
          },
        );
      },
    );
  }
}
