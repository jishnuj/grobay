import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/category.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/category/category_bloc.dart';
import 'package:grobay/presentation/bloc/product/product_bloc.dart';
import 'package:grobay/presentation/bloc/startup/startup_bloc.dart';
import 'package:grobay/presentation/page/category/category_products_page.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/refersh_widget.dart';
import 'package:grobay/presentation/widget/skelton_loaders.dart';
import 'package:grobay/presentation/widget/ui_helper.dart';

class CategoryListWidget extends StatelessWidget {
  final bool showAsList;
  final bool isShowingFullData;
  final Function onViewAllTap;
  final bool showTitle;
  const CategoryListWidget(
      {Key key,
      this.showAsList = true,
      this.showTitle = true,
      this.isShowingFullData = false,
      this.onViewAllTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!this.showAsList) {
      BlocProvider.of<CategoryBloc>(context).add(FetchCategoryEvent());
    }
    return SliverToBoxAdapter(
      child: BlocBuilder<StartupBloc, StartupState>(
        builder: (context, appState) {
          if (appState is StartupSuccess) {
            return Container(
              color: !showAsList ? Palette.onPrimary : Palette.teritaryColor,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  if (this.showTitle) _buildTitle(context),
                  SizedBox(
                    height: 10.0,
                  ),
                  showAsList
                      ? _buildCategoryListHorizontal(appState)
                      : _buildCategoryGrid(),
                  UIHelper.verticalSpaceLarge()
                ],
              ),
            );
          } else if (appState is StartupFailed) {
            return _buildErrorBox(appState, context);
          }
          return SizedBox();
        },
      ),
    );
  }

  Widget _buildErrorBox(StartupFailed startupFailed, BuildContext context) {
    return Container(
      child: CustomErrorWidget(
        callerName: "category_list_startup",
        msg: startupFailed.message,
        onNetworkError: () {
          BlocProvider.of<StartupBloc>(context).add(FetchStartupEvent());
        },
      ),
    );
  }

  void _onPageRefresh(context) {
    BlocProvider.of<CategoryBloc>(context).add(FetchCategoryEvent());
  }

  BlocBuilder<CategoryBloc, CategoryState> _buildCategoryGrid() {
    return BlocBuilder<CategoryBloc, CategoryState>(
      builder: (context, categoryState) {
        if (categoryState is CategorySuccess) {
          return Container(
              constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height - 200),
              decoration: BoxDecoration(color: Palette.onPrimary),
              child: CustomRefreshWidget(
                onRefresh: () {
                  this._onPageRefresh(context);
                },
                child: GridView.builder(
                    padding:
                        EdgeInsets.symmetric(horizontal: 10, vertical: 10.0),
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2),
                    itemCount: categoryState.categoryResposne.categories.length,
                    itemBuilder: (context, index) {
                      return _buildcategoryCard(index, context,
                          categoryState.categoryResposne.categories[index]);
                    }),
              ));
        } else if (categoryState is CategoryFailed) {
          return _buildCategoryErrorBox(categoryState, context);
        }
        return CategoryListLoader();
      },
    );
  }

  Widget _buildCategoryErrorBox(
      CategoryFailed categoryFailed, BuildContext context) {
    return Container(
      child: CustomErrorWidget(
        callerName: "category_list",
        msg: categoryFailed.message,
        onNetworkError: () {
          BlocProvider.of<StartupBloc>(context).add(FetchStartupEvent());
        },
      ),
    );
  }

  Container _buildCategoryListHorizontal(StartupSuccess appState) {
    return Container(
      color: Palette.teritaryColor,
      height: 150.0,
      child: ListView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        scrollDirection: Axis.horizontal,
        itemCount: appState.startupResposne.categories.length,
        itemBuilder: (context, index) {
          return _buildcategoryCard(
              index, context, appState.startupResposne.categories[index]);
        },
      ),
    );
  }

  Container _buildTitle(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 25),
      // color: Theme.of(context).highlightColor.withOpacity(0.1),
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      child: Text(
        tr('featuredCategories').toUpperCase(),
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.subtitle1.copyWith(),
      ),
    );
  }

  Widget _buildcategoryCard(
      int index, BuildContext context, Category category) {
    return InkWell(
      onTap: () {
        _onCategorySelected(context, category);
      },
      child: Container(
        // width: 150.0,
        margin: !showAsList
            ? EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0)
            : EdgeInsets.zero,
        decoration: BoxDecoration(
            color: !showAsList ? Palette.onPrimary : Palette.teritaryColor,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow:
                !showAsList ? [BoxShadow(color: Palette.teritaryColor)] : []),
        child: Column(
          children: [
            _buildCategoryImage(category),
            _buildCategoryName(category, context)
          ],
        ),
      ),
    );
  }

  Container _buildCategoryName(Category category, BuildContext context) {
    return Container(
        padding: EdgeInsets.all(5.0),
        child:
            Text(category.name, style: Theme.of(context).textTheme.bodyText1));
  }

  Expanded _buildCategoryImage(Category category) {
    return Expanded(
        child: Container(
      padding: EdgeInsets.all(20),
      child: CachedNetworkImage(
        width: 50,
        imageUrl: category.image,
        placeholder: (context, url) => Center(
          child: Container(
            width: 70,
            height: 70,
            child: Center(
                child: Text(
              "C",
              style: Theme.of(context).textTheme.headline4,
            )),
            decoration: BoxDecoration(
                color: Palette.primary.withOpacity(0.1),
                borderRadius: BorderRadius.circular(70.0)),
          ),
        ),
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
    ));
  }

  void _onCategorySelected(BuildContext context, Category category) {
    BlocProvider.of<ProductBloc>(context)
        .add(FetchProductByCategoryEvent(parentCatId: category.id));
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return CategoryProductsPage(
        parentCategory: category,
      );
    }));
  }
}
