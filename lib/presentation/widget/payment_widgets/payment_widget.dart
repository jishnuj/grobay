import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/payment_methods.dart';
import 'package:grobay/presentation/widget/payment_widgets/cod_payment_widget.dart';
import 'package:grobay/presentation/widget/payment_widgets/flutterwave_widget.dart';
import 'package:grobay/presentation/widget/payment_widgets/midtrans_widget.dart';
import 'package:grobay/presentation/widget/payment_widgets/paypal_payment_widget.dart';
import 'package:grobay/presentation/widget/payment_widgets/paystack_widget.dart';
import 'package:grobay/presentation/widget/payment_widgets/paytm_widget.dart';
import 'package:grobay/presentation/widget/payment_widgets/payu_payment_widget.dart';
import 'package:grobay/presentation/widget/payment_widgets/razorpay_widget.dart';
import 'package:grobay/presentation/widget/payment_widgets/sslcommerz_widget.dart';
import 'package:grobay/presentation/widget/payment_widgets/stripe_widget.dart';
import 'package:logger/logger.dart';

class PaymentWidget extends StatelessWidget {
  final Function(BuildContext, Map<String, dynamic>) onPaymentComplete;
  final Function(String) onPaymentCancelled;
  final Map<String, dynamic> options;
  final PAYMENT_METHODS selectedMethod;
  PaymentWidget({
    Key key,
    this.options,
    this.selectedMethod,
    this.onPaymentCancelled,
    this.onPaymentComplete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Logger().i(this.options);
    if (selectedMethod.name == PAYMENT_METHODS.COD.name) {
      return _buildCODPayment();
    } else if (selectedMethod.name == PAYMENT_METHODS.RAZORPAY.name) {
      return _buildRazorpayPayment();
    } else if (selectedMethod.name == PAYMENT_METHODS.PAYSTACK.name) {
      return _buildPaymentStackPayment();
    } else if (selectedMethod.name == PAYMENT_METHODS.FLUTTERWAVE.name) {
      return _buildFlutterwavePayment();
    } else if (selectedMethod.name == PAYMENT_METHODS.PAYPAL.name) {
      return _buildPaypal();
    } else if (selectedMethod.name == PAYMENT_METHODS.STRIPE.name) {
      return _buildStripe();
    } else if (selectedMethod.name == PAYMENT_METHODS.PAYUMONEY.name) {
      return _buildPayUPayment();
    } else if (selectedMethod.name == PAYMENT_METHODS.MIDTRANS.name) {
      return _buildMidtrans();
    } else if (selectedMethod.name == PAYMENT_METHODS.PAYTM.name) {
      return _buildPaytmWidget();
    } else if (selectedMethod.name == PAYMENT_METHODS.SSLCOMMERZ.name) {
      return _buildSSLCommerzWidget();
    }
    return Container(
      height: MediaQuery.of(context).size.height / 1.2,
      child: Center(child: Text(tr('error'))),
    );
  }

  CodPaymentWidget _buildCODPayment() {
    return CodPaymentWidget(
        onPaymentCancelled: this.onPaymentCancelled,
        onComplete: onPaymentComplete);
  }

  RazorpayWidget _buildRazorpayPayment() {
    return RazorpayWidget(
        onPaymentCancelled: this.onPaymentCancelled,
        onComplete: onPaymentComplete,
        options: this.options);
  }

  PaystackPaymentWidget _buildPaymentStackPayment() {
    return PaystackPaymentWidget(
        onPaymentCancelled: this.onPaymentCancelled,
        onComplete: onPaymentComplete,
        options: this.options);
  }

  FlutterWaveWidget _buildFlutterwavePayment() {
    return FlutterWaveWidget(
        onPaymentCancelled: this.onPaymentCancelled,
        onComplete: onPaymentComplete,
        options: this.options);
  }

  PaypalPaymentWidget _buildPaypal() {
    return PaypalPaymentWidget(
        onPaymentCancelled: this.onPaymentCancelled,
        onComplete: onPaymentComplete,
        options: this.options);
  }

  StripWidget _buildStripe() {
    return StripWidget(
        onPaymentCancelled: this.onPaymentCancelled,
        onComplete: onPaymentComplete,
        options: this.options);
  }

  PayUPaymentWidget _buildPayUPayment() {
    return PayUPaymentWidget(
      onPaymentCancelled: this.onPaymentCancelled,
      onComplete: onPaymentComplete,
      options: this.options,
    );
  }

  MidTransWidget _buildMidtrans() {
    return MidTransWidget(
        title: AppConfig.APP_NAME,
        onPaymentCancelled: this.onPaymentCancelled,
        onComplete: onPaymentComplete,
        options: this.options);
  }

  PaytmWidget _buildPaytmWidget() {
    return PaytmWidget(
        onPaymentCancelled: this.onPaymentCancelled,
        onComplete: onPaymentComplete,
        options: this.options);
  }

  SSlCommerzWidget _buildSSLCommerzWidget() {
    return SSlCommerzWidget(
        onPaymentCancelled: this.onPaymentCancelled,
        onComplete: onPaymentComplete,
        options: this.options);
  }
}
