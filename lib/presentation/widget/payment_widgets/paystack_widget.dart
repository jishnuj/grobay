import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_paystack/flutter_paystack.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:logger/logger.dart';

class PaystackPaymentWidget extends StatefulWidget {
  final Function(BuildContext, Map<String, dynamic>) onComplete;
  final Function(String) onPaymentCancelled;
  final Map<String, dynamic> options;
  PaystackPaymentWidget(
      {Key key, this.onComplete, this.onPaymentCancelled, this.options})
      : super(key: key);

  @override
  _PaystackPaymentWidgetState createState() => _PaystackPaymentWidgetState();
}

class _PaystackPaymentWidgetState extends State<PaystackPaymentWidget> {
  final plugin = PaystackPlugin();

  @override
  void initState() {
    plugin.initialize(publicKey: widget.options['paystack']['public_key']);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 5), () {
      this.startPayment(context);
    });
    return UIUtilWidget.buildCircularProgressIndicatorWithText(
        tr('loadingPaystack'));
  }

  void startPayment(BuildContext context) async {
    Logger().e(widget.options['paystack']['currency']);

    Charge charge = Charge()
      ..amount = (widget.options['amount'] * 100).toInt()
      ..email = widget.options['email']
      ..currency = widget.options['paystack']['currency']
      ..reference = _getReference();
    // or ..accessCode = _getAccessCodeFrmInitialization()
    CheckoutResponse response = await plugin.checkout(
      context,
      method: CheckoutMethod.card, // Defaults to CheckoutMethod.selectable
      charge: charge,
    );
    if (response.status) {
      widget.onComplete(context, {
        "orderId": response.reference,
        "paymentId": response.reference,
        "signature": response.reference
      });
    } else {
      widget.onPaymentCancelled(response.message);
    }
  }

  String _getReference() {
    String platform;
    if (Platform.isIOS) {
      platform = 'iOS';
    } else {
      platform = 'Android';
    }

    return 'Ref_ChargedFrom${platform}_${DateTime.now().millisecondsSinceEpoch}';
  }
}
