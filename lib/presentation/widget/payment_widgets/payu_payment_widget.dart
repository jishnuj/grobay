import 'dart:convert';
import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_payu_unofficial/models/payment_params_model.dart';
import 'package:flutter_payu_unofficial/models/payment_result.dart';
import 'package:flutter_payu_unofficial/flutter_payu_unofficial.dart';
import 'package:flutter_payu_unofficial/models/payment_status.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:crypto/crypto.dart';
import 'package:logger/logger.dart';

class PayUPaymentWidget extends StatelessWidget {
  final Function(BuildContext, Map<String, dynamic>) onComplete;
  final Function(String) onPaymentCancelled;
  final Map<String, dynamic> options;
  PayUPaymentWidget(
      {Key key, this.onComplete, this.onPaymentCancelled, this.options})
      : super(key: key);

  PaymentParams setupPayment() {
    String txnId = _getReference();
    PaymentParams _paymentParam = PaymentParams(
      merchantID: this.options['payu']['mearchantId'],
      merchantKey: this.options['payu']['mearchantKey'],
      salt: this.options['payu']['salt'],
      // merchantID: "5009230",
      // merchantKey: "WAv0y2P2",
      // salt: "T8IPBnkBPk",
      amount: this.options['amount'].toString(),
      transactionID: txnId,
      firstName: this.options['name'],
      email: this.options['email'],
      productName: this.options['payu']['itemName'],
      phone: this.options['contact'],
      fURL: "https://www.payumoney.com/mobileapp/payumoney/failure.php",
      sURL: "https://www.payumoney.com/mobileapp/payumoney/success.php",
      udf1: "udf1",
      udf2: "udf2",
      udf3: "udf3",
      udf4: "udf4",
      udf5: "udf5",
      udf6: "",
      udf7: "",
      udf8: "",
      udf9: "",
      udf10: "",
      hash: "",
      isDebug:
          this.options['payu']['payuMoneyMode'] == 'sandbox' ? true : false,
    ); //
    var bytes = utf8.encode(
        "${_paymentParam.merchantKey}|${_paymentParam.transactionID}|${_paymentParam.amount}|${_paymentParam.productName}|${_paymentParam.firstName}|${_paymentParam.email}|udf1|udf2|udf3|udf4|udf5||||||${_paymentParam.salt}");
    String localHash = sha512.convert(bytes).toString();
    _paymentParam.hash = localHash;
    return _paymentParam;
  }

  Future startPayment(paymentparam, BuildContext context) async {
    try {
      PayuPaymentResult _paymentResult =
          await FlutterPayuUnofficial.initiatePayment(
              paymentParams: paymentparam, showCompletionScreen: true);
      //Checks for success and prints result

      if (_paymentResult != null) {
        //_paymentResult.status is String of course. Directly fetched from payU's Payment response. More statuses can be compared manually
        Logger().i(_paymentResult.response);
        if (_paymentResult.status == PayuPaymentStatus.success) {
          this.onComplete(context, {
            "orderId": _paymentResult.response['result']['mihpayid'] +
                "_" +
                _paymentResult.response['result']['bank_ref_num'],
            "paymentId": _paymentResult.response['result']['mihpayid'] +
                "_" +
                _paymentResult.response['result']['bank_ref_num'],
            "signature": _paymentResult.response['result']['mihpayid'] +
                "_" +
                _paymentResult.response['result']['bank_ref_num'],
          });
        } else if (_paymentResult.status == PayuPaymentStatus.failed) {
          this.onPaymentCancelled("Transaction Failed");
        } else if (_paymentResult.status == PayuPaymentStatus.cancelled) {
          this.onPaymentCancelled(tr('transactionCancelled'));
        } else {
          print("Response: ${_paymentResult.response}");
          print("Status: ${_paymentResult.status}");
        }
      } else {
        this.onPaymentCancelled(tr('transactionFailed'));
      }
    } catch (e) {
      print(e);
    }
  }

  String _getReference() {
    String platform;
    if (Platform.isIOS) {
      platform = 'iOS';
    } else {
      platform = 'Android';
    }

    return 'Ref_ChargedFrom${platform}_${DateTime.now().millisecondsSinceEpoch}';
  }

  @override
  Widget build(BuildContext context) {
    PaymentParams _paymentParam = this.setupPayment();
    this.startPayment(_paymentParam, context);
    return UIUtilWidget.buildCircularProgressIndicatorWithText(tr('loadingPayU'));
  }
}
