import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/presentation/widget/payment_widgets/blocs/create_stripe_payment/create_stripe_payment_bloc.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:logger/logger.dart';

class StripWidget extends StatefulWidget {
  final Function(BuildContext, Map<String, dynamic>) onComplete;
  final Function(String) onPaymentCancelled;
  final Map<String, dynamic> options;
  StripWidget(
      {Key key,
      this.title,
      this.onComplete,
      this.onPaymentCancelled,
      this.options})
      : super(key: key);

  final String title;

  @override
  _StripWidgetWidgetState createState() => _StripWidgetWidgetState();
}

class _StripWidgetWidgetState extends State<StripWidget> {
  bool isDebug = true;

  @override
  void initState() {
    super.initState();
  }

  String txid;

  @override
  Widget build(BuildContext context) {
    this.txid = _getReference();
    BlocProvider.of<CreateStripePaymentBloc>(context)
        .add(FetchCreateStripepayment(
      addresss: widget.options['address'],
      username: widget.options['username'],
      city: widget.options['city'],
      amount: (widget.options['amount']).toString(),
      orderId: this.txid,
      postalCode: widget.options['pincode'],
    ));
    return BlocBuilder<CreateStripePaymentBloc, CreateStripePaymentState>(
      builder: (context, state) {
        if (state is CreateStripePaymentSuccess) {
          Logger().i(state.response['clientSecret']);
          this.startPayment(context, state.response);
        }
        return UIUtilWidget.buildCircularProgressIndicatorWithText(
            tr('loadingStripe'));
      },
    );
  }

  startPayment(BuildContext context, _paymentSheetData) async {
    try {
      Stripe.publishableKey = _paymentSheetData['publishableKey'];
      await Stripe.instance.initPaymentSheet(
          paymentSheetParameters: SetupPaymentSheetParameters(
        googlePay: PaymentSheetGooglePay(
            merchantCountryCode: AppConfig.COUNTRY_CODE,
            currencyCode: AppConfig.COUNTRY_CODE,
            testEnv:
                widget.options['stripe']['stripeMode'] == "1" ? false : true),
        applePay: PaymentSheetApplePay(
          merchantCountryCode: AppConfig.COUNTRY_CODE,
        ),
        style: ThemeMode.dark,
        merchantDisplayName: AppConfig.APP_NAME,
        paymentIntentClientSecret: _paymentSheetData['clientSecret'],
      ));

      await Stripe.instance.presentPaymentSheet();
      widget.onComplete(
          context, {"orderId": txid, "paymentId": txid, "signature": txid});
    } catch (e) {
      Logger().i(e.toString());
      if (e is StripeException) {
        if (e.error.code == FailureCode.Canceled) {
          widget.onPaymentCancelled(tr('transactionCancelled'));
        } else {
          widget.onPaymentCancelled(tr('transactionFailed'));
        }
      } else {
        widget.onPaymentCancelled(e.toString());
      }
    }
  }

  String _getReference() {
    String platform;
    if (Platform.isIOS) {
      platform = 'iOS';
    } else {
      platform = 'Android';
    }

    return 'Ref_ChargedFrom${platform}_${DateTime.now().millisecondsSinceEpoch}';
  }
}
