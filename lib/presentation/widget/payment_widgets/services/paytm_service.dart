import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

import 'package:logger/logger.dart';

class PaytmServices {
  String domain;

  PaytmServices(bool isSandBoxMode) {
    if (isSandBoxMode) {
      domain =
          "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?"; // for sandbox mode
    } else {
      domain =
          "https://securegw.paytm.in/theia/api/v1/initiateTransaction?"; // for production mode
    }
  }

  Future<PaytmInitiateTranscationResponse> initiateTranscation(
      {String signature,
      String mid,
      String orderId,
      String websiteName,
      String callbackUrl,
      String txnAmount,
      String customerId,
      String customerName,
      String email,
      String mobile}) async {
    try {
      var requestBody = {
        "body": {
          "requestType": "Payment",
          "mid": "$mid",
          "orderId": "$orderId",
          "websiteName": "$websiteName",
          "custId": "$customerId",
          "callbackUrl": "$callbackUrl",
          "txnAmount": {
            "value": "$txnAmount",
            "currency": "INR",
          },
          "userInfo": {
            "mobile": "$mobile",
            "email": "$email",
            "firstName": "${customerName.split(' ')[0]}",
            "lastName": "${customerName.split(' ')[1]}",
          }
        },
        "head": {'signature': signature}
      };
      var response = await http.post(
          Uri.parse("${domain}mid=$mid&orderId=$orderId"),
          body: convert.jsonEncode(requestBody),
          headers: {"content-type": "application/json"});
      Logger().i("${domain}mid=$mid&orderId=$orderId");
      Logger().i(response.body);
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return PaytmInitiateTranscationResponse.fromJson(body);
      } else {
        throw Exception(body["message"]);
      }
    } catch (e) {
      rethrow;
    }
  }
}

class PaytmInitiateTranscationResponse {
  ResultInfo resultInfo;
  String txnToken;
  bool isPromoCodeValid;
  bool authenticated;

  PaytmInitiateTranscationResponse(
      {this.resultInfo,
      this.txnToken,
      this.isPromoCodeValid,
      this.authenticated});

  PaytmInitiateTranscationResponse.fromJson(Map<String, dynamic> json) {
    resultInfo = json['resultInfo'] != null
        ? new ResultInfo.fromJson(json['resultInfo'])
        : null;
    txnToken = json['txnToken'];
    isPromoCodeValid = json['isPromoCodeValid'];
    authenticated = json['authenticated'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resultInfo != null) {
      data['resultInfo'] = this.resultInfo.toJson();
    }
    data['txnToken'] = this.txnToken;
    data['isPromoCodeValid'] = this.isPromoCodeValid;
    data['authenticated'] = this.authenticated;
    return data;
  }
}

class ResultInfo {
  String resultStatus;
  String resultCode;
  String resultMsg;

  ResultInfo({this.resultStatus, this.resultCode, this.resultMsg});

  ResultInfo.fromJson(Map<String, dynamic> json) {
    resultStatus = json['resultStatus'];
    resultCode = json['resultCode'];
    resultMsg = json['resultMsg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['resultStatus'] = this.resultStatus;
    data['resultCode'] = this.resultCode;
    data['resultMsg'] = this.resultMsg;
    return data;
  }
}
