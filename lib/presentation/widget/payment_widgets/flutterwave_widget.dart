import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutterwave_standard/flutterwave.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:logger/logger.dart';

class FlutterWaveWidget extends StatefulWidget {
  final Function(BuildContext, Map<String, dynamic>) onComplete;
  final Function(String) onPaymentCancelled;
  final Map<String, dynamic> options;
  FlutterWaveWidget(
      {Key key,
      this.title,
      this.onComplete,
      this.onPaymentCancelled,
      this.options})
      : super(key: key);

  final String title;

  @override
  _FlutterWaveWidgetState createState() => _FlutterWaveWidgetState();
}

class _FlutterWaveWidgetState extends State<FlutterWaveWidget> {
  bool isDebug = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 5), () {
      this.startPayment(context);
    });
    return UIUtilWidget.buildCircularProgressIndicatorWithText(
        tr('loadingFlutterwave'));
  }

  startPayment(BuildContext context) async {
    final style = FlutterwaveStyle(
        appBarText: "FlutterWave - ${AppConfig.APP_NAME}",
        appBarIcon: Icon(Icons.payment, color: Palette.onSecondary),
        appBarColor: Palette.primary,
        appBarTitleTextStyle: TextStyle(color: Palette.onSecondary),
        buttonColor: Palette.buttonBg,
        buttonTextStyle: TextStyle(
          color: Palette.buttonText,
          fontWeight: FontWeight.bold,
          fontSize: 18,
        ),
        dialogCancelTextStyle: TextStyle(
          color: Colors.redAccent,
          fontSize: 18,
        ),
        dialogContinueTextStyle: TextStyle(
          color: Colors.blue,
          fontSize: 18,
        ));
    final Customer customer = Customer(
      email: widget.options['email'],
      name: widget.options['name'],
      phoneNumber: widget.options['contact'],
    );

    final Flutterwave flutterwave = Flutterwave(
        context: context,
        style: style,
        publicKey: widget.options['flutterwave']['public_key'],
        currency: widget.options['flutterwave']['currency_code'],
        txRef: _getReference(),
        amount: widget.options['amount'].toString(),
        customer: customer,
        paymentOptions: "ussd, card, barter, payattitude",
        customization: Customization(title: "Grobay Payment"),
        isTestMode: widget.options['flutterwave']['flutterWaveMode']=="1"?false:true);
    final ChargeResponse response = await flutterwave.charge();
    if (response != null) {
      Logger().i(response.toJson());
      widget.onComplete(context, {
        "orderId": response.txRef,
        "paymentId": response.txRef,
        "signature": response.txRef
      });
    } else {
      widget.onPaymentCancelled(tr('transactionFailed'));
    }
  }

  String _getReference() {
    String platform;
    if (Platform.isIOS) {
      platform = 'iOS';
    } else {
      platform = 'Android';
    }

    return 'Ref_ChargedFrom${platform}_${DateTime.now().millisecondsSinceEpoch}';
  }
}
