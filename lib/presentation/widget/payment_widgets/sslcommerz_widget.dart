import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class SSlCommerzWidget extends StatefulWidget {
  final Function(BuildContext, Map<String, dynamic>) onComplete;
  final Function(String) onPaymentCancelled;
  final Map<String, dynamic> options;
  SSlCommerzWidget(
      {Key key,
      this.title,
      this.onComplete,
      this.onPaymentCancelled,
      this.options})
      : super(key: key);

  final String title;

  @override
  _SSlCommerzWidgetWidgetState createState() => _SSlCommerzWidgetWidgetState();
}

class _SSlCommerzWidgetWidgetState extends State<SSlCommerzWidget> {
  bool isDebug = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 5), () {
      this.startPayment(context);
    });
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(30.0),
        child: UIUtilWidget.buildBorderedContainer(
            child: Text(tr('sslecommerzUnavailableMsg'),
            style: Theme.of(context).textTheme.headline6.copyWith(color: Palette.highlightColor),
            textAlign: TextAlign.center,)),
      ),
    );
  }

  startPayment(BuildContext context) async {}

  String _getReference() {
    String platform;
    if (Platform.isIOS) {
      platform = 'iOS';
    } else {
      platform = 'Android';
    }

    return 'Ref_ChargedFrom${platform}_${DateTime.now().millisecondsSinceEpoch}';
  }
}
