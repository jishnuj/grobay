import 'package:flutter/material.dart';

class CodPaymentWidget extends StatelessWidget {
  final Function(BuildContext , Map<String,dynamic>) onComplete;
  final Function(String) onPaymentCancelled;
  const CodPaymentWidget({Key key, this.onComplete, this.onPaymentCancelled})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    this.onComplete(context,{});
    return Container(
        height: MediaQuery.of(context).size.height / 1.4,
        child: Column(
          children: [
            CircularProgressIndicator(),
          ],
        ));
  }
}
