part of 'create_paytm_bloc.dart';

@immutable
abstract class CreatePaytmState {}

class CreatePaytmInitial extends CreatePaytmState {}

class CreatePaytmChecksumSuccess extends CreatePaytmState {
  final PaytmChecksumResponse response;
  CreatePaytmChecksumSuccess({this.response});
}

class ValidatePaytmPaymentSuccess extends CreatePaytmState {
  final PaytmValidationResponse response;
  ValidatePaytmPaymentSuccess({this.response});
}

class CreatePaytmChecksumFailed extends CreatePaytmState {}

class ValidatePaytmPaymentFailed extends CreatePaytmState {}

class CreatePaytmChecksumLoading extends CreatePaytmState {}
