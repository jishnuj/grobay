part of 'create_paytm_bloc.dart';

@immutable
abstract class CreatePaytmEvent {}

class FetchPaytmCheckSumEvent extends CreatePaytmEvent {
  final String cust_id;
  final String channel_id;
  final String order_id;
  final String txn_amount;
  final String industry_type_id;
  final String website;
  FetchPaytmCheckSumEvent(
      {this.cust_id,
      this.channel_id,
      this.order_id,
      this.txn_amount,
      this.industry_type_id,
      this.website});
}

class FetchValidatePaymentEvent extends CreatePaytmEvent {
  final String orderId;
  FetchValidatePaymentEvent({this.orderId});
}

class ResetPaytmCheckSumEvent extends CreatePaytmEvent {}
