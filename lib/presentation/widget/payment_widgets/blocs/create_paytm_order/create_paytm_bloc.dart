import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/paytm_checksum_response.dart';
import 'package:grobay/api/response/paytm_validation_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'create_paytm_event.dart';
part 'create_paytm_state.dart';

class CreatePaytmBloc extends Bloc<CreatePaytmEvent, CreatePaytmState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  CreatePaytmBloc() : super(CreatePaytmInitial()) {
    on<CreatePaytmEvent>((event, emit) async {
      if (event is FetchPaytmCheckSumEvent) {
        try {
          emit(CreatePaytmChecksumLoading());
          PaytmChecksumResponse response = await this
              .apiBridge
              .createPaytmChecksum(
                  cust_id: event.cust_id,
                  channel_id: event.channel_id,
                  industry_type_id: event.industry_type_id,
                  order_id: event.order_id,
                  txn_amount: event.txn_amount,
                  website: event.website);
          emit(CreatePaytmChecksumSuccess(response: response));
        } catch (e) {
          emit(CreatePaytmChecksumFailed());
        }
      } else if (event is FetchValidatePaymentEvent) {
        try {
          PaytmValidationResponse paytmValidationResponse = await this
              .apiBridge
              .verifyPaytmValidation(orderId: event.orderId);
          emit(ValidatePaytmPaymentSuccess(response: paytmValidationResponse));
        } catch (e) {
          emit(ValidatePaytmPaymentFailed());
        }
      } else if (event is ResetPaytmCheckSumEvent) {
        emit(CreatePaytmInitial());
      }
    });
  }
}
