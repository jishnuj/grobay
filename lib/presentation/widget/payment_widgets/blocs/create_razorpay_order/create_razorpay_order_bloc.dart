import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/create_razorpay_order_resposne.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'create_razorpay_order_event.dart';
part 'create_razorpay_order_state.dart';

class CreateRazorpayOrderBloc
    extends Bloc<CreateRazorpayOrderEvent, CreateRazorpayOrderState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  CreateRazorpayOrderBloc() : super(CreateRazorpayOrderInitial()) {
    on<CreateRazorpayOrderEvent>((event, emit) async {
      try {
        if (event is FetchCreateRazorpayOrder) {
          emit(CreateRazorpayOrderLoading());
          CreateRazorpayOrderResponse createRazorpayOrderResponse =
              await this.apiBridge.createRazorpayOrder(amount: event.amount);
          emit(CreateRazorpayOrderSuccess(
              createRazorpayOrderResponse: createRazorpayOrderResponse));
        }
      }catch (e) {
        emit(CreateRazorpayOrderFailed());
      }
    });
  }
}
