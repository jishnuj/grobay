part of 'create_razorpay_order_bloc.dart';

@immutable
abstract class CreateRazorpayOrderEvent {}

class FetchCreateRazorpayOrder extends CreateRazorpayOrderEvent {
  final String amount;
  FetchCreateRazorpayOrder({this.amount});
}
