part of 'create_razorpay_order_bloc.dart';

@immutable
abstract class CreateRazorpayOrderState {}

class CreateRazorpayOrderInitial extends CreateRazorpayOrderState {}


class CreateRazorpayOrderSuccess extends CreateRazorpayOrderState {
  final CreateRazorpayOrderResponse createRazorpayOrderResponse;
  CreateRazorpayOrderSuccess({this.createRazorpayOrderResponse});
}

class CreateRazorpayOrderFailed extends CreateRazorpayOrderState {}

class CreateRazorpayOrderLoading extends CreateRazorpayOrderState {}
