import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'create_stripe_payment_event.dart';
part 'create_stripe_payment_state.dart';

class CreateStripePaymentBloc
    extends Bloc<CreateStripePaymentEvent, CreateStripePaymentState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  CreateStripePaymentBloc() : super(CreateStripePaymentInitial()) {
    on<CreateStripePaymentEvent>((event, emit) async {
      try {
        if (event is FetchCreateStripepayment) {
          emit(CreateStripePaymentLoading());
          Map<dynamic, dynamic> response = await this
              .apiBridge
              .createStripePayment(
                  address: event.addresss,
                  amount: event.amount,
                  city: event.city,
                  postalCode: event.postalCode,
                  orderId: event.orderId,
                  username: event.username);
          emit(CreateStripePaymentSuccess(response: response));
        } else if (event is ResetCreateStripePayment) {
          emit(CreateStripePaymentInitial());
        }
      } catch (e) {
        emit(CreateStripePaymentFailed());
      }
    });
  }
}
