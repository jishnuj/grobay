part of 'create_stripe_payment_bloc.dart';

@immutable
abstract class CreateStripePaymentState {}

class CreateStripePaymentInitial extends CreateStripePaymentState {}

class CreateStripePaymentSuccess extends CreateStripePaymentState {
  final Map<dynamic, dynamic> response;
  CreateStripePaymentSuccess({this.response});
}

class CreateStripePaymentFailed extends CreateStripePaymentState {}

class CreateStripePaymentLoading extends CreateStripePaymentState {}
