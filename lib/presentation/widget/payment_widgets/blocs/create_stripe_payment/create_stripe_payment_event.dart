part of 'create_stripe_payment_bloc.dart';

@immutable
abstract class CreateStripePaymentEvent {}

class FetchCreateStripepayment extends CreateStripePaymentEvent {
  final String username;
  final String addresss;
  final String postalCode;
  final String city;
  final String amount;
  final String orderId;
  FetchCreateStripepayment(
      {this.username,
      this.addresss,
      this.postalCode,
      this.city,
      this.amount,
      this.orderId});
}

class ResetCreateStripePayment extends CreateStripePaymentEvent {}