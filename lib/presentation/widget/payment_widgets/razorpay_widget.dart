import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/presentation/widget/payment_widgets/blocs/create_razorpay_order/create_razorpay_order_bloc.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:logger/logger.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

class RazorpayWidget extends StatelessWidget {
  final Function(BuildContext, Map<String, dynamic>) onComplete;
  final Function(String) onPaymentCancelled;
  final Map<String, dynamic> options;
  RazorpayWidget(
      {Key key, this.onComplete, this.onPaymentCancelled, this.options})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _razorpay = Razorpay();
    Logger().i(this.options.toString());
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS,
        (PaymentSuccessResponse response) {
      this.onComplete(context, {
        "orderId": response.orderId,
        "paymentId": response.paymentId,
        "signature": response.signature
      });
    });
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR,
        (PaymentFailureResponse response) {
      this.onPaymentCancelled(response.message);
    });
    try {
      _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
    } catch (e) {
      this.onPaymentCancelled(e.toString());
    }
    BlocProvider.of<CreateRazorpayOrderBloc>(context).add(
        FetchCreateRazorpayOrder(
            amount: (options['amount'] * 100).toInt().toString()));
    return BlocConsumer<CreateRazorpayOrderBloc, CreateRazorpayOrderState>(
      listener: (context, state) {
        if (state is CreateRazorpayOrderSuccess) {
          _razorpay.open({
            "key": options['razorpay']['key'],
            "name": options['name'],
            'order_id': state.createRazorpayOrderResponse.id,
            'amount': (options['amount'] * 100).toInt().toString(),
            'description': options['description'],
            'prefill': {
              'contact': options['contact'],
              'email': options['email']
            }
          });
        } else if (state is CreateRazorpayOrderFailed) {
          UIUtilWidget.showSnackBar(tr('razorpayPaymentFailed'));
        }
      },
      builder: (context, state) {
        return UIUtilWidget.buildCircularProgressIndicatorWithText(
            tr('loadingRazorpayGateway'));
      },
    );
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    // Do something when an external wallet was selected
  }
}
