import 'dart:core';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/presentation/widget/payment_widgets/services/paypal_service.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:logger/logger.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PaypalPaymentWidget extends StatefulWidget {
  final Function onComplete;
  final Function onPaymentCancelled;
  final Map<String, dynamic> options;
  PaypalPaymentWidget({this.onComplete, this.onPaymentCancelled, this.options});

  @override
  State<StatefulWidget> createState() {
    return PaypalPaymentWidgetState();
  }
}

class PaypalPaymentWidgetState extends State<PaypalPaymentWidget> {
  String checkoutUrl;
  String executeUrl;
  String accessToken;
  Map<dynamic, dynamic> defaultCurrency;
  bool isEnableShipping = false;
  bool isEnableAddress = false;
  String returnURL = AppConfig.PAYPAL_RETURN_URL;
  String cancelURL = AppConfig.PAYPAL_CANCEL_URL;
  PaypalServices services;

  void _setDefaultCurrencyDetails() {
    defaultCurrency = {
      "symbol": widget.options['paypal']['currency_code'],
      "currency": widget.options['paypal']['currency_code'],
      "decimalDigits": 2,
      "symbolBeforeTheNumber": true,
    };
  }

  @override
  void initState() {
    services = PaypalServices(
        widget.options['paypal']['paypalMode'].toString().toLowerCase() ==
                'sandbox'
            ? true
            : false);
    super.initState();
    this._setDefaultCurrencyDetails();
    Future.delayed(Duration.zero, () async {
      try {
        accessToken = await services.getAccessToken();
        final transactions = getOrderParams();
        Logger().i(transactions);
        final res =
            await services.createPaypalPayment(transactions, accessToken);
        if (res != null) {
          setState(() {
            checkoutUrl = res["approvalUrl"];
            executeUrl = res["executeUrl"];
          });
        }
      } catch (e) {
        Get.showSnackbar(GetSnackBar(
            mainButton: TextButton(
              child: Text(
                tr('close'),
                style: Theme.of(context)
                    .textTheme
                    .subtitle1
                    .copyWith(color: Colors.white),
              ),
              onPressed: () {
                widget.onPaymentCancelled(tr('payapalTransactionCancelled'));
              },
            ),
            duration: Duration(days: 10),
            messageText: Text(
              e.toString(),
              style: Theme.of(context)
                  .textTheme
                  .subtitle1
                  .copyWith(color: Colors.white),
            )));
      }
    });
  }

  Map<String, dynamic> getOrderParams() {
    List items = [
      {
        "name": widget.options['paypal']['itemName'],
        "quantity": "1",
        "price": widget.options['amount'].toString(),
        "currency": defaultCurrency["currency"]
      }
    ];

    // checkout invoice details
    String totalAmount = widget.options['amount'].toString();
    String userFirstName = widget.options['name'].split(' ')[0] ?? "";
    String userLastName = widget.options['name'].split(' ')[1] ?? "";
    String addressCity = widget.options['city'];
    String addressStreet = widget.options['address'];
    String addressZipCode = widget.options['pincode'];
    String addressCountry = widget.options['country'];
    String addressState = widget.options['state'];
    String addressPhoneNumber = widget.options['contact'];

    Map<String, dynamic> temp = {
      "intent": "sale",
      "payer": {"payment_method": "paypal"},
      "transactions": [
        {
          "amount": {
            "total": totalAmount,
            "currency": defaultCurrency["currency"],
          },
          "description": widget.options['description'],
          "payment_options": {
            "allowed_payment_method": "INSTANT_FUNDING_SOURCE"
          },
          "item_list": {
            "items": items,
            if (isEnableShipping && isEnableAddress)
              "shipping_address": {
                "recipient_name": userFirstName + " " + userLastName,
                "line1": addressStreet,
                "line2": "",
                "city": addressCity,
                "country_code": addressCountry,
                "postal_code": addressZipCode,
                "phone": addressPhoneNumber,
                "state": addressState
              },
          }
        }
      ],
      "note_to_payer":
          'Contact ${AppConfig.APP_NAME} for any questions on your order.',
      "redirect_urls": {"return_url": returnURL, "cancel_url": cancelURL}
    };
    return temp;
  }

  @override
  Widget build(BuildContext context) {
    if (checkoutUrl != null) {
      return Scaffold(
        appBar: AppBar(
          title: Text("${AppConfig.APP_NAME}"),
          backgroundColor: Theme.of(context).backgroundColor,
          leading: GestureDetector(
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
              onTap: () {
                widget.onPaymentCancelled(tr('payapalTransactionCancelled'));
              }),
        ),
        body: _buildBody(context),
      );
    } else {
      return UIUtilWidget.buildCircularProgressIndicatorWithText(
          tr('loadingPaypal'));
    }
  }

  WebView _buildBody(BuildContext context) {
    return WebView(
      initialUrl: checkoutUrl,
      javascriptMode: JavascriptMode.unrestricted,
      navigationDelegate: (NavigationRequest request) async {
        if (request.url.contains(returnURL)) {
          final uri = Uri.parse(request.url);
          final payerID = uri.queryParameters['PayerID'];
          if (payerID != null) {
            try {
              String id = await services.executePayment(
                  executeUrl, payerID, accessToken);
              if (id != null) {
                widget.onComplete(
                    context, {"orderId": id, "paymentId": id, "signature": id});
              }
            } catch (e) {
              widget.onPaymentCancelled("Payapal Transaction Failed");
            }
          } else {
            widget.onPaymentCancelled(tr('payapalTransactionFailed'));
          }
        }
        if (request.url.contains(cancelURL)) {
          widget.onPaymentCancelled(tr('payapalTransactionCancelled'));
        }
        return NavigationDecision.navigate;
      },
    );
  }
}
