import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/core/constant/payment_methods.dart';
import 'package:grobay/presentation/bloc/settings/settings_bloc.dart';
import 'package:grobay/presentation/cubit/settings_cubit.dart';

class PaymentSelectWidget extends StatelessWidget {
  final String groupvalue;
  final Function onChanged;
  final bool enableCod;
  final bool enablePayumoney;
  final bool enableMidtrans;
  final bool enablePaypal;
  final bool enablePaystack;
  final bool enableRazorpay;
  final bool enableFlutterWave;
  final bool enableStripe;
  final bool enablePaytm;
  final bool enableSslEcommerce;
  const PaymentSelectWidget(
      {Key key,
      this.groupvalue,
      this.onChanged,
      this.enableCod,
      this.enablePayumoney,
      this.enablePaypal,
      this.enablePaystack,
      this.enableRazorpay,
      this.enableFlutterWave,
      this.enableMidtrans,
      this.enableStripe,
      this.enablePaytm,
      this.enableSslEcommerce})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, settingsState) {
        if (settingsState is SettingsSuccess) {
          return Container(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              if (this.enableCod)
                _buildCreatePaymentType(
                    context, PAYMENT_METHODS.COD, tr('cod')),
              if (this.enableRazorpay)
                _buildCreatePaymentType(
                    context, PAYMENT_METHODS.RAZORPAY, tr('razorPay')),
              if (this.enablePaystack)
                _buildCreatePaymentType(
                    context, PAYMENT_METHODS.PAYSTACK, tr('paystack')),
              if (this.enableFlutterWave)
                _buildCreatePaymentType(
                    context, PAYMENT_METHODS.FLUTTERWAVE, tr('flutterWave')),
              if (this.enablePaypal)
                _buildCreatePaymentType(
                    context, PAYMENT_METHODS.PAYPAL, tr('paypal')),
              if (this.enableStripe)
                _buildCreatePaymentType(
                    context, PAYMENT_METHODS.STRIPE, tr('stripe')),
              if (this.enablePayumoney)
                _buildCreatePaymentType(
                    context, PAYMENT_METHODS.PAYUMONEY, tr('payUMoney')),
              if (this.enableMidtrans)
                _buildCreatePaymentType(
                    context, PAYMENT_METHODS.MIDTRANS, tr('midtrans')),
              if (this.enablePaytm)
                _buildCreatePaymentType(
                    context, PAYMENT_METHODS.PAYTM, tr('paytm')),
              if (this.enableSslEcommerce)
                _buildCreatePaymentType(context, PAYMENT_METHODS.SSLCOMMERZ,
                    tr('sslEcommerzPayments')),
            ]),
          );
        }
        return SizedBox();
      },
    );
  }

  Widget _buildCreatePaymentType(
      BuildContext context, PAYMENT_METHODS paymentMethods, String title) {
    return Card(
      child: RadioListTile(
          activeColor: Palette.buttonBg,
          value: paymentMethods,
          groupValue:
              BlocProvider.of<SettingsCubit>(context).state.paymentMethod,
          onChanged: (val) {
            print(val);
            BlocProvider.of<SettingsCubit>(context).setPaymentMethod(val);
          },
          title: Text(title)),
    );
  }
}
