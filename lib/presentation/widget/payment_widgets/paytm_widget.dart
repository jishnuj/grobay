import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/payment_widgets/blocs/create_paytm_order/create_paytm_bloc.dart';
import 'package:grobay/presentation/widget/payment_widgets/services/paytm_service.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:logger/logger.dart';
import 'package:paytm_allinonesdk/paytm_allinonesdk.dart';

class PaytmWidget extends StatefulWidget {
  final Function(BuildContext, Map<String, dynamic>) onComplete;
  final Function(String) onPaymentCancelled;
  final Map<String, dynamic> options;
  PaytmWidget(
      {Key key,
      this.title,
      this.onComplete,
      this.onPaymentCancelled,
      this.options})
      : super(key: key);

  final String title;

  @override
  _PaytmWidgetWidgetState createState() => _PaytmWidgetWidgetState();
}

class _PaytmWidgetWidgetState extends State<PaytmWidget> {
  String reference;
  @override
  void initState() {
    super.initState();
    reference = this._getReference();
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<CreatePaytmBloc>(context).add(FetchPaytmCheckSumEvent(
        cust_id: widget.options['username'],
        order_id: reference,
        txn_amount: widget.options['amount'].toString(),
        channel_id: widget.options['paytm']['paytmMode'] == "sandbox"
            ? AppConfig.PAYTM_MOBILE_APP_CHANNEL_ID_DEMO_VAL
            : AppConfig.PAYTM_MOBILE_APP_CHANNEL_ID_LIVE_VAL,
        industry_type_id: widget.options['paytm']['paytmMode'] == "sandbox"
            ? AppConfig.PAYTM_INDUSTRY_TYPE_ID_DEMO_VAL
            : AppConfig.PAYTM_INDUSTRY_TYPE_ID_LIVE_VAL,
        website: widget.options['paytm']['paytmMode'] == "sandbox"
            ? AppConfig.PAYTM_WEBSITE_DEMO_VAL
            : AppConfig.PAYTM_WEBSITE_LIVE_VAL));
    return BlocConsumer<CreatePaytmBloc, CreatePaytmState>(
      listener: (context, state) {
        if (state is CreatePaytmChecksumSuccess) {
          PaytmServices paytmServices =
              PaytmServices(widget.options['paytm']['paytmMode'] == "sandbox");
          paytmServices
              .initiateTranscation(
            mid: state.response.data.mID,
            orderId: state.response.data.oRDERID,
            customerId: state.response.data.cUSTID,
            callbackUrl: state.response.data.cALLBACKURL,
            websiteName: state.response.data.wEBSITE,
            txnAmount: state.response.data.tXNAMOUNT,
            signature: state.response.signature,
            //
            customerName: widget.options['name'],
            email: widget.options['email'],
            mobile: widget.options['contact'],
          )
              .then((value) {
            this.startPayment(context, state, value.txnToken);
          });
        } else if (state is ValidatePaytmPaymentSuccess) {
          widget.onComplete(context, {
            "orderId": state.response.body.orderId,
            "paymentId": state.response.body.txnId,
            "signature": state.response.head.signature
          });
        }
      },
      builder: (context, state) {
        if (state is CreatePaytmChecksumFailed) {
          return CustomErrorWidget(
            msg: tr('errorPaymentFailed'),
            callerName: "paytmpage",
            showbackButton: true,
          );
        } else if (state is ValidatePaytmPaymentFailed) {
          return CustomErrorWidget(
            msg: tr('errorPaymentValidation'),
            callerName: "paytmpage",
            showbackButton: true,
          );
        }
        return UIUtilWidget.buildCircularProgressIndicatorWithText(
            tr('loadingPaytm'));
      },
    );
  }

  startPayment(BuildContext context, CreatePaytmChecksumSuccess state,
      String token) async {
    try {
      var response = AllInOneSdk.startTransaction(
          state.response.data.mID,
          state.response.data.oRDERID,
          state.response.data.tXNAMOUNT,
          token,
          state.response.data.cALLBACKURL,
          widget.options['paytm']['paytmMode'] == "sandbox" ? true : false,
          false);
      response.then((value) {
        BlocProvider.of<CreatePaytmBloc>(context).add(
            FetchValidatePaymentEvent(orderId: state.response.data.oRDERID));
      }).catchError((err) {
        Logger().i(err);
        widget.onPaymentCancelled("Payment Failed");
      });
    } catch (err) {
      Logger().i(err);
      widget.onPaymentCancelled(err.toString());
    }
  }

  String _getReference() {
    String platform;
    if (Platform.isIOS) {
      platform = 'iOS';
    } else {
      platform = 'Android';
    }

    return 'Ref_ChargedFrom${platform}_${DateTime.now().millisecondsSinceEpoch}';
  }
}
