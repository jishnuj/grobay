import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/seller/seller_bloc.dart';
import 'package:grobay/presentation/bloc/startup/startup_bloc.dart';
import 'package:grobay/presentation/page/seller/seller_products_page.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class SellerWidget extends StatelessWidget {
  final bool showAsList;
  final bool isShowingFullData;
  final bool showTitle;
  final showBackButton;
  const SellerWidget(
      {Key key,
      this.showAsList = true,
      this.showTitle = true,
      this.isShowingFullData = false,
      this.showBackButton = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(child: BlocBuilder<StartupBloc, StartupState>(
      builder: (context, appState) {
        if (appState is StartupSuccess) {
          return Container(
            color: !showAsList ? Colors.white : Palette.teritaryColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (this.showTitle) _buildTitle(context),
                UIUtilWidget.verticalSpaceMedium(),
                showAsList
                    ? _buildSellerListHorizontal(appState)
                    : _buildSellerGrid(appState),
              ],
            ),
          );
        }
        return SizedBox();
      },
    ));
  }

  Container _buildSellerGrid(StartupSuccess appState) {
    return Container(
        child: GridView.builder(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10.0),
            shrinkWrap: true,
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            itemCount: appState.startupResposne.seller.length,
            itemBuilder: (context, index) {
              return _buildSellerCard(index, context, appState);
            }));
  }

  Container _buildSellerListHorizontal(StartupSuccess appState) {
    return Container(
      color: Palette.teritaryColor,
      height: 200.0,
      child: ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        scrollDirection: Axis.horizontal,
        itemCount: appState.startupResposne.seller.length,
        itemBuilder: (context, index) {
          return _buildSellerCard(index, context, appState);
        },
      ),
    );
  }

  Container _buildTitle(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 25),
      // color: Theme.of(context).highlightColor.withOpacity(0.1),
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      child: Text(
        tr('featuredSeller').toUpperCase(),
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.subtitle1.copyWith(),
      ),
    );
  }

  Widget _buildSellerCard(
      int index, BuildContext context, StartupSuccess appState) {
    return InkWell(
      onTap: () {
        _onSellerSelect(context, appState, index);
      },
      child: Container(
        width: 150.0,
        child: Card(
            child: Column(
          children: [
            _buildSellerImage(appState, index),
            _buildSellerName(appState, index, context)
          ],
        )),
      ),
    );
  }

  void _onSellerSelect(
      BuildContext context, StartupSuccess appState, int index) {
    BlocProvider.of<SellerBloc>(context)
        .add(SetSellerEvent(seller: appState.startupResposne.seller[index]));
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return SellerProductsPage();
    }));
  }

  Container _buildSellerName(
      StartupSuccess appState, int index, BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        child: Text(appState.startupResposne.seller[index].name,
            textAlign: TextAlign.center,
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.subtitle2));
  }

  Expanded _buildSellerImage(StartupSuccess appState, int index) {
    return Expanded(
        child: Container(
      padding: EdgeInsets.all(20),
      // color: Colors.greenAccent,
      child: CachedNetworkImage(
        imageUrl: appState.startupResposne.seller[index].logo,
        placeholder: (context, url) => Container(
            child: Center(
          child: CircularProgressIndicator(
            color: Palette.onSecondary,
          ),
        )),
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
    ));
  }
}
