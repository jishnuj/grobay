import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:grobay/core/constant/palette.dart';

class PushNotificationCardWidget extends StatelessWidget {
  final RemoteMessage value;
  const PushNotificationCardWidget({Key key, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                tr('youHaveNewNotification'),
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    .copyWith(color: Palette.onPrimary),
              ),
              Text(jsonDecode(this.value.data["data"])["title"],
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodyText2.copyWith(
                        color: Palette.onPrimary,
                      )),
            ],
          ),
        ),
      ],
    );
  }
}
