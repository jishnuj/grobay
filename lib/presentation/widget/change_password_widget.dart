import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/change_password/changepassword_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/widget/password_textfield_widget.dart';

class ChangePassword extends StatelessWidget {
  ChangePassword({Key key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();
  TextEditingController oldpasswordController = TextEditingController();
  TextEditingController newpasswordController = TextEditingController();
  TextEditingController retypeNewpasswordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 1.5,
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(40),topRight: Radius.circular(40)),
          border: Border.all(color: Palette.changePasswordBottomSheetBorder, width: 2)),
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  tr('updatePassword'),
                  style: Theme.of(context).textTheme.headline4,
                ),
                IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.close))
              ],
            ),
            SizedBox(
              height: 20,
            ),
            BlocBuilder<UserloginBloc, UserloginState>(
              builder: (context, userLoginState) {
                if (userLoginState is UserLoginSuccess) {
                  return PasswordTextfieldWidget(
                    textEditingController: this.oldpasswordController,
                    hintText: tr('oldPassword'),
                    validator: (val) {

                      if (val.isEmpty ||
                          !((md5.convert(utf8.encode(val)).toString()) ==
                              userLoginState.userResponse.user.password)) {
                        return tr('oldPasswordNotCorrect');
                      }
                      return null;
                    },
                  );
                }
                return SizedBox();
              },
            ),
            SizedBox(
              height: 20,
            ),
            PasswordTextfieldWidget(
              textEditingController: this.newpasswordController,
              hintText: tr('newPassword'),
            ),
            SizedBox(
              height: 20,
            ),
            PasswordTextfieldWidget(
              validator: (val) {
                if (val == this.newpasswordController.text) {
                  return null;
                }
                return tr('newPasswordsDontMatch');
              },
              textEditingController: this.retypeNewpasswordController,
              hintText: tr('retypeNewPassword'),
            ),
            SizedBox(
              height: 50,
            ),
            BlocConsumer<ChangepasswordBloc, ChangepasswordState>(
              listener: (context, changePasswordState) {
                if (changePasswordState is ChangepasswordSuccess) {
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text(changePasswordState.message)));
                  BlocProvider.of<UserloginBloc>(context)
                      .add(FetchLogoutEvent());
                  Navigator.pushNamedAndRemoveUntil(
                      context, "login", (route) => false);
                } else if (changePasswordState is ChangepasswordFailed) {
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text(changePasswordState.message)));
                }
              },
              builder: (context, changePasswordState) {
                if (changePasswordState is ChangepasswordLoading) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        BlocProvider.of<ChangepasswordBloc>(context).add(
                            UpdatePasswordEvent(
                                password: newpasswordController.text));
                      }
                    },
                    child: Text(tr('changePassword')));
              },
            )
          ],
        ),
      ),
    );
  }
}
