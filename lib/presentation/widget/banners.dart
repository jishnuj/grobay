import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/slider_image.dart';
import 'package:grobay/presentation/bloc/startup/startup_bloc.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';

class Banners extends StatelessWidget {
  const Banners({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StartupBloc, StartupState>(
      builder: (context, appState) {
        if (appState is StartupSuccess) {
          return CarouselSlider(
            options: CarouselOptions(
                height: 230.0,
                disableCenter: true,
                viewportFraction: 1.0,
                autoPlay: true),
            items: appState.startupResposne.sliderImages.map((i) {
              return Builder(
                builder: (BuildContext context) {
                  return _buildBannerHorizontalList(context, i);
                },
              );
            }).toList(),
          );
        } else if (appState is StartupFailed) {
          return CustomErrorWidget(
            callerName: "banner",
            msg: appState.message,
            // onNetworkError: () {
            //   BlocProvider.of<StartupBloc>(context).add(FetchStartupEvent());
            // },
          );
        }
        return SizedBox();
      },
    );
  }

  Container _buildBannerHorizontalList(BuildContext context, SliderImage i) {
    return Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(color: Colors.amber),
        child: CachedNetworkImage(
          fit: BoxFit.fill,
          imageUrl: i.image,
          placeholder: (context, url) => Container(
            color: Colors.lime[200],
          ),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ));
  }
}
