import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/constant/assets.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/core/constant/sidemenu.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class SideMenuWidget extends StatefulWidget {
  SideMenuWidget({Key key}) : super(key: key);

  @override
  _SideMenuWidgetState createState() => _SideMenuWidgetState();
}

class _SideMenuWidgetState extends State<SideMenuWidget> {
  int _selectedIndex = 1;
  @override
  Widget build(BuildContext context) {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: BlocBuilder<UserloginBloc, UserloginState>(
        builder: (context, userLoginState) {
          return Container(
            child: ListView.builder(
                itemCount: sidemenu.length + 1,
                padding: EdgeInsets.zero,
                itemBuilder: (context, index) {
                  if (index == 0) {
                    return _buildSideMenuHeader(context, userLoginState);
                  }
                  if (sidemenu[index - 1]['usermenu'] == 0) {
                    return _buildMenuTile(index, context);
                  } else if (sidemenu[index - 1]['usermenu'] == 1 &&
                      userLoginState is UserLoginSuccess) {
                    return _buildMenuTile(index, context);
                  }
                  return SizedBox();
                }),
          );
        },
      ),
    );
  }

  ListTile _buildMenuTile(int index, BuildContext context) {
    return ListTile(
      leading: Icon(sidemenu[index - 1]["icon"]),
      title: Text(sidemenu[index - 1]["name"]),
      trailing: this._selectedIndex == index
          ? Icon(
              Icons.arrow_forward_ios_outlined,
              size: 15.0,
              color: Theme.of(context).colorScheme.secondary,
            )
          : SizedBox(),
      onTap: () {
        // Update the state of the app.
        // ...
        if (sidemenu[index - 1]["onpressed"] != null) {
          Navigator.pop(context);
          sidemenu[index - 1]["onpressed"](context);
        } else {
          if (this._selectedIndex == index) {
            Navigator.pop(context);
          } else {
            setState(() {
              this._selectedIndex = index;
            });
            Navigator.pop(context);
            Navigator.pushNamed(context, sidemenu[index - 1]["path"]);
          }
        }
      },
    );
  }

  SafeArea _buildSideMenuHeader(
      BuildContext context, UserloginState userLoginState) {
    return SafeArea(
      child: Container(
          padding: EdgeInsets.all(10.0),
          height: 130,
          decoration: BoxDecoration(
              color: Platform.isIOS ? Palette.onPrimary : Palette.primary,
              // border: Border(bottom: BorderSide(color: Colors.grey))

              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30.0),
                  bottomRight: Radius.circular(30.0))),
          child: userLoginState is UserLoginSuccess
              ? _buiildUserHeader(context, userLoginState)
              : _buildGuestHeader(context)),
    );
  }

  InkWell _buiildUserHeader(
      BuildContext context, UserLoginSuccess userLoginState) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, "profile");
      },
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _buildProfileImage(userLoginState),
            UIUtilWidget.verticalSpaceMedium(),
            _buildWalletBalance(userLoginState, context)
          ],
        ),
      ),
    );
  }

  Padding _buildProfileImage(UserLoginSuccess userLoginState) {
    if (userLoginState.userResponse.user.profile.isNotEmpty) {
      return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: CachedNetworkImage(
            height: 75,
            width: 75,
            imageUrl: userLoginState.userResponse.user.profile,
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(75),
                image: DecorationImage(
                    image: NetworkImage(
                      userLoginState.userResponse.user.profile,
                    ),
                    fit: BoxFit.cover,
                    colorFilter:
                        ColorFilter.mode(Colors.red, BlendMode.colorBurn)),
              ),
            ),
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ));
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Container(
          clipBehavior: Clip.hardEdge,
          height: 75,
          width: 75,
          decoration: BoxDecoration(
              border: Border.all(
                  color:
                      Platform.isAndroid ? Palette.buttonBg : Palette.primary),
              borderRadius: BorderRadius.circular(50.0)),
          child: Icon(
            Icons.person,
            size: 50,
            color: Palette.onPrimary,
          )),
    );
  }

  Column _buildWalletBalance(
      UserLoginSuccess userLoginState, BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          userLoginState.userResponse.user.data[0].name,
          style: Theme.of(context)
              .textTheme
              .headline5
              .copyWith(color: Palette.onSecondary),
        ),
        Text(
          userLoginState.userResponse.user.mobile,
          style: Theme.of(context)
              .textTheme
              .subtitle1
              .copyWith(color: Palette.onSecondary),
        ),
        Text(
          '${tr('wallet')} - ${CurrencyWidget.getFixedPriceCurrencyString(double.parse(userLoginState.userResponse.user.balance))}',
          style: Theme.of(context)
              .textTheme
              .subtitle1
              .copyWith(color: Palette.onSecondary),
        )
      ],
    );
  }

  Container _buildGuestHeader(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            Assets.ICON_HEADER,
            width: 70,
          ),
          UIUtilWidget.verticalSpaceMedium(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                tr('welcomeGuest'),
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(color: Palette.onSecondary),
              ),
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, "login");
                },
                child: Text(
                  tr('loginRegister'),
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1
                      .copyWith(color: Palette.onSecondary),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
