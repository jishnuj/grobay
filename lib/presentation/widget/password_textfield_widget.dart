import 'package:flutter/material.dart';
import 'package:grobay/core/constant/palette.dart';

class PasswordTextfieldWidget extends StatefulWidget {
  final Function(String) validator;
  final TextEditingController textEditingController;
  final InputDecoration inputDecoration;
  final bool showBorder;
  final BorderRadius borderRadius;
  final String hintText;

  PasswordTextfieldWidget(
      {Key key,
      this.validator,
      this.textEditingController,
      this.hintText,
      this.borderRadius,
      this.showBorder = true,
      this.inputDecoration})
      : super(key: key);

  @override
  _PasswordTextfieldWidgetState createState() =>
      _PasswordTextfieldWidgetState();
}

class _PasswordTextfieldWidgetState extends State<PasswordTextfieldWidget> {
  bool obscureText = true;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.textEditingController,
      obscureText: obscureText,
      keyboardType: TextInputType.visiblePassword,
      validator: widget.validator,
      decoration: widget.inputDecoration ?? !widget.showBorder
          ? _buildNoBorderInputDecoration()
          : _buildBorderedInputDecoration(),
    );
  }

  InputDecoration _buildNoBorderInputDecoration() {
    return InputDecoration(
      border: InputBorder.none,
      prefixIcon: Icon(Icons.lock),
      suffixIcon: IconButton(
        icon: obscureText ? Icon(Icons.visibility) : Icon(Icons.visibility_off),
        onPressed: () {
          setState(() {
            obscureText = !obscureText;
          });
        },
      ),
      hintText: widget.hintText,
    );
  }

  InputDecoration _buildBorderedInputDecoration() {
    OutlineInputBorder _outlineInputBorder = OutlineInputBorder(
        borderRadius: widget.borderRadius ?? BorderRadius.circular(10.0),
        borderSide: BorderSide(color: Palette.disabledColor));
    return InputDecoration(
        prefixIcon: Icon(Icons.lock),
        suffixIcon: IconButton(
          icon:
              obscureText ? Icon(Icons.visibility) : Icon(Icons.visibility_off),
          onPressed: () {
            setState(() {
              obscureText = !obscureText;
            });
          },
        ),
        hintText: widget.hintText,
        border: _outlineInputBorder);
  }
}
