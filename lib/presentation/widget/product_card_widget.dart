import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/product.dart';
import 'package:grobay/api/model/variant.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/page/product/product_detail_page.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:grobay/presentation/widget/wishlist_icon_widget.dart';

// ignore: must_be_immutable
class ProductCardWidget extends StatefulWidget {
  Variants selectedVariant;
  final bool showFavouriteIcon;
  final Product product;
  ProductCardWidget({
    Key key,
    this.product,
    this.showFavouriteIcon = true,
  }) : super(key: key) {
    this.selectedVariant = product.variants[0];
  }

  @override
  _ProductCardWidgetState createState() => _ProductCardWidgetState();
}

class _ProductCardWidgetState extends State<ProductCardWidget> {
  @override
  Widget build(BuildContext context) {
    return _buildProductCard(context);
  }

  double calculateTaxPrice(double price) {
    if (widget.product.taxPercentage != null) {
      return (price +
          (price * double.parse(widget.product.taxPercentage)) / 100);
    } else {
      return price;
    }
  }

  Widget _buildProductCard(BuildContext context) {
    return Stack(
      children: [
        Card(
          margin: EdgeInsets.symmetric(vertical: 5.0),
          child: Container(
            padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
            child: InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return ProductDetailsPage(
                    product: widget.product,
                    productId: widget.product.id,
                  );
                }));
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      CachedNetworkImage(
                        height: 80,
                        width: 80,
                        imageUrl: widget.product.image,
                        placeholder: (context, url) => Container(
                          decoration: BoxDecoration(
                              color: Palette.imageCacheColor,
                              borderRadius: BorderRadius.circular(50.0)),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Flexible(
                              child: Text(
                                this.widget.product.name,
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .subtitle1
                                    .copyWith(
                                        color: Palette.onSecondary,
                                        fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(
                              width: 25.0,
                            )
                          ],
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          widget.selectedVariant.measurement +
                              " " +
                              widget.selectedVariant.measurementUnitName + " ("+
                              ((double.parse(widget.selectedVariant.price) -
                                          double.parse(widget.selectedVariant
                                              .discountedPrice)) /
                                      double.parse(
                                          widget.selectedVariant.price))
                                  .floorToDouble()
                                  .toString() + "${tr('off')})",
                          style: Theme.of(context)
                              .primaryTextTheme
                              .subtitle2
                              .copyWith(
                                color: Palette.onSecondary,
                              ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        RichText(
                            text: TextSpan(
                                text:
                                    '${CurrencyWidget.getFixedPriceCurrencyString(calculateTaxPrice(double.parse(widget.selectedVariant.discountedPrice)))}  ',
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .subtitle1
                                    .copyWith(
                                      color: Palette.onSecondary,
                                    ),
                                children: [
                              TextSpan(
                                  text:
                                      '${CurrencyWidget.getFixedPriceCurrencyString(calculateTaxPrice(double.parse(widget.selectedVariant.price)))}',
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .subtitle1
                                      .copyWith(
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color:
                                              Theme.of(context).disabledColor))
                            ])),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Flexible(
                              child: DropdownButtonFormField<Variants>(
                                isExpanded: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
                                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
                                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
                                  contentPadding: EdgeInsets.all(0.0)
                                ),
                                alignment: Alignment.bottomLeft,
                                isDense: true,
                                items: widget.product.variants.map((value) {
                                  return DropdownMenuItem<Variants>(
                                
                                    value: value,
                                    child: new Text(value.measurement +
                                        " " +
                                        value.measurementUnitName
                                        ,style: Theme.of(context).textTheme.subtitle2
                                        ),
                                  );
                                }).toList(),
                                value: widget.selectedVariant,
                                onChanged: (value) {
                                  setState(() {
                                    widget.selectedVariant = value;
                                  });
                                },
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            widget.selectedVariant.stock != "0"
                                ? _buildAddRemoveButton(context)
                                : Text(
                                    widget.selectedVariant.serveFor,
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle2
                                        .copyWith(color: Colors.red),
                                  ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        if (widget.showFavouriteIcon)
          Align(
            child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 15.0, horizontal: 10.0),
                child: WishListIconWidget(
                    product: widget.product, isMiniType: true)),
            alignment: Alignment.topRight,
          ),
      ],
    );
  }

  Widget _buildAddRemoveButton(BuildContext context) {
    UserloginState userLoginState =
        BlocProvider.of<UserloginBloc>(context).state;
    return BlocConsumer<CartCubit, CartCubitState>(
      listener: (context, cartCubitState) {
        if (BlocProvider.of<CartCubit>(context).state.lastUpdatedVariantId ==
            widget.selectedVariant.id) if ((BlocProvider.of<CartCubit>(context)
                        .getVariantsCount(widget.selectedVariant.id)
                        .toDouble() *
                    double.parse(widget.selectedVariant.measurement)) >=
                double.parse(widget.selectedVariant.stock) &&
            !cartCubitState.isFromCart) {
          UIUtilWidget.clearAllSnackBars();
          UIUtilWidget.showSnackBarWidget(
              Text(
                  "${tr('only')} ${BlocProvider.of<CartCubit>(context).getVariantsCount(widget.selectedVariant.id)} ${tr('isLeft')}"),
              seconds: 1);
        } else if (BlocProvider.of<CartCubit>(context)
                    .getVariantsCount(widget.selectedVariant.id) ==
                10 &&
            !cartCubitState.isFromCart) {
          UIUtilWidget.clearAllSnackBars();
          UIUtilWidget.showSnackBarWidget(
              Text(tr('apologiesMaxProdQuantityReached')),
              seconds: 1);
        }
      },
      builder: (context, cartCubitState) {
        return Container(
          width: 130,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                onTap: () {
                  BlocProvider.of<CartCubit>(context).removeProductVariant(
                      widget.product.id, widget.selectedVariant.id,
                      userId: userLoginState is UserLoginSuccess
                          ? userLoginState.userResponse.user.userId
                          : null);
                },
                child: Container(
                  padding: EdgeInsets.all(4.0),
                  decoration: BoxDecoration(
                      color: Palette.buttonBg,
                      border: Border.all(
                        color: Palette.buttonBg,
                      ),
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Icon(
                    Icons.remove,
                    size: 15.0,
                    color: Palette.buttonText,
                  ),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Expanded(
                  child: Text(
                BlocProvider.of<CartCubit>(context)
                            .getVariantsCount(widget.selectedVariant.id) !=
                        null
                    ? BlocProvider.of<CartCubit>(context)
                        .getVariantsCount(widget.selectedVariant.id)
                        .toString()
                    : 0.toString(),
                textAlign: TextAlign.center,
              )),
              SizedBox(
                width: 15,
              ),
              InkWell(
                onTap: () {
                  BlocProvider.of<CartCubit>(context).addProductVariant(
                      widget.product.id,
                      widget.selectedVariant.id,
                      double.parse(widget.selectedVariant.stock),
                      double.parse(widget.selectedVariant.measurement),
                      userId: userLoginState is UserLoginSuccess
                          ? userLoginState.userResponse.user.userId
                          : null);
                },
                child: Container(
                  padding: EdgeInsets.all(4.0),
                  decoration: BoxDecoration(
                      color:  Palette.buttonBg,
                      border: Border.all(
                        color:  Palette.buttonBg,
                      ),
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Icon(
                    Icons.add,
                    size: 15.0,
                    color:  Palette.buttonText,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
