import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/presentation/bloc/cart/cart_bloc.dart';
import 'package:grobay/presentation/bloc/user_address/useraddress_bloc.dart';
import 'package:grobay/presentation/cubit/settings_cubit.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';

class OrderConfirmWidget extends StatelessWidget {
  const OrderConfirmWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: BlocBuilder<CartBloc, CartState>(
        builder: (context, cartState) {
          UseraddressSuccess useraddressSuccess =
              BlocProvider.of<UseraddressBloc>(context).state;
          if (cartState is CartSuccess) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
              height: 240,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(tr('itemAmount')),
                      Text(CurrencyWidget.getFixedPriceCurrencyString(
                          cartState.totalPrice))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(tr('deliveryCharge')),
                      Text(CurrencyWidget.getFixedPriceCurrencyString(
                          double.parse(useraddressSuccess
                              .selectedAddress.deliveryCharges)))
                    ],
                  ),
                
                  if (cartState.promoCode != null)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(tr('promoCodeDiscount')),
                        Text(CurrencyWidget.getFixedPriceCurrencyString(
                            double.parse(cartState.promoCodeDiscount)))
                      ],
                    ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(tr('total')),
                      Text(CurrencyWidget.getFixedPriceCurrencyString(
                          (double.parse(useraddressSuccess
                                  .selectedAddress.deliveryCharges) +
                              cartState.discountedTotalPrice)))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                       tr('finalTotal'),
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      Text(
                        CurrencyWidget.getFixedPriceCurrencyString(
                            (double.parse(useraddressSuccess
                                    .selectedAddress.deliveryCharges) +
                                cartState.discountedTotalPrice)),
                        style: Theme.of(context).textTheme.headline6,
                      )
                    ],
                  ),
                  Expanded(
                      child: TextField(
                    decoration: InputDecoration(hintText: tr('specialNotes')),
                    onChanged: (val) {
                      BlocProvider.of<SettingsCubit>(context).setNote(val);
                    },
                  )),
                  Expanded(
                      child: Row(
                    children: [
                      Expanded(
                          child: ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(tr('cancel')))),
                      SizedBox(
                        width: 10.0,
                      ),
                      Expanded(
                          child: ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                                Navigator.pushNamed(context, "orderprocess");
                              },
                              child: Text(tr('confirm')))),
                    ],
                  ))
                ],
              ),
            );
          }
          return SizedBox();
        },
      ),
    );
  }
}
