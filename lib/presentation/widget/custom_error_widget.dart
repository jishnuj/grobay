import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grobay/core/constant/assets.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/core/platform/network_connection_service.dart';
import 'package:grobay/presentation/widget/network_connectivity_adapter.dart';

class CustomErrorWidget extends StatefulWidget {
  final String msg;
  final String callerName;
  final bool showWarningIcon;
  final Icon image;
  final bool showbackButton;
  final bool showIcon;
  final Color backgroundColor;
  final FlutterErrorDetails errorDetails;
  final Function onNetworkError;
  CustomErrorWidget(
      {Key key,
      this.msg,
      this.showIcon = true,
      this.image,
      this.callerName,
      this.errorDetails,
      this.onNetworkError,
      this.showbackButton = false,
      this.backgroundColor,
      this.showWarningIcon = false})
      : super(key: key);

  @override
  _CustomErrorWidgetState createState() => _CustomErrorWidgetState();
}

class _CustomErrorWidgetState extends State<CustomErrorWidget> {
  NetworkConnectionService _networkConnectionService =
      GetIt.I.get<NetworkConnectionService>();

  @override
  void initState() {
    super.initState();

    Timer.run(() {
      _networkConnectionService.checkConnection().then((value) async {
        if (value && widget.onNetworkError != null) {
          widget.onNetworkError();
        }
      });
    });
  }

  void showErroMessage() {
    String msg = widget.msg;
    Get.showSnackbar(GetSnackBar(
      message: msg,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return NetworkConnectivityAdapter(
      key: widget.key,
      callerName: widget.callerName,
      onNetworkAvailable: widget.onNetworkError ?? () {},
      child: Container(
        color: widget.backgroundColor ?? Palette.secondary,
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(vertical: 25.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (widget.showWarningIcon)
              Image.asset(
                Assets.LOGO,
                width: MediaQuery.of(context).size.width / 1.1,
                height: MediaQuery.of(context).size.width / 1.1,
                fit: BoxFit.fill,
              ),
            widget.image == null ? _buildErrorIcon() : widget.image,
            _buildErrorText(),
            widget.showbackButton
                ? OutlinedButton(
                    style: OutlinedButton.styleFrom(
                        side: BorderSide(color: Palette.highlightColor)),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      tr('goback'),
                      style: Theme.of(context)
                          .textTheme
                          .bodyText2
                          .copyWith(color: Palette.highlightColor),
                    ))
                : SizedBox()
          ],
        ),
      ),
    );
  }

  Text _buildErrorText() {
    return Text(
      widget.msg ?? tr('failedToFetchData'),
      textAlign: TextAlign.center,
      style: GoogleFonts.robotoCondensed(
        color: Palette.disabledColor,
        fontSize: 21.0,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget _buildErrorIcon() {
    return widget.showIcon?( widget.showWarningIcon
        ? Icon(
            Icons.report_problem_outlined,
            size: 43.0,
            color: Palette.disabledColor,
          )
        : Icon(
            Icons.info_outlined,
            size: 43,
            color: Palette.disabledColor,
          )):SizedBox();
  }
}
