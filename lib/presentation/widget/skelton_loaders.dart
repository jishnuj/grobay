import 'package:flutter/material.dart';
import 'package:grobay/api/model/product.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/widget/side_menu_widget.dart';
import 'package:grobay/presentation/widget/ui_helper.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class ProductDetailsSkeltonloader extends StatelessWidget {
  final Product product;
  const ProductDetailsSkeltonloader({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: Text(
          product.name,
          style: Theme.of(context)
              .textTheme
              .headline6
              .copyWith(color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
            child: SkeletonLoader(
          baseColor: Colors.grey[300],
          builder: Column(
            children: [
              SizedBox(
                height: 5.0,
              ),
              Container(
                  margin: EdgeInsets.symmetric(vertical: 7.5, horizontal: 15.0),
                  decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.circular(20.0)),
                  height: 325,
                  child: SkeletonLoader(builder: Icon(Icons.image_outlined))),
              Container(
                margin: EdgeInsets.symmetric(vertical: 7.5, horizontal: 15.0),
                decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(20.0)),
                height: 100,
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 7.5, horizontal: 15.0),
                child: SkeletonGridLoader(
                  items: 2,
                  builder: Container(
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(20.0)),
                    height: 50,
                  ),
                ),
              )
            ],
          ),
        )),
      ),
    );
  }
}

class HomePageSkeltonloader extends StatelessWidget {
  const HomePageSkeltonloader({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenuWidget(),
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/logo.png",
          width: 50,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
            child: SkeletonLoader(
          baseColor: Colors.grey[300],
          builder: Column(
            children: [
              UIHelper.verticalSpaceMedium(),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Palette.teritaryColor),
                margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
                height: 100,
              ),
              Container(
                  margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Palette.teritaryColor),
                  height: 300,
                  child: SkeletonLoader(builder: Icon(Icons.image_outlined))),
              Container(
                margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Palette.teritaryColor),
                height: 100,
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
                child: SkeletonGridLoader(
                  items: 2,
                  builder: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Palette.teritaryColor),
                    height: 50,
                  ),
                ),
              )
            ],
          ),
        )),
      ),
    );
  }
}

class SellerProductListLoader extends StatelessWidget {
  const SellerProductListLoader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: SkeletonLoader(
      baseColor: Colors.grey[300],
      builder: Column(
          children: [1, 2, 3, 4, 5, 6, 7, 8, 9]
              .map((e) => Container(
                   decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Palette.teritaryColor),
                    height: 80,
                    margin: EdgeInsets.symmetric(vertical: 5.0),
                  ))
              .toList()),
    ));
  }
}

class FavouriteListLoader extends StatelessWidget {
  const FavouriteListLoader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 7.5),
        child: SkeletonLoader(
          baseColor: Colors.grey[300],
          builder: Column(
              children: [1, 2, 3, 4, 5, 6, 7, 8, 9]
                  .map((e) => Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Palette.teritaryColor),
                        height: 130,
                        margin: EdgeInsets.symmetric(vertical: 5.0),
                      ))
                  .toList()),
        ));
  }
}

class OrderListLoader extends StatelessWidget {
  const OrderListLoader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        // margin: EdgeInsets.symmetric(horizontal: 10.0),
        child: SkeletonLoader(
      baseColor: Colors.grey[300],
      builder: Column(
          children: [1, 2, 3, 4, 5, 6, 7, 8, 9]
              .map((e) => Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Palette.teritaryColor),
                    height: 130,
                    margin: EdgeInsets.symmetric(vertical: 5.0),
                  ))
              .toList()),
    ));
  }
}

class CartListLoader extends StatelessWidget {
  const CartListLoader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
          // margin: EdgeInsets.symmetric(horizontal: 5.0),
          child: SkeletonLoader(
        baseColor: Colors.grey[300],
        builder: Column(
            children: [1, 2, 3, 4, 5, 6, 7, 8, 9]
                .map((e) => Container(
                      decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(10.0)),
                      height: 100,
                      margin: EdgeInsets.symmetric(vertical: 5.0),
                    ))
                .toList()),
      )),
    );
  }
}

class AddressListLoader extends StatelessWidget {
  const AddressListLoader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
          margin: EdgeInsets.symmetric(vertical: 15.0),
          child: SkeletonLoader(
        baseColor: Colors.grey[300],
        builder: Column(
            children: [1, 2, 3]
                .map((e) => Container(
                      decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(10.0)),
                      height: 130,
                      margin:
                          EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                    ))
                .toList()),
      )),
    );
  }
}

class CategoryListLoader extends StatelessWidget {
  const CategoryListLoader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0),
          child: SkeletonGridLoader(
            items: 10,
            itemsPerRow: 2,
            crossAxisSpacing: 20.0,
            mainAxisSpacing: 20.0,
            baseColor: Colors.grey[300],
            builder: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Palette.teritaryColor),
              height: 100,
              margin: EdgeInsets.symmetric(vertical: 5.0),
            ),
          )),
    );
  }
}

class SearchListLoader extends StatelessWidget {
  const SearchListLoader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15.0,vertical: 7.5),
          child: SkeletonLoader(
            baseColor: Colors.grey[300],
            builder: Column(
                children: [1, 2, 3, 4, 5, 6, 7, 8, 9]
                    .map((e) => Container(
                          decoration: BoxDecoration(
                              color: Colors.grey,
                              borderRadius: BorderRadius.circular(10.0)),
                          height: 100,
                          margin: EdgeInsets.symmetric(vertical: 5.0),
                        ))
                    .toList()),
          )),
    );
  }
}

class CategoryProductListLoader extends StatelessWidget {
  const CategoryProductListLoader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        // margin: EdgeInsets.symmetric(horizontal: 10.0),
        child: SkeletonLoader(
      baseColor: Palette.teritaryColor,
      builder: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: [1, 2, 3, 4]
          //       .map((e) => Container(
          //             height: 80,
          //             decoration: BoxDecoration(
          //                 color: Palette.teritaryColor,
          //                 borderRadius: BorderRadius.circular(20)),
          //             width: (MediaQuery.of(context).size.width / 4.6) - 9.5,
          // margin:
          //     EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
          //           ))
          //       .toList(),
          // ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 5.0),
            height: 100,
            decoration: BoxDecoration(
                color: Palette.teritaryColor,
                borderRadius: BorderRadius.circular(20)),
            width: MediaQuery.of(context).size.width,
          ),
          SizedBox(
            height: 15.0,
          ),
          Column(
              children: [1, 2, 3, 4, 5, 6, 7, 8, 9]
                  .map((e) => Container(
                        decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.circular(30)),
                        height: 80,
                        margin: EdgeInsets.symmetric(vertical: 10.0),
                      ))
                  .toList()),
        ],
      ),
    ));
  }
}
