import 'package:flutter/material.dart';
import 'package:grobay/core/constant/palette.dart';

class CustomButtonWidget extends StatelessWidget {
  final Function() onTap;
  final String text;
  const CustomButtonWidget({Key key, @required this.onTap, @required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 50),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: Palette.buttonBg),
        child: Center(
          child: Text(
            text,
            style: TextStyle(
                color: Palette.buttonText, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
