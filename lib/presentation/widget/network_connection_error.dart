import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:grobay/core/constant/assets.dart';
import 'package:grobay/presentation/widget/ui_helper.dart';


class NetworkConnectionError extends StatelessWidget {
  const NetworkConnectionError({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              Assets.LOGO,
              width: MediaQuery.of(context).size.width / 1.1,
              height: MediaQuery.of(context).size.width / 1.1,
              fit: BoxFit.fill,
            ),
            Text(
              tr('noInternetConnection'),
              style: Theme.of(context)
                  .textTheme
                  .headline6
                  .copyWith(fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            UIHelper.verticalSpaceMedium(),
            Text(
              tr('pleaseCheckYourInternetConnection'),
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(color: Colors.grey),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
