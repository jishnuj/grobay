import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/product.dart';
import 'package:grobay/api/model/section.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/startup/startup_bloc.dart';
import 'package:grobay/presentation/page/product/product_detail_page.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

import 'currency_widget.dart';

class FeaturedSectionWidget extends StatelessWidget {
  const FeaturedSectionWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: BlocBuilder<StartupBloc, StartupState>(
        builder: (context, appState) {
          if (appState is StartupSuccess) {
            return Container(
              margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
              child: Column(
                children: appState.startupResposne.sections
                    .map((e) => Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            _buildFeatureTitle(e, context),
                            _buildProductList(e)
                          ],
                        ))
                    .toList(),
              ),
            );
          } else if (appState is StartupFailed) {
            return _buildErrorBox(appState, context);
          }
          return SizedBox();
        },
      ),
    );
  }

  Widget _buildErrorBox(StartupFailed startupFailed, BuildContext context) {
    return Container(
      child: CustomErrorWidget(
        callerName: "featured_section_widget",
        msg: startupFailed.message,
        onNetworkError: () {
          BlocProvider.of<StartupBloc>(context).add(FetchStartupEvent());
        },
      ),
    );
  }

  Flexible _buildProductList(Section e) {
    return Flexible(
      child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
          itemCount: e.products.length,
          itemBuilder: (context, index) {
            return _buildProductCard(index, e, context);
          }),
    );
  }

  Container _buildFeatureTitle(Section e, BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20.0, top: 10.0),
      margin: EdgeInsets.only(top: 30.0),
      child: Text(
        e.title.toUpperCase(),
        textAlign: TextAlign.center,
        style: Theme.of(context)
            .textTheme
            .subtitle1
            .copyWith(fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildProductCard(int index, Section section, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return ProductDetailsPage(
            productId: section.products[index].id,
            product: section.products[index],
          );
        }));
      },
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              _buildProductImage(section, index),
              UIUtilWidget.verticalSpaceCustom(30.0),
              _buildProductName(section, index, context),
              _buildProductPrice(section, index, context)
            ],
          ),
        ),
      ),
    );
  }

  Text _buildProductName(Section section, int index, BuildContext context) {
    return Text(
      section.products[index].name,
      style: Theme.of(context).textTheme.subtitle2,
      overflow: TextOverflow.ellipsis,
    );
  }

  Text _buildProductPrice(Section section, int index, BuildContext context) {
    return Text(
        ' ${CurrencyWidget.getFixedPriceCurrencyString(calculateTaxPrice(section.products[index]))}',
        style: Theme.of(context)
            .textTheme
            .subtitle2
            .copyWith(color: Palette.homeCurrencyTextColor));
  }

  Expanded _buildProductImage(Section section, int index) {

    return Expanded(
        child: CachedNetworkImage(
      imageUrl: section.products[index].image,
      width: 100,
      placeholder: (context, url) => Container(
          child: Center(
        child: CircularProgressIndicator(
          color: Palette.onSecondary,
        ),
      )),
      errorWidget: (context, url, error) => Icon(Icons.broken_image_rounded,color: Colors.grey,),
    ));
  }

  double calculateTaxPrice(Product product) {
    double price = double.parse(product.variants[0].discountedPrice);
    return (price + (price * double.parse(product.taxPercentage)) / 100);
  }
}
