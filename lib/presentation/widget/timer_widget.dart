import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:otp_count_down/otp_count_down.dart';

class TimerWidget extends StatefulWidget {
  final Function onResend;
  TimerWidget({Key key, @required this.onResend}) : super(key: key);

  @override
  _TimerWidgetState createState() => _TimerWidgetState();
}

class _TimerWidgetState extends State<TimerWidget> {
  String _countDown = "0";
  OTPCountDown _otpCountDown;
  bool disableResend = true;
  final int _otpTimeInMS = 1000 * 1 * 60;

  @override
  void initState() {
    _startCountDown();
    super.initState();
  }

  void _startCountDown() {
    _otpCountDown = OTPCountDown.startOTPTimer(
      timeInMS: _otpTimeInMS,
      currentCountDown: (String countDown) {
        _countDown = countDown;
        setState(() {});
      },
      onFinish: () {
        setState(() {
          disableResend = false;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      margin: EdgeInsets.symmetric(horizontal: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          disableResend
              ? Text(
                  tr('resend'),
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2
                      .copyWith(color: Palette.disabledColor),
                )
              : InkWell(
                  onTap: () {
                    _startCountDown();
                    widget.onResend();
                  },
                  child: Text( tr('resend'),
                      style: Theme.of(context)
                          .textTheme
                          .subtitle2
                          .copyWith(color: Palette.onSecondary))),
          Center(
            child: Text(
              _countDown,
              style: Theme.of(context).textTheme.subtitle2,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _otpCountDown.cancelTimer();
    super.dispose();
  }
}
