import 'package:flutter/widgets.dart';
import 'package:grobay/core/config/app_config.dart';

class CurrencyWidget extends StatelessWidget {
  final TextStyle style;
  final double price;
  final int fixedCount;
  const CurrencyWidget(
      {this.fixedCount = 2, this.style, @required this.price, Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
        CurrencyWidget.getFixedPriceCurrencyString(this.price,
            fixedCount: this.fixedCount),
        style: this.style);
  }

  static getFixedPriceCurrencyString(double price, {int fixedCount}) {
    return AppConfig.CURRENCY + " " + price.toStringAsFixed(fixedCount ?? 2);
  }

  static getFixedDoubleString(double price, {int fixedCount}) {
    return price.toStringAsFixed(fixedCount ?? 2);
  }
}
