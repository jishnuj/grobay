import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/cart/cart_bloc.dart';
import 'package:grobay/presentation/bloc/user_address/useraddress_bloc.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/pincode_list_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

enum CartFooterType { CHECKOUT, NORMAL, PAYMENT }

class CartFooterWidget extends StatelessWidget {
  final CartFooterType cartFooterType;
  final Function onPressed;
  const CartFooterWidget({
    this.onPressed,
    this.cartFooterType = CartFooterType.NORMAL,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (this.cartFooterType == CartFooterType.CHECKOUT)
      return _buildCheckout();
    else
      return _buildNormal();
  }

  BlocBuilder<CartBloc, CartState> _buildCheckout() {
    return BlocBuilder<CartBloc, CartState>(
      builder: (context, cartState) {
        if (cartState is CartSuccess) {
          return Container(
            padding: EdgeInsets.symmetric(
              vertical: 15,
              horizontal: 30,
            ),
            // height: 174,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
              boxShadow: [
                BoxShadow(
                  offset: Offset(0, -15),
                  blurRadius: 20,
                  color: Color(0xFFDADADA).withOpacity(0.15),
                )
              ],
            ),
            child: SafeArea(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 20),
                  Column(
                    children: [
                      _buildSubTotal(context, cartState),
                      SizedBox(
                        height: 10.0,
                      ),
                      if (cartState.promoCode != null)
                        _buildPromocodeDiscount(context, cartState),
                      if (cartState.promoCode != null)
                        SizedBox(
                          height: 10,
                        ),
                      _buildDeliveryCharge(),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                  Divider(),
                  _buildDiscount(context, cartState),
                  SizedBox(height: 20),
                  _buildFinalTotal(cartState),
                ],
              ),
            ),
          );
        }
        return SizedBox();
      },
    );
  }

  Row _buildPromocodeDiscount(BuildContext context, CartSuccess cartState) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          tr('promocode'),
          style: Theme.of(context)
              .primaryTextTheme
              .subtitle2
              .copyWith(color: Colors.black),
        ),
        Text(
          ' - ${CurrencyWidget.getFixedPriceCurrencyString(double.parse(
            cartState.promoCodeDiscount,
          ))}',
          style: Theme.of(context)
              .primaryTextTheme
              .subtitle2
              .copyWith(color: Colors.black),
        )
      ],
    );
  }

  Row _buildSubTotal(BuildContext context, CartSuccess cartState) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          tr('subtotal'),
          style: Theme.of(context)
              .primaryTextTheme
              .subtitle1
              .copyWith(color: Colors.black),
        ),
        Text(
          ' + ${CurrencyWidget.getFixedPriceCurrencyString(cartState.discountedTotalPrice)}',
          style: Theme.of(context)
              .primaryTextTheme
              .subtitle2
              .copyWith(color: Colors.black),
        ),
      ],
    );
  }

  Widget _buildDiscount(BuildContext context, CartSuccess cartState) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          tr('youSaved', args: [
            CurrencyWidget.getFixedPriceCurrencyString(cartState.savedPrice)
          ]),
          style: Theme.of(context)
              .primaryTextTheme
              .subtitle1
              .copyWith(color: Colors.black, fontSize: 15),
        ),
      ],
    );
  }

  BlocBuilder<UseraddressBloc, UseraddressState> _buildFinalTotal(
      CartSuccess cartState) {
    return BlocBuilder<UseraddressBloc, UseraddressState>(
      builder: (context, userAddressState) {
        if (userAddressState is UseraddressSuccess) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text.rich(
                TextSpan(
                  text: tr('total') + "\n",
                  children: [
                    cartState.useWallet
                        ? TextSpan(
                            text: cartState.fullWalletPay
                                ? '${CurrencyWidget.getFixedPriceCurrencyString(0.00)} \n'
                                : '${CurrencyWidget.getFixedPriceCurrencyString(double.parse(cartState.priceAfterWalletBalanceAdded.toString()))} ',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                          )
                        : TextSpan(
                            text:
                                '${CurrencyWidget.getFixedPriceCurrencyString((double.parse(userAddressState.selectedAddress.deliveryCharges) + cartState.discountedTotalPrice))} \n',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                          ),
                  ],
                ),
              ),
              SizedBox(
                  width: 190,
                  child: SizedBox(
                    width: double.infinity,
                    height: 56,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        primary: Palette.checkOutButtonBg,
                      ),
                      onPressed: () {
                        if (cartState.isDeliverable) {
                          if (this.onPressed != null) {
                            this.onPressed();
                          } else {
                            Navigator.pushNamed(
                                context, "ordertimepaymentselect");
                          }
                        } else {
                          UIUtilWidget.clearAllSnackBars();
                          UIUtilWidget.showSnackBarWidget(
                              Text(tr('notDeliverableToThisAddress')),
                              seconds: 1);
                        }
                      },
                      child: Text(
                        tr('checkout'),
                        style: TextStyle(
                            fontSize: (18), color: Palette.checkOutButtonText),
                      ),
                    ),
                  )),
            ],
          );
        }
        return SizedBox();
      },
    );
  }

  BlocBuilder<UseraddressBloc, UseraddressState> _buildDeliveryCharge() {
    return BlocBuilder<UseraddressBloc, UseraddressState>(
      builder: (context, userAddressState) {
        if (userAddressState is UseraddressSuccess) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Delivery charge",
                style: Theme.of(context)
                    .primaryTextTheme
                    .subtitle1
                    .copyWith(color: Colors.black),
              ),
              Text(
                ' + ${CurrencyWidget.getFixedPriceCurrencyString(double.parse(userAddressState.selectedAddress.deliveryCharges))}',
                style: Theme.of(context)
                    .primaryTextTheme
                    .subtitle2
                    .copyWith(color: Colors.black),
              ),
            ],
          );
        }
        return SizedBox();
      },
    );
  }

  Container _buildNormal() {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 15,
        horizontal: 30,
      ),
      // height: 174,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -15),
            blurRadius: 20,
            color: Color(0xFFDADADA).withOpacity(0.15),
          )
        ],
      ),
      child: SafeArea(
        child: BlocBuilder<CartBloc, CartState>(
          builder: (context, cartState) {
            if (cartState is CartSuccess) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (cartState.pincode.pincode == "All")
                    InkWell(
                      onTap: () {
                        Scaffold.of(context).showBottomSheet(
                          (context) => PincodeList(),
                        );
                      },
                      child: Text(
                        "Please select a pincode",
                        style: Theme.of(context)
                            .textTheme
                            .subtitle2
                            .copyWith(color: Colors.red),
                      ),
                    ),
                  if (!cartState.isDeliverable &&
                      cartState.pincode.pincode != "All")
                    InkWell(
                      onTap: () {
                        Scaffold.of(context).showBottomSheet(
                          (context) => PincodeList(),
                        );
                      },
                      child: Text(
                        "Some Products Not Deliverable in your pincode",
                        style: Theme.of(context)
                            .textTheme
                            .subtitle2
                            .copyWith(color: Colors.red),
                      ),
                    ),
                  SizedBox(height: 20),
                  if (this.cartFooterType == CartFooterType.PAYMENT)
                    _buildFinalTotal(cartState),
                  if (this.cartFooterType == CartFooterType.NORMAL)
                    _buildCartTotal(cartState, context),
                ],
              );
            }
            return SizedBox();
          },
        ),
      ),
    );
  }

  Row _buildCartTotal(CartSuccess cartState, BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text.rich(
          TextSpan(
            text: "Total:\n",
            children: [
              TextSpan(
                text:
                    '${CurrencyWidget.getFixedPriceCurrencyString(cartState.totalPrice)}',
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ],
          ),
        ),
        SizedBox(
            width: 190,
            child: SizedBox(
              width: double.infinity,
              height: 56,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  primary: Palette.checkOutButtonBg,
                ),
                onPressed: () {
                  this.onPressed(cartState);
                },
                child: Text(
                  "Checkout",
                  style: TextStyle(
                      fontSize: (18), color: Palette.checkOutButtonText),
                ),
              ),
            )),
      ],
    );
  }
}
