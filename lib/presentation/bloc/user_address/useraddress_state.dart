part of 'useraddress_bloc.dart';

@immutable
abstract class UseraddressState {}

class UseraddressInitial extends UseraddressState {}

class UseraddressLoading extends UseraddressState {
  UseraddressLoading();
}

class UseraddressSuccess extends UseraddressState {
  final UserAddressResponse userAddressResponse;
  final Address selectedAddress;
  final UseraddressEvent lastEvent;
  UseraddressSuccess(
      {this.userAddressResponse, this.selectedAddress, this.lastEvent});
}

class UseraddressSaveSuccess extends UseraddressState {
  UseraddressSaveSuccess();
}

class UseraddressUpdateSuccess extends UseraddressState {
  UseraddressUpdateSuccess();
}

class UseraddressFailed extends UseraddressState {
  final String message;
  UseraddressFailed({this.message});
}
