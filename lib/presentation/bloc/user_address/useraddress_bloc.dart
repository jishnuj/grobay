import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/model/address.dart';
import 'package:grobay/api/response/user_address_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'useraddress_event.dart';
part 'useraddress_state.dart';

class UseraddressBloc extends Bloc<UseraddressEvent, UseraddressState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  UseraddressBloc() : super(UseraddressInitial()) {
    on<UseraddressEvent>((event, emit) async {
      try {
        if (event is FetchUserAddressEvent) {
          emit(UseraddressLoading());
          UserAddressResponse userAddressResponse =
              await this.apiBridge.fetchAddress();
          Address selectedAddress = userAddressResponse.data
              .firstWhere((element) => element.isDefault == "1", orElse: () {
            return null;
          });
          emit(UseraddressSuccess(
              lastEvent: event,
              userAddressResponse: userAddressResponse,
              selectedAddress: selectedAddress ?? userAddressResponse.data[0]));
        } else if (event is SelectUserAddressEvent) {
          UseraddressSuccess previous = this.state;
          emit(UseraddressSuccess(
              lastEvent: event,
              userAddressResponse: previous.userAddressResponse,
              selectedAddress: event.address));
        } else if (event is DeleteUserAddressEvent) {
          UseraddressSuccess previous = this.state;
          emit(UseraddressLoading());
          await this.apiBridge.deleteAddress(event.addressId);
          List<Address> addresses = previous.userAddressResponse.data
              .where((element) => element.id != event.addressId)
              .toList();
          if (addresses.length != 0) {
            Address selectedAddress = addresses
                .firstWhere((element) => element.isDefault == "1", orElse: () {
              return null;
            });
            previous.userAddressResponse.data = addresses;
            emit(UseraddressSuccess(
                lastEvent: event,
                userAddressResponse: previous.userAddressResponse,
                selectedAddress: selectedAddress ?? addresses[0]));
          } else {
            emit(UseraddressFailed(message: "No address Saved"));
          }
        } else if (event is SaveAddressEvent) {
          emit(UseraddressLoading());
          await this.apiBridge.saveAddress(
              id: event.id,
              isUpdate: event.isUpdate,
              isDefault: event.isDefault,
              address: event.address,
              name: event.name,
              mobile: event.mobile,
              alternateMobile: event.alternateMobile,
              pincodeId: event.pincodeId,
              latitude: event.latitude,
              longitude: event.longitude,
              areaId: event.areaId,
              cityId: event.cityId,
              type: event.type,
              state: event.state,
              country: event.country,
              landmark: event.landmark);
          emit(UseraddressLoading());
          UserAddressResponse userAddressResponse =
              await this.apiBridge.fetchAddress();
          Address selectedAddress = userAddressResponse.data
              .firstWhere((element) => element.isDefault == "1", orElse: () {
            return null;
          });
          emit(UseraddressSuccess(
              lastEvent: event,
              userAddressResponse: userAddressResponse,
              selectedAddress: selectedAddress ?? userAddressResponse.data[0]));
        }
      } catch (e) {
        emit(UseraddressFailed(message: e.toString()));
      }
    });
  }
}
