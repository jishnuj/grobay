part of 'useraddress_bloc.dart';

@immutable
abstract class UseraddressEvent {}

class FetchUserAddressEvent extends UseraddressEvent {
  final int limit;
  final int offset;
  FetchUserAddressEvent({this.offset, this.limit});
}

class DeleteUserAddressEvent extends UseraddressEvent {
  final String addressId;
  DeleteUserAddressEvent({this.addressId});
}

class SelectUserAddressEvent extends UseraddressEvent {
  final Address address;
  SelectUserAddressEvent({this.address});
}

class SaveAddressEvent extends UseraddressEvent {
  final String address;
  final String id;
  final String userId;
  final String name;
  final String mobile;
  final String alternateMobile;
  final String pincodeId;
  final String type;
  final String state;
  final String country;
  final String areaId;
  final String cityId;
  final String landmark;
  final String isDefault;
  final bool isUpdate;
  final double latitude;
  final double longitude;
  SaveAddressEvent(
      {this.address,
      this.id,
      this.isDefault,
      this.isUpdate,
      this.userId,
      this.name,
      this.mobile,
      this.alternateMobile,
      this.pincodeId,
      this.type,
      this.state,
      this.country,
      this.areaId,
      this.cityId,
      this.latitude,
      this.longitude,
      this.landmark});
}
