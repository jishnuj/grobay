part of 'forgot_password_bloc.dart';

@immutable
abstract class ForgotPasswordEvent {}

class ForgotFetchOTPEvent extends ForgotPasswordEvent {
  final String mobileNo;
  final String countryCode;
  ForgotFetchOTPEvent({this.mobileNo, this.countryCode});
}

class ForgotFetchVerifyOTPEvent extends ForgotPasswordEvent {
  final String mobileNo;
  final String smsCode;
  ForgotFetchVerifyOTPEvent({
    this.mobileNo,
    this.smsCode,
  });
}

class ForgotResetPasswordEvent extends ForgotPasswordEvent {
  final String mobileNo;
  final String password;
  ForgotResetPasswordEvent({this.mobileNo, this.password});
}

class ResetUserForgotPasswordEvent extends ForgotPasswordEvent {}
