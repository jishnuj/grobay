import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';

part 'forgot_password_event.dart';
part 'forgot_password_state.dart';

class ForgotPasswordBloc
    extends Bloc<ForgotPasswordEvent, ForgotPasswordState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  FirebaseAuth auth = FirebaseAuth.instance;
  String verificationId;
  ForgotPasswordBloc() : super(ForgotPasswordInitial()) {
    on<ForgotPasswordEvent>((event, emit) async {
      if (event is ResetUserForgotPasswordEvent) {
        emit(ForgotPasswordInitial());
      }
      if (event is ForgotFetchOTPEvent) {
        await _sendSMS(event, emit);
      } else if (event is ForgotFetchVerifyOTPEvent) {
        await _verifyOtp(event, emit);
      } else if (event is ForgotResetPasswordEvent) {
        try {
          emit(UserResetPasswordLoading());
          String messsage = await this
              .apiBridge
              .forgotPassword(event.password, event.mobileNo);
          emit(UserResetPasswordSuccess());
        } catch (e) {
          emit(UserResetPasswordFailed(msg: e.toString()));
        }
      }
    });
  }

  _sendSMS(ForgotFetchOTPEvent event, emit) async {
    try {
      emit(ForgotUserCheckNumberLoading());
      Map<String, dynamic> verified =
          await this.apiBridge.verifyUser(event.mobileNo);
      if (!verified['status']) {
        await auth.verifyPhoneNumber(
          phoneNumber: "+" + event.countryCode + event.mobileNo,
          verificationCompleted:
              (PhoneAuthCredential phoneAuthCredential) async {
            await _signinFirebase(phoneAuthCredential, emit);
          },
          verificationFailed: (FirebaseAuthException e) async {
            Logger().e(e.toString());
            emit(ForgotUserCheckNumberFailed(msg: e.message));
          },
          codeSent: (String verificationId, int resendToken) async {
            Logger().i("OTP SEND");
            this.verificationId = verificationId;
          },
          codeAutoRetrievalTimeout: (String verificationId) {},
        );
        emit(ForgotUserCheckNumberSuccess());
      } else {
        emit(ForgotUserCheckNumberFailed(msg: verified['msg']));
      }
    } catch (e) {
      emit(ForgotUserCheckNumberFailed(msg: e.toString()));
    }
  }

  _verifyOtp(ForgotFetchVerifyOTPEvent event, emit) async {
    try {
      PhoneAuthCredential credential = PhoneAuthProvider.credential(
          verificationId: this.verificationId, smsCode: event.smsCode);
      await _signinFirebase(credential, emit);
    } catch (e) {
      emit(ForgotUserOTPVerifyFailed(msg: e.toString()));
    }
  }

  _signinFirebase(PhoneAuthCredential phoneAuthCredential, emit) async {
    emit(ForgotUserOTPVerifyLoading());
    UserCredential userCredential =
        await auth.signInWithCredential(phoneAuthCredential);
    print(userCredential.toString());
    emit(ForgotUserOTPVerifySuccess());
  }
}
