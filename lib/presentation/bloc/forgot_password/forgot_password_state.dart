part of 'forgot_password_bloc.dart';

@immutable
abstract class ForgotPasswordState {}

class ForgotPasswordInitial extends ForgotPasswordState {}

class ForgotUserCheckNumberLoading extends ForgotPasswordState {}

class ForgotUserCheckNumberSuccess extends ForgotPasswordState {
  ForgotUserCheckNumberSuccess();
}

class ForgotUserCheckNumberFailed extends ForgotPasswordState {
  final String msg;
  ForgotUserCheckNumberFailed({this.msg});
  @override
  String toString() {
    return msg;
  }
}

class ForgotUserOTPVerifyLoading extends ForgotPasswordState {}

class ForgotUserOTPVerifyFailed extends ForgotPasswordState {
  final String msg;
  ForgotUserOTPVerifyFailed({this.msg});
  @override
  String toString() {
    return msg;
  }
}

class ForgotUserOTPVerifySuccess extends ForgotPasswordState {}

class UserResetPasswordLoading extends ForgotPasswordState {}

class UserResetPasswordSuccess extends ForgotPasswordState {}

class UserResetPasswordFailed extends ForgotPasswordState {
  final String msg;
  UserResetPasswordFailed({this.msg});
  @override
  String toString() {
    return msg;
  }
}
