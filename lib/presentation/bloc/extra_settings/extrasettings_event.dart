part of 'extrasettings_bloc.dart';

@immutable
abstract class ExtrasettingsEvent {}

class FetchAboutEvent extends ExtrasettingsEvent {}

class FetchContactEvent extends ExtrasettingsEvent {}

class FetchTermsEvent extends ExtrasettingsEvent {}

class FetchPrivacyEvent extends ExtrasettingsEvent {}

class FetchFAQEvent extends ExtrasettingsEvent {}
