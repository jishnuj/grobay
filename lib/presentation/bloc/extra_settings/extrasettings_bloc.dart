import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/extra_setting_response.dart';
import 'package:grobay/api/response/faq_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'extrasettings_event.dart';
part 'extrasettings_state.dart';

class ExtrasettingsBloc extends Bloc<ExtrasettingsEvent, ExtrasettingsState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  ExtrasettingsBloc() : super(ExtrasettingsInitial()) {
    on<ExtrasettingsEvent>((event, emit) async {
      emit(ExtrasettingsLoading());
      try {
        if (event is FetchContactEvent) {
          ExtraSettingResponse extraSettingResponse =
              await this.apiBridge.fetchContact();
          emit(ExtrasettingsSuccess(extraSettingResponse: extraSettingResponse));
        } else if (event is FetchAboutEvent) {
          ExtraSettingResponse extraSettingResponse =
              await this.apiBridge.fetchAbout();
          emit(ExtrasettingsSuccess(extraSettingResponse: extraSettingResponse));
        } else if (event is FetchPrivacyEvent) {
          ExtraSettingResponse extraSettingResponse =
              await this.apiBridge.fetchPrivacy();
          emit(ExtrasettingsSuccess(extraSettingResponse: extraSettingResponse));
        } else if (event is FetchTermsEvent) {
          ExtraSettingResponse extraSettingResponse =
              await this.apiBridge.fetchTerms();
          emit(ExtrasettingsSuccess(extraSettingResponse: extraSettingResponse));
        } else if (event is FetchFAQEvent) {
          FaqResponse faqResponse =
              await this.apiBridge.fetchFaq();
          emit(ExtrasettingsFaqSuccess(faqResponse: faqResponse));
        }
      } catch (e) {
        emit(ExtrasettingsFailed(message: e.toString()));
      }
    });
  }
}
