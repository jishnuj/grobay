part of 'extrasettings_bloc.dart';

@immutable
abstract class ExtrasettingsState {}

class ExtrasettingsInitial extends ExtrasettingsState {}

class ExtrasettingsLoading extends ExtrasettingsState {}

class ExtrasettingsSuccess extends ExtrasettingsState {
  final ExtraSettingResponse extraSettingResponse;

  ExtrasettingsSuccess({this.extraSettingResponse});
}

class ExtrasettingsFaqSuccess extends ExtrasettingsState {
  final FaqResponse faqResponse;
  ExtrasettingsFaqSuccess({this.faqResponse});
}

class ExtrasettingsFailed extends ExtrasettingsState {
  final String message;
  ExtrasettingsFailed({this.message});
}
