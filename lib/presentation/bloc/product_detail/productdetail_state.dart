part of 'productdetail_bloc.dart';

@immutable
abstract class ProductdetailState {}

class ProductdetailInitial extends ProductdetailState {}

class ProductdetailLoading extends ProductdetailState {}

class ProductdetailSuccess extends ProductdetailState {
  final Product product;
  final Variants selectedvariant;
  ProductdetailSuccess({this.product, this.selectedvariant});
}

class ProductdetailFailed extends ProductdetailState {
  final String message;
  ProductdetailFailed({this.message});
}
