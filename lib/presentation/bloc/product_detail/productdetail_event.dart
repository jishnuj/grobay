part of 'productdetail_bloc.dart';

@immutable
abstract class ProductdetailEvent {}

class FetchProductDetailEvent extends ProductdetailEvent {
  final String productId;
  FetchProductDetailEvent({this.productId});
}

class SelectVariantEvent extends ProductdetailEvent {
  final Variants selectedvariant;
  SelectVariantEvent({this.selectedvariant});
}