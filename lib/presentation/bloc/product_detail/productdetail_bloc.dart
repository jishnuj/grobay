import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/model/product.dart';
import 'package:grobay/api/model/variant.dart';
import 'package:grobay/api/response/product_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'productdetail_event.dart';
part 'productdetail_state.dart';

class ProductdetailBloc extends Bloc<ProductdetailEvent, ProductdetailState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  ProductdetailBloc() : super(ProductdetailInitial()) {
    on<ProductdetailEvent>((event, emit) async {
    // emit(ProductdetailFailed(message: "Tsttestestest".toString()));
      try {
        if (event is FetchProductDetailEvent) {
          emit(ProductdetailLoading());
          ProductResponse productResponse = await this
              .apiBridge
              .getProducts( productId: event.productId);
          emit(ProductdetailSuccess(
              product: productResponse.products[0],
              selectedvariant: productResponse.products[0].variants[0]));
        } else if (event is SelectVariantEvent) {
          ProductdetailSuccess previous = this.state;
          emit(ProductdetailSuccess(
              product: previous.product,
              selectedvariant: event.selectedvariant));
        }
      }  catch (e) {
        emit(ProductdetailFailed(message: e.toString()));
      }
    });
  }
}
