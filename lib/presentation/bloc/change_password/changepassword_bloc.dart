import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'changepassword_event.dart';
part 'changepassword_state.dart';

class ChangepasswordBloc
    extends Bloc<ChangepasswordEvent, ChangepasswordState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  ChangepasswordBloc() : super(ChangepasswordInitial()) {
    on<ChangepasswordEvent>((event, emit) async {
      if (event is UpdatePasswordEvent) {
        try {
          emit(ChangepasswordLoading());
          String response =
              await this.apiBridge.updatePassword(password: event.password);
          emit(ChangepasswordSuccess(message: response));
        } catch (e) {
          emit(ChangepasswordFailed(message: e.toString()));
        }
      }
    });
  }
}
