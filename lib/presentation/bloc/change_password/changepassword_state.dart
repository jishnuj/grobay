part of 'changepassword_bloc.dart';

@immutable
abstract class ChangepasswordState {}

class ChangepasswordInitial extends ChangepasswordState {}

class ChangepasswordLoading extends ChangepasswordState {}

class ChangepasswordSuccess extends ChangepasswordState {
  final String message;
  ChangepasswordSuccess({this.message});
}

class ChangepasswordFailed extends ChangepasswordState {
  final String message;
  ChangepasswordFailed({this.message});
}
