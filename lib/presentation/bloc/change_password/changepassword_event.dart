part of 'changepassword_bloc.dart';

@immutable
abstract class ChangepasswordEvent {}

class UpdatePasswordEvent extends ChangepasswordEvent {
  final String password;
  UpdatePasswordEvent({this.password});
}
