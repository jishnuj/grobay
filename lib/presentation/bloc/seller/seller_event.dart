part of 'seller_bloc.dart';

@immutable
abstract class SellerEvent {}


class FetchSeller extends SellerEvent {
  String sellerId;
  FetchSeller({this.sellerId});
}

class SetSellerEvent extends SellerEvent {
  Seller seller;
  SetSellerEvent({this.seller});
}