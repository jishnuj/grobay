import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/model/seller.dart';
import 'package:grobay/api/response/seller_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'seller_event.dart';
part 'seller_state.dart';

class SellerBloc extends Bloc<SellerEvent, SellerState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  SellerBloc() : super(SellerInitial()) {
    on<SellerEvent>((event, emit) async {
      try {
        if (event is FetchSeller) {
          emit(SellerLoading());
          SellerResponse sellerResponse =
              await this.apiBridge.getSeller(sellerId: event.sellerId);
          emit(SellerSuccess(sellerResposne: sellerResponse));
        } else if (event is SetSellerEvent) {
          emit(SellerLoading());
          emit(SellerSuccess(
              sellerResposne: SellerResponse(sellers: [event.seller])));
        }
      } catch (e) {
        emit(SellerFailed(message: e.toString()));
      }
    });
  }
}
