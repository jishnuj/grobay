part of 'seller_bloc.dart';

@immutable
abstract class SellerState {}

class SellerInitial extends SellerState {}

class SellerLoading extends SellerState {}

class SellerSuccess extends SellerState {
  final SellerResponse sellerResposne;
  String sellerId;
  SellerSuccess({this.sellerResposne, this.sellerId});
}

class SellerFailed extends SellerState {
  final String message;
  SellerFailed({this.message});
}
