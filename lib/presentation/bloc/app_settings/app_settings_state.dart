part of 'app_settings_bloc.dart';

@immutable
abstract class AppSettingsState {}

class AppSettingsInitial extends AppSettingsState {}

class AppSettingsLoading extends AppSettingsState {}

class AppSettingsSuccess extends AppSettingsState {
  final AppSettingsResponse appSettingsResponse;
  AppSettingsSuccess({this.appSettingsResponse});
}


class AppSettingsFailed extends AppSettingsState {
  final String message;
  AppSettingsFailed({this.message});
}
