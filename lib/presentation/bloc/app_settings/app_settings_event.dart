part of 'app_settings_bloc.dart';

@immutable
abstract class AppSettingsEvent {}

class FetchAppSettings extends AppSettingsEvent {}
