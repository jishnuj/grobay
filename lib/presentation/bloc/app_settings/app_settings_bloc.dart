import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/app_settings_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/platform/location_service.dart';
import 'package:meta/meta.dart';

part 'app_settings_event.dart';
part 'app_settings_state.dart';

class AppSettingsBloc extends Bloc<AppSettingsEvent, AppSettingsState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  LocationService locationService = GetIt.I.get<LocationService>();
  AppSettingsBloc() : super(AppSettingsInitial()) {
    on<AppSettingsEvent>((event, emit) async {
      if (event is FetchAppSettings) {
        try {
          emit(AppSettingsLoading());
          AppSettingsResponse appSettingsResponse =
              await this.apiBridge.getAppSettings();
          AppConfig.CURRENCY = appSettingsResponse.settings.currency;
          AppConfig.MAXIMUM_ITEMS_ALLOWED_IN_CART =
              int.parse(appSettingsResponse.settings.maxCartItemsCount);
          AppConfig.COUNTRY_CODE = await this.locationService.getCountryCode();
          AppConfig.MAXIMUM_WALLET_REFILL_AMOUNT =
              double.parse(appSettingsResponse.settings.userWalletRefillLimit);
          AppConfig.MINIMUM_ORDER_AMOUNT =
              double.parse(appSettingsResponse.settings.minOrderAmount);
          AppConfig.MINIMUM_REFER_AND_ORDER_AMOUNT = double.parse(
              appSettingsResponse.settings.minReferEarnOrderAmount);
          AppConfig.MAXIMUM_REFER_AND_EARN_AMOUNT =
              double.parse(appSettingsResponse.settings.maxReferEarnAmount);

          emit(AppSettingsSuccess(appSettingsResponse: appSettingsResponse));
        } catch (e) {
          emit(AppSettingsFailed(message: e.toString()));
        }
      }
    });
  }
}
