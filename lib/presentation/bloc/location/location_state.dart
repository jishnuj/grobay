part of 'location_bloc.dart';

@immutable
abstract class LocationState {}

class LocationInitial extends LocationState {}

class LocationLoading extends LocationState {}

class LocationSuccess extends LocationState {
  final LocationResponse locationResposne;
  final Location selectedLocation;
  LocationSuccess({this.locationResposne,this.selectedLocation});
}

class LocationFailed extends LocationState {
  final String message;
  LocationFailed({this.message});
}
