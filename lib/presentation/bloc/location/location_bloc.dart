import 'package:get_it/get_it.dart';
import 'package:grobay/api/model/location.dart';
import 'package:grobay/api/response/location_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:grobay/core/constant/storage_keys.dart';
import 'package:grobay/core/platform/local_storage_service.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:meta/meta.dart';

part 'location_event.dart';
part 'location_state.dart';

class LocationBloc extends HydratedBloc<LocationEvent, LocationState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  LocalStorageService localStorageService = GetIt.I.get<LocalStorageService>();
  LocationSuccess originalStateBeforeSearch;
  LocationBloc() : super(LocationInitial()) {
    on<LocationEvent>((event, emit) async {
      try {
        if (event is FetchLocationEvent) {
          await _fetchLocation(emit);
        } else if (event is SelectLocationEvent) {
          _selectLocation(emit, event);
        } else if (event is ResetLocationSearchEvent) {
          _resetLocationSearch(emit);
        } else if (event is SearchLocationEvent) {
          _searchLocation(event, emit);
        }
      } catch (e) {
        emit(LocationFailed(message: e.toString()));
      }
    });
  }

  void _searchLocation(SearchLocationEvent event, Emitter<LocationState> emit) {
    LocationSuccess previous = this.state;
    if (this.originalStateBeforeSearch == null) {
      originalStateBeforeSearch = previous;
    }
    RegExp exp = new RegExp(
      RegExp.escape(event.searchTerm),
      caseSensitive: false,
    );
    List<Location> locations = previous.locationResposne.locations
        .where((element) => exp.hasMatch(element.pincode))
        .toList();

    emit(LocationSuccess(
        locationResposne: LocationResponse(locations: locations),
        selectedLocation: previous.selectedLocation));
  }

  void _resetLocationSearch(Emitter<LocationState> emit) {
    LocationSuccess previous = originalStateBeforeSearch;
    emit(previous);
    originalStateBeforeSearch = null;
  }

  void _selectLocation(Emitter<LocationState> emit, SelectLocationEvent event) {
    LocationSuccess previous = this.state;
    emit(LocationLoading());
    localStorageService.setItem(
        StorageKeys.Pincode.toString(), event.location.id);
    emit(LocationSuccess(
        locationResposne: previous.locationResposne,
        selectedLocation: event.location));
  }

  Future<void> _fetchLocation(Emitter<LocationState> emit) async {
    LocationState locationState = this.state;
    emit(LocationLoading());
    LocationResponse locationResponse = await this.apiBridge.getLocations();
    locationResponse.locations.insert(0, Location(id: "", pincode: "All"));

    emit(LocationSuccess(
        locationResposne: locationResponse,
        selectedLocation: locationState is LocationSuccess
            ? locationState.selectedLocation
            : locationResponse.locations[0]));
  }

  @override
  LocationState fromJson(Map<String, dynamic> json) {
    if (json['locationResponse'] != null) {
      LocationSuccess locationSuccess = LocationSuccess(
          locationResposne: LocationResponse.fromJson(json['locationResponse']),
          selectedLocation: Location.fromJson(json['selectedLocation']));
      localStorageService.setItem(
          StorageKeys.Pincode.toString(), locationSuccess.selectedLocation.id);
      return locationSuccess;
    }
    return LocationInitial();
  }

  @override
  Map<String, dynamic> toJson(LocationState state) {
    if (state is LocationSuccess) {
      return {
        "locationResponse": state.locationResposne,
        "selectedLocation": state.selectedLocation.toJson(),
      };
    }
    return {};
  }
}
