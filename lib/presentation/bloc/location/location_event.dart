part of 'location_bloc.dart';

@immutable
abstract class LocationEvent {}

class FetchLocationEvent extends LocationEvent {
  final int getPincodes;
  FetchLocationEvent({this.getPincodes});
}

class SelectLocationEvent extends LocationEvent {
  final Location location;
  SelectLocationEvent({this.location});
}

class SearchLocationEvent extends LocationEvent {
  final String searchTerm;
  SearchLocationEvent({this.searchTerm});
}

class ResetLocationSearchEvent extends LocationEvent {}
