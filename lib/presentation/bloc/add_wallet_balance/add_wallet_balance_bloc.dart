import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/add_wallet_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'add_wallet_balance_event.dart';
part 'add_wallet_balance_state.dart';

class AddWalletBalanceBloc
    extends Bloc<AddWalletBalanceEvent, AddWalletBalanceState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  AddWalletBalanceBloc() : super(AddWalletBalanceInitial()) {
    on<AddWalletBalanceEvent>((event, emit) async {
      try {
        if (event is FetchAddWalletbalanceEvent) {
          emit(AddWalletBalanceLoading());
          AddWalletResponse addWalletResponse = await this
              .apiBridge
              .createWalletTransaction(
                  amount: event.amount, message: event.message);
          emit(AddWalletBalanceSuccess(addWalletResponse: addWalletResponse));
        }
        else if(event is ResetStateEvent){
          emit(AddWalletBalanceInitial());
        }
      } catch (e) {
        emit(AddWalletBalanceFailed(message: e.toString()));
      }
    });
  }
}
