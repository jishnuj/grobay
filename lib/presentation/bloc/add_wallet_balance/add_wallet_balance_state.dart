part of 'add_wallet_balance_bloc.dart';

@immutable
abstract class AddWalletBalanceState {}

class AddWalletBalanceInitial extends AddWalletBalanceState {}

class AddWalletBalanceLoading extends AddWalletBalanceState {}

class AddWalletBalanceSuccess extends AddWalletBalanceState {
  final AddWalletResponse addWalletResponse;
  AddWalletBalanceSuccess({this.addWalletResponse});
}

class AddWalletBalanceFailed extends AddWalletBalanceState {
  final String message;
  AddWalletBalanceFailed({this.message});
}
