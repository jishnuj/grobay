part of 'add_wallet_balance_bloc.dart';

@immutable
abstract class AddWalletBalanceEvent {}

class FetchAddWalletbalanceEvent extends AddWalletBalanceEvent {
  final String amount;
  final String message;
  FetchAddWalletbalanceEvent({this.amount, this.message});
}


class ResetStateEvent extends AddWalletBalanceEvent {}