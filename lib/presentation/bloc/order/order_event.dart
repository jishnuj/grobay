part of 'order_bloc.dart';

@immutable
abstract class OrderEvent {}

class CreateOrderEvent extends OrderEvent {
  final String email;

  final String taxId;
  final String taxPercentage;

  final String quantity;
  final String addressId;
  final String walletBalanceUsed;
  final String deliveryTime;
  final String orderNote;
  final String total;
  final String deliveryCharge;
  final String finalTotal;
  final String placeOrder;
  final String walletused;
  final String productVariantId;
  final String paymentMethod;
  final String promoCode;
  final String promoCodeDiscount;
  CreateOrderEvent({
    @required this.quantity,
    this.taxPercentage,
    this.email,
    this.taxId,
    @required this.addressId,
    @required this.walletBalanceUsed,
    @required this.deliveryCharge,
    @required this.deliveryTime,
    @required this.orderNote,
    @required this.total,
    @required this.finalTotal,
    @required this.placeOrder,
    @required this.walletused,
    @required this.paymentMethod,
    @required this.productVariantId,
    this.promoCode,
    this.promoCodeDiscount,
  });
}

class FetchOrderEvent extends OrderEvent {
  final String offset;
  final String limit;
  FetchOrderEvent({this.offset, this.limit});
}

class UpdateOrderEvent extends OrderEvent {
  final String orderItemId;
  final String orderId;
  final String status;
  UpdateOrderEvent({this.orderItemId, this.orderId, this.status});
}
