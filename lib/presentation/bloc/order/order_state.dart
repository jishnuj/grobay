part of 'order_bloc.dart';

@immutable
abstract class OrderState {}

class OrderInitial extends OrderState {}

class OrderCreateLoading extends OrderState {}

class OrderFetchLoading extends OrderState {}

class OrderUpdateLoading extends OrderState {}

class OrderCreateSuccess extends OrderState {
  final String taxid;
  final OrderSaveResponse orderSaveResponse;
  OrderCreateSuccess({this.orderSaveResponse, this.taxid});
}

class OrderSuccess extends OrderState {
  final bool isPageLoading;
  final bool isPageFailed;
  final OrderFetchResponse orderFetchResponse;
  OrderSuccess(
      {this.orderFetchResponse, this.isPageFailed=false, this.isPageLoading=false});
}

class OrderUpdateSuccess extends OrderSuccess {
  final OrderFetchResponse orderFetchResponse;
  final String msg;
  OrderUpdateSuccess({this.orderFetchResponse, this.msg});
}

class OrderCreateFailed extends OrderState {
  final String message;
  OrderCreateFailed({this.message});
}

class OrderFetchFailed extends OrderState {
  final String message;
  OrderFetchFailed({this.message});
}
