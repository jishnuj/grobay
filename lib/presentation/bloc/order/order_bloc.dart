import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/order_fetch_response.dart';
import 'package:grobay/api/response/order_save_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:meta/meta.dart';

part 'order_event.dart';
part 'order_state.dart';

class OrderBloc extends Bloc<OrderEvent, OrderState> {
  static int lastOffset = 0;
  static int total = 0;
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  OrderBloc() : super(OrderInitial()) {
    on<OrderEvent>((event, emit) async {
      if (event is CreateOrderEvent) {
        await _createOrder(emit, event);
      } else if (event is FetchOrderEvent) {
        await _fetchOrders(event, emit);
      } else if (event is UpdateOrderEvent) {
        await _updateOrder(emit, event);
      }
    });
  }

  Future<void> _updateOrder(
      Emitter<OrderState> emit, UpdateOrderEvent event) async {
    OrderSuccess previous = this.state;
    emit(OrderUpdateLoading());
    String msg = await this
        .apiBridge
        .updateOrderRequest(event.status, event.orderId, event.orderItemId);
    previous.orderFetchResponse.data.forEach((order) {
      if (order.id == event.orderId) {
        order.items.forEach((item) {
          if (item.id == event.orderItemId) {
            item.activeStatus = event.status;
          }
        });
      }
    });
    emit(OrderUpdateSuccess(
        msg: msg, orderFetchResponse: previous.orderFetchResponse));
  }

  Future<void> _fetchOrders(
      FetchOrderEvent event, Emitter<OrderState> emit) async {
    try {
      if (event.offset == null) {
        lastOffset = 0;
        total = 0;
        emit(OrderFetchLoading());
      } else {
        lastOffset = lastOffset + AppConfig.PAGE_LIMIT;
      }

      if (lastOffset == 0 || lastOffset < total) {
        if (this.state is OrderSuccess) {
          OrderSuccess previous = this.state;
          emit(OrderSuccess(
              orderFetchResponse: previous.orderFetchResponse,
              isPageLoading: true));
        }
        OrderFetchResponse orderFetchResponse = await this
            .apiBridge
            .fetchAllOrders(
                offset: ((total + lastOffset) - total).toString(),
                limit: event.limit);

        if (orderFetchResponse.data.length != 0) {
          if (this.state is OrderSuccess) {
            OrderSuccess previous = this.state;
            orderFetchResponse.data
                .forEach((e) => previous.orderFetchResponse.data.add(e));
            emit(OrderSuccess(orderFetchResponse: previous.orderFetchResponse));
          } else {
            emit(OrderSuccess(orderFetchResponse: orderFetchResponse));
          }
        }
        total = int.parse(orderFetchResponse.total);
      }
    } catch (e) {
      if (this.state is OrderSuccess) {
        OrderSuccess previous = this.state;
        lastOffset = lastOffset - AppConfig.PAGE_LIMIT;
        emit(OrderSuccess(
            orderFetchResponse: previous.orderFetchResponse,
            isPageFailed: true));
      } else {
        emit(OrderFetchFailed(message: e.toString()));
      }
    }
  }

  Future<void> _createOrder(
      Emitter<OrderState> emit, CreateOrderEvent event) async {
    try {
      emit(OrderCreateLoading());
      OrderSaveResponse orderSaveResponse = await this.apiBridge.createOrder(
          quantity: event.quantity,
          addressId: event.addressId,
          walletBalanceUsed: event.walletBalanceUsed,
          deliveryTime: event.deliveryTime,
          orderNote: event.orderNote,
          total: event.total,
          deliveryCharge: event.deliveryCharge,
          finalTotal: event.finalTotal,
          placeOrder: event.placeOrder,
          walletused: event.walletused,
          productVariantId: event.productVariantId,
          paymentMethod: event.paymentMethod,
          promoCode: event.promoCode,
          promoCodeDiscount: event.promoCodeDiscount);

      await this.apiBridge.removeAllFromCart();
      emit(OrderCreateSuccess(
          taxid: event.taxId, orderSaveResponse: orderSaveResponse));
    } catch (e) {
      emit(OrderCreateFailed(message: e.toString()));
    }
  }
}
