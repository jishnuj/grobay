part of 'keyword_bloc.dart';

@immutable
abstract class KeywordState {}

class KeywordInitial extends KeywordState {}

class KeywordLoading extends KeywordState {}

class KeywordSuccess extends KeywordState {
  final KeywordResponse keywordResposne;
  final List<String> filtered;
  KeywordSuccess({this.keywordResposne,this.filtered});
}

class KeywordFailed extends KeywordState {
  final String message;
  KeywordFailed({this.message});
}