part of 'keyword_bloc.dart';

@immutable
abstract class KeywordEvent {}

class FetchKeywordEvent extends KeywordEvent {}

class FilterKeywordEvent extends KeywordEvent {
  final String search;
  FilterKeywordEvent({this.search});
}

class ClearFilteredKeyword extends KeywordEvent{}