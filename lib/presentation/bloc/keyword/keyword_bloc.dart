import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/keyword_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'keyword_event.dart';
part 'keyword_state.dart';

class KeywordBloc extends Bloc<KeywordEvent, KeywordState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  KeywordBloc() : super(KeywordInitial()) {
    on<KeywordEvent>((event, emit) async {
      try {
        if (event is FetchKeywordEvent) {
          emit(KeywordLoading());
          KeywordResponse keywordResponse = await this.apiBridge.getKeywords();
          emit(KeywordSuccess(keywordResposne: keywordResponse, filtered: []));
        } else if (event is FilterKeywordEvent) {
          KeywordSuccess previous = this.state;
          emit(KeywordLoading());
          if (event.search.trim().isEmpty) {
            emit(KeywordSuccess(
                keywordResposne: previous.keywordResposne, filtered: []));
          } else {
            List<String> keyword = previous.keywordResposne.keywords
                .where((element) => element.contains(event.search))
                .toList();
            emit(KeywordSuccess(
                keywordResposne: previous.keywordResposne,
                filtered: keyword ?? []));
          }
        } else if (event is ClearFilteredKeyword) {
          KeywordSuccess previous = this.state;
          emit(KeywordSuccess(
              keywordResposne: previous.keywordResposne, filtered: []));
        }
      } catch (e) {
        emit(KeywordFailed(message: e.toString()));
      }
    });
  }
}
