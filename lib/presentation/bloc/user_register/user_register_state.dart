part of 'user_register_bloc.dart';

@immutable
abstract class UserRegisterState {}

class UserRegisterInitial extends UserRegisterState {}

class UserCheckNumberLoading extends UserRegisterState {}

class UserCheckNumberSuccess extends UserRegisterState {
  UserCheckNumberSuccess();
}

class UserCheckNumberFailed extends UserRegisterState {
  final String msg;
  UserCheckNumberFailed({this.msg});
  @override
  String toString() {
    return msg;
  }
}

class UserOTPVerifyLoading extends UserRegisterState {}

class UserOTPVerifyFailed extends UserRegisterState {
  final String msg;
  UserOTPVerifyFailed({this.msg});
  @override
  String toString() {
    return msg;
  }
}

class UserOTPVerifySuccess extends UserRegisterState {}

class UserCreateLoading extends UserRegisterState {}

class UserCreateSuccess extends UserRegisterState {}

class UserCreateFailed extends UserRegisterState {
  final String msg;
  UserCreateFailed({this.msg});
  @override
  String toString() {
    return msg;
  }
}
