part of 'user_register_bloc.dart';

@immutable
abstract class UserRegisterEvent {}

class FetchOTPEvent extends UserRegisterEvent {
  final String mobileNo;
  final String name;
  final String password;
  final String email;
  final int countryCode;
  final String friendsCode;
  FetchOTPEvent(
      {this.name,
      this.password,
      this.email,
      this.mobileNo,
      this.countryCode,
      this.friendsCode});
}

class FetchVerifyOTPEvent extends UserRegisterEvent {
  final String mobileNo;
  final String name;
  final String password;
  final String email;
  final String smsCode;
  final int countryCode;
  final String friendsCode;
  FetchVerifyOTPEvent(
      {this.name,
      this.password,
      this.email,
      this.mobileNo,
      this.smsCode,
      this.friendsCode,
      this.countryCode});
}

class ResetUserRegisterEvent extends UserRegisterEvent {}
