import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';

part 'user_register_event.dart';
part 'user_register_state.dart';

class UserRegisterBloc extends Bloc<UserRegisterEvent, UserRegisterState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  FirebaseAuth auth = FirebaseAuth.instance;
  String verificationId;
  UserRegisterBloc() : super(UserRegisterInitial()) {
    on<UserRegisterEvent>((event, emit) async {
      if (event is ResetUserRegisterEvent) {
        emit(UserRegisterInitial());
      } else if (event is FetchOTPEvent) {
        await _sendSMS(event, emit);
      } else if (event is FetchVerifyOTPEvent) {
        await _verifyOtp(event, emit);
      }
    });
  }

  _sendSMS(FetchOTPEvent event, emit) async {
    try {
      emit(UserCheckNumberLoading());
      Map<String, dynamic> verified =
          await this.apiBridge.verifyUser(event.mobileNo);
      //true means this number is not registered
      if (verified['status']) {
        await auth.verifyPhoneNumber(
          phoneNumber: "+" + event.countryCode.toString() + event.mobileNo,
          verificationCompleted:
              (PhoneAuthCredential phoneAuthCredential) async {
            await _signinFirebase(phoneAuthCredential, emit);
            await _createUser(emit,
                email: event.email,
                friendsCode: event.friendsCode,
                countryCode: event.countryCode,
                mobileNo: event.mobileNo,
                name: event.name,
                password: event.password);
          },
          verificationFailed: (FirebaseAuthException e) async {
            Logger().e(e.toString());
            emit(UserCheckNumberFailed(msg: e.message));
          },
          codeSent: (String verificationId, int resendToken) async {
            Logger().i("OTP SEND");
            this.verificationId = verificationId;
          },
          codeAutoRetrievalTimeout: (String verificationId) {},
        );
        emit(UserCheckNumberSuccess());
      } else {
        emit(UserCheckNumberFailed(msg: verified['msg']));
      }
    } catch (e) {
      emit(UserCheckNumberFailed(msg: e.toString()));
    }
  }

  _verifyOtp(FetchVerifyOTPEvent event, emit) async {
    try {
      PhoneAuthCredential credential = PhoneAuthProvider.credential(
          verificationId: this.verificationId, smsCode: event.smsCode);
      await _signinFirebase(credential, emit);
      await _createUser(emit,
          email: event.email,
          mobileNo: event.mobileNo,
          countryCode: event.countryCode,
          friendsCode: event.friendsCode,
          name: event.name,
          password: event.password);
    } catch (e) {
      emit(UserOTPVerifyFailed(msg: e.toString()));
    }
  }

  _signinFirebase(PhoneAuthCredential phoneAuthCredential, emit) async {
    emit(UserOTPVerifyLoading());
    UserCredential userCredential =
        await auth.signInWithCredential(phoneAuthCredential);
    print(userCredential.toString());
    emit(UserOTPVerifySuccess());
  }

  _createUser(emit,
      {String name,
      String mobileNo,
      String email,
      String password,
      String friendsCode,
      int countryCode}) async {
    try {
      emit(UserCreateLoading());
      await this.apiBridge.register(mobileNo, true,
          countryCode: countryCode,
          name: name,
          email: email,
          friendsCode: friendsCode,
          password: password);
      emit(UserCreateSuccess());
    } catch (e) {
      emit(UserCreateFailed(msg: e.toString()));
    }
  }
}
