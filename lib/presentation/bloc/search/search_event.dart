part of 'search_bloc.dart';

@immutable
abstract class SearchEvent {}

class SearchProductEvent extends SearchEvent {
  final String search;
  SearchProductEvent({this.search});
}

class ResetSearchEvent extends SearchEvent {}