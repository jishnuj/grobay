import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/product_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  SearchBloc() : super(SearchInitial()) {
    on<SearchEvent>((event, emit) async {
      try {
        if (event is SearchProductEvent) {
          emit(SearchLoading());
          ProductResponse productResponse = await this
              .apiBridge
              .getProducts( search: event.search);
          emit(SearchSuccess(searchResposne: productResponse));
        } else if(event is ResetSearchEvent){
          emit(SearchInitial());
        }
      } catch (e) {
        emit(SearchFailed(message: e.toString()));
      }
    });
  }
}
