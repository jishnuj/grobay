part of 'search_bloc.dart';

@immutable
abstract class SearchState {}

class SearchInitial extends SearchState {}

class SearchLoading extends SearchState{}

class SearchSuccess extends SearchState {
  final ProductResponse searchResposne;
  SearchSuccess({this.searchResposne});
}

class SearchFailed extends SearchState {
  final String message;
  SearchFailed({this.message});
}
