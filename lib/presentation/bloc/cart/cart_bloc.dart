import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/model/address.dart';
import 'package:grobay/api/model/location.dart';
import 'package:grobay/api/response/cart_response.dart';
import 'package:grobay/api/response/promocode_response.dart';
import 'package:grobay/api/response/varaint_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:meta/meta.dart';

part 'cart_event.dart';
part 'cart_state.dart';

enum STEPS { CART, ADDRESS, CHECKOUT, ORDER }

class CartBloc extends Bloc<CartEvent, CartState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();

  static STEPS steps = STEPS.CART;

  CartBloc() : super(CartInitial()) {
    on<CartEvent>((event, emit) async {
      try {
        if (event is ResetCartEvent) {
          emit(CartInitial());
        }

        //Loads The User Cart data From API
        else if (event is FetchUserCartData) {
          await _fetchUserCartData(emit, event);
        }

        // Fetch Cart Data Of Guest User
        else if (event is FetchCartVaraints) {
          if (event.cartCubit.state.variantIds.length == 0) {
            emit(CartInitial());
          } else {
            emit(CartLoading());
            VariantsResponse variantsResponse = await this
                .apiBridge
                .getCartVaraints(
                    variantIds: event.varaintIds,
                    pincodeId: event.pincode.id,
                    userId: event.userId);
            double total = 0;
            bool isDeliverable = true;
            bool isOutOfStock = false;
            variantsResponse.variantProducts.forEach((v) {
              if (v.stock == "0") {
                isOutOfStock = true;
              }
              List<String> availablePincodes =
                  v.item[0].pincodes.trim().split(",");
              if ((availablePincodes.length != 0 &&
                      availablePincodes[0] != "") &&
                  !(availablePincodes.contains(event.pincode.id))) {
                isDeliverable = false;
              }

              double productTotal = 0;
              int variantCount = event.cartCubit.getVariantsCount(v.id) ?? 0;
              if (variantCount != 0) {
                productTotal = double.parse(v.discountedPrice) * variantCount;
              }

              total += productTotal;
            });
            emit(CartSuccess(
              isDeliverable: isDeliverable,
              isOutOfStock: isOutOfStock,
              pincode: event.pincode,
              userId: event.userId,
              variantsResponse: variantsResponse,
              discountedTotalPrice: total,
              savedPrice: 0.0,
              totalPrice: total,
            ));
          }
        } else // Recalculate Cart Data Of Guest User and User
        if (event is RecalculateCartVarants) {
          if (this.state is CartSuccess) {
            CartSuccess previous = this.state;
            if (event.userId != null) {
              checkItemsUser(event, previous, emit);
            } else {
              checkItemsGuest(event, previous, emit);
            }
          }
        }
      } catch (e) {
        emit(CartFailed(message: e.toString()));
      }

      //Apply Promocode
      if (event is CartApplyPromoCode) {
        CartSuccess previous = this.state;
        emit(CartLoading());

        try {
          PromoCodeResponse promoCodeResponse = await this
              .apiBridge
              .validatePromoCode(
                  event.promoCode, previous.totalPrice.toString());
          emit(CartSuccess(
              promoCode: event.promoCode,
              promoCodeDiscount: promoCodeResponse.discount,
              pincode: previous.pincode,
              cartResponse: previous.cartResponse,
              totalPrice: previous.totalPrice,
              discountedTotalPrice:
                  double.parse(promoCodeResponse.discountedAmount),
              savedPrice: previous.savedPrice,
              isDeliverable: true,
              isOutOfStock: false));
        } catch (e) {
          emit(CartSuccess(
              promoCodeFailedMessage: e.toString(),
              promoCode: event.promoCode,
              pincode: previous.pincode,
              cartResponse: previous.cartResponse,
              totalPrice: previous.totalPrice,
              discountedTotalPrice: previous.totalPrice,
              savedPrice: previous.savedPrice,
              isDeliverable: true,
              isOutOfStock: false));
        }
      }
      //Remove Promocode
      else if (event is CartRemovePromoCode) {
        CartSuccess previous = this.state;
        emit(CartLoading());
        emit(CartSuccess(
            pincode: previous.pincode,
            cartResponse: previous.cartResponse,
            totalPrice: previous.totalPrice,
            discountedTotalPrice: previous.totalPrice,
            savedPrice: previous.savedPrice,
            isDeliverable: true,
            isOutOfStock: false));
      } else if (event is CartToggleWalletEvent) {
        CartSuccess previous = this.state;
        if (previous.useWallet) {
          emit(CartSuccess(
              useWallet: false,
              walletBalance: 0,
              fullWalletPay: false,
              priceAfterWalletBalanceAdded: 0,
              promoCodeFailedMessage: previous.promoCodeFailedMessage,
              promoCode: previous.promoCode,
              promoCodeDiscount: previous.promoCodeDiscount,
              pincode: previous.pincode,
              cartResponse: previous.cartResponse,
              totalPrice: previous.totalPrice,
              discountedTotalPrice: previous.discountedTotalPrice,
              savedPrice: previous.savedPrice,
              isDeliverable: true,
              isOutOfStock: false));
        } else {
          bool fullWalletPay = false;
          double amountLeftOnWallet = 0;
          double amountLeftToPay = 0;
          if (double.parse(event.walletBalance) >=
              (previous.discountedTotalPrice +
                  double.parse(event.address.deliveryCharges))) {
            fullWalletPay = true;
            amountLeftOnWallet = double.parse(event.walletBalance) -
                (previous.discountedTotalPrice +
                    double.parse(event.address.deliveryCharges));
            amountLeftToPay = 0;
          } else {
            fullWalletPay = false;
            amountLeftOnWallet = 0.0;
            amountLeftToPay = (previous.discountedTotalPrice +
                    double.parse(event.address.deliveryCharges)) -
                double.parse(event.walletBalance);
          }
          emit(CartSuccess(
              useWallet: true,
              walletBalance: amountLeftOnWallet,
              fullWalletPay: fullWalletPay,
              priceAfterWalletBalanceAdded: amountLeftToPay,
              promoCodeFailedMessage: previous.promoCodeFailedMessage,
              promoCode: previous.promoCode,
              promoCodeDiscount: previous.promoCodeDiscount,
              pincode: previous.pincode,
              cartResponse: previous.cartResponse,
              totalPrice: previous.totalPrice,
              discountedTotalPrice: previous.discountedTotalPrice,
              savedPrice: previous.savedPrice,
              isDeliverable: true,
              isOutOfStock: false));
        }
      }
    });
  }

  Future<void> _fetchUserCartData(Emitter<CartState> emit, FetchUserCartData event) async {
      CartState previous = this.state;
    emit(CartLoading());
    await event.cartCubit.addAllVariant(event.userId);
    CartResponse cartResponse = await this.apiBridge.getUserCart(
        addressId: event.address != null ? event.address.id : "",
        pincodeId: event.pincode != null ? event.pincode.id : "");
    event.cartCubit.syncCartUserData(cartResponse.cart.data);
    if (previous is CartSuccess && CartBloc.steps == STEPS.ORDER) {
      emit(CartSuccess(
          promoCodeDiscount: previous.promoCodeDiscount,
          promoCode: previous.promoCode,
          promoCodeFailedMessage: previous.promoCodeFailedMessage,
          pincode: event.pincode,
          cartResponse: cartResponse,
          totalPrice: previous.totalPrice,
          discountedTotalPrice: previous.discountedTotalPrice,
          savedPrice:
              this.calculateSavedPrice(event.cartCubit, cartResponse),
          isDeliverable: true,
          isOutOfStock: false));
    } else {
      emit(CartSuccess(
          pincode: event.pincode,
          cartResponse: cartResponse,
          totalPrice: double.parse(cartResponse.cart.totalAmount),
          discountedTotalPrice:
              double.parse(cartResponse.cart.totalAmount),
          savedPrice:
              this.calculateSavedPrice(event.cartCubit, cartResponse),
          isDeliverable: true,
          isOutOfStock: false));
    }
  }

  double calculateTaxPrice(double price, double taxPercentage) {
    return (price + (price * taxPercentage) / 100);
  }

  double calculateSavedPrice(CartCubit cartCubit, CartResponse cartResponse) {
    double savedPrice = 0;

    cartResponse.cart.data.forEach((ci) {
      double productSavedPrice = 0;
      int variantCount = cartCubit.getVariantsCount(ci.productVariantId) ?? 0;
      if (variantCount != 0) {
        productSavedPrice = ((double.parse(ci.item[0].price) * variantCount) -
            (double.parse(ci.item[0].discountedPrice) * variantCount));
      }
      savedPrice += productSavedPrice;
    });
    return savedPrice;
  }

  checkItemsUser(RecalculateCartVarants event, CartSuccess previous, emit) {
    CartResponse cartResponse = previous.cartResponse;
    if (cartResponse != null) {
      double discountedTotal = 0;
      bool isDeliverable = true;
      bool isOutOfStock = false;
      cartResponse.cart.data.forEach((ci) {
        if (ci.item[0].stock == "0" || ci.item[0].isAvailable == false) {
          isOutOfStock = true;
        }
        double productDiscountedTotal = 0;
        if (!event.cartCubit.checkProductAvailabelInPincode(
            ci.item[0].pincodes, previous.pincode.id)) {
          isDeliverable = false;
        }
        int variantCount =
            event.cartCubit.getVariantsCount(ci.productVariantId) ?? 0;
        if (variantCount != 0) {
          productDiscountedTotal = calculateTaxPrice(
                  double.parse(ci.item[0].discountedPrice),
                  double.parse(ci.item[0].taxPercentage)) *
              variantCount;
        }

        discountedTotal += productDiscountedTotal;
      });
      emit(CartSuccess(
          promoCode: previous.promoCode,
          promoCodeDiscount: previous.promoCodeDiscount,
          promoCodeFailedMessage: previous.promoCodeFailedMessage,
          walletBalance: previous.walletBalance,
          fullWalletPay: previous.fullWalletPay,
          priceAfterWalletBalanceAdded: previous.priceAfterWalletBalanceAdded,
          cartResponse: previous.cartResponse,
          isDeliverable: isDeliverable,
          isOutOfStock: isOutOfStock,
          pincode: previous.pincode,
          userId: previous.userId,
          savedPrice: this.calculateSavedPrice(event.cartCubit, cartResponse),
          totalPrice: discountedTotal,
          discountedTotalPrice: discountedTotal =
              discountedTotal - double.parse(previous.promoCodeDiscount)));
    }
  }

  checkItemsGuest(RecalculateCartVarants event, CartSuccess previous, emit) {
    VariantsResponse variantsResponse = previous.variantsResponse;
    if (variantsResponse != null) {
      double total = 0;
      bool isDeliverable = true;
      bool isOutOfStock = false;
      variantsResponse.variantProducts.forEach((v) {
        if (v.stock == "0") {
          isOutOfStock = true;
        }
        double productTotal = 0;
        if (!event.cartCubit.checkProductAvailabelInPincode(
            v.item[0].pincodes, previous.pincode.id)) {
          isDeliverable = false;
        }
        int variantCount = event.cartCubit.getVariantsCount(v.id) ?? 0;
        if (variantCount != 0) {
          productTotal = calculateTaxPrice(
                  double.parse(v.item[0].discountedPrice),
                  double.parse(v.item[0].taxPercentage)) *
              variantCount;
        }

        total += productTotal;
      });
      emit(CartSuccess(
          walletBalance: previous.walletBalance,
          fullWalletPay: previous.fullWalletPay,
          priceAfterWalletBalanceAdded: previous.priceAfterWalletBalanceAdded,
          isDeliverable: isDeliverable,
          isOutOfStock: isOutOfStock,
          pincode: previous.pincode,
          userId: previous.userId,
          variantsResponse: variantsResponse,
          totalPrice: total,
          discountedTotalPrice: total,
          savedPrice: 0.0));
    }
  }
}
