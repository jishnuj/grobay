part of 'cart_bloc.dart';

@immutable
abstract class CartEvent {}

class FetchCartVaraints extends CartEvent {
  final List<String> varaintIds;
  final Location pincode;
  final String userId;
  final CartCubit cartCubit;
  FetchCartVaraints(
      {this.varaintIds, this.pincode, this.userId, @required this.cartCubit});
}

class CartToggleWalletEvent extends CartEvent {
  final String walletBalance;
  final Address address;
  CartToggleWalletEvent({this.walletBalance, this.address});
}

class FetchUserCartData extends CartEvent {
  final Location pincode;
  final Address address;
  final String userId;
  final CartCubit cartCubit;
  final bool saveBeforeFetching;
  FetchUserCartData(
      {this.pincode,
      this.userId,
      this.address,
      @required this.cartCubit,
      this.saveBeforeFetching = false});
}

class ResetCartEvent extends CartEvent {}

class RecalculateCartVarants extends CartEvent {
  final CartCubit cartCubit;
  final String userId;
  RecalculateCartVarants({@required this.cartCubit, this.userId});
}

class CartApplyPromoCode extends CartEvent {
  final String promoCode;
  CartApplyPromoCode({this.promoCode});
}

class CartRemovePromoCode extends CartEvent {}
