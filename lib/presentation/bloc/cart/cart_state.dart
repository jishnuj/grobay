part of 'cart_bloc.dart';

@immutable
abstract class CartState {}

class CartInitial extends CartState {}

class CartLoading extends CartState {}

class CartSuccess extends CartState {
  final CartResponse cartResponse;
  final VariantsResponse variantsResponse;
  final Location pincode;
  final String userId;
  final double totalPrice;
  final double discountedTotalPrice;
  final double savedPrice;

  final bool useWallet;
  final bool fullWalletPay;
  final double walletBalance;
  final double priceAfterWalletBalanceAdded;

  final bool isDeliverable;
  final bool isOutOfStock;
  final String promoCodeFailedMessage;
  final String promoCode;
  final String promoCodeDiscount;
  CartSuccess({
    this.promoCodeFailedMessage,
    this.fullWalletPay=false,
    this.useWallet=false,
    this.walletBalance = 0.0,
    this.priceAfterWalletBalanceAdded = 0.0,
    this.promoCodeDiscount = "0.0",
    this.promoCode,
    this.variantsResponse,
    this.cartResponse,
    this.pincode,
    this.userId,
    this.totalPrice,
    this.discountedTotalPrice,
    this.savedPrice,
    this.isOutOfStock,
    this.isDeliverable,
  });
}

class CartFailed extends CartState {
  final String message;
  CartFailed({this.message});
}


