part of 'product_bloc.dart';

@immutable
abstract class ProductEvent {}

class FetchProductByCategoryEvent extends ProductEvent {
  final bool showLoading;
  final String parentCatId;
  final String catId;
  FetchProductByCategoryEvent(
      {this.catId, this.parentCatId, this.showLoading = true});
}

class FetchProductBySellerEvent extends ProductEvent {
  final bool showLoading;
  final String sellerId;
  FetchProductBySellerEvent({this.sellerId, this.showLoading = true});
}
