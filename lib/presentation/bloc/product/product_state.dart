part of 'product_bloc.dart';

@immutable
abstract class ProductState {}

class ProductInitial extends ProductState {}

class ProductLoading extends ProductState{}

class ProductSuccess extends ProductState {
  final ProductResponse productResposne;
  ProductSuccess({this.productResposne});
}

class ProductFailed extends ProductState {
  final String message;
  ProductFailed({this.message});
}
