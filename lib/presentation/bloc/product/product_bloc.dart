import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/product_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  ProductBloc() : super(ProductInitial()) {
    on<ProductEvent>((event, emit) async {
      try {
        if (event is FetchProductByCategoryEvent) {
          if (event.showLoading) emit(ProductLoading());
          ProductResponse productResponse = await this.apiBridge.getProducts(
              categoryId: event.parentCatId, subcategoryId: event.catId);
          emit(ProductSuccess(productResposne: productResponse));
        } else if (event is FetchProductBySellerEvent) {
          if (event.showLoading) emit(ProductLoading());
          ProductResponse productResponse =
              await this.apiBridge.getProducts(sellerId: event.sellerId);
          emit(ProductSuccess(productResposne: productResponse));
        }
      } catch (e) {
        emit(ProductFailed(message: e.toString()));
      }
    });
  }
}
