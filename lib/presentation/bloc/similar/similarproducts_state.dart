part of 'similarproducts_bloc.dart';

@immutable
abstract class SimilarproductsState {}

class SimilarproductsInitial extends SimilarproductsState {}

class SimilarproductsLoading extends SimilarproductsState{}

class SimilarproductsSuccess extends SimilarproductsState {
  final ProductResponse similarproductsResposne;
  SimilarproductsSuccess({this.similarproductsResposne});
}

class SimilarproductsFailed extends SimilarproductsState {
  final String message;
  SimilarproductsFailed({this.message});
}
