part of 'similarproducts_bloc.dart';

@immutable
abstract class SimilarproductsEvent {}

class FetchSimilarProduct extends SimilarproductsEvent {
  final String categoryId;
  final String productId;
  FetchSimilarProduct({this.categoryId,this.productId});
}
