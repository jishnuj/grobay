import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/product_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'similarproducts_event.dart';
part 'similarproducts_state.dart';

class SimilarproductsBloc
    extends Bloc<SimilarproductsEvent, SimilarproductsState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  SimilarproductsBloc() : super(SimilarproductsInitial()) {
    on<SimilarproductsEvent>((event, emit) async {
      try {
        if (event is FetchSimilarProduct) {
          emit(SimilarproductsLoading());
          ProductResponse productResponse = await this.apiBridge.getProducts(
              getSimilarProducts: 1,
              categoryId: event.categoryId,
              productId: event.productId);
          emit(
              SimilarproductsSuccess(similarproductsResposne: productResponse));
        }
      } catch (e) {
        emit(SimilarproductsFailed(message: e.toString()));
      }
    });
  }
}
