part of 'transaction_bloc.dart';

@immutable
abstract class TransactionState {}

class TransactionInitial extends TransactionState {}

class TransactionLoading extends TransactionState {}

class TransactionSuccess extends TransactionState {
  final bool isPageLoading;
  final bool isPageFailed;
  final TransactionHistoryResponse transactionHistoryResponse;
  TransactionSuccess({this.transactionHistoryResponse,this.isPageLoading=false,this.isPageFailed=false});
}

class TransactionFailed extends TransactionState {
  final String message;
  TransactionFailed({this.message});
}
