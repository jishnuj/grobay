part of 'transaction_bloc.dart';

@immutable
abstract class TransactionEvent {}

class FetchTransaction extends TransactionEvent {
  final String offset;
  final String limit;
  final String userId;
  final String type;
  FetchTransaction({this.offset, this.limit, this.userId, this.type});
}

class AddTransactionEvent extends TransactionEvent {
  final String id;
  final String amount;
  final String status;
  final String type;
  final String dateCreated;
  final String message;
  AddTransactionEvent(
      {this.amount,
      this.type,
      this.dateCreated,
      this.message,
      this.id,
      this.status});
}
