import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/model/transaction.dart';
import 'package:grobay/api/response/transaction_history_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/app_constants.dart';
import 'package:meta/meta.dart';

part 'transaction_event.dart';
part 'transaction_state.dart';

class TransactionBloc extends Bloc<TransactionEvent, TransactionState> {
  static int lastOffset = 0;
  static int total = 0;
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  TransactionBloc() : super(TransactionInitial()) {
    on<TransactionEvent>((event, emit) async {
      if (event is FetchTransaction) {
        if (event.offset == null) {
          lastOffset = 0;
          total = 0;
          emit(TransactionLoading());
        } else {
          lastOffset = lastOffset + AppConfig.PAGE_LIMIT;
        }
        if (lastOffset == 0 || lastOffset < total) {
          try {
            if (this.state is TransactionSuccess) {
              TransactionSuccess previous = this.state;
              emit(TransactionSuccess(
                  transactionHistoryResponse:
                      previous.transactionHistoryResponse,
                  isPageLoading: true));
            }
            TransactionHistoryResponse transactionHistoryResponse = await this
                .apiBridge
                .fetchTransactionHistory(event.type,
                    offset: ((total + lastOffset) - total).toString(),
                    limit: event.limit);

            if (transactionHistoryResponse.transaction.length != 0) {
              if (this.state is TransactionSuccess) {
                TransactionSuccess previous = this.state;
                transactionHistoryResponse.transaction.forEach((e) =>
                    previous.transactionHistoryResponse.transaction.add(e));
                emit(TransactionSuccess(
                    transactionHistoryResponse:
                        previous.transactionHistoryResponse));
              } else {
                emit(TransactionSuccess(
                    transactionHistoryResponse: transactionHistoryResponse));
              }
            }
            total = int.parse(transactionHistoryResponse.total);
          } catch (e) {
            if (lastOffset == 0) {
              emit(TransactionFailed(message: e.toString()));
            } else {
              if (this.state is TransactionSuccess) {
                lastOffset = lastOffset - AppConfig.PAGE_LIMIT;
                TransactionSuccess previous = this.state;
                emit(TransactionSuccess(
                    isPageLoading: false,
                    isPageFailed: true,
                    transactionHistoryResponse:
                        previous.transactionHistoryResponse));
              }
            }
          }
        }
      } else if (event is AddTransactionEvent) {
        if (this.state is TransactionSuccess) {
          TransactionSuccess previous = this.state;
          previous.transactionHistoryResponse.transaction.insert(
              0,
              Transaction(
                  id: event.id,
                  amount: event.amount,
                  status: event.status == "1" ? "credit" : "debit",
                  dateCreated: event.dateCreated,
                  message: event.message,
                  type: event.type));
          emit(TransactionSuccess(
              transactionHistoryResponse: previous.transactionHistoryResponse));
        } else {
          this.add(
              FetchTransaction(type: AppConstants.WALLET_TRANSACTION_HISTORY));
        }
      }
    });
  }
}
