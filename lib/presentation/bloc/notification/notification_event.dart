part of 'notification_bloc.dart';

@immutable
abstract class NotificationEvent {}


class FetchNotificationEvent extends NotificationEvent {
  FetchNotificationEvent();
}
