part of 'notification_bloc.dart';

@immutable
abstract class NotificationState {}

class NotificationInitial extends NotificationState {}

class NotificationLoading extends NotificationState {}

class NotificationSuccess extends NotificationState {
  final NotificationResponse notificationResponse;
  NotificationSuccess({this.notificationResponse});
}

class NotificationFailed extends NotificationState {
  final String message;
  NotificationFailed({this.message});
}
