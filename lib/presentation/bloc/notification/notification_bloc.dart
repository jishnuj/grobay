import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/notifications_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'notification_event.dart';
part 'notification_state.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  NotificationBloc() : super(NotificationInitial()) {
    on<NotificationEvent>((event, emit) async {
      if (event is FetchNotificationEvent) {
        try {
          emit(NotificationLoading());
          NotificationResponse notificationResponse =
              await this.apiBridge.getNotifications();
          emit(NotificationSuccess(notificationResponse: notificationResponse));
        } catch (e) {
          emit(NotificationFailed());
        }
      }
    });
  }
}
