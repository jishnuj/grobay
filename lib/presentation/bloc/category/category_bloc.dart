import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/category_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'category_event.dart';
part 'category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  CategoryBloc() : super(CategoryInitial()) {
    on<CategoryEvent>((event, emit) async {
      try {
        if (event is FetchCategoryEvent) {
          emit(CategoryLoading());
          CategoryResponse categoryResponse =
              await this.apiBridge.getCategories();
          emit(CategorySuccess(categoryResposne: categoryResponse));
        }
      } catch (e) {
        emit(CategoryFailed(message: e.toString()));
      }
    });
  }
}
