part of 'category_bloc.dart';

@immutable
abstract class CategoryState {}

class CategoryInitial extends CategoryState {}

class CategoryLoading extends CategoryState {}

class CategorySuccess extends CategoryState {
  final CategoryResponse categoryResposne;
  CategorySuccess({this.categoryResposne});
}

class CategoryFailed extends CategoryState {
  final String message;
  CategoryFailed({this.message});
}
