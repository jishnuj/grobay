part of 'category_bloc.dart';

@immutable
abstract class CategoryEvent {}

class FetchCategoryEvent  extends CategoryEvent {
  FetchCategoryEvent();
}