part of 'user_location_bloc.dart';

@immutable
abstract class UserLocationEvent {}

class SetUserLocation extends UserLocationEvent {}

class SetCustomUserLocation extends UserLocationEvent {
  final double lat;
  final double long;
  SetCustomUserLocation({this.lat, this.long});
}
