part of 'user_location_bloc.dart';

@immutable
abstract class UserLocationState {}

class UserLocationInitial extends UserLocationState {}

class UserLocationSuccess extends UserLocationState {
  final Coordinates coordinates;
  final Placemark address;
  UserLocationSuccess({this.coordinates, this.address});
}

class UserLocationLoading extends UserLocationState {}

class UserLocationFailed extends UserLocationState {
  final String message;
  UserLocationFailed({this.message});
}
