import 'package:bloc/bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/core/platform/location_service.dart';
import 'package:meta/meta.dart';

part 'user_location_event.dart';
part 'user_location_state.dart';

class UserLocationBloc extends Bloc<UserLocationEvent, UserLocationState> {
  LocationService locationService = GetIt.I.get<LocationService>();
  UserLocationBloc() : super(UserLocationInitial()) {
    on<UserLocationEvent>((event, emit) async {
      if (event is SetUserLocation) {
        try {
          Coordinates coordinates = await locationService.getLatLon();
          Placemark address = await locationService.getCurrentPosition();
          emit(UserLocationSuccess(address: address, coordinates: coordinates));
        } catch (e) {
          emit(UserLocationFailed(message: e.toString()));
        }
      } else if (event is SetCustomUserLocation) {
        Coordinates coordinates = Coordinates(event.lat, event.long);
        Placemark address = await locationService.getCustomPosition(
            coordinates.latitude, coordinates.longitude);
        emit(UserLocationSuccess(address: address, coordinates: coordinates));
      }
    });
  }
}
