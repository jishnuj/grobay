part of 'settings_bloc.dart';

@immutable
abstract class SettingsState {}

class SettingsInitial extends SettingsState {}

class SettingsLoading extends SettingsState {}

class SettingsSuccess extends SettingsState {
  final PaymentSettingsResponse paymentSettingsResponse;
  final TimeSlotsConfigResponse timeSlotsConfigResponse;
  final TimeSlotsResponse timeSlotsResponse;
  SettingsSuccess(
      {this.paymentSettingsResponse,
      this.timeSlotsConfigResponse,
      this.timeSlotsResponse});
}


class SettingsFailed extends SettingsState {
  final String message;
  SettingsFailed({this.message});
}
