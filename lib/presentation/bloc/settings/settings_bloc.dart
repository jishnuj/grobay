import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/payment_settings_response.dart';
import 'package:grobay/api/response/time_slots_config_response.dart';
import 'package:grobay/api/response/time_slots_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  SettingsBloc() : super(SettingsInitial()) {
    on<SettingsEvent>((event, emit) async {
      try {
        if (event is FetchSettings) {
          emit(SettingsLoading());
          PaymentSettingsResponse paymentSettingsResponse =
              await this.apiBridge.getPaymentSettings();
          TimeSlotsConfigResponse timeSlotsConfigResponse =
              await this.apiBridge.getTimeSlotsConfig();
          TimeSlotsResponse timeSlotsResponse =
              await this.apiBridge.getTimeSlots();
          emit(SettingsSuccess(
              paymentSettingsResponse: paymentSettingsResponse,
              timeSlotsConfigResponse: timeSlotsConfigResponse,
              timeSlotsResponse: timeSlotsResponse));
        } else if(event is FetchpaymentSettings){
            emit(SettingsLoading());
             PaymentSettingsResponse paymentSettingsResponse =
              await this.apiBridge.getPaymentSettings();
              emit(SettingsSuccess(paymentSettingsResponse: paymentSettingsResponse));
        }
      } catch (e) {
        emit(SettingsFailed(message:  e.toString()));
      }
    });
  }
}
