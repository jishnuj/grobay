part of 'settings_bloc.dart';

@immutable
abstract class SettingsEvent {}

class FetchSettings extends SettingsEvent {}

class FetchpaymentSettings extends SettingsEvent {}