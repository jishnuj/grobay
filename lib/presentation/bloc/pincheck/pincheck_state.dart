part of 'pincheck_bloc.dart';

@immutable
abstract class PincheckState {}

class PincheckInitial extends PincheckState {}

class PincheckCurrentState extends PincheckState {
  List<String> unavailableProductsIds; //For Cart Checking
  bool isUnavailable;
  String unavailableProductId;
  PincheckCurrentState(
      {this.unavailableProductId,
      this.unavailableProductsIds,
      this.isUnavailable});
}
