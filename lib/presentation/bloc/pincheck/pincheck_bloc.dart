import 'package:bloc/bloc.dart';
import 'package:grobay/api/model/location.dart';
import 'package:grobay/api/model/product.dart';
import 'package:meta/meta.dart';

part 'pincheck_event.dart';
part 'pincheck_state.dart';

class PincheckBloc extends Bloc<PincheckEvent, PincheckState> {
  PincheckBloc() : super(PincheckInitial()) {
    on<PincheckEvent>((event, emit) {
      try {
        if (event is CheckifAvailableEvent) {
          List<String> productPincodesId =
              event.product.pincodes.trim().split(",");
          if (productPincodesId.length == 0 ||
              productPincodesId[0]=="" ||
              productPincodesId.contains(event.currentlySelected.id)) {
            emit(PincheckCurrentState(
                isUnavailable: false,
                unavailableProductId: event.product.id,
                unavailableProductsIds: []));
          } else {
            emit(PincheckCurrentState(
                isUnavailable: true,
                unavailableProductId: event.product.id,
                unavailableProductsIds: []));
          }
        }
      } catch (e) {
        //TODO
      }
    });
  }

  
}
