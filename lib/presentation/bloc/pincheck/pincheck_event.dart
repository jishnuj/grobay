part of 'pincheck_bloc.dart';

@immutable
abstract class PincheckEvent {}

class CheckifAvailableEvent extends PincheckEvent{
  final Product product;
  final Location currentlySelected;
  CheckifAvailableEvent({this.product,this.currentlySelected});

}


