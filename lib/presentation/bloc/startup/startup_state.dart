part of 'startup_bloc.dart';

@immutable
abstract class StartupState {}

class StartupInitial extends StartupState {}

class StartupLoading extends StartupState {}

class StartupSuccess extends StartupState {
  final StartupResposne startupResposne;
  StartupSuccess({this.startupResposne});
}

class StartupFailed extends StartupState {
  final String message;
  StartupFailed({this.message});
}
