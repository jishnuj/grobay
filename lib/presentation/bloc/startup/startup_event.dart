part of 'startup_bloc.dart';

@immutable
abstract class StartupEvent {}

class FetchStartupEvent extends StartupEvent {
  FetchStartupEvent();
}
