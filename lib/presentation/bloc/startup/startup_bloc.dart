import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/startup_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'startup_event.dart';
part 'startup_state.dart';

class StartupBloc extends Bloc<StartupEvent, StartupState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  StartupBloc() : super(StartupInitial()) {
    on<StartupEvent>((event, emit) async {
        // emit(StartupFailed(message: "Eroor Test".toString()));
      try {
        if (event is FetchStartupEvent) {
          emit(StartupLoading());
          StartupResposne startupResposne = await this
              .apiBridge
              .getStartupData();
              await this.apiBridge.storeFCMId();
          emit(StartupSuccess(startupResposne: startupResposne));
        }
      } catch (e) {
        emit(StartupFailed(message: e.toString()));
      }
    });
  }
}
