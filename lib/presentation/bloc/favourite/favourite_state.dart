part of 'favourite_bloc.dart';

@immutable
abstract class FavouriteState {}

class FavouriteInitial extends FavouriteState {}

class FavouriteLoading extends FavouriteState {
  FavouriteLoading();
}

class FavouriteSuccess extends FavouriteState {
  final FavouriteResponse favouriteResponse;
  FavouriteSuccess({this.favouriteResponse});
}

class FavouriteSuccessGuest extends FavouriteState {
  final List<Product> products;
  FavouriteSuccessGuest({this.products});
}

class FavouriteSaveSuccess extends FavouriteState {
  final List<String> productIds;
  FavouriteSaveSuccess({this.productIds});
}

class FavouriteFailed extends FavouriteState {
  final String message;
  FavouriteFailed({this.message});
}
