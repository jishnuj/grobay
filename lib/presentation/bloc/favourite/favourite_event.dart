part of 'favourite_bloc.dart';

@immutable
abstract class FavouriteEvent {}

class FetchFavouriteEvent extends FavouriteEvent {
  final String userId;
  FetchFavouriteEvent({this.userId});
}

class SaveFavouriteEvent extends FavouriteEvent {
  final String productId;
  final Product product;
  final bool isGuest;
  SaveFavouriteEvent({this.productId, this.isGuest,this.product});
}

class RemoveFavouriteEvent extends FavouriteEvent {
  final String productId;
  final bool isGuest;
  final Product product;
  RemoveFavouriteEvent({this.productId, this.isGuest,this.product});
}

class SyncGuestUserFav extends FavouriteEvent{}

class ResetFavouriteEvent extends FavouriteEvent {}
