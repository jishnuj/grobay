import 'package:get_it/get_it.dart';
import 'package:grobay/api/model/favourite.dart';
import 'package:grobay/api/model/product.dart';
import 'package:grobay/api/response/favourite_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:meta/meta.dart';

part 'favourite_event.dart';
part 'favourite_state.dart';

class FavouriteBloc extends HydratedBloc<FavouriteEvent, FavouriteState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  FavouriteBloc() : super(FavouriteInitial()) {
    on<FavouriteEvent>((event, emit) async {
      try {
        if (event is FetchFavouriteEvent) {
          await _fetchFavourite(emit);
        } else if (event is SaveFavouriteEvent) {
          await _saveFavouriteEvent(event, emit);
        } else if (event is RemoveFavouriteEvent) {
          await _removeFavourite(event, emit);
        } else if (event is ResetFavouriteEvent) {
          emit(FavouriteInitial());
        } else if (event is SyncGuestUserFav) {
          await _buildSyncGuestUserFav(emit);
        }
      } catch (e) {
        emit(FavouriteFailed(message: e.toString()));
      }
    });
  }

  Future<void> _saveFavouriteEvent(
      SaveFavouriteEvent event, Emitter<FavouriteState> emit) async {
    if (event.isGuest) {
      addToFvaouriteGuest(event, emit);
    } else {
      await this.apiBridge.addTofavorites(event.productId);
    }
  }

  Future<void> _fetchFavourite(Emitter<FavouriteState> emit) async {
    emit(FavouriteLoading());
    FavouriteResponse favouriteResponse =
        await this.apiBridge.fetchfavourites();
    emit(FavouriteSuccess(favouriteResponse: favouriteResponse));
  }

  Future<void> _buildSyncGuestUserFav(Emitter<FavouriteState> emit) async {
    FavouriteState previous = this.state;
    if (previous is FavouriteSuccessGuest) {
      previous.products.forEach((element) async {
        await this.apiBridge.addTofavorites(element.id).then((value) => {});
      });
      emit(FavouriteInitial());
    }
  }

  Future<void> _removeFavourite(
      RemoveFavouriteEvent event, Emitter<FavouriteState> emit) async {
    if (event.isGuest) {
      removeFromFvaouriteGuest(event, emit);
    } else {
      FavouriteSuccess previous = this.state;
      List<Favourite> newfavs = previous.favouriteResponse.favourites
          .where((element) => element.productId != event.productId)
          .toList();
      if (newfavs == null || newfavs.length == 0) {
        emit(FavouriteFailed(
            message: "No item(s) found in user's favorite list!"));
      } else {
        previous.favouriteResponse.favourites = newfavs;
        previous.favouriteResponse.total =
            previous.favouriteResponse.favourites.length.toString();
        emit(FavouriteSuccess(favouriteResponse: previous.favouriteResponse));
      }
      await this.apiBridge.removefavorites(event.productId);
    }
  }

  addToFvaouriteGuest(SaveFavouriteEvent event, emit) {
    FavouriteState previous = this.state;
    if (previous is FavouriteSuccessGuest) {
      previous.products.add(event.product);
      emit(FavouriteSuccessGuest(products: previous.products));
    } else {
      emit(FavouriteSuccessGuest(products: [event.product]));
    }
  }

  removeFromFvaouriteGuest(RemoveFavouriteEvent event, emit) {
    FavouriteState previous = this.state;
    if (previous is FavouriteSuccessGuest) {
      List<Product> products =
          previous.products.where((p) => p.id != event.productId).toList();
      if (products.length == 0) {
        emit(FavouriteInitial());
      } else {
        emit(FavouriteSuccessGuest(products: products ?? []));
      }
    }
  }

  @override
  FavouriteState fromJson(Map<String, dynamic> json) {
    List<Product> products = [];
    if (json['products'] != null) {
      json['products'].forEach((element) {
        products.add(Product.fromJson(element));
      });
      return FavouriteSuccessGuest(products: products);
    }
    return FavouriteInitial();
  }

  @override
  Map<String, dynamic> toJson(FavouriteState state) {
    Map<String, dynamic> products = Map();
    products.addAll({"products": []});
    if (state is FavouriteSuccessGuest) {
      state.products.forEach((element) {
        products['products'].add(element.toJson());
      });
      return products;
    }
  }
}
