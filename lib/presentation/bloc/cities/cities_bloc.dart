import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/model/areas.dart';
import 'package:grobay/api/model/city.dart';
import 'package:grobay/api/response/city_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'cities_event.dart';
part 'cities_state.dart';

class CitiesBloc extends Bloc<CitiesEvent, CitiesState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  CitiesBloc() : super(CitiesInitial()) {
    on<CitiesEvent>((event, emit) async {
      try {
        if (event is FetchCitiesevent) {
          emit(CitiesLoading());
          CitiesResponse citiesResponse = await this.apiBridge.getCities();
          emit(CitiesSuccess(
              citiesResponse: citiesResponse,
              selectedAreas: citiesResponse.data[0].areas[0],
              selectedCity: citiesResponse.data[0]));
        } else if (event is SelectCityevent) {
          CitiesSuccess previous = this.state;
          emit(CitiesSuccess(
              citiesResponse: previous.citiesResponse,
              selectedAreas: event.selectedCity.areas[0],
              selectedCity: event.selectedCity));
        } else if (event is SelectCityByIdevent) {
          CitiesSuccess previous = this.state;
          City city = previous.citiesResponse.data
              .singleWhere((c) => c.id == event.cityId);
          emit(CitiesSuccess(
              citiesResponse: previous.citiesResponse,
              selectedAreas: city.areas[0],
              selectedCity: city));
        } else if (event is SelectAreaEvent) {
          CitiesSuccess previous = this.state;
          emit(CitiesSuccess(
              citiesResponse: previous.citiesResponse,
              selectedAreas: event.selectedAreas,
              selectedCity: previous.selectedCity));
        }
      } catch (e) {
        emit(CitiesFailed(message: e.toString()));
      }
    });
  }
}
