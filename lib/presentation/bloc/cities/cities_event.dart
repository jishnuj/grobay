part of 'cities_bloc.dart';

@immutable
abstract class CitiesEvent {}

class FetchCitiesevent extends CitiesEvent {}

class SelectCityevent extends CitiesEvent {
  final City selectedCity;
  SelectCityevent({this.selectedCity});
}

class SelectCityByIdevent extends CitiesEvent {
  final String cityId;
  SelectCityByIdevent({this.cityId});
}


class SelectAreaEvent extends CitiesEvent {
  final Areas selectedAreas;
  SelectAreaEvent({this.selectedAreas});
}
