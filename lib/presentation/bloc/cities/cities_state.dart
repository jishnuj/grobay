part of 'cities_bloc.dart';

@immutable
abstract class CitiesState {}

class CitiesInitial extends CitiesState {}

class CitiesLoading extends CitiesState {
  CitiesLoading();
}

class CitiesSuccess extends CitiesState {
  final CitiesResponse citiesResponse;
  final City selectedCity;
  final Areas selectedAreas;
  CitiesSuccess({this.selectedCity, this.selectedAreas, this.citiesResponse});
}

class CitiesFailed extends CitiesState {
  final String message;
  CitiesFailed({this.message});
}
