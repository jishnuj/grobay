part of 'subcategory_bloc.dart';

@immutable
abstract class SubcategoryState {}

class SubcategoryInitial extends SubcategoryState {}

class SubCategoryLoading extends SubcategoryState {}

class SubCategorySuccess extends SubcategoryState {
  final SubCategoryResponse subcategoryResposne;
  int parentCategoryId;
  SubCategorySuccess({this.subcategoryResposne,this.parentCategoryId});
}

class SubCategoryFailed extends SubcategoryState {
  final String message;
  SubCategoryFailed({this.message});
}
