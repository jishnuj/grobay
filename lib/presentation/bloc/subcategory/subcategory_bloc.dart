import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/subcategory_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'subcategory_event.dart';
part 'subcategory_state.dart';

class SubcategoryBloc extends Bloc<SubcategoryEvent, SubcategoryState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  SubcategoryBloc() : super(SubcategoryInitial()) {
    on<SubcategoryEvent>((event, emit) async {
      try {
        if (event is FetchSubCategoryEvent) {
          emit(SubCategoryLoading());
          SubCategoryResponse subCategoryResponse =
              await this.apiBridge.getSubCategories(event.categoryId);
          emit(SubCategorySuccess(subcategoryResposne: subCategoryResponse));
        }
      } catch (e) {
        emit(SubCategoryFailed(message: e.toString()));
      }
    });
  }
}
