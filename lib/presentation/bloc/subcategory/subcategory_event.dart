part of 'subcategory_bloc.dart';

@immutable
abstract class SubcategoryEvent {}

class FetchSubCategoryEvent extends SubcategoryEvent {
  final int categoryId;
  FetchSubCategoryEvent({this.categoryId});
}