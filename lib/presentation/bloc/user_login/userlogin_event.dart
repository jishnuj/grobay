part of 'userlogin_bloc.dart';

@immutable
abstract class UserloginEvent {}

class FetchUserlogin extends UserloginEvent {
  final String username;
  final String password;
  FetchUserlogin({this.username, this.password});
}

class UpdateUserDetailsEvent extends UserloginEvent {
  final String name;
  final String email;
  final String mobile;
  UpdateUserDetailsEvent({this.name, this.email, this.mobile});
}

class UpdateUserImageEvent extends UserloginEvent {
  final File image;
  UpdateUserImageEvent({this.image});
}

class ReloadUserDetails extends UserloginEvent {
  final String userId;
  ReloadUserDetails({this.userId});
}

class FetchLogoutEvent extends UserloginEvent {}
