import 'dart:io';

import 'package:get_it/get_it.dart';
import 'package:grobay/api/response/update_profileimage_response.dart';
import 'package:grobay/api/response/user_response.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:grobay/core/constant/storage_keys.dart';
import 'package:grobay/core/platform/firebase_service.dart';
import 'package:grobay/core/platform/local_storage_service.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:meta/meta.dart';

part 'userlogin_event.dart';
part 'userlogin_state.dart';

class UserloginBloc extends HydratedBloc<UserloginEvent, UserloginState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  LocalStorageService localStorageService = GetIt.I.get<LocalStorageService>();
  FireBaseService fireBaseService = GetIt.I.get<FireBaseService>();
  UserloginBloc() : super(UserloginInitial()) {
    on<UserloginEvent>((event, emit) async {
      if (event is FetchUserlogin) {
        await _login(emit, event);
      } else if (event is ReloadUserDetails) {
        await _reloadUserDetails(emit);
      } else if (event is UpdateUserDetailsEvent) {
        await _updateUserDetails(emit, event);
      } else if (event is UpdateUserImageEvent) {
        await _updateUserImage(emit, event);
      } else if (event is FetchLogoutEvent) {
        await _logout(emit);
      }
    });
  }

  Future<void> _login(
      Emitter<UserloginState> emit, FetchUserlogin event) async {
    try {
      emit(UserLoginLoading());

      UserResponse userResponse = await this.apiBridge.login(
            username: event.username,
            password: event.password,
          );
      await localStorageService.setItem(
          StorageKeys.UserId.toString(), userResponse.user.userId);
      emit(UserLoginSuccess(userResponse: userResponse));
    } catch (e) {
      emit(UserLoginFailed(message: e.toString()));
    }
  }

  Future<void> _reloadUserDetails(Emitter<UserloginState> emit) async {
    UserloginState previous = this.state;
    if (previous is UserLoginSuccess) {
      UserResponse userResponse = await this.apiBridge.getUserDetails();
      await localStorageService.setItem(
          StorageKeys.UserId.toString(), userResponse.user.userId);
      previous.userResponse.user.balance = userResponse.user.balance;
      previous.userResponse.user.email = userResponse.user.email;
      previous.userResponse.user.profile = userResponse.user.profile;
      previous.userResponse.user.name = userResponse.user.name;
      emit(UserLoginSuccess(userResponse: previous.userResponse));
    } else {
      emit(UserloginInitial());
    }
  }

  Future<void> _updateUserDetails(
      Emitter<UserloginState> emit, UpdateUserDetailsEvent event) async {
    UserLoginSuccess previous = this.state;
    try {
      emit(UserLoginSuccess(
          userResponse: previous.userResponse, userUpdateLoading: true));
      String message = await this.apiBridge.updateProfile(
            name: event.name,
            email: event.email,
            mobile: event.mobile,
          );
      previous.userResponse.user.name = event.name;
      previous.userResponse.user.mobile = event.mobile;
      previous.userResponse.user.email = event.email;
      emit(UserLoginSuccess(
        userResponse: previous.userResponse,
        userUpdateLoading: false,
        userUpdateMsg: message,
      ));
    } catch (e) {
      emit(UserLoginSuccess(
        userResponse: previous.userResponse,
        userUpdateLoading: false,
        userUpdateFailed: true,
        userUpdateMsg: e.toString(),
      ));
    }
  }

  Future<void> _logout(Emitter<UserloginState> emit) async {
    emit(UserLoginLoading());
    await fireBaseService.deleteToken();
    await localStorageService.deleteAll();
    await this.apiBridge.storeFCMId();
    emit(UserloginInitial());
  }

  Future<void> _updateUserImage(
      Emitter<UserloginState> emit, UpdateUserImageEvent event) async {
    UserLoginSuccess previous = this.state;
    try {
      emit(UserLoginSuccess(
          userResponse: previous.userResponse, userImageUpdateLoading: true));
      UpdateProfileImageResponse updateProfileImageResponse =
          await this.apiBridge.updateProfileImage(event.image);
      //update exitsing profile response
      previous.userResponse.user.profile = updateProfileImageResponse.profile;
      emit(UserLoginSuccess(
        userResponse: previous.userResponse,
        userImageUpdateLoading: false,
        userImageUpdateMsg: updateProfileImageResponse.message,
      ));
    } catch (e) {
      emit(UserLoginSuccess(
        userResponse: previous.userResponse,
        userImageUpdateLoading: false,
        userImageUpdateFailed: true,
        userImageUpdateMsg: e.toString(),
      ));
    }
  }

  @override
  UserloginState fromJson(Map<String, dynamic> json) {
    if (json['name'] != null) {
      UserLoginSuccess userLoginSuccess =
          UserLoginSuccess(userResponse: UserResponse.fromJson(json));

      localStorageService.setItem(StorageKeys.UserId.toString(),
          userLoginSuccess.userResponse.user.userId);
      return userLoginSuccess;
    }
  }

  @override
  Map<String, dynamic> toJson(UserloginState state) {
    if (state is UserLoginSuccess) {
      return state.userResponse.toJson();
    }
  }
}
