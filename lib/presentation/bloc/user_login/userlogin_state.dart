part of 'userlogin_bloc.dart';

@immutable
abstract class UserloginState {}

class UserloginInitial extends UserloginState {}

class UserLoginLoading extends UserloginState {}

class UserLoginSuccess extends UserloginState {
  final UserResponse userResponse;
  //Used For User Update Logic
  final bool userUpdateFailed;
  final String userUpdateMsg;
  final bool userUpdateLoading;
  //Used For user Profile Image
  final bool userImageUpdateFailed;
  final String userImageUpdateMsg;
  final bool userImageUpdateLoading;
  UserLoginSuccess(
      {this.userResponse,
      this.userUpdateFailed = false,
      this.userUpdateMsg,
      this.userUpdateLoading = false,
      this.userImageUpdateFailed = false,
      this.userImageUpdateMsg,
      this.userImageUpdateLoading = false});
}

class UserLoginFailed extends UserloginState {
  final String message;
  UserLoginFailed({this.message});
}
