part of 'payment_confirm_bloc.dart';

@immutable
abstract class PaymentConfirmState {}

class PaymentConfirmInitial extends PaymentConfirmState {}

class PaymentConfirmLoading extends PaymentConfirmState {}

class PaymentConfirmSuccess extends PaymentConfirmState {
  PaymentConfirmSuccess();
}

class PaymentConfirmFailed extends PaymentConfirmState {
  final String message;
  PaymentConfirmFailed({this.message});
}
