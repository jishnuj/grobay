part of 'payment_confirm_bloc.dart';

@immutable
abstract class PaymentConfirmEvent {}


class FetchCreatePaymentConfirm extends PaymentConfirmEvent {
  final String paymentMethod;
  final String taxId;
  final String orderId;
  final String taxPercentage;
  final String finalTotal;
  final String email;
  FetchCreatePaymentConfirm({
    this.taxPercentage,
    this.paymentMethod,
    this.taxId,
    this.orderId,
    this.finalTotal,
    this.email
  });
}

class ResetPaymentConfirm extends PaymentConfirmEvent {}