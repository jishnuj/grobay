import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:meta/meta.dart';

part 'payment_confirm_event.dart';
part 'payment_confirm_state.dart';

class PaymentConfirmBloc
    extends Bloc<PaymentConfirmEvent, PaymentConfirmState> {
  ApiBridge apiBridge = GetIt.I.get<ApiBridge>();
  PaymentConfirmBloc() : super(PaymentConfirmInitial()) {
    on<PaymentConfirmEvent>((event, emit) async {
      try {
        emit(PaymentConfirmLoading());
        if (event is FetchCreatePaymentConfirm) {
          if (event.paymentMethod != "cod" && event.paymentMethod!="wallet") {
            if (event.paymentMethod == "paystack") {
              await this.apiBridge.verifyPaystackTransaction(
                  email: event.email,
                  reference: event.taxId,
                  amount: event.finalTotal);
            }

            await this.apiBridge.createTransaction(
                txnId: event.taxId,
                taxPercentage: event.taxPercentage,
                orderId: event.orderId,
                status: "success",
                type: event.paymentMethod,
                amount: event.finalTotal,
                transactiondate: DateTime.now().toString());
          }
          emit(PaymentConfirmSuccess());
        } else if (event is ResetPaymentConfirm) {
          emit(PaymentConfirmInitial());
        }
      } catch (e) {
        emit(PaymentConfirmFailed(message: e.toString()));
      }
    });
  }
}
