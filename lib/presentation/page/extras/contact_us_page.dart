import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:grobay/presentation/bloc/extra_settings/extrasettings_bloc.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';

class ContactUsPage extends StatelessWidget {
  const ContactUsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ExtrasettingsBloc>(context).add(FetchContactEvent());
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            CustomAppbar(
              title: tr('contactUs'),
              showAction: false,
              showLocation: false,
              showBanner: false,
              showTitle: true,
              showLogo: false,
              showSearchField: false,
            ),

            SliverToBoxAdapter(
                child: BlocBuilder<ExtrasettingsBloc, ExtrasettingsState>(
              builder: (context, state) {
                if (state is ExtrasettingsSuccess) {
                  return Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 15.0),
                      child: Html(data: state.extraSettingResponse.data));
                } else if (state is ExtrasettingsFailed) {
                  return Container(
                       height: MediaQuery.of(context).size.height/2,
                    child: CustomErrorWidget(
                      callerName: "contact_us",
                      msg: tr('failedToFetchContactUs'),
                      onNetworkError: () {
                        BlocProvider.of<ExtrasettingsBloc>(context)
                            .add(FetchContactEvent());
                      },
                    ),
                  );
                }
                return Container(
                  height: MediaQuery.of(context).size.height / 2,
                  child: Center(child: CircularProgressIndicator()),
                );
              },
            )),
            // _buildContact()
          ],
        ),
      ),
    );
  }

  SliverToBoxAdapter _buildContact() {
    return SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildTextField("Email"),
            _buildTextField("Name"),
            _buildTextField("Subject"),
            _buildTextField("Content", maxlines: 10),
            SizedBox(
              height: 15.0,
            ),
          ],
        ),
      ),
    );
  }

  Container _buildTextField(String label,
      {TextEditingController editingController,
      TextInputType textInputType,
      int maxlines}) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey[200]))),
      child: TextField(
        maxLines: maxlines ?? 1,
        keyboardType: textInputType ?? TextInputType.name,
        controller: editingController ?? TextEditingController(),
        decoration: InputDecoration(
            hintText: label,
            hintStyle: TextStyle(color: Colors.grey),
            border: InputBorder.none),
      ),
    );
  }
}
