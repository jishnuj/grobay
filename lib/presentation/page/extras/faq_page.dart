import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/extra_settings/extrasettings_bloc.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';

class FaqPage extends StatelessWidget {
  const FaqPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ExtrasettingsBloc>(context).add(FetchFAQEvent());
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            CustomAppbar(
              title: tr('faq'),
              showAction: false,
              showLogo: false,
              showTitle: true,
              showLocation: false,
              showBanner: false,
              showSearchField: false,
            ),
            BlocBuilder<ExtrasettingsBloc, ExtrasettingsState>(
              builder: (context, state) {
                if (state is ExtrasettingsFaqSuccess) {
                  return SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) {
                    return ExpansionTile(
                      expandedCrossAxisAlignment: CrossAxisAlignment.start,
                      expandedAlignment: Alignment.topLeft,
                      childrenPadding: EdgeInsets.symmetric(
                          horizontal: 15.0, vertical: 10.0),
                      title: Text(state.faqResponse.faq[index].question),
                      children: [
                        Text(
                          state.faqResponse.faq[index].answer,
                          style: Theme.of(context)
                              .primaryTextTheme
                              .subtitle1
                              .copyWith(
                                color: Palette.onSecondary,
                              ),
                        )
                      ],
                    );
                  }, childCount: state.faqResponse.faq.length));
                } else if (state is ExtrasettingsFailed) {
                  return Container(
                       height: MediaQuery.of(context).size.height/2,
                    child: CustomErrorWidget(
                      callerName: "faq",
                      msg: tr('failedToFetchFaq'),
                      onNetworkError: () {
                        BlocProvider.of<ExtrasettingsBloc>(context)
                            .add(FetchFAQEvent());
                      },
                    ),
                  );
                }
                return SliverToBoxAdapter(
                  child: Container(
                    height: MediaQuery.of(context).size.height / 2,
                    child: Center(child: CircularProgressIndicator()),
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
