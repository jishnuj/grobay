import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:grobay/presentation/bloc/extra_settings/extrasettings_bloc.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';

class AboutUsPage extends StatelessWidget {
  const AboutUsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ExtrasettingsBloc>(context).add(FetchAboutEvent());
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            CustomAppbar(
              title: tr('aboutUs'),
              showAction: false,
              showLocation: false,
              showLogo: false,
              showTitle: true,
              showBanner: false,
              showSearchField: false,
            ),
            SliverToBoxAdapter(
                child: BlocBuilder<ExtrasettingsBloc, ExtrasettingsState>(
              builder: (context, state) {
                if (state is ExtrasettingsSuccess) {
                  return Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 15.0),
                      child: Html(data: state.extraSettingResponse.data));
                } else if (state is ExtrasettingsFailed) {
                  return Container(
                    height: MediaQuery.of(context).size.height / 2,
                    child: CustomErrorWidget(
                      callerName: "about_us",
                      msg: tr('failedToFetchAboutUs'),
                      onNetworkError: () {
                        BlocProvider.of<ExtrasettingsBloc>(context)
                            .add(FetchAboutEvent());
                      },
                    ),
                  );
                }
                return Container(
                  height: MediaQuery.of(context).size.height / 2,
                  child: Center(child: CircularProgressIndicator()),
                );
              },
            )),
          ],
        ),
      ),
    );
  }
}
