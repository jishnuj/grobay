import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:grobay/presentation/widget/category_lsit_widget.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';

class CategoriesListPage extends StatelessWidget {
  const CategoriesListPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        CustomAppbar(
          showBanner: false,
          showLogo: false,
          showTitle: true,
          showLocation: false,
          title: tr('category'),
          showSearchField: false,
        ),
        CategoryListWidget(
          showAsList: false,
          showTitle: false,
          isShowingFullData: true,
        ),
      ],
    );
  }
}
