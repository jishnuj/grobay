import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/category.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/product/product_bloc.dart';
import 'package:grobay/presentation/bloc/subcategory/subcategory_bloc.dart';
import 'package:grobay/presentation/page/product/product_list_page.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/product_card_widget.dart';
import 'package:grobay/presentation/widget/skelton_loaders.dart';

class CategoryProductsPage extends StatelessWidget {
  final Category parentCategory;
  const CategoryProductsPage({Key key, this.parentCategory}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (parentCategory.status == null) {
      BlocProvider.of<ProductBloc>(context)
          .add(FetchProductByCategoryEvent(parentCatId: parentCategory.id));
    }
    BlocProvider.of<SubcategoryBloc>(context).add(
        FetchSubCategoryEvent(categoryId: int.parse(this.parentCategory.id)));
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            _buildCustomappBar(),
            _buildSubCategorylist(),
            _buildCategoryList()
          ],
        ),
      ),
    );
  }

  SliverPadding _buildCategoryList() {
    return SliverPadding(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
      sliver: BlocBuilder<ProductBloc, ProductState>(
        builder: (context, productState) {
          if (productState is ProductSuccess) {
            return SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
              return ProductCardWidget(
                product: productState.productResposne.products[index],
              );
            }, childCount: productState.productResposne.products.length));
          } else if (productState is ProductFailed) {
            return _buildErrorBox(productState);
          }
          return SliverToBoxAdapter(
            child: CategoryProductListLoader(),
          );
        },
      ),
    );
  }

  SliverToBoxAdapter _buildErrorBox(ProductFailed productState) {
    return SliverToBoxAdapter(
        child: Container(
            height: 300,
            child: CustomErrorWidget(
              msg: productState.message,
              callerName: "category_products_page",
            )));
  }

  CustomAppbar _buildCustomappBar() {
    return CustomAppbar(
      showTitle: true,
      showBanner: false,
      showLocation: false,
      showSearchField: false,
      showSearchIcon: true,
      title: parentCategory.name,
      elevation: 0.0,
    );
  }

  BlocBuilder<SubcategoryBloc, SubcategoryState> _buildSubCategorylist() {
    return BlocBuilder<SubcategoryBloc, SubcategoryState>(
      builder: (context, subcategoryState) {
        if (subcategoryState is SubCategorySuccess) {
          if (subcategoryState.subcategoryResposne.categories.length == 0)
            return SliverToBoxAdapter(
              child: SizedBox(),
            );
          return SliverAppBar(
            backgroundColor: Palette.onPrimary,
            pinned: true,
            leading: SizedBox.shrink(),
            collapsedHeight:
                subcategoryState.subcategoryResposne.categories.length == 0
                    ? kToolbarHeight
                    : 110.0,
            flexibleSpace: ListView.builder(
                padding: EdgeInsets.all(10.0),
                scrollDirection: Axis.horizontal,
                itemCount:
                    subcategoryState.subcategoryResposne.categories.length,
                itemBuilder: (context, index) {
                  return _buildSubCategoryCard(
                      context, subcategoryState, index);
                }),
          );
        }

        return SliverToBoxAdapter(
            child: Center(child: LinearProgressIndicator()));
      },
    );
  }

  InkWell _buildSubCategoryCard(
      BuildContext context, SubCategorySuccess subcategoryState, int index) {
    return InkWell(
      borderRadius: BorderRadius.circular(20.0),
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return ProductListPage(
            useLocalBloc: true,
            parentCategory: parentCategory,
            subCategory: subcategoryState.subcategoryResposne.categories[index],
          );
        }));
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
        decoration: BoxDecoration(
          
         
            borderRadius: BorderRadius.circular(10.0),
         ),
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(45),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: CachedNetworkImageProvider( subcategoryState.subcategoryResposne.categories[index].image))),
            height: 45,
            width: 45,
            ),
           
            Flexible(
                child: Text(subcategoryState
                    .subcategoryResposne.categories[index].name)),
          ],
        ),
      ),
    );
  }
}
