import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/assets.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/cart/cart_bloc.dart';
import 'package:grobay/presentation/bloc/favourite/favourite_bloc.dart';
import 'package:grobay/presentation/bloc/location/location_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/widget/password_textfield_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class LoginPage extends StatelessWidget {
  LoginPage({Key key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();
  Widget build(BuildContext context) {
    TextEditingController usernameController = TextEditingController();
    TextEditingController passwordController = TextEditingController();

    return Scaffold(
      body: Container(
        width: double.infinity,
        color: Palette.primary,
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              UIUtilWidget.verticalSpaceCustom(80),
              _buildHeaderLogo(),
              UIUtilWidget.verticalSpaceLarge(),
              Expanded(
                child: Container(
                  color: Palette.secondary,
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.all(30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          _buildHeaderText(context),
                          UIUtilWidget.verticalSpaceCustom(60),
                          Container(
                            child: Column(
                              children: <Widget>[
                                _buildEmailPhoneTextField(usernameController),
                                _buildPasswordTextField(passwordController),
                              ],
                            ),
                          ),
                          UIUtilWidget.verticalSpaceMedium(),
                          BlocConsumer<UserloginBloc, UserloginState>(
                            listener: (context, userState) {
                              if (userState is UserLoginSuccess) {
                                _onUserLoginSuccess(context, userState);
                              } else if (userState is UserLoginFailed) {
                                UIUtilWidget.showSnackBar(userState.message);
                              }
                            },
                            builder: (context, userState) {
                              if (userState is UserLoginLoading) {
                                return Center(
                                    child: CircularProgressIndicator());
                              }
                              return _buildLoginButton(context,
                                  usernameController, passwordController);
                            },
                          ),
                          UIUtilWidget.verticalSpaceCustom(30),
                          _buildForgotPasswordButton(context),
                          UIUtilWidget.verticalSpaceCustom(30),
                          _buildDontHaveAccountButton(context),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Container _buildHeaderLogo() {
    return Container(
      alignment: Alignment.center,
      child: Image.asset(
        Assets.LOGO,
        width: 180,
      ),
    );
  }

  RichText _buildHeaderText(BuildContext context) {
    return RichText(
        textAlign: TextAlign.center,
        text: TextSpan(children: [
          TextSpan(
              text: tr('loginTo'),
              style: Theme.of(context)
                  .primaryTextTheme
                  .headline6
                  .copyWith(color: Palette.onSecondary)),
          TextSpan(
               text:  AppConfig.APP_NAME.toUpperCase(),
              style: Theme.of(context)
                  .primaryTextTheme
                  .headline6
                  .copyWith(color: Palette.highlightColor)),
        ]));
  }

  Container _buildEmailPhoneTextField(
      TextEditingController textEditingController) {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return tr('pleaseEnterYourPhoneNumber');
          }
          return null;
        },
        controller: textEditingController,
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.phone_android),
            contentPadding:
                EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
            hintText: tr('phoneNumber'),
            hintStyle: TextStyle(color: Palette.disabledColor),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(color: Palette.onSecondary))),
      ),
    );
  }

  Container _buildPasswordTextField(
      TextEditingController textEditingController) {
    return Container(
        padding: EdgeInsets.all(10),
        child: PasswordTextfieldWidget(
          borderRadius: BorderRadius.circular(25.0),
          validator: (value) {
            if (value.isEmpty) {
              return tr('pleaseEnterYourPassword');
            }
            return null;
          },
          hintText: tr('password'),
          textEditingController: textEditingController,
        ));
  }

  Widget _buildLoginButton(BuildContext context, TextEditingController username,
      TextEditingController password) {
    return InkWell(
      onTap: () {
        if (_formKey.currentState.validate()) {
          BlocProvider.of<UserloginBloc>(context).add(
              FetchUserlogin(username: username.text, password: password.text));
        }
      },
      child: Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 10.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: Palette.buttonBg),
        child: Center(
          child: Text(
            tr('loginHeading'),
            style: TextStyle(
                color: Palette.buttonText, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }

  InkWell _buildForgotPasswordButton(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, "forgotpassword");
      },
      child: Text(
       tr('forgotPassword')+"?",
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.subtitle2.copyWith(
            color: Palette.disabledColor,
            fontWeight: FontWeight.w200,
            decoration: TextDecoration.underline),
      ),
    );
  }

  InkWell _buildDontHaveAccountButton(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.pushReplacementNamed(context, "register");
        },
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              text: tr('dontHaveAnAccount'),
              style: Theme.of(context)
                  .textTheme
                  .subtitle1
                  .copyWith(color: Palette.disabledColor),
              children: [
                TextSpan(
                    text: tr('register'),
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1
                        .copyWith(color: Palette.buttonBg))
              ]),
        ));
  }

  void _onUserLoginSuccess(BuildContext context, UserLoginSuccess userState) {
    // Get Current Selected Location
    LocationSuccess locationSuccess =
        BlocProvider.of<LocationBloc>(context).state;

    //Sync favourites
    BlocProvider.of<FavouriteBloc>(context).add(SyncGuestUserFav());
    // Sync Cart data [OK]

    BlocProvider.of<CartBloc>(context).add(FetchUserCartData(
      pincode: locationSuccess.selectedLocation,
      userId: userState.userResponse.user.userId,
      cartCubit: BlocProvider.of<CartCubit>(context),
    ));
    Navigator.pushNamedAndRemoveUntil(context, "home", (route) => false);
  }
}
