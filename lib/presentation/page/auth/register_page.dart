import 'package:country_code_picker/country_code_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/bloc/user_register/user_register_bloc.dart';
import 'package:grobay/presentation/widget/custom_button_widget.dart';
import 'package:grobay/presentation/widget/normal_textfield_widget.dart';
import 'package:grobay/presentation/widget/password_textfield_widget.dart';
import 'package:grobay/presentation/widget/timer_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class RegisterPage extends StatelessWidget {
  RegisterPage({Key key}) : super(key: key);

  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController mobileNo = TextEditingController();
  TextEditingController countryCode = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController friendsCode = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<UserRegisterBloc>(context).add(ResetUserRegisterEvent());
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Palette.primary, Palette.primary.withOpacity(0.6)])),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            UIUtilWidget.verticalSpaceCustom(80),
            _buildHeaderText(),
            UIUtilWidget.verticalSpaceLarge(),
            Expanded(
              child: BlocConsumer<UserRegisterBloc, UserRegisterState>(
                listener: (context, state) {
                  if (state is UserCheckNumberFailed ||
                      state is UserOTPVerifyFailed ||
                      state is UserCreateFailed) {
                    _onError(state);
                  } else if (state is UserOTPVerifySuccess) {
                    _onOTPVerifySucces();
                  } else if (state is UserCreateSuccess) {
                    _onUserCreateSuccess(context);
                  }
                },
                builder: (context, state) {
                  if (state is UserRegisterInitial ||
                      state is UserCheckNumberFailed) {
                    return _buildRegisterationForm(context);
                  } else if (state is UserCheckNumberSuccess ||
                      state is UserOTPVerifyFailed) {
                    return _buildOTPForm(context);
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  void _onOTPVerifySucces() {
    UIUtilWidget.showSnackBar(tr('otpVerifiedSuccessfully'));
  }

  Padding _buildHeaderText() {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: [
              BackButton(),
              Text(
               tr('register'),
                style: TextStyle(color: Palette.onSecondary, fontSize: 40),
              ),
            ],
          ),
          UIUtilWidget.verticalSpaceMedium(),
          Text(
            tr('createNewAccount'),
            style: TextStyle(color: Palette.onSecondary, fontSize: 18),
          )
        ],
      ),
    );
  }

  Container _buildRegisterationForm(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(60), topRight: Radius.circular(60))),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(30),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                UIUtilWidget.verticalSpaceCustom(60),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Color.fromRGBO(225, 95, 27, .3),
                            blurRadius: 20,
                            offset: Offset(0, 10))
                      ]),
                  child: Column(
                    children: <Widget>[
                      _buildNameField(),
                      _buildEmailField(),
                      _buildPhoneNumberField(context),
                      _buildPasswordfField(),
                      _buildReferralCodeField(),
                    ],
                  ),
                ),
                UIUtilWidget.verticalSpaceCustom(40),
                _buildNavigateToLogin(context),
                UIUtilWidget.verticalSpaceCustom(40),
                _buildRegisterButton(context),
                UIUtilWidget.verticalSpaceCustom(40),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildNameField() {
    return NormalTextField(
      formKey: _formKey,
      defaultValidator: FORM_VALIDATORS.NAME,
      controller: name,
      icon: Icons.person,
      keyboardType: TextInputType.name,
      hintText: tr('fullName'),
    );
  }

  Widget _buildEmailField() {
    return NormalTextField(
      formKey: _formKey,
      defaultValidator: FORM_VALIDATORS.EMAIL,
      controller: email,
      keyboardType: TextInputType.emailAddress,
      icon: Icons.email,
      hintText: tr('email'),
    );
  }

  Container _buildPhoneNumberField(context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Palette.teritaryColor))),
      child: TextFormField(
        onChanged: (value) {
          // _formKey.currentState.validate();
        },
        validator: (value) {
          if (value == null || value.isEmpty) {
            return tr('pleaseEnterYourPhoneNumber');
          }
          return null;
        },
        keyboardType: TextInputType.phone,
        controller: mobileNo,
        decoration: InputDecoration(
            prefixIcon: _buildCountryCodeSelector(context),
            hintText: tr('phoneNumber'),
            hintStyle: TextStyle(color: Palette.disabledColor),
            border: InputBorder.none),
      ),
    );
  }

  Container _buildCountryCodeSelector(context) {
    return Container(
      width: 80,
      child: CountryCodePicker(
        onInit: (value) {
          AppConfig.COUNTRY_DIAL_CODE = value.dialCode;
          countryCode.text = value.dialCode;
        },
        flagWidth: 15.0,
        onChanged: (value) {
          countryCode.text = value.dialCode;
        },

        dialogTextStyle: Theme.of(context)
            .textTheme
            .headline6
            .copyWith(color: Palette.onSecondary),
        textStyle: Theme.of(context)
            .textTheme
            .subtitle1
            .copyWith(color: Palette.disabledColor),
        // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
        initialSelection: AppConfig.COUNTRY_CODE,
        padding: EdgeInsets.zero,
        // optional. aligns the flag and the Text left
        alignLeft: true,
      ),
    );
  }

  Container _buildPasswordfField() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Palette.teritaryColor))),
        child: PasswordTextfieldWidget(
          validator: getValidators(FORM_VALIDATORS.PASSWORD),
          showBorder: false,
          hintText: tr('password'),
          textEditingController: password,
        ));
  }

  Widget _buildReferralCodeField() {
    return NormalTextField(
      keyboardType: TextInputType.text,
      defaultValidator: FORM_VALIDATORS.REFERRALCODE,
      formKey: _formKey,
      hintText: tr('rfc'),
      controller: friendsCode,
      icon: Icons.person_add,
    );
  }

  Widget _buildRegisterButton(
    BuildContext context,
  ) {
    return CustomButtonWidget(
        onTap: () {
          if (_formKey.currentState.validate()) {
            BlocProvider.of<UserRegisterBloc>(context).add(FetchOTPEvent(
                mobileNo: mobileNo.text,
                email: email.text,
                password: password.text,
                friendsCode: friendsCode.text,
                countryCode: int.parse(
                    countryCode.text.substring(1, countryCode.text.length)),
                name: name.text));
          }
        },
        text: tr('register'));
  }

  InkWell _buildNavigateToLogin(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.pushReplacementNamed(context, "login");
        },
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              text:tr('alreadyHaveAnAccount'),
              style: Theme.of(context)
                  .textTheme
                  .subtitle1
                  .copyWith(color: Palette.disabledColor),
              children: [
                TextSpan(
                    text: tr('login'),
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1
                        .copyWith(color: Palette.buttonBg))
              ]),
        ));
  }

  Container _buildOTPForm(BuildContext context) {
    TextEditingController smsCode = TextEditingController();
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(60), topRight: Radius.circular(60))),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(30),
          child: Column(
            children: <Widget>[
              UIUtilWidget.verticalSpaceCustom(60),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromRGBO(225, 95, 27, .3),
                          blurRadius: 20,
                          offset: Offset(0, 10))
                    ]),
                child: Column(
                  children: <Widget>[
                    _buildDisabledNumberField(),
                    _buildOtpField(smsCode),
                  ],
                ),
              ),
              TimerWidget(
                onResend: () {
                  BlocProvider.of<UserRegisterBloc>(context).add(FetchOTPEvent(
                      mobileNo: mobileNo.text,
                      email: email.text,
                      password: password.text,
                      friendsCode: friendsCode.text,
                      countryCode: int.parse(countryCode.text
                          .substring(1, countryCode.text.length)),
                      name: name.text));
                },
              ),
              _buildVerifyButton(context, smsCode),
              UIUtilWidget.verticalSpaceCustom(50),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildDisabledNumberField() {
    return NormalTextField(
      hintText: countryCode.text + mobileNo.text,
      enabled: false,
    );
  }

  Widget _buildOtpField(TextEditingController smsCode) {
    return NormalTextField(
      controller: smsCode,
      hintText: tr('enterOTP'),
    );
  }

  Widget _buildVerifyButton(
      BuildContext context, TextEditingController smsCode) {
    return CustomButtonWidget(
        onTap: () {
          if (smsCode.text.trim().isEmpty) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(tr('pleaseEnterOtp'))));
          } else {
            BlocProvider.of<UserRegisterBloc>(context).add(FetchVerifyOTPEvent(
                smsCode: smsCode.text,
                mobileNo: mobileNo.text,
                email: email.text,
                friendsCode: friendsCode.text,
                password: password.text,
                countryCode: int.parse(
                    countryCode.text.substring(1, countryCode.text.length)),
                name: name.text));
          }
        },
        text: tr('verify'));
  }

  void _onError(UserRegisterState state) {
    UIUtilWidget.showSnackBar(state.toString());
  }

  void _onUserCreateSuccess(BuildContext context) {
    UIUtilWidget.showSnackBar(tr('userRegisteredSuccessfully'));
    BlocProvider.of<UserloginBloc>(context)
        .add(FetchUserlogin(username: mobileNo.text, password: password.text));
    Navigator.pushReplacementNamed(context, "login");
  }
}
