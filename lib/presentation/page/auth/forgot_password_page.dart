import 'package:country_code_picker/country_code_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/forgot_password/forgot_password_bloc.dart';
import 'package:grobay/presentation/widget/custom_button_widget.dart';
import 'package:grobay/presentation/widget/normal_textfield_widget.dart';
import 'package:grobay/presentation/widget/password_textfield_widget.dart';
import 'package:grobay/presentation/widget/timer_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class ForgotPasswordPage extends StatelessWidget {
  ForgotPasswordPage({Key key}) : super(key: key);

  TextEditingController mobileNo = TextEditingController();
  TextEditingController countryCode = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController passwordConfirm = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _passwordFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ForgotPasswordBloc>(context)
        .add(ResetUserForgotPasswordEvent());
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Palette.primary, Palette.primary.withOpacity(0.6)])),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            UIUtilWidget.verticalSpaceCustom(80),
            _buildHeaderText(),
            UIUtilWidget.verticalSpaceLarge(),
            Expanded(
              child: BlocConsumer<ForgotPasswordBloc, ForgotPasswordState>(
                listener: (context, state) {
                  if (state is ForgotUserCheckNumberFailed ||
                      state is ForgotUserOTPVerifyFailed ||
                      state is UserResetPasswordFailed) {
                    UIUtilWidget.showSnackBar(state.toString());
                  } else if (state is ForgotUserOTPVerifySuccess) {
                    _onOTPVerificationSuccess();
                  } else if (state is UserResetPasswordSuccess) {
                    _onPasswordResetSuccess(context);
                  }
                },
                builder: (context, state) {
                  if (state is ForgotPasswordInitial ||
                      state is ForgotUserCheckNumberFailed) {
                    return _buildMobileForm(context);
                  } else if (state is ForgotUserCheckNumberSuccess ||
                      state is ForgotUserOTPVerifyFailed) {
                    return _buildOTPForm(context);
                  } else if (state is ForgotUserOTPVerifySuccess) {
                    return _buildPasswordForm(context);
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Padding _buildHeaderText() {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: [
              BackButton(),
              Text(
                tr('forgotPassword'),
                style: TextStyle(color: Palette.onSecondary, fontSize: 30),
              ),
            ],
          ),
          UIUtilWidget.verticalSpaceMedium()
        ],
      ),
    );
  }

  Container _buildMobileForm(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(60), topRight: Radius.circular(60))),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(30),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                UIUtilWidget.verticalSpaceCustom(60),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Color.fromRGBO(225, 95, 27, .3),
                            blurRadius: 20,
                            offset: Offset(0, 10))
                      ]),
                  child: Column(
                    children: <Widget>[
                      _buildPhoneNumberField(context),
                    ],
                  ),
                ),
                UIUtilWidget.verticalSpaceCustom(50),
                _buildConfirmButton(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildConfirmButton(
    BuildContext context,
  ) {
    return CustomButtonWidget(
        onTap: () {
          if (_formKey.currentState.validate()) {
            BlocProvider.of<ForgotPasswordBloc>(context).add(
                ForgotFetchOTPEvent(
                    mobileNo: mobileNo.text, countryCode: countryCode.text));
          }
        },
        text: tr('confirm'));
  }

  Container _buildPhoneNumberField(context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Palette.teritaryColor))),
      child: TextFormField(
        onChanged: (value) {
          // _formKey.currentState.validate();
        },
        validator: (value) {
          if (value == null || value.isEmpty) {
            return tr('pleaseEnterYourPhoneNumber');
          }
          return null;
        },
        keyboardType: TextInputType.phone,
        controller: mobileNo,
        decoration: InputDecoration(
            prefixIcon: _buildCountryCodeSelector(context),
            hintText: tr('phoneNumber'),
            hintStyle: TextStyle(color: Palette.disabledColor),
            border: InputBorder.none),
      ),
    );
  }

  Container _buildCountryCodeSelector(context) {
    return Container(
      width: 80,
      child: CountryCodePicker(
        onInit: (value) {
          AppConfig.COUNTRY_DIAL_CODE = value.dialCode;
          countryCode.text = value.dialCode;
        },
        flagWidth: 15.0,
        onChanged: (value) {
          countryCode.text = value.dialCode;
        },

        dialogTextStyle: Theme.of(context)
            .textTheme
            .headline6
            .copyWith(color: Palette.onSecondary),
        textStyle: Theme.of(context)
            .textTheme
            .subtitle1
            .copyWith(color: Palette.disabledColor),
        // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
        initialSelection: AppConfig.COUNTRY_CODE,
        padding: EdgeInsets.zero,
        // optional. aligns the flag and the Text left
        alignLeft: true,
      ),
    );
  }

  Container _buildOTPForm(BuildContext context) {
    TextEditingController smsCode = TextEditingController();
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(60), topRight: Radius.circular(60))),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(30),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 60,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromRGBO(225, 95, 27, .3),
                          blurRadius: 20,
                          offset: Offset(0, 10))
                    ]),
                child: Column(
                  children: <Widget>[
                    _buildDisabledNumberField(),
                    _buildOtpField(smsCode),
                  ],
                ),
              ),
              TimerWidget(
                onResend: () {
                  BlocProvider.of<ForgotPasswordBloc>(context)
                      .add(ForgotFetchOTPEvent(
                    mobileNo: countryCode.text + mobileNo.text,
                  ));
                },
              ),
              _buildVerifyButton(context, smsCode),
              UIUtilWidget.verticalSpaceCustom(50),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildVerifyButton(
      BuildContext context, TextEditingController smsCode) {
    return CustomButtonWidget(
        onTap: () {
          if (smsCode.text.trim().isEmpty) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(tr('pleaseEnterOtp'))));
          } else {
            BlocProvider.of<ForgotPasswordBloc>(context)
                .add(ForgotFetchVerifyOTPEvent(
              smsCode: smsCode.text,
              mobileNo: mobileNo.text,
            ));
          }
        },
        text: tr('verify'));
  }

  Widget _buildDisabledNumberField() {
    return NormalTextField(
      enabled: false,
      hintText: countryCode.text + mobileNo.text,
    );
  }

  Widget _buildOtpField(TextEditingController smsCode) {
    return NormalTextField(
      controller: smsCode,
      hintText: tr('enterOtp'),
    );
  }

  Widget _buildPasswordForm(context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(60), topRight: Radius.circular(60))),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(30),
          child: Form(
            key: _passwordFormKey,
            child: Column(
              children: <Widget>[
                UIUtilWidget.verticalSpaceCustom(60),
                Container(
                  child: Column(
                    children: <Widget>[
                      _buildPasswordField(),
                      UIUtilWidget.verticalSpaceLarge(),
                      _buildPasswordRenterField(),
                    ],
                  ),
                ),
                UIUtilWidget.verticalSpaceCustom(50),
                _buildPasswordResetButton(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  PasswordTextfieldWidget _buildPasswordField() {
    return PasswordTextfieldWidget(
      borderRadius: BorderRadius.circular(50),
      validator: getValidators(FORM_VALIDATORS.PASSWORD),
      hintText: tr('newPassword'),
      textEditingController: password,
    );
  }

  PasswordTextfieldWidget _buildPasswordRenterField() {
    return PasswordTextfieldWidget(
      borderRadius: BorderRadius.circular(50),
      validator: getValidators(FORM_VALIDATORS.PASSWORD),
      hintText: tr('reEnterPassword'),
      textEditingController: passwordConfirm,
    );
  }

  Widget _buildPasswordResetButton(context) {
    return CustomButtonWidget(
        onTap: () {
          if (_passwordFormKey.currentState.validate()) {
            if (password.text != passwordConfirm.text) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(tr('passwordDoNotMatch')),
              ));
            } else {
              BlocProvider.of<ForgotPasswordBloc>(context).add(
                  ForgotResetPasswordEvent(
                      mobileNo: mobileNo.text, password: password.text));
            }
          }
        },
        text: tr('resetPassword'));
  }

  void _onOTPVerificationSuccess() {
    UIUtilWidget.showSnackBar(tr('otpVerifiedSuccessfully'));
  }

  void _onPasswordResetSuccess(BuildContext context) {
    UIUtilWidget.showSnackBar(tr('passwordResetSuccsess'));
    Navigator.pushReplacementNamed(context, "login");
  }
}
