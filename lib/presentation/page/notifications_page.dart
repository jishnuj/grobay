import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:grobay/api/model/category.dart';
import 'package:grobay/api/model/product.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/notification/notification_bloc.dart';
import 'package:grobay/presentation/page/category/category_products_page.dart';
import 'package:grobay/presentation/page/product/product_detail_page.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/ui_helper.dart';

class NotificationsPage extends StatelessWidget {
  const NotificationsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<NotificationBloc>(context).add(FetchNotificationEvent());
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            CustomAppbar(
              title: tr('notifications'),
              showAction: false,
              showLocation: false,
              showTitle: true,
              showBanner: false,
              showSearchField: false,
            ),
            BlocBuilder<NotificationBloc, NotificationState>(
              builder: (context, notificationState) {
                if (notificationState is NotificationSuccess) {
                  return SliverPadding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    sliver: SliverList(
                        delegate: SliverChildBuilderDelegate((context, index) {
                      return _buildNotificationCard(
                          notificationState, index, context);
                    },
                            childCount: notificationState
                                .notificationResponse.data.length)),
                  );
                } else if (notificationState is NotificationLoading) {
                  return SliverToBoxAdapter(
                    child: Container(
                        height: MediaQuery.of(context).size.height / 2,
                        child: Center(child: CircularProgressIndicator())),
                  );
                }
                return SliverToBoxAdapter(
                  child: Container(),
                );
              },
            )
          ],
        ),
      ),
    );
  }

  Widget _buildNotificationCard(
      NotificationSuccess notificationState, int index, BuildContext context) {
    return InkWell(
      onTap: () {
        if (notificationState.notificationResponse.data[index].type ==
            "product") {
          Get.to(ProductDetailsPage(
            productId:
                notificationState.notificationResponse.data[index].typeId,
            product: Product(
                id: notificationState.notificationResponse.data[index].typeId,
                name: "..."),
          ));
        } else if (notificationState.notificationResponse.data[index].type ==
            "category") {
          Get.to(CategoryProductsPage(
            parentCategory: Category(
                id: notificationState.notificationResponse.data[index].typeId,
                name: notificationState.notificationResponse.data[index].name),
          ));
        }
      },
      child: Card(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (notificationState.notificationResponse.data[index].image
                  .trim()
                  .isNotEmpty)
                CachedNetworkImage(
                  alignment: Alignment.topLeft,
                  height: 80,
                  width: MediaQuery.of(context).size.width - 20,
                  imageUrl:
                      notificationState.notificationResponse.data[index].image,
                  placeholder: (context, url) => Container(
                    decoration: BoxDecoration(
                      color: Colors.lime[200],
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              if (notificationState.notificationResponse.data[index].image
                  .trim()
                  .isNotEmpty)
                UIHelper.verticalSpaceSmall(),
              Text(notificationState.notificationResponse.data[index].name,
                  style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                      color: Palette.onSecondary, fontWeight: FontWeight.bold)),
              if (notificationState.notificationResponse.data[index].subtitle
                  .trim()
                  .isNotEmpty)
                UIHelper.verticalSpaceSmall(),
              if (notificationState.notificationResponse.data[index].subtitle
                  .trim()
                  .isNotEmpty)
                Text(
                  notificationState.notificationResponse.data[index].subtitle,
                  style: Theme.of(context).primaryTextTheme.bodyText1.copyWith(
                        color: Palette.onSecondary,
                      ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
