import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/presentation/bloc/app_settings/app_settings_bloc.dart';
import 'package:grobay/presentation/bloc/cart/cart_bloc.dart';
import 'package:grobay/presentation/bloc/location/location_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/page/cart/cart_page_guest.dart';
import 'package:grobay/presentation/page/cart/cart_page_user.dart';
import 'package:grobay/presentation/widget/cart_footer_widget.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/refersh_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:logger/logger.dart';
import 'package:visibility_detector/visibility_detector.dart';

class CartPage extends StatelessWidget {
  const CartPage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<AppSettingsBloc>(context).add(FetchAppSettings());
    return VisibilityDetector(
      key: UniqueKey(),
      onVisibilityChanged: (visibilityInfo) {
        _onVisibilityChanged(visibilityInfo);
      },
      child: Scaffold(
        body: BlocBuilder<AppSettingsBloc, AppSettingsState>(
          builder: (context, appSettingsState) {
            if (appSettingsState is AppSettingsLoading ||
                appSettingsState is AppSettingsInitial) {
              return _buildLoadingTemplate(context);
            } else if (appSettingsState is AppSettingsSuccess) {
              return BlocBuilder<LocationBloc, LocationState>(
                builder: (context, locationState) {
                  if (locationState is LocationLoading) {
                    return UIUtilWidget.buildCircularProgressIndicator();
                  } else if (locationState is LocationSuccess) {
                    return SafeArea(
                        child: CustomRefreshWidget(
                      onRefresh: () {
                        if (BlocProvider.of<UserloginBloc>(context).state
                            is UserLoginSuccess) {
                          this._onUser(context, locationState,
                              BlocProvider.of<UserloginBloc>(context).state);
                        } else {
                          this._onGuestUser(context, locationState);
                        }
                      },
                      child: CustomScrollView(
                        slivers: [
                          BlocBuilder<CartCubit, CartCubitState>(
                            builder: (context, state) {
                              if (BlocProvider.of<CartBloc>(context).state
                                  is CartSuccess) {
                                _onLoginSuccess(context);
                              }
                              return CustomAppbar(
                                title:
                                    "${tr('myCart')} (${BlocProvider.of<CartCubit>(context).getVaraintNonDuplicateArray().length} " +
                                        (BlocProvider.of<CartCubit>(context)
                                                    .getVaraintNonDuplicateArray()
                                                    .length ==
                                                1
                                            ? tr('item')
                                            : tr('items')) + ")",
                                showBanner: false,
                                showTitle: true,
                                showLocation: false,
                                showAction: false,
                                showSearchField: false,
                              );
                            },
                          ),
                          _buildCartBody(locationState)
                        ],
                      ),
                    ));
                  }
                  return SizedBox();
                },
              );
            }
            return UIUtilWidget.buildCircularProgressIndicator();
          },
        ),
        bottomNavigationBar: BlocBuilder<AppSettingsBloc, AppSettingsState>(
          builder: (context, appSettingsState) {
            if (appSettingsState is AppSettingsSuccess) {
              return FutureBuilder(
                  future: Future.delayed(Duration(seconds: 3), () => "delayed"),
                  initialData: null,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) {
                      return _buildCheckoutCard(context);
                    }
                    return UIUtilWidget.verticalSpaceCustom(0.0);
                  });
            }
            return UIUtilWidget.verticalSpaceCustom(0.0);
          },
        ),
      ),
    );
  }

  Scaffold _buildLoadingTemplate(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          CustomAppbar(
            title:
                "${tr('myCart')} (${BlocProvider.of<CartCubit>(context).getVaraintNonDuplicateArray().length} " +
                    (BlocProvider.of<CartCubit>(context)
                                .getVaraintNonDuplicateArray()
                                .length ==
                            1
                        ? tr('item')
                        : tr('items')) + ")",
            showBanner: false,
            showLogo: false,
            showTitle: true,
            showLocation: false,
            showAction: false,
            showSearchField: false,
          ),
          SliverToBoxAdapter(
            child: Container(
              height: MediaQuery.of(context).size.height / 1.5,
              child: UIUtilWidget.buildCircularProgressIndicatorWithText(
                  tr('cartLoading')),
            ),
          ),
        ],
      ),
    );
  }

  void _onVisibilityChanged(VisibilityInfo visibilityInfo) {
    var visiblePercentage = visibilityInfo.visibleFraction * 100;
    if (visiblePercentage == 100) {
      CartBloc.steps = STEPS.CART;
    }
  }

  BlocBuilder<UserloginBloc, UserloginState> _buildCartBody(
      LocationSuccess locationSuccess) {
    return BlocBuilder<UserloginBloc, UserloginState>(
      builder: (context, loginState) {
        if (loginState is UserLoginSuccess) {
          //Load User Cart Details
          _onUser(context, locationSuccess, loginState);
          return CartPageuser();
        } else {
          //Load Guest Cart Details
          _onGuestUser(context, locationSuccess);
        }
        return CartPageguest();
      },
    );
  }

  void _onUser(BuildContext context, LocationSuccess locationSuccess,
      UserLoginSuccess loginState) {
    BlocProvider.of<CartBloc>(context).add(FetchUserCartData(
      pincode: locationSuccess.selectedLocation,
      userId: loginState.userResponse.user.userId,
      cartCubit: BlocProvider.of<CartCubit>(context),
    ));
  }

  void _onGuestUser(BuildContext context, LocationSuccess locationSuccess) {
    BlocProvider.of<CartBloc>(context).add(FetchCartVaraints(
        pincode: locationSuccess.selectedLocation,
        userId: "",
        cartCubit: BlocProvider.of<CartCubit>(context),
        varaintIds:
            BlocProvider.of<CartCubit>(context).getVaraintNonDuplicateArray()));
  }

  void _onLoginSuccess(BuildContext context) {
    UserloginState userloginState =
        BlocProvider.of<UserloginBloc>(context).state;
    if (userloginState is UserLoginSuccess) {
      BlocProvider.of<CartBloc>(context).add(RecalculateCartVarants(
          userId: userloginState.userResponse.user.userId,
          cartCubit: BlocProvider.of<CartCubit>(context)));
    } else {
      BlocProvider.of<CartBloc>(context).add(RecalculateCartVarants(
          cartCubit: BlocProvider.of<CartCubit>(context)));
    }
  }

  Widget _buildCheckoutCard(BuildContext context) {
    return CartFooterWidget(
      onPressed: (cartState) {
        _onCartCheckout(cartState, context);
      },
    );
  }

  void _onCartCheckout(cartState, BuildContext context) {
    Logger().w(cartState.totalPrice);
    if (cartState.totalPrice < AppConfig.MINIMUM_ORDER_AMOUNT) {
      UIUtilWidget.showSnackBar(
          "${tr('minimumOrderAmountIs')} ${AppConfig.MINIMUM_ORDER_AMOUNT}");
    } else if (cartState.pincode.pincode == "All") {
      UIUtilWidget.showSnackBar(tr('pleaseSelectAPincode'));
    } else {
      if (!cartState.isDeliverable) {
        UIUtilWidget.showSnackBar(tr('someProductsNotDeliverable'));
      } else if (cartState.isOutOfStock) {
        UIUtilWidget.showSnackBar(tr('someProductsAreOutOfStock'));
      } else {
        if (BlocProvider.of<UserloginBloc>(context).state is UserLoginSuccess) {
          Navigator.pushNamed(context, "selectaddress");
        } else {
          Navigator.pushNamed(context, "login");
        }
      }
    }
  }
}
