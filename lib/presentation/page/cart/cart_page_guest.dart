import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/variant_product.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/cart/cart_bloc.dart';

import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/skelton_loaders.dart';

class CartPageguest extends StatelessWidget {
  const CartPageguest({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        height: MediaQuery.of(context).size.height / 1.4,
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
        child: BlocConsumer<CartBloc, CartState>(
          listener: (context, cartState) {
            if (cartState is CartSuccess) {
              if (BlocProvider.of<CartCubit>(context)
                      .getVaraintNonDuplicateArray()
                      .length ==
                  0) {
                BlocProvider.of<CartBloc>(context).add(ResetCartEvent());
              }
            }
          },
          builder: (context, cartState) {
            if (cartState is CartSuccess &&
                cartState.variantsResponse != null) {
              // Just to make the appsettings and cartsuccess loader apper as same widget
              return FutureBuilder(
                future: Future.delayed(Duration(seconds: 1), () => "delayed"),
                initialData: null,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    return _buildCartProductList(cartState);
                  }
                  return CartListLoader();
                },
              );
            } else if (cartState is CartFailed) {
              return CustomErrorWidget(
                msg: cartState.message,
              );
            } else if (cartState is CartLoading) {
              return CartListLoader();
            }
            return _buildEmptyCartText(context);
          },
        ),
      ),
    );
  }

  Container _buildEmptyCartText(BuildContext context) {
    return Container(
        height: 200,
        child: Center(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.shopping_cart_outlined,
              size: 100,
            ),
            Text(
              tr('noItemsInCart'),
              style: Theme.of(context).textTheme.headline5,
            )
          ],
        )));
  }

  ListView _buildCartProductList(CartSuccess cartState) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: cartState.variantsResponse.variantProducts.length,
      itemBuilder: (context, index) {
        if (BlocProvider.of<CartCubit>(context).getVariantsCount(
                cartState.variantsResponse.variantProducts[index].id) ==
            0) {
          return SizedBox();
        }
        return Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Dismissible(
            key: UniqueKey(),
            direction: DismissDirection.endToStart,
            onDismissed: (direction) {
              BlocProvider.of<CartCubit>(context).deleteProductVariant(
                  cartState.variantsResponse.variantProducts[index].productId,
                  cartState.variantsResponse.variantProducts[index].id);
            },
            background: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: Color(0xFFFFE6E6),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Row(
                children: [Spacer(), Icon(Icons.delete)],
              ),
            ),
            child: _buildCartProductCard(context, cartState, index,
                cartState.variantsResponse.variantProducts[index]),
          ),
        );
      },
    );
  }

  Widget _buildCartProductCard(BuildContext context, CartSuccess cartState,
      int index, VariantProduct variantProduct) {
    return Container(
      decoration: BoxDecoration(
          color: Palette.productCartCardColor,
          borderRadius: BorderRadius.circular(15)),
      child: Row(
        children: [
          SizedBox(width: 5),
          SizedBox(
            width: 88,
            child: AspectRatio(
              aspectRatio: 0.88,
              child: Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Palette.productCartCardColor,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: CachedNetworkImage(
                      imageUrl: variantProduct.item[0].image,
                      imageBuilder: (context, imageProvider) => Container(
                            width: 80.0,
                            height: 80.0,
                            decoration: BoxDecoration(
                              color:Palette.imageCacheColor,
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: imageProvider, fit: BoxFit.cover),
                            ),
                          ))),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 15.0, top: 10),
                  child: Text(
                    variantProduct.item[0].name,
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    maxLines: 2,
                  ),
                ),
                SizedBox(height: 10),
                if ((variantProduct.item[0].pincodes.split(',').length != 0 &&
                        variantProduct.item[0].pincodes.split(',')[0] != "") &&
                    !variantProduct.item[0].pincodes
                        .split(',')
                        .contains(cartState.pincode.id))
                  Text(
                    tr('notDeliverable'),
                    style: Theme.of(context)
                        .textTheme
                        .subtitle2
                        .copyWith(color: Colors.red),
                  ),
                SizedBox(height: 10),
                Text(
                  '${variantProduct.measurement} ${variantProduct.item[0].unit}  ',
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                      fontWeight: FontWeight.w600,
                      color: Palette.productCartCardSecondaryText),
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    BlocBuilder<CartCubit, CartCubitState>(
                      builder: (context, state) {
                        return Text.rich(
                          TextSpan(
                              text:
                                  '${CurrencyWidget.getFixedPriceCurrencyString(calculateTaxPrice(variantProduct))}  ',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(),
                              children: [
                                TextSpan(
                                    text:
                                        " x ${BlocProvider.of<CartCubit>(context).getVariantsCount(variantProduct.id).toString()}",
                                    style:
                                        Theme.of(context).textTheme.bodyText1),
                              ]),
                        );
                      },
                    ),
                    variantProduct.stock != "0"
                        ? _buildAddRemoveButton(context, variantProduct)
                        : Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: Text(
                              tr('soldOut'),
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle2
                                  .copyWith(color: Colors.red),
                            ),
                          ),
                  ],
                ),
                SizedBox(height: 10),
              ],
            ),
          )
        ],
      ),
    );
  }

  double calculateTaxPrice(VariantProduct variantProduct) {
    double price = double.parse(variantProduct.item[0].discountedPrice);
    return (price +
        (price * double.parse(variantProduct.item[0].taxPercentage)) / 100);
  }

  Widget _buildAddRemoveButton(
      BuildContext context, VariantProduct variantProduct) {
    return IntrinsicWidth(
      child: Container(
        // width: 120,
        margin: EdgeInsets.only(right: 10.0),
        child: BlocBuilder<CartCubit, CartCubitState>(
          builder: (context, cartCubitState) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: () {
                    BlocProvider.of<CartCubit>(context).removeProductVariant(
                        variantProduct.productId, variantProduct.id);
                  },
                  child: Container(
                    padding: EdgeInsets.all(4.0),
                    decoration: BoxDecoration(
                      color: Palette.buttonBg,
                        border: Border.all(
                    color: Palette.buttonBg,
                        ),
                        borderRadius: BorderRadius.circular(10.0)),
                    child: Icon(
                      Icons.remove,
                      size: 15.0,
               color: Palette.buttonText,
                    ),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                    child: Text(
                  BlocProvider.of<CartCubit>(context)
                      .getVariantsCount(variantProduct.id)
                      .toString(),
                  textAlign: TextAlign.center,
                )),
                SizedBox(
                  width: 15,
                ),
                InkWell(
                  onTap: () {
                    BlocProvider.of<CartCubit>(context).addProductVariant(
                      variantProduct.productId,
                      variantProduct.id,
                      double.parse(variantProduct.stock),
                      double.parse(variantProduct.measurement),
                    );
                  },
                  child: Container(
                    padding: EdgeInsets.all(4.0),
                    decoration: BoxDecoration(
                         color: Palette.buttonBg,
                        border: Border.all(
                          color: Palette.buttonBg,
                        ),
                        borderRadius: BorderRadius.circular(10.0)),
                    child: Icon(
                      Icons.add,
                      size: 15.0,
                       color: Palette.buttonText,
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
