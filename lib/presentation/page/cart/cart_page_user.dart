import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/cart.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/cart/cart_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/skelton_loaders.dart';

class CartPageuser extends StatelessWidget {
  const CartPageuser({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    UserLoginSuccess userLoginSuccess =
        BlocProvider.of<UserloginBloc>(context).state;

    return SliverToBoxAdapter(
      child: Container(
        height: MediaQuery.of(context).size.height / 1.4,
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
        child: BlocConsumer<CartBloc, CartState>(
          listener: (context, cartState) {
            if (cartState is CartSuccess) {
              if (BlocProvider.of<CartCubit>(context)
                      .getVaraintNonDuplicateArray()
                      .length ==
                  0) {
                BlocProvider.of<CartBloc>(context).add(ResetCartEvent());
              }
            }
          },
          builder: (context, cartState) {
            if (cartState is CartSuccess && cartState.cartResponse != null) {
              // Just to make the appsettings and cartsuccess loader apper as same widget
              return FutureBuilder(
                future: Future.delayed(Duration(seconds: 1), () => "delayed"),
                initialData: null,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    return _buildProductCartList(cartState, userLoginSuccess);
                  }
                  return CartListLoader();
                },
              );
            } else if (cartState is CartFailed) {
              return CustomErrorWidget(
                msg: cartState.message,
                image: Icon(
                  Icons.shopping_cart_outlined,
                  size: 100,
                ),
              );
            } else if (cartState is CartLoading) {
              return CartListLoader();
            }
            return _buildCartEmpty(context);
          },
        ),
      ),
    );
  }

  ListView _buildProductCartList(
      CartSuccess cartState, UserLoginSuccess userLoginSuccess) {
    return ListView.builder(
      // shrinkWrap: true,
      itemCount: cartState.cartResponse.cart.data.length,
      itemBuilder: (context, index) {
        if (BlocProvider.of<CartCubit>(context).getVariantsCount(
                cartState.cartResponse.cart.data[index].productVariantId) ==
            0) {
          return SizedBox();
        }
        return Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Dismissible(
            key: UniqueKey(),
            direction: DismissDirection.endToStart,
            onDismissed: (direction) {
              BlocProvider.of<CartCubit>(context).deleteProductVariant(
                cartState.cartResponse.cart.data[index].productId,
                cartState.cartResponse.cart.data[index].productVariantId,
                isFromCart: true,
                userId: userLoginSuccess.userResponse.user.userId,
              );
            },
            background: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: Color(0xFFFFE6E6),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Row(
                children: [Spacer(), Icon(Icons.delete)],
              ),
            ),
            child: _buildCartProductCard(
              context,
              cartState,
              index,
            ),
          ),
        );
      },
    );
  }

  Container _buildCartEmpty(BuildContext context) {
    return Container(
        height: 200,
        child: Center(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.shopping_cart_outlined,
              size: 100,
            ),
            Text(
              tr('noItemsInCart'),
              style: Theme.of(context).textTheme.headline5,
            )
          ],
        )));
  }

  double calculateTaxPrice(CartItem cartItem) {
    double price = double.parse(cartItem.item[0].discountedPrice);
    return (price +
        (price * double.parse(cartItem.item[0].taxPercentage)) / 100);
  }

  Widget _buildCartProductCard(
      BuildContext context, CartSuccess cartState, int index) {
    return Container(
      decoration: BoxDecoration(
          color: Palette.productCartCardColor,
          borderRadius: BorderRadius.circular(15)),
      child: Row(
        children: [
          SizedBox(width: 5),
          Container(
            width: 88,
            child: AspectRatio(
              aspectRatio: 0.88,
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Palette.productCartCardColor,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: CachedNetworkImage(
                  imageUrl:
                      cartState.cartResponse.cart.data[index].item[0].image,
                  imageBuilder: (context, imageProvider) => Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                      color: Palette.imageCacheColor,
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: imageProvider, fit: BoxFit.cover),
                    ),
                  ),
                  height: 80,
                  placeholder: (context, url) =>
                      Container(color: Palette.imageCacheColor),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 15.0, top: 10),
                  child: Text(
                    cartState.cartResponse.cart.data[index].item[0].name,
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    maxLines: 2,
                  ),
                ),
                SizedBox(height: 10),
                if (!BlocProvider.of<CartCubit>(context)
                    .checkProductAvailabelInPincode(
                        cartState
                            .cartResponse.cart.data[index].item[0].pincodes,
                        cartState.pincode.id))
                  Text(
                    tr('notDeliverable'),
                    style: Theme.of(context)
                        .textTheme
                        .subtitle2
                        .copyWith(color: Colors.red),
                  ),
                SizedBox(height: 10),
                Text(
                  '${cartState.cartResponse.cart.data[index].item[0].measurement} ${cartState.cartResponse.cart.data[index].item[0].unit}  ',
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                      fontWeight: FontWeight.w600,
                      color: Palette.productCartCardSecondaryText),
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    BlocBuilder<CartCubit, CartCubitState>(
                      builder: (context, state) {
                        return Text.rich(
                          TextSpan(
                              text:
                                  '${CurrencyWidget.getFixedPriceCurrencyString(calculateTaxPrice(cartState.cartResponse.cart.data[index]))}  ',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(),
                              children: [
                                TextSpan(
                                    text:
                                        " x ${BlocProvider.of<CartCubit>(context).getVariantsCount(cartState.cartResponse.cart.data[index].productVariantId).toString()}",
                                    style:
                                        Theme.of(context).textTheme.bodyText1),
                              ]),
                        );
                      },
                    ),
                    cartState.cartResponse.cart.data[index].item[0].stock != "0"
                        ? _buildAddRemoveButton(
                            context, cartState.cartResponse.cart.data[index])
                        : Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: Text(
                              tr('soldOut'),
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle2
                                  .copyWith(color: Colors.red),
                            ),
                          ),
                  ],
                ),
                SizedBox(height: 10),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildAddRemoveButton(BuildContext context, CartItem cartItem) {
    return IntrinsicWidth(
      child: BlocBuilder<UserloginBloc, UserloginState>(
        builder: (context, userLoginState) {
          return Container(
            // width: 120,
            margin: EdgeInsets.only(right: 10.0),
            child: BlocBuilder<CartCubit, CartCubitState>(
              builder: (context, cartCubitState) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InkWell(
                      onTap: () {
                        BlocProvider.of<CartCubit>(context)
                            .removeProductVariant(
                                cartItem.productId, cartItem.productVariantId,
                                isFromCart: true,
                                userId: userLoginState is UserLoginSuccess
                                    ? userLoginState.userResponse.user.userId
                                    : null);
                      },
                      child: Container(
                        padding: EdgeInsets.all(4.0),
                        decoration: BoxDecoration(
                            color: Palette.buttonBg,
                            border: Border.all(
                              color: Palette.buttonBg,
                            ),
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Icon(
                          Icons.remove,
                          size: 15.0,
                          color: Palette.buttonText,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Expanded(
                        child: Text(
                      BlocProvider.of<CartCubit>(context)
                          .getVariantsCount(cartItem.productVariantId)
                          .toString(),
                      textAlign: TextAlign.center,
                    )),
                    SizedBox(
                      width: 15,
                    ),
                    InkWell(
                      onTap: () {
                        BlocProvider.of<CartCubit>(context).addProductVariant(
                            cartItem.productId,
                            cartItem.productVariantId,
                            double.parse(cartItem.item[0].stock),
                            double.parse(cartItem.item[0].measurement),
                            userId: userLoginState is UserLoginSuccess
                                ? userLoginState.userResponse.user.userId
                                : null);
                      },
                      child: Container(
                        padding: EdgeInsets.all(4.0),
                        decoration: BoxDecoration(
                            color: Palette.buttonBg,
                            border: Border.all(
                              color: Palette.buttonBg,
                            ),
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Icon(
                          Icons.add,
                          size: 15.0,
                          color: Palette.buttonText,
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          );
        },
      ),
    );
  }
}
