import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/product/product_bloc.dart';
import 'package:grobay/presentation/bloc/seller/seller_bloc.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/product_card_widget.dart';
import 'package:grobay/presentation/widget/skelton_loaders.dart';
import 'package:grobay/presentation/widget/ui_helper.dart';

class SellerProductsPage extends StatelessWidget {
  SellerProductsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocBuilder<SellerBloc, SellerState>(
          builder: (context, sellerState) {
            if (sellerState is SellerSuccess) {
              BlocProvider.of<ProductBloc>(context).add(
                  FetchProductBySellerEvent(
                      sellerId: sellerState.sellerResposne.sellers[0].id));
            }
            if (sellerState is SellerSuccess) {
              return _buildBody(context, sellerState);
            } else if (sellerState is SellerFailed) {
              return CustomErrorWidget(
                msg: sellerState.message,
              );
            }
            return Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [CircularProgressIndicator()],
              ),
            );
          },
        ),
      ),
    );
  }

  CustomScrollView _buildBody(BuildContext context, SellerSuccess sellerState) {
    return CustomScrollView(
      slivers: [
        _buildAppBar(context, sellerState),
        _buildSellerDetailsBox(context, sellerState),
        _buildProductList()
      ],
    );
  }

  SliverPadding _buildProductList() {
    return SliverPadding(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
      sliver: BlocBuilder<ProductBloc, ProductState>(
        builder: (context, productState) {
          if (productState is ProductSuccess) {
            return SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
              return ProductCardWidget(
                product: productState.productResposne.products[index],
              );
            }, childCount: productState.productResposne.products.length));
          } else if (productState is ProductFailed) {
            return SliverToBoxAdapter(
                child: Container(
              height: 300,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [Text(productState.message)],
              ),
            ));
          }
          return SliverToBoxAdapter(child: SellerProductListLoader());
        },
      ),
    );
  }

  SliverToBoxAdapter _buildSellerDetailsBox(
      BuildContext context, SellerSuccess sellerState) {
    return SliverToBoxAdapter(
      child: Container(
        child: Column(
          children: [_buildSellerDetails(sellerState, context)],
        ),
      ),
    );
  }

  CustomAppbar _buildAppBar(BuildContext context, SellerSuccess sellerState) {
      return CustomAppbar(
      showBanner: false,
      showLocation: false,
      showLogo: false,
      showSearchIcon: true,
      showTitle: true,
      title: sellerState.sellerResposne.sellers[0].name,
      showSearchField: false,
    );
   
  }

  Widget _buildSellerDetails(SellerSuccess sellerState, BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
            Palette.teritaryColor,
            Palette.teritaryColor.withOpacity(0.5),
            Palette.teritaryColor,
          ])),
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
      child: Column(
        children: [
                UIHelper.verticalSpaceSmall(),
          CachedNetworkImage(
            imageUrl: sellerState.sellerResposne.sellers[0].logo,
            imageBuilder: (context, imageProvider) => Container(
              width: 120.0,
              height: 120.0,
              decoration: BoxDecoration(
                color: Palette.onPrimary,
                border: Border.all(color: Palette.onPrimary),
                shape: BoxShape.circle,
                image:
                    DecorationImage(image: imageProvider, fit: BoxFit.contain),
              ),
            ),
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
          UIHelper.verticalSpaceMedium(),
          Text(
            sellerState.sellerResposne.sellers[0].name.trim(),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline5.copyWith(
                fontWeight: FontWeight.bold, color: Palette.onSecondary),
          ),
          if (sellerState.sellerResposne.sellers[0].storeDescription
              .trim()
              .isNotEmpty)
            Html(
                style: {"*": Style(textAlign: TextAlign.center)},
                data: sellerState.sellerResposne.sellers[0].storeDescription),
          UIHelper.verticalSpaceExtraSmall(),
          Text(
            sellerState.sellerResposne.sellers[0].sellerAddress.trim(),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.subtitle1,
          ),
          UIHelper.verticalSpaceMedium(),
        ],
      ),
    );
  }
}
