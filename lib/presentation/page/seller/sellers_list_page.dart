import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/seller_widget.dart';

class SellersListPage extends StatelessWidget {
  const SellersListPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            CustomAppbar(
              showBanner: false,
              showLocation: false,
              showSearchIcon: true,
              showSearchField: false,
              title: tr('seller'),
            ),
            SellerWidget(
              showBackButton: true,
              showTitle: false,
              isShowingFullData: true,
            ),
          ],
        ),
      ),
    );
  }
}
