import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/keyword/keyword_bloc.dart';
import 'package:grobay/presentation/bloc/search/search_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/product_card_widget.dart';
import 'package:grobay/presentation/widget/skelton_loaders.dart';


class SearchPage extends StatelessWidget {
  SearchPage({Key key}) : super(key: key);
  TextEditingController _searchController = TextEditingController();
  FocusNode searchNode = FocusNode();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        iconTheme: IconThemeData(color: Palette.onSecondary),
        backgroundColor: Palette.secondary,
        title: Text(tr('search'),
            style: Theme.of(context)
                .textTheme
                .subtitle1
                .copyWith(color: Palette.onSecondary)),
      ),
      body: SafeArea(
        child: Column(
          children: [
            _buildSearchFielld(context),
          
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [_buildkeywords(), _buildSearchresults()],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Container _buildSearchFielld(BuildContext context) {
    OutlineInputBorder outlineInputBorder = OutlineInputBorder(
        borderRadius: BorderRadius.circular(15.0),
        borderSide: BorderSide(color: Palette.buttonText));
    return Container(
        height: 70,
        color: Palette.teritaryColor,
        padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
        child: TextField(
          autofocus: true,
          focusNode: searchNode,
          controller: _searchController,
          onChanged: (val) {
            BlocProvider.of<KeywordBloc>(context)
                .add(FilterKeywordEvent(search: val));
          },
          decoration: InputDecoration(
              isDense: true,
              hintText: tr('searchProducts'),
              contentPadding:
                  EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
              fillColor: Colors.white,
              suffixIcon: Padding(
                padding: EdgeInsets.zero,
                child: Container(
                  decoration: BoxDecoration(
                      color: Palette.buttonBg,
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(15.0),
                          topRight: Radius.circular(15.0))),
                  child: InkWell(
                    onTap: () {
                      UserloginState userloginState =
                          BlocProvider.of<UserloginBloc>(context).state;

                      BlocProvider.of<SearchBloc>(context).add(
                          SearchProductEvent(search: _searchController.text));
                    },
                    child: Icon(
                      Icons.search,
                      color: Palette.buttonText,
                    ),
                  ),
                ),
              ),
              filled: true,
              focusedBorder: outlineInputBorder,
              enabledBorder: outlineInputBorder.copyWith(
                  borderSide: BorderSide(color: Palette.buttonBg)),
              border: outlineInputBorder),
        ));
  }

  BlocBuilder<SearchBloc, SearchState> _buildSearchresults() {
    return BlocBuilder<SearchBloc, SearchState>(
      builder: (context, productSearchState) {
        if (productSearchState is SearchSuccess) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0,vertical: 7.5),
            child: Column(
              children: productSearchState.searchResposne.products
                  .map((e) => ProductCardWidget(
                        product: e,
                      ))
                  .toList(),
            ),
          );
        } else if (productSearchState is SearchFailed) {
          return Container(
              height: MediaQuery.of(context).size.height / 1.5,
              child: CustomErrorWidget(
                showWarningIcon: false,
                msg: productSearchState.message,
              ));
        } else if (productSearchState is SearchLoading) {
          return SearchListLoader();
        }
        return SizedBox();
      },
    );
  }

  BlocBuilder<KeywordBloc, KeywordState> _buildkeywords() {
    return BlocBuilder<KeywordBloc, KeywordState>(
      builder: (context, keywordState) {
        if (keywordState is KeywordSuccess) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: keywordState.filtered
                .map((e) => InkWell(
                      onTap: () {
                        WidgetsBinding.instance.focusManager.primaryFocus
                            ?.unfocus();
                        _searchController.text = e;
                        BlocProvider.of<KeywordBloc>(context)
                            .add(ClearFilteredKeyword());
                        BlocProvider.of<SearchBloc>(context)
                            .add(SearchProductEvent(search: e));
                      },
                      child: Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: Palette.teritaryColor))),
                          padding: EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 15.0),
                          child: Text(
                            e,
                            style: Theme.of(context).textTheme.subtitle2,
                          )),
                    ))
                .toList(),
          );
        }
        return SizedBox();
      },
    );
  }
}
