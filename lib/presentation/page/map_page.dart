import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/user_location/user_location_bloc.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

GoogleMapController mapController;

// ignore: must_be_immutable
class MapPage extends StatelessWidget {
  String googleApikey = AppConfig.GOOGLE_MAP_KEY;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BlocBuilder<UserLocationBloc, UserLocationState>(
          builder: (context, userLocationState) {
            if (userLocationState is UserLocationSuccess) {
              return Padding(
                padding: EdgeInsets.all(15),
                child: InkWell(
                  onTap: () {
                    BlocProvider.of<UserLocationBloc>(context)
                        .add(SetCustomUserLocation(
                      lat: MapCameraSave.cameraPosition.target.latitude,
                      long: MapCameraSave.cameraPosition.target.longitude,
                    ));
                    Navigator.pop(context);
                  },
                  child: Card(
                    child: Container(
                        padding: EdgeInsets.all(0),
                        width: MediaQuery.of(context).size.width - 40,
                        child: ListTile(
                          trailing: Icon(Icons.done_rounded),
                          leading: Icon(
                            Icons.location_on,
                            color: Colors.blue,
                          ),
                          title: Text(
                            userLocationState.address.name +
                                " " +
                                userLocationState.address.street +
                                " " +
                                userLocationState.address.postalCode,
                            style: TextStyle(fontSize: 18),
                          ),
                          dense: true,
                        )),
                  ),
                ),
              );
            }
            return UIUtilWidget.buildCircularProgressIndicator();
          },
        ),
        appBar: AppBar(
          iconTheme: IconThemeData(color: Palette.onSecondary),
          backgroundColor: Palette.secondary,
          title: Text(
            tr('selectYourLocation'),
            style: Theme.of(context)
                .textTheme
                .subtitle1
                .copyWith(color: Palette.onSecondary),
          ),
        ),
        body: Container(
            height: MediaQuery.of(context).size.height,
            child: _buildGoogleMap(context)));
  }

  Widget _buildGoogleMap(BuildContext context) {
    double lat;
    double lng;
    UserLocationState userLocationState =
        BlocProvider.of<UserLocationBloc>(context).state;
    if (userLocationState is UserLocationSuccess) {
      lat = userLocationState.coordinates.latitude;
      lng = userLocationState.coordinates.longitude;
    }
    return Stack(children: [
      GoogleMap(
        myLocationEnabled: true,
        //Map widget from google_maps_flutter package
        zoomGesturesEnabled: true, //enable Zoom in, out on map
        initialCameraPosition: CameraPosition(
          //innital position in map
          target: LatLng(lat, lng), //initial position
          zoom: 14.0, //initial zoom level
        ),
        mapType: MapType.normal, //map type
        onMapCreated: (controller) {
          mapController = controller;
        },
        onCameraMove: (CameraPosition cameraPositiona) {
          MapCameraSave.cameraPosition = cameraPositiona;
        },
        onCameraIdle: () async {
          if (MapCameraSave.cameraPosition != null) {
            BlocProvider.of<UserLocationBloc>(context)
                .add(SetCustomUserLocation(
              lat: MapCameraSave.cameraPosition.target.latitude,
              long: MapCameraSave.cameraPosition.target.longitude,
            ));
          }
        },
      ),
      Center(
          //picker image on google map
          child: Icon(
        Icons.location_on,
        color: Colors.blue,
        size: 30,
      )),
    ]);
  }
}

class MapCameraSave {
  static CameraPosition cameraPosition;
}
