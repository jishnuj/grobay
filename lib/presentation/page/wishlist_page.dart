import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/product.dart';
import 'package:grobay/presentation/bloc/favourite/favourite_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/wishlistchangenotifier_cubit.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/product_card_widget.dart';
import 'package:grobay/presentation/widget/skelton_loaders.dart';

class WishListPage extends StatelessWidget {
  const WishListPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        CustomAppbar(
          showBanner: false,
          showLocation: false,
          showLogo: false,
          showTitle: true,
          title: tr('favourite'),
          showSearchField: false,
        ),
        BlocBuilder<UserloginBloc, UserloginState>(
          builder: (context, userLoginState) {
            if (userLoginState is UserLoginSuccess) {
              if (!(BlocProvider.of<FavouriteBloc>(context).state
                  is FavouriteSuccessGuest)) {
                BlocProvider.of<FavouriteBloc>(context).add(FetchFavouriteEvent(
                    userId: userLoginState.userResponse.user.userId));
              }
              return BlocBuilder<FavouriteBloc, FavouriteState>(
                builder: (context, favState) {
                  if (favState is FavouriteSuccess) {
                    return _buildUserFavourite(favState, userLoginState);
                  } else if (favState is FavouriteFailed) {
                    return SliverToBoxAdapter(
                        child: _buildErrorBox(favState, context));
                  }
                  return SliverToBoxAdapter(child: FavouriteListLoader());
                },
              );
            }

            if (userLoginState is UserloginInitial) {
              return BlocBuilder<FavouriteBloc, FavouriteState>(
                builder: (context, favState) {
                  if (favState is FavouriteSuccessGuest) {
                    return _buildGuestFavourite(favState);
                  } else if (favState is FavouriteFailed) {
                    return SliverToBoxAdapter(
                        child: _buildErrorBox(favState, context));
                  }
                  return SliverToBoxAdapter(
                    child: Container(
                      height: MediaQuery.of(context).size.height / 1.5,
                      child: Center(
                        child: Text(tr('noItemsInFavourite')),
                      ),
                    ),
                  );
                },
              );
            }
            return SliverToBoxAdapter(child: SizedBox());
          },
        ),
      ],
    );
  }

  SliverList _buildGuestFavourite(FavouriteSuccessGuest favState) {
    return SliverList(
        delegate: SliverChildBuilderDelegate((context, index) {
      return Dismissible(
        key: UniqueKey(),
        direction: DismissDirection.endToStart,
        onDismissed: (direction) {
          BlocProvider.of<FavouriteBloc>(context).add(RemoveFavouriteEvent(
              isGuest: true, productId: favState.products[index].id));
          BlocProvider.of<WishlistchangenotifierCubit>(context)
              .productWishListStatusChanged(favState.products[index], false);
        },
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 7.5),
          child: ProductCardWidget(
            showFavouriteIcon: false,
            product: favState.products[index],
          ),
        ),
      );
    }, childCount: favState.products.length));
  }

  SliverList _buildUserFavourite(
      FavouriteSuccess favState, UserLoginSuccess userLoginState) {
    return SliverList(
        delegate: SliverChildBuilderDelegate((context, index) {
      return Dismissible(
        key: UniqueKey(),
        direction: DismissDirection.endToStart,
        onDismissed: (direction) {
          BlocProvider.of<FavouriteBloc>(context).add(RemoveFavouriteEvent(
              product: _buildProduct(favState, index),
              isGuest: !(userLoginState is UserLoginSuccess),
              productId:
                  favState.favouriteResponse.favourites[index].productId));
          BlocProvider.of<WishlistchangenotifierCubit>(context)
              .productWishListStatusChanged(
                  _buildProduct(favState, index), false);
        },
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 7.5),
          child: ProductCardWidget(
            showFavouriteIcon: false,
            product: _buildProduct(favState, index),
          ),
        ),
      );
    }, childCount: int.parse(favState.favouriteResponse.total)));
  }

  Widget _buildErrorBox(FavouriteFailed favState, BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height - 200,
      child: Center(
        child: CustomErrorWidget(
          msg: favState.message,
        ),
      ),
    );
  }

  Product _buildProduct(FavouriteSuccess favState, int index) {
    return Product(
        isFavorite: favState.favouriteResponse.favourites[index].isFavorite,
        id: favState.favouriteResponse.favourites[index].productId,
        categoryId: favState.favouriteResponse.favourites[index].categoryId,
        subcategoryId:
            favState.favouriteResponse.favourites[index].subcategoryId,
        pincodes: "",
        returnStatus: favState.favouriteResponse.favourites[index].returnStatus,
        cancelableStatus:
            favState.favouriteResponse.favourites[index].cancelableStatus,
        image: favState.favouriteResponse.favourites[index].image,
        otherImages: favState.favouriteResponse.favourites[index].otherImages,
        madeIn: favState.favouriteResponse.favourites[index].madeIn,
        manufacturer: favState.favouriteResponse.favourites[index].manufacturer,
        description: favState.favouriteResponse.favourites[index].description,
        name: favState.favouriteResponse.favourites[index].name,
        variants: favState.favouriteResponse.favourites[index].variants);
  }
}
