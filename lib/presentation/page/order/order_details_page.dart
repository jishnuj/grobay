import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grobay/api/response/order_fetch_response.dart';
import 'package:grobay/core/constant/app_constants.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/order/order_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/cubit/order_cubit.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';

import 'package:timeline_tile/timeline_tile.dart';

class OrderDetailsPage extends StatelessWidget {
  final Order order;
  const OrderDetailsPage({Key key, this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
        child: BlocConsumer<CartCubit, CartCubitState>(
          listener: (context, state) {
            if (state.isReorderSuccess == true) {
              Navigator.pushNamed(context, "cart");
            } else if (state.isReorderFailed == true) {
              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text(state.errMsg)));
            }
          },
          builder: (context, state) {
            if (state.isLoading) {
              return Center(child: CircularProgressIndicator());
            }
            return ElevatedButton(
              child: Text(tr('reOrder')),
              onPressed: () {
                BlocProvider.of<CartCubit>(context).reorder(order);
              },
            );
          },
        ),
      ),
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            CustomAppbar(
              title: "${tr('orderDetails')} - ${order.id}",
              showAction: false,
              showLocation: false,
              showBanner: false,
              showSearchField: false,
              showTitle: true,
            ),
            SliverToBoxAdapter(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Palette.orderDetailsPageOtpBoxColor),
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      child: Center(
                        child: Text(
                          "${tr("shareOtpWithDeliveryAgent")} ${order.otp}",
                          style: Theme.of(context).textTheme.subtitle2,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.5,
                    ),
                    Divider(),
                    SizedBox(
                      height: 7.5,
                    ),
                    BlocBuilder<OrderBloc, OrderState>(
                      builder: (context, orderUpdateState) {
                        if (orderUpdateState is OrderUpdateLoading) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        return Column(
                          children: order.items
                              .map((e) => _buildProductCard(context, order, e))
                              .toList(),
                        );
                      },
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    _buildPriceDetails(context),
                    SizedBox(
                      height: 20.0,
                    ),
                    _buildAddress(context),
                    SizedBox(
                      height: 20.0,
                    ),
                    if (order.orderNote.trim().isNotEmpty)
                      Card(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              tr("specialNote"),
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Text(order.orderNote)
                          ],
                        ),
                      )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Container _buildAddress(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            tr('address').toUpperCase(),
            style: Theme.of(context).textTheme.subtitle1,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text(
            order.address,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ],
      ),
    );
  }

  Column _buildPriceDetails(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('priceDetails'),
          style: Theme.of(context).textTheme.subtitle1,
        ),
        SizedBox(
          height: 10.0,
        ),
        _buildPriceColumn(tr('itemsAmount'), order.total, context,
            leadingCharacter: "+"),
        _buildPriceColumn(tr('deliveryCharge'), order.deliveryCharge, context,
            leadingCharacter: "+"),
        _buildPriceColumn(
            tr('total'),
            (double.parse(order.total) + double.parse(order.deliveryCharge))
                .toString(),
            context,
            valueStyle:
                Theme.of(context).textTheme.headline6.copyWith(fontSize: 18),
            titleStyle:
                Theme.of(context).textTheme.headline6.copyWith(fontSize: 18)),
        SizedBox(
          height: 10,
        ),
        _buildPriceColumn(tr('discount'), order.discount, context,
            leadingCharacter: "-"),
        _buildPriceColumn(tr('promoCodeDiscount'), order.promoDiscount, context,
            leadingCharacter: "-"),
        _buildPriceColumn(tr('walletBalance'), order.walletBalance, context,
            leadingCharacter: "-"),
        SizedBox(
          height: 10,
        ),
        _buildPriceColumn(tr('finalTotal'), order.finalTotal, context,
            titleStyle: Theme.of(context).textTheme.headline6,
            valueStyle: Theme.of(context).textTheme.headline6),
      ],
    );
  }

  Widget _buildProductCard(
      BuildContext context, Order order, OrderedProduct e) {
    List<List<String>> placeholder = [
      [AppConstants.ORDER_PROCESSED, ""],
      [AppConstants.ORDER_SHIPPED, ""],
      [AppConstants.ORDER_DELIVERED, ""],
    ];

    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  CachedNetworkImage(height: 80, width: 80, imageUrl: e.image),
                  SizedBox(
                    width: 15.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        e.name,
                        style: Theme.of(context).textTheme.subtitle2,
                      ),
                      Text(
                        "${tr('qty')} ${e.quantity}",
                        style: Theme.of(context).textTheme.subtitle2,
                      ),
                      Text(
                        '${CurrencyWidget.getFixedPriceCurrencyString(double.parse(e.subTotal))}  ',
                        style: Theme.of(context).textTheme.subtitle2,
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        "${tr('via')} ${order.paymentMethod.toUpperCase()}",
                        style: Theme.of(context).textTheme.subtitle2,
                      )
                    ],
                  ),
                ],
              ),
              if (BlocProvider.of<OrderCubit>(context)
                  .checkIfCancellable(order, e))
                Container(
                  margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                  child: ElevatedButton(
                      onPressed: () {
                        BlocProvider.of<OrderBloc>(context).add(
                            UpdateOrderEvent(
                                orderId: e.orderId,
                                orderItemId: e.id,
                                status: AppConstants.ORDER_CANCELLED));
                      },
                      child: Text(tr('cancel'))),
                ),
              if (BlocProvider.of<OrderCubit>(context)
                  .checkIfReturnable(order, e))
                Container(
                  margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                  child: ElevatedButton(
                      onPressed: () {
                        BlocProvider.of<OrderBloc>(context).add(
                            UpdateOrderEvent(
                                orderId: e.orderId,
                                orderItemId: e.id,
                                status: AppConstants.ORDER_RETURNED));
                      },
                      child: Text(tr('return'))),
                )
            ],
          ),
          if (!BlocProvider.of<OrderCubit>(context)
                  .checkIfAlreadyCancelled(e) &&
              !BlocProvider.of<OrderCubit>(context).checkIfAlreadyreturned(e))
            Container(
              margin: EdgeInsets.only(top: 30),
              height: 100,
              child: Center(
                child: ListView(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  children: e.status.length != 4
                      ? (e.status +
                              placeholder.sublist(
                                  e.status.length - 1, placeholder.length))
                          .map((st) => _buildTimeLine(
                              st[0],
                              st[1],
                              (e.status + placeholder.sublist(e.status.length - 1, placeholder.length))
                                  .indexOf(st),
                              (e.status +
                                      placeholder.sublist(e.status.length - 1,
                                          placeholder.length))
                                  .length,
                              context))
                          .toList()
                      : e.status
                          .map((st) => _buildTimeLine(
                              st[0], st[1], e.status.indexOf(st), e.status.length, context))
                          .toList(),
                ),
              ),
            ),
          if (BlocProvider.of<OrderCubit>(context).checkIfAlreadyCancelled(e))
            Container(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                child: Text(AppConstants.ORDER_CANCELLED.toUpperCase(),
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1
                        .copyWith(color: Colors.red))),
          if (BlocProvider.of<OrderCubit>(context).checkIfAlreadyreturned(e))
            Container(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                child: Text(AppConstants.ORDER_RETURNED.toUpperCase(),
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1
                        .copyWith(color: Colors.red))),
        ],
      ),
    );
  }

  Row _buildPriceColumn(String title, String value, BuildContext context,
      {String leadingCharacter, TextStyle titleStyle, TextStyle valueStyle}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: titleStyle ?? Theme.of(context).textTheme.subtitle2,
        ),
        Row(
          children: [
            if (leadingCharacter != null)
              Text(
                leadingCharacter + "  ",
                style: Theme.of(context).textTheme.subtitle1,
              ),
            Text(
              '${CurrencyWidget.getFixedPriceCurrencyString(double.parse(value))}  ',
              style: valueStyle ?? Theme.of(context).textTheme.subtitle2,
            )
          ],
        )
      ],
    );
  }

  Widget _buildTimeLine(String title, String date, int index, int totalCount,
      BuildContext context) {
    return TimelineTile(
      alignment: TimelineAlign.start,
      axis: TimelineAxis.horizontal,
      lineXY: 0.1,
      isFirst: index == 0,
      isLast: index == totalCount - 1,
      indicatorStyle: IndicatorStyle(
        width: 30,
        height: 30,
        indicator: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.fromBorderSide(
              BorderSide(
                color:
                    date.isEmpty ? Colors.black.withOpacity(0.2) : Colors.blue,
                width: 4,
              ),
            ),
          ),
          child: Center(
            child: Text((index + 1).toString(),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle2),
          ),
        ),
        drawGap: true,
      ),
      beforeLineStyle: LineStyle(
        color: date.isEmpty
            ? Colors.black.withOpacity(0.2)
            : Colors.blue.withOpacity(0.7),
      ),
      endChild: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "  " + title.toUpperCase().trim() + "  ",
              textAlign: TextAlign.center,
              style: GoogleFonts.jura(
                  color: Colors.black,
                  fontSize: 13,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              "  " + date.split(" ").first.trim()+ "  ",
              textAlign: TextAlign.center,
              style: GoogleFonts.jura(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
            ),
            Text(
              "  " + date.split(" ").last.trim() + "  ",
              textAlign: TextAlign.center,
              style: GoogleFonts.jura(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
