import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:grobay/api/model/time_slots.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/core/constant/payment_methods.dart';
import 'package:grobay/presentation/bloc/cart/cart_bloc.dart';
import 'package:grobay/presentation/bloc/settings/settings_bloc.dart';
import 'package:grobay/presentation/bloc/user_address/useraddress_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/settings_cubit.dart';
import 'package:grobay/presentation/widget/cart_footer_widget.dart';
import 'package:grobay/presentation/widget/order_confirm_widget.dart';
import 'package:grobay/presentation/widget/payment_widgets/payment_select_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class OrderTimePaymentSelectPage extends StatelessWidget {
  const OrderTimePaymentSelectPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CartBloc.steps = STEPS.ORDER;
    BlocProvider.of<SettingsCubit>(context)
        .setDate(DateTime.now().add(Duration(days: 0)).toString(), 0);
    BlocProvider.of<SettingsBloc>(context).add(FetchSettings());
    BlocProvider.of<UserloginBloc>(context).add(ReloadUserDetails());
    return Scaffold(
      bottomNavigationBar: _buildCheckoutCard(context),
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: true,
              backgroundColor: Palette.onPrimary,
              collapsedHeight: 60,
              title: Text(
                tr('payment'),
                style: Theme.of(context).accentTextTheme.headline6.copyWith(
                      color: Palette.onSecondary,
                    ),
              ),
            ),
            _buildBody(context)
          ],
        ),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SliverToBoxAdapter(child: BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, settingsState) {
        if (settingsState is SettingsLoading) {
          return Container(
              height: MediaQuery.of(context).size.height / 2,
              child: Center(
                child: CircularProgressIndicator(),
              ));
        }
        return BlocBuilder<SettingsCubit, SettingsCubitState>(
          builder: (context, state) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _buildWalletBalance(context),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 15.0),
                  child: Text(
                    tr('preferedDeliveryTime'),
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                _buildTimeSlotsConfig(context),
                _buildTimeSlots(),
                SizedBox(
                  height: 15.0,
                ),
                BlocBuilder<CartBloc, CartState>(
                  builder: (context, cartState) {
                    if (cartState is CartSuccess && cartState.fullWalletPay) {
                      return SizedBox();
                    }
                    return Container(
                      color: Palette.secondary,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 15.0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 15.0),
                            child: Text(
                              tr('paymentMethods'),
                              style: Theme.of(context).textTheme.headline6,
                            ),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 15.0),
                            child: _buildPaymentMethods(),
                          ),
                          SizedBox(
                            height: 30,
                          )
                        ],
                      ),
                    );
                  },
                )
              ],
            );
          },
        );
      },
    ));
  }

  BlocBuilder<UserloginBloc, UserloginState> _buildWalletBalance(
      BuildContext context) {
    UseraddressSuccess useraddressSuccess =
        BlocProvider.of<UseraddressBloc>(context).state;
    return BlocBuilder<UserloginBloc, UserloginState>(
      builder: (context, userState) {
        if (userState is UserLoginSuccess &&
            double.parse(userState.userResponse.user.balance) > 0) {
          return BlocBuilder<CartBloc, CartState>(
            builder: (context, cartState) {
              return BlocBuilder<SettingsCubit, SettingsCubitState>(
                builder: (context, settingState) {
                  return Card(
                    margin: EdgeInsets.symmetric(horizontal: 15.0),
                    child: CheckboxListTile(
                        value: (settingState.paymentMethod ==
                                PAYMENT_METHODS.WALLET) &&
                            (cartState is CartSuccess && cartState.useWallet),
                        onChanged: (val) {
                          BlocProvider.of<SettingsCubit>(context)
                              .setPaymentMethod(
                                  val ? PAYMENT_METHODS.WALLET : null);
                          BlocProvider.of<CartBloc>(context).add(
                              CartToggleWalletEvent(
                                  address: useraddressSuccess.selectedAddress,
                                  walletBalance:
                                      userState.userResponse.user.balance));
                        },
                        title: Text(
                            (cartState is CartSuccess && cartState.useWallet)
                                ? '${tr('wallet')} ${AppConfig.CURRENCY} ${cartState.walletBalance}'
                                : "${tr('wallet')}  ${AppConfig.CURRENCY} ${userState.userResponse.user.balance}",
                            style: Theme.of(context)
                                .textTheme
                                .headline6
                                .copyWith(color: Colors.grey))),
                  );
                },
              );
            },
          );
        }
        return Container();
      },
    );
  }

  bool checkIfDateAvailable(BuildContext context, TimeSlots e) {
    if (BlocProvider.of<SettingsCubit>(context).state.date != null) {
      DateTime selectedDate =
          DateTime.parse(BlocProvider.of<SettingsCubit>(context).state.date);
      DateTime selectedDateTime = DateTime.parse(
          "${selectedDate.toString().split(" ")[0]} ${e.lastOrderTime}");
      if (selectedDateTime.isAfter(DateTime.now())) {
        return true;
      }
    }
    return false;
  }

  Widget _buildTimeSlots() {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, settingsState) {
        if (settingsState is SettingsSuccess) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            // decoration: BoxDecoration(border: Border.all(color: Colors.black)),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: settingsState.timeSlotsResponse.timeSlots
                    .map((e) => Card(
                          child: RadioListTile(
                              activeColor: Palette.buttonBg,
                              value: e,
                              groupValue:
                                  BlocProvider.of<SettingsCubit>(context)
                                      .state
                                      .timeSlots,
                              onChanged: (val) {
                                if (this.checkIfDateAvailable(context, e)) {
                                  BlocProvider.of<SettingsCubit>(context)
                                      .setTime(e);
                                } else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                          content: Text(
                                              tr('timeSlotNotAvailable'))));
                                }
                              },
                              title: Text(
                                e.title,
                                style: this.checkIfDateAvailable(context, e)
                                    ? Theme.of(context)
                                        .textTheme
                                        .subtitle2
                                        .copyWith(color: Colors.black)
                                    : Theme.of(context)
                                        .textTheme
                                        .subtitle2
                                        .copyWith(color: Colors.grey),
                              )),
                        ))
                    .toList()),
          );
        }
        return SizedBox();
      },
    );
  }

  Widget _buildTimeSlotsConfig(BuildContext context) {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, settingsState) {
        if (settingsState is SettingsSuccess) {
          return Container(
              padding: EdgeInsets.all(10),
              height: 150,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return _buildTimeSlotConfigCard(context, index);
                },
                itemCount: int.parse(settingsState
                        .timeSlotsConfigResponse.timeSlotConfig.allowedDays) +
                    1,
              ));
        }
        return SizedBox();
      },
    );
  }

  InkWell _buildTimeSlotConfigCard(BuildContext context, int index) {
    return InkWell(
      onTap: () {
        BlocProvider.of<SettingsCubit>(context).setDate(
            DateTime.now().add(Duration(days: index)).toString(), index);
      },
      child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          padding: const EdgeInsets.all(8.0),
          width: 80,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: BlocProvider.of<SettingsCubit>(context).state.dateIndex ==
                      index
                  ? Palette.buttonBg
                  : Colors.white,
              border: Border.all(color: Colors.grey)),
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                DateFormat("EE dd")
                    .format(DateTime.now().add(Duration(days: index))),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle2.copyWith(
                      color: BlocProvider.of<SettingsCubit>(context)
                                  .state
                                  .dateIndex ==
                              index
                          ? Colors.white
                          : Colors.black,
                    ),
              ),
              Text(
                DateFormat("MMM")
                    .format(DateTime.now().add(Duration(days: index))),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle2.copyWith(
                      color: BlocProvider.of<SettingsCubit>(context)
                                  .state
                                  .dateIndex ==
                              index
                          ? Colors.white
                          : Colors.black,
                    ),
              ),
            ],
          ))),
    );
  }

  Widget _buildPaymentMethods() {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, settingsState) {
        if (settingsState is SettingsSuccess) {
          return PaymentSelectWidget(
            enableCod: settingsState
                    .paymentSettingsResponse.paymentMethods.codPaymentMethod
                    .trim() ==
                "1",
            enablePayumoney: settingsState.paymentSettingsResponse
                    .paymentMethods.payumoneyPaymentMethod
                    .trim() ==
                "1",
            enableMidtrans: settingsState.paymentSettingsResponse.paymentMethods
                    .midtransPaymentMethod
                    .trim() ==
                "1",
            enablePaystack: settingsState.paymentSettingsResponse.paymentMethods
                    .paystackPaymentMethod
                    .trim() ==
                "1",
            enablePaypal: settingsState
                    .paymentSettingsResponse.paymentMethods.paypalPaymentMethod
                    .trim() ==
                "1",
            enableRazorpay: settingsState.paymentSettingsResponse.paymentMethods
                    .razorpayPaymentMethod
                    .trim() ==
                "1",
            enableFlutterWave: settingsState.paymentSettingsResponse
                    .paymentMethods.flutterwavePaymentMethod
                    .trim() ==
                "1",
            enableStripe: settingsState
                    .paymentSettingsResponse.paymentMethods.stripePaymentMethod
                    .trim() ==
                "1",
            enablePaytm: settingsState
                    .paymentSettingsResponse.paymentMethods.paytmPaymentMethod
                    .trim() ==
                "1",
            enableSslEcommerce: settingsState
                    .paymentSettingsResponse.paymentMethods.paytmPaymentMethod
                    .trim() ==
                "1",
          );
        }
        return SizedBox();
      },
    );
  }

  Widget _buildCheckoutCard(BuildContext context) {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, settingsState) {
        if (settingsState is SettingsLoading) {
          return UIUtilWidget.verticalSpaceCustom(0.0);
        }
        return CartFooterWidget(
          cartFooterType: CartFooterType.PAYMENT,
          onPressed: () {
            UIUtilWidget.clearAllSnackBars();
            SettingsCubitState settingsCubitState =
                BlocProvider.of<SettingsCubit>(context).state;
            SettingsState settingsState =
                BlocProvider.of<SettingsBloc>(context).state;
            if ((settingsState is SettingsSuccess &&
                settingsState.timeSlotsResponse.timeSlots.length > 0 &&
                settingsCubitState.timeSlots == null)) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(tr('pleaseSelectTimeSlot')),
              ));
            } else if ((settingsCubitState.paymentMethod == null)) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(tr('pleaseSelectPaymentType')),
              ));
            } else {
              showOrderDialog(context);
            }
          },
        );
      },
    );
  }

  void showOrderDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return OrderConfirmWidget();
        });
  }
}
