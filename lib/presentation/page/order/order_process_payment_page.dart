import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/payment_methods.dart';
import 'package:grobay/presentation/bloc/cart/cart_bloc.dart';
import 'package:grobay/presentation/bloc/order/order_bloc.dart';
import 'package:grobay/presentation/bloc/payment_confirm/payment_confirm_bloc.dart';
import 'package:grobay/presentation/bloc/settings/settings_bloc.dart';
import 'package:grobay/presentation/bloc/user_address/useraddress_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/cubit/settings_cubit.dart';
import 'package:grobay/presentation/page/home_page.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/payment_widgets/blocs/create_stripe_payment/create_stripe_payment_bloc.dart';
import 'package:grobay/presentation/widget/payment_widgets/payment_widget.dart';
import 'package:logger/logger.dart';

class OrderProcessPaymentpage extends StatelessWidget {
  const OrderProcessPaymentpage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    CartState cartSuccess = BlocProvider.of<CartBloc>(context).state;
    UseraddressSuccess useraddressSuccess =
        BlocProvider.of<UseraddressBloc>(context).state;
    UserLoginSuccess userLoginSuccess =
        BlocProvider.of<UserloginBloc>(context).state;
    SettingsCubitState settingsCubitState =
        BlocProvider.of<SettingsCubit>(context).state;
    return Scaffold(
      body: SafeArea(
        child: BlocConsumer<OrderBloc, OrderState>(
          listener: (context, orderState) {
            if (orderState is OrderCreateSuccess) {
              BlocProvider.of<CreateStripePaymentBloc>(context)
                  .add(ResetCreateStripePayment());
              this.createTransaction(context, orderState.taxid,
                  cartSuccess: cartSuccess,
                  userLoginSuccess: userLoginSuccess,
                  useraddressSuccess: useraddressSuccess,
                  settingsCubitState: settingsCubitState,
                  orderCreateSuccess: orderState);
            }
          },
          builder: (context, orderState) {
            if (orderState is OrderCreateSuccess) {
              return BlocBuilder<PaymentConfirmBloc, PaymentConfirmState>(
                builder: (context, paymentConfirm) {
                  if (paymentConfirm is PaymentConfirmSuccess) {
                    BlocProvider.of<CartBloc>(context).add(FetchUserCartData(
                        cartCubit: BlocProvider.of<CartCubit>(context)));
                    BlocProvider.of<SettingsCubit>(context).reset();
                    BlocProvider.of<CartCubit>(context).clearAll();
                    return _buildOrderSuccess(context);
                  } else if (paymentConfirm is PaymentConfirmFailed) {
                    return _buildPaymentConfirmfailed(context, paymentConfirm);
                  }
                  return _buildLoading(context, tr('confirmingPayment'));
                },
              );
            } else if (orderState is OrderCreateFailed) {
              return _buildOrderCreateError(context, orderState);
            } else if (orderState is OrderCreateLoading) {
              return _buildLoading(context, tr('creatingOrder'));
            }
            if (BlocProvider.of<CartBloc>(context).state is CartSuccess) {
              return Container(
                  child: _buildBody(context,
                      cartSuccess: cartSuccess,
                      userLoginSuccess: userLoginSuccess,
                      useraddressSuccess: useraddressSuccess,
                      settingsCubitState: settingsCubitState));
            }
            return SizedBox();
          },
        ),
      ),
    );
  }

  Container _buildPaymentConfirmfailed(
      BuildContext context, PaymentConfirmFailed paymentConfirm) {
    return Container(
        height: MediaQuery.of(context).size.height - 50,
        child: Column(
          children: [
            Text(paymentConfirm.message),
            ElevatedButton(
                onPressed: () {
                  //How to handle things here
                },
                child: Text(tr('ok')))
          ],
        ));
  }

  Container _buildOrderCreateError(
      BuildContext context, OrderCreateFailed orderState) {
    return Container(
        height: MediaQuery.of(context).size.height - 50,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(orderState.message),
            ElevatedButton(
                onPressed: () {
                  //How to handle things here
                },
                child: Text(tr('ok')))
          ],
        ));
  }

  Container _buildLoading(BuildContext context, String text) {
    return Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15.0),
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              tr('dontPressBackButton'),
              style: Theme.of(context).textTheme.headline6,
            ),
            SizedBox(
              height: 20.0,
            ),
            CircularProgressIndicator(),
            SizedBox(
              height: 20.0,
            ),
            Text(
              text,
              style: Theme.of(context).textTheme.subtitle2,
            )
          ],
        ));
  }

  ElevatedButton _buildOrderSuccessViewOrderDetailsButton(
      BuildContext context) {
    return ElevatedButton(
        onPressed: () {
          BlocProvider.of<PaymentConfirmBloc>(context)
              .add(ResetPaymentConfirm());

          Navigator.pushAndRemoveUntil(context,
              MaterialPageRoute(builder: (context) {
            return HomePage(
              index: 3,
            );
          }), (route) => false);
        },
        child: Text(
          tr('viewOrderDetails'),
          style: Theme.of(context)
              .textTheme
              .subtitle1
              .copyWith(color: Colors.white),
        ));
  }

  ElevatedButton _buildOrderSuccessContinueButton(BuildContext context) {
    return ElevatedButton(
        onPressed: () {
          BlocProvider.of<PaymentConfirmBloc>(context)
              .add(ResetPaymentConfirm());
          Navigator.pushAndRemoveUntil(context,
              MaterialPageRoute(builder: (context) {
            return HomePage(
              index: 0,
            );
          }), (route) => false);
        },
        child: Text(
          tr('continueShopping'),
          style: Theme.of(context)
              .textTheme
              .subtitle1
              .copyWith(color: Colors.white),
        ));
  }

  Container _buildOrderSuccess(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.done_all,
            size: 100,
          ),
          Text(
            tr('orderSuccess'),
            style: Theme.of(context).textTheme.headline3,
          ),
          SizedBox(
            height: 20,
          ),
          _buildOrderSuccessContinueButton(context),
          SizedBox(
            height: 20,
          ),
          _buildOrderSuccessViewOrderDetailsButton(context),
        ],
      ),
    );
  }

  Map<String, dynamic> _createRazorpayConfig(SettingsSuccess settingsState) {
    return {
      "key": settingsState.paymentSettingsResponse.paymentMethods.razorpayKey,
      "key_secret": settingsState
          .paymentSettingsResponse.paymentMethods.razorpaySecretKey,
      "currency": AppConfig.CURRENCY,
      "razorpayMode":
          settingsState.paymentSettingsResponse.paymentMethods.isProduction
    };
  }

  Map<String, dynamic> _createPayStackConfig(SettingsSuccess settingsState) {
    Logger().e(
        settingsState.paymentSettingsResponse.paymentMethods.paystackPublicKey);
    return {
      "public_key": settingsState
          .paymentSettingsResponse.paymentMethods.paystackPublicKey,
      "private_key": settingsState
          .paymentSettingsResponse.paymentMethods.paystackSecretKey,
      "currency": AppConfig.CURRENCY,
      "paystackMode":
          settingsState.paymentSettingsResponse.paymentMethods.isProduction
    };
  }

  Map<String, dynamic> _createFlutterwaveConfig(SettingsSuccess settingsState) {
    return {
      "public_key": settingsState
          .paymentSettingsResponse.paymentMethods.flutterwavePublicKey,
      "private_key": settingsState
          .paymentSettingsResponse.paymentMethods.flutterwaveSecretKey,
      "enc_key": settingsState
          .paymentSettingsResponse.paymentMethods.flutterwaveEncryptionKey,
      "flutterWaveMode":
          settingsState.paymentSettingsResponse.paymentMethods.isProduction,
      "currency_code": settingsState
          .paymentSettingsResponse.paymentMethods.flutterwaveCurrencyCode
    };
  }

  Map<String, dynamic> _createPaypalConfig(
      context, SettingsSuccess settingsState) {
    CartSuccess cartSuccess = BlocProvider.of<CartBloc>(context).state;
    String itemName = "";
    cartSuccess.cartResponse.cart.data.forEach((element) {
      itemName += (element.item[0].name + ",");
    });
    return {
      "currency_code": settingsState
          .paymentSettingsResponse.paymentMethods.paypalCurrencyCode,
      "paypalMode":
          settingsState.paymentSettingsResponse.paymentMethods.paypalMode,
      "email": settingsState
          .paymentSettingsResponse.paymentMethods.paypalBusinessEmail,
      "itemName": "${AppConfig.APP_NAME}  $itemName",
      "quantity": cartSuccess.cartResponse.cart.data.length.toString()
    };
  }

  createStripeConfig(BuildContext context, SettingsSuccess settingsState) {
    return {
      "stripeMode":
          settingsState.paymentSettingsResponse.paymentMethods.isProduction
    };
  }

  Map<String, dynamic> _createPayUConfig(
      context, SettingsSuccess settingsState) {
    CartSuccess cartSuccess = BlocProvider.of<CartBloc>(context).state;
    String itemName = "";
    cartSuccess.cartResponse.cart.data.forEach((element) {
      itemName += (element.item[0].name + ",");
    });
    return {
      "mearchantKey": settingsState
          .paymentSettingsResponse.paymentMethods.payumoneyMerchantKey,
      "mearchantId": settingsState
          .paymentSettingsResponse.paymentMethods.payumoneyMerchantId,
      "salt":
          settingsState.paymentSettingsResponse.paymentMethods.payumoneySalt,
      "payuMoneyMode":
          settingsState.paymentSettingsResponse.paymentMethods.payumoneyMode,
      "currency": AppConfig.CURRENCY,
      "itemName": "${AppConfig.APP_NAME}  $itemName",
    };
  }

  Map<String, dynamic> _createMidtransConfig(SettingsSuccess settingsState) {
    return {
      "client_key": settingsState
          .paymentSettingsResponse.paymentMethods.midtransClientKey,
      "server_key": settingsState
          .paymentSettingsResponse.paymentMethods.midtransServerKey,
      "merchant_id": settingsState
          .paymentSettingsResponse.paymentMethods.midtransMerchantId,
      "currency": AppConfig.CURRENCY,
      "midtransMode":
          settingsState.paymentSettingsResponse.paymentMethods.isProduction
    };
  }

  Map<String, dynamic> _createPaytmConfig(SettingsSuccess settingsState) {
    return {
      "mearchantId":
          settingsState.paymentSettingsResponse.paymentMethods.paytmMerchantId,
      "mearchantKey":
          settingsState.paymentSettingsResponse.paymentMethods.paytmMerchantKey,
      "paytmMode":
          settingsState.paymentSettingsResponse.paymentMethods.paytmMode,
    };
  }

  Map<String, dynamic> _createSSLCommerzConfig(SettingsSuccess settingsState) {
    return {
      "secret": settingsState
          .paymentSettingsResponse.paymentMethods.sslCommereceSecretKey,
      "storeId": settingsState
          .paymentSettingsResponse.paymentMethods.sslCommereceStoreId,
      "mode":
          settingsState.paymentSettingsResponse.paymentMethods.sslCommereceMode,
    };
  }

  String _calculateFinalTotal(
      CartSuccess cartSuccess, UseraddressSuccess useraddressSuccess) {
    return CurrencyWidget.getFixedDoubleString(cartSuccess.useWallet
        ? cartSuccess.fullWalletPay
            ? 0.0
            : cartSuccess.priceAfterWalletBalanceAdded
        : (double.parse(useraddressSuccess.selectedAddress.deliveryCharges) +
            cartSuccess.discountedTotalPrice));
  }

  String _getWalletBalanceUsed(
      CartSuccess cartSuccess, UserLoginSuccess userLoginSuccess) {
    return CurrencyWidget.getFixedDoubleString(cartSuccess.useWallet
        ? (double.parse(userLoginSuccess.userResponse.user.balance) -
            cartSuccess.walletBalance)
        : 0.0);
  }

  String _getSubTotal(CartSuccess cartSuccess) {
    return CurrencyWidget.getFixedDoubleString(cartSuccess.totalPrice);
  }

  String _getDeliveryCharge(UseraddressSuccess useraddressSuccess) {
    return CurrencyWidget.getFixedDoubleString(
        double.parse(useraddressSuccess.selectedAddress.deliveryCharges));
  }

  void _onPaymentCompleted(context, options,
      {CartSuccess cartSuccess,
      UserLoginSuccess userLoginSuccess,
      UseraddressSuccess useraddressSuccess,
      SettingsCubitState settingsCubitState}) async {
    Logger().i(options);
    this.createOrder(
      context,
      options['paymentId'],
      cartSuccess: cartSuccess,
      userLoginSuccess: userLoginSuccess,
      useraddressSuccess: useraddressSuccess,
      settingsCubitState: settingsCubitState,
    );
  }

  void _onPaymentCancelled(BuildContext context, String msg) {
    Navigator.pop(context);
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(msg ?? tr('paymentFailed')),
    ));
  }

  Widget _buildBody(BuildContext context,
      {CartSuccess cartSuccess,
      UserLoginSuccess userLoginSuccess,
      UseraddressSuccess useraddressSuccess,
      SettingsCubitState settingsCubitState}) {
    if (cartSuccess is CartSuccess) {
      return BlocBuilder<SettingsBloc, SettingsState>(
        builder: (context, settingsState) {
          if (settingsState is SettingsSuccess) {
            if (!cartSuccess.fullWalletPay) {
              return _buildPaymentWidget(
                  cartSuccess,
                  userLoginSuccess,
                  useraddressSuccess,
                  settingsCubitState,
                  context,
                  settingsState);
            } else {
              this.createOrder(
                context,
                "",
                cartSuccess: cartSuccess,
                userLoginSuccess: userLoginSuccess,
                useraddressSuccess: useraddressSuccess,
                settingsCubitState: settingsCubitState,
              );
            }
          }

          return _buildLoading(context, tr('creatingOrder'));
        },
      );
    }
    return SizedBox();
  }

  PaymentWidget _buildPaymentWidget(
      CartSuccess cartSuccess,
      UserLoginSuccess userLoginSuccess,
      UseraddressSuccess useraddressSuccess,
      SettingsCubitState settingsCubitState,
      BuildContext context,
      SettingsSuccess settingsState) {
    return PaymentWidget(
      onPaymentComplete: (context, options) => this._onPaymentCompleted(
        context,
        options,
        cartSuccess: cartSuccess,
        userLoginSuccess: userLoginSuccess,
        useraddressSuccess: useraddressSuccess,
        settingsCubitState: settingsCubitState,
      ),
      onPaymentCancelled: (msg) => this._onPaymentCancelled(context, msg),
      options: {
        'razorpay': this._createRazorpayConfig(settingsState),
        'paystack': this._createPayStackConfig(settingsState),
        'flutterwave': this._createFlutterwaveConfig(settingsState),
        'paypal': this._createPaypalConfig(context, settingsState),
        "stripe": this.createStripeConfig(context, settingsState),
        'payu': this._createPayUConfig(context, settingsState),
        'midtrans': this._createMidtransConfig(settingsState),
        'paytm': this._createPaytmConfig(settingsState),
        'sslcommerz': this._createSSLCommerzConfig(settingsState),
        //OTHER DETAILS
        "username": userLoginSuccess.userResponse.user.email,
        "address": useraddressSuccess.selectedAddress.address,
        "city": useraddressSuccess.selectedAddress.city,
        "pincode": useraddressSuccess.selectedAddress.pincode,
        "state": useraddressSuccess.selectedAddress.state,
        "country": useraddressSuccess.selectedAddress.country,
        'amount': double.parse(
            this._calculateFinalTotal(cartSuccess, useraddressSuccess)),
        'name': userLoginSuccess.userResponse.user.name,
        'description':
            "${AppConfig.APP_NAME} Cart Checkout Payment for ${this._calculateFinalTotal(cartSuccess, useraddressSuccess)}",
        'contact': userLoginSuccess.userResponse.user.mobile,
        'email': userLoginSuccess.userResponse.user.email
      },
      selectedMethod: settingsCubitState.paymentMethod,
    );
  }

  void createTransaction(BuildContext context, String taxid,
      {CartSuccess cartSuccess,
      UserLoginSuccess userLoginSuccess,
      UseraddressSuccess useraddressSuccess,
      SettingsCubitState settingsCubitState,
      OrderCreateSuccess orderCreateSuccess}) {
    if (cartSuccess is CartSuccess) {
      BlocProvider.of<PaymentConfirmBloc>(context).add(
          FetchCreatePaymentConfirm(
              paymentMethod: settingsCubitState.paymentMethod.name,
              taxId: taxid,
              taxPercentage: "0.0",
              orderId: orderCreateSuccess.orderSaveResponse.orderId,
              email: userLoginSuccess.userResponse.user.email,
              finalTotal:
                  this._calculateFinalTotal(cartSuccess, useraddressSuccess)));
    }
  }

  void createOrder(BuildContext context, String txid,
      {CartSuccess cartSuccess,
      UserLoginSuccess userLoginSuccess,
      UseraddressSuccess useraddressSuccess,
      SettingsCubitState settingsCubitState}) {
    if (cartSuccess is CartSuccess) {
      List<String> qty = [];
      BlocProvider.of<CartCubit>(context)
          .getVaraintNonDuplicateArray()
          .forEach((vid) {
        qty.add(BlocProvider.of<CartCubit>(context)
            .getVariantsCount(vid)
            .toString());
      });
      ///////
      BlocProvider.of<OrderBloc>(context).add(
        _createOrderEventModel(txid, userLoginSuccess, cartSuccess,
            useraddressSuccess, settingsCubitState, qty, context),
      );
    }
  }

  CreateOrderEvent _createOrderEventModel(
      String txid,
      UserLoginSuccess userLoginSuccess,
      CartSuccess cartSuccess,
      UseraddressSuccess useraddressSuccess,
      SettingsCubitState settingsCubitState,
      List<String> qty,
      BuildContext context) {
    Logger().w(cartSuccess.promoCodeFailedMessage);
    Logger().w(cartSuccess.promoCode);
    return CreateOrderEvent(
        taxId: txid,
        email: userLoginSuccess.userResponse.user.email,
        walletBalanceUsed: _getWalletBalanceUsed(cartSuccess, userLoginSuccess),
        placeOrder: "1",
        total: _getSubTotal(cartSuccess),
        addressId: useraddressSuccess.selectedAddress.id,
        deliveryTime:
            '${settingsCubitState.date}+-+${settingsCubitState.timeSlots.title}',
        orderNote: settingsCubitState.note,
        deliveryCharge: _getDeliveryCharge(useraddressSuccess),
        finalTotal: this._calculateFinalTotal(cartSuccess, useraddressSuccess),
        walletused: cartSuccess.useWallet.toString(),
        paymentMethod: settingsCubitState.paymentMethod.name,
        quantity: qty.join(","),
        productVariantId: BlocProvider.of<CartCubit>(context)
            .getVaraintNonDuplicateArray()
            .join(","),
        promoCode: (cartSuccess.promoCode != null &&
                cartSuccess.promoCodeFailedMessage == null)
            ? cartSuccess.promoCode
            : "",
        promoCodeDiscount: (cartSuccess.promoCode != null &&
                cartSuccess.promoCodeFailedMessage == null)
            ? cartSuccess.promoCodeDiscount
            : "0.0");
  }
}
