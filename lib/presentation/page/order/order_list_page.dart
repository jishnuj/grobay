import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/order/order_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/page/order/order_details_page.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/custom_button_widget.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/refersh_widget.dart';
import 'package:grobay/presentation/widget/skelton_loaders.dart';
import 'package:grobay/presentation/widget/ui_helper.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class OrderListPage extends StatelessWidget {
  const OrderListPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (BlocProvider.of<UserloginBloc>(context).state is UserLoginSuccess) {
      BlocProvider.of<OrderBloc>(context)
          .add(FetchOrderEvent(limit: AppConfig.PAGE_LIMIT.toString()));
    }

    ScrollController _scrollController = new ScrollController();
    _setScrollListener(_scrollController, context);
    return Scaffold(
      bottomNavigationBar: BlocBuilder<OrderBloc, OrderState>(
        builder: (context, orderState) {
          if (orderState is OrderSuccess && orderState.isPageLoading) {
            return LinearProgressIndicator();
          } else if (orderState is OrderSuccess && orderState.isPageFailed) {
            return UIUtilWidget.buildRetryBox(tr('retry'), () {
              BlocProvider.of<OrderBloc>(context).add(FetchOrderEvent(
                  offset: OrderBloc.lastOffset.toString(),
                  limit: AppConfig.PAGE_LIMIT.toString()));
            });
          }
          return UIUtilWidget.verticalSpaceCustom(0.0);
        },
      ),
      body: BlocBuilder<UserloginBloc, UserloginState>(
        builder: (context, userLoginState) {
          if (userLoginState is UserloginInitial) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomErrorWidget(
                  msg: tr('loginToSeeYourOrders'),
                  image: Icon(
                    Icons.person,
                    size: 100.0,
                  ),
                ),
                UIUtilWidget.verticalSpaceLarge(),
                CustomButtonWidget(
                    onTap: () {
                      Navigator.pushNamed(context, "login");
                    },
                    text: tr('login'))
              ],
            );
          }
          return CustomRefreshWidget(
            onRefresh: () {
              BlocProvider.of<OrderBloc>(context)
                  .add(FetchOrderEvent(limit: AppConfig.PAGE_LIMIT.toString()));
            },
            child: CustomScrollView(
              controller: _scrollController,
              slivers: [
                CustomAppbar(
                  showLogo: false,
                  showTitle: true,
                  title: tr('orders'),
                  showBanner: false,
                  showSearchField: false,
                ),
                SliverPadding(
                  sliver: _buildOrderList(),
                  padding: EdgeInsets.all(10),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  BlocConsumer<OrderBloc, OrderState> _buildOrderList() {
    return BlocConsumer<OrderBloc, OrderState>(
      listener: (context, orderState) {},
      builder: (context, orderState) {
        if (orderState is OrderSuccess &&
                orderState.isPageFailed &&
                orderState.orderFetchResponse.data.length == 0 ||
            orderState is OrderFetchFailed) {
          return SliverToBoxAdapter(
            child: CustomErrorWidget(
              msg: tr('unableToLoadOrders'),
            ),
          );
        }

        if (orderState is OrderSuccess) {
          OrderSuccess _orderState = orderState;
          return SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
            return _bulidOrderCard(_orderState, index, context);
          }, childCount: _orderState.orderFetchResponse.data.length));
        }
        return SliverToBoxAdapter(child: OrderListLoader());
      },
    );
  }

  void _setScrollListener(
      ScrollController _scrollController, BuildContext context) {
    _scrollController
      ..addListener(() {
        if (_scrollController.offset >=
                _scrollController.position.maxScrollExtent &&
            !_scrollController.position.outOfRange) {
          // call fetch more method here
          OrderState orderState = BlocProvider.of<OrderBloc>(context).state;
          if (orderState is OrderSuccess && !orderState.isPageLoading) {
            BlocProvider.of<OrderBloc>(context).add(FetchOrderEvent(
                offset: OrderBloc.lastOffset.toString(),
                limit: AppConfig.PAGE_LIMIT.toString()));
          }
        }
      });
  }

  Card _bulidOrderCard(
      OrderSuccess orderState, int index, BuildContext context) {
    List<String> itemNames = [];
    orderState.orderFetchResponse.data[index].items.forEach((element) {
      itemNames.add(element.name);
    });

    return Card(
      margin: EdgeInsets.symmetric(vertical: 7.5),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildOrderNo(orderState, index, context),
            _buildItemCount(orderState, index, context),
            UIHelper.verticalSpaceSmall(),
            _buildItemNames(itemNames, context),
            Divider(
              color: Colors.grey,
            ),
            _buildOtherOrderDetails(orderState, index, context),
          ],
        ),
      ),
    );
  }

  Row _buildOrderNo(OrderSuccess orderState, int index, BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "${tr('orderNo')} ${orderState.orderFetchResponse.data[index].id}",
          style: Theme.of(context)
              .primaryTextTheme
              .headline6
              .copyWith(color: Palette.orderCardTextColorPrimary),
        ),
        IconButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return OrderDetailsPage(
                  order: orderState.orderFetchResponse.data[index],
                );
              }));
            },
            icon: Icon(Icons.arrow_forward_ios))
      ],
    );
  }

  Row _buildItemCount(
      OrderSuccess orderState, int index, BuildContext context) {
    return Row(
      children: [
        Text(
          "${orderState.orderFetchResponse.data[index].items.length} " +
              (orderState.orderFetchResponse.data[index].items.length == 1
                  ? tr('item')
                  : tr('items')),
          style: Theme.of(context)
              .primaryTextTheme
              .subtitle1
              .copyWith(color: Palette.orderCardTextColorSecondary),
        ),
        UIHelper.horizontalSpaceSmall(),
        Row(
          children: orderState.orderFetchResponse.data[index].items
              .map(
                (e) => CachedNetworkImage(
                  imageUrl: e.image,
                  imageBuilder: (context, imageProvider) => Container(
                    width: 20.0,
                    height: 20.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: imageProvider, fit: BoxFit.cover),
                    ),
                  ),
                  placeholder: (context, url) => CircularProgressIndicator(),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              )
              .toList(),
        ),
      ],
    );
  }

  Text _buildItemNames(List<String> itemNames, BuildContext context) {
    return Text(
      itemNames.join(","),
      style: Theme.of(context)
          .primaryTextTheme
          .subtitle1
          .copyWith(color: Colors.black),
    );
  }

  Row _buildOtherOrderDetails(
      OrderSuccess orderState, int index, BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${tr('placedOrderOn')} ${orderState.orderFetchResponse.data[index].dateAdded}",
              style: Theme.of(context)
                  .primaryTextTheme
                  .subtitle2
                  .copyWith(color: Colors.black),
            ),
            Text(
              '${CurrencyWidget.getFixedPriceCurrencyString(double.parse(orderState.orderFetchResponse.data[index].total))}  ',
              style: Theme.of(context)
                  .primaryTextTheme
                  .subtitle2
                  .copyWith(color: Colors.black),
            ),
          ],
        ),
      ],
    );
  }
}
