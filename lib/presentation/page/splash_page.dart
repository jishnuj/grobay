import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/core/constant/assets.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/core/constant/storage_keys.dart';
import 'package:grobay/core/platform/local_storage_service.dart';
import 'package:grobay/presentation/bloc/app_settings/app_settings_bloc.dart';
import 'package:grobay/presentation/bloc/keyword/keyword_bloc.dart';
import 'package:grobay/presentation/bloc/location/location_bloc.dart';
import 'package:grobay/presentation/bloc/startup/startup_bloc.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _initStartupAPIS(context);

    return BlocListener<AppSettingsBloc, AppSettingsState>(
      listener: (context, appSettingsState) {
        if (appSettingsState is AppSettingsSuccess) {
          _checkIfFirstTimeOpening(context);
        } else if (appSettingsState is AppSettingsFailed) {
          UIUtilWidget.showSnackBar("Error");
        }
      },
      child: Scaffold(
        backgroundColor: Palette.primary,
        body: _buildBody(),
      ),
    );
  }

  CustomErrorWidget _buildErrorBox(AppSettingsFailed appSettingsState) {
    return CustomErrorWidget(
      msg: appSettingsState.message,
    );
  }

  Container _buildBody() {
    return Container(
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            Assets.SPLASH_IMAGE,
            height: 150,
          ),
          UIUtilWidget.buildCircularProgressIndicator(),
        ],
      ),
    );
  }

  _initStartupAPIS(BuildContext context) {
    BlocProvider.of<StartupBloc>(context).add(FetchStartupEvent());
    BlocProvider.of<AppSettingsBloc>(context).add(FetchAppSettings());
    BlocProvider.of<LocationBloc>(context).add(FetchLocationEvent());
    BlocProvider.of<KeywordBloc>(context).add(FetchKeywordEvent());
  }

  _checkIfFirstTimeOpening(BuildContext context) {
    LocalStorageService localStorageService =
        GetIt.I.get<LocalStorageService>();
    Future.delayed(Duration(seconds: 2)).then((value) {
      localStorageService.getItem(StorageKeys.isFirstTime.key).then((data) {
        if (data != null) {
          Navigator.pushNamedAndRemoveUntil(context, "home", (route) => false);
        } else {
          localStorageService.setItem(StorageKeys.isFirstTime.key, "1");
          Navigator.pushNamedAndRemoveUntil(context, "intro", (route) => false);
        }
      });
    });
  }
}
