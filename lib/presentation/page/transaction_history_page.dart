import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/app_constants.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/transaction/transaction_bloc.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/refersh_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class TransactionHistoryPage extends StatelessWidget {
  const TransactionHistoryPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<TransactionBloc>(context).add(FetchTransaction(
        type: AppConstants.USER_TRANSACTION_HISTORY,
        limit: AppConfig.PAGE_LIMIT.toString()));
    ScrollController _scrollController = new ScrollController();
    _scrollController
      ..addListener(() {
        // var triggerFetchMoreSize =
        //     0.9 * ;

        if (_scrollController.offset >=
                _scrollController.position.maxScrollExtent &&
            !_scrollController.position.outOfRange) {
          // call fetch more method here
          TransactionState transactionState =
              BlocProvider.of<TransactionBloc>(context).state;
          if (transactionState is TransactionSuccess &&
              !transactionState.isPageLoading) {
            BlocProvider.of<TransactionBloc>(context).add(FetchTransaction(
                type: AppConstants.USER_TRANSACTION_HISTORY,
                offset: TransactionBloc.lastOffset.toString(),
                limit: AppConfig.PAGE_LIMIT.toString()));
          }
        }
      });
    return Scaffold(
        bottomNavigationBar: BlocBuilder<TransactionBloc, TransactionState>(
          builder: (context, transactonState) {
            if (transactonState is TransactionSuccess &&
                transactonState.isPageLoading) {
              return LinearProgressIndicator();
            } else if (transactonState is TransactionSuccess &&
                transactonState.isPageFailed) {
              return UIUtilWidget.buildRetryBox("retry", () {
                BlocProvider.of<TransactionBloc>(context).add(FetchTransaction(
                    type: AppConstants.USER_TRANSACTION_HISTORY,
                    offset: TransactionBloc.lastOffset.toString(),
                    limit: AppConfig.PAGE_LIMIT.toString()));
              });
            }
            return SizedBox();
          },
        ),
        body: SafeArea(
          child: CustomRefreshWidget(
            onRefresh: () {
              BlocProvider.of<TransactionBloc>(context).add(FetchTransaction(
                  type: AppConstants.USER_TRANSACTION_HISTORY,
                  limit: AppConfig.PAGE_LIMIT.toString()));
            },
            child: CustomScrollView(
              controller: _scrollController,
              slivers: [
                CustomAppbar(
                  title: tr('transactionHistory'),
                  showBanner: false,
                  showSearchField: false,
                  showTitle: true,
                ),
                BlocBuilder<TransactionBloc, TransactionState>(
                  builder: (context, transactonState) {
                    if (transactonState is TransactionSuccess) {
                      return SliverList(
                          delegate: SliverChildBuilderDelegate(
                              (context, index) {
                        return Card(
                          child: Container(
                            margin: EdgeInsets.symmetric(
                                vertical: 15.0, horizontal: 15.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  tr('order') + " #" +
                                      transactonState.transactionHistoryResponse
                                          .transaction[index].txnId
                                          .toString(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6
                                      .copyWith(
                                          color: Palette
                                              .transactionDetailsCardTextPrimary),
                                ),
                                Divider(),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                     tr('via') + " ${transactonState.transactionHistoryResponse.transaction[index].type}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2
                                          .copyWith(
                                              color: Palette
                                                  .transactionDetailsCardTextSecondary),
                                    ),
                                    Container(
                                      child: Text(
                                        transactonState
                                            .transactionHistoryResponse
                                            .transaction[index]
                                            .status,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2
                                            .copyWith(
                                                color: Palette
                                                    .transactionDetailsCardTextSecondary),
                                      ),
                                    )
                                  ],
                                ),
                                Divider(),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(tr('dateAndTime'),
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2
                                            .copyWith(
                                                color: Palette
                                                    .transactionDetailsCardTextSecondary)),
                                    Text(
                                       tr('amount') + " ${CurrencyWidget.getFixedPriceCurrencyString(double.parse(transactonState.transactionHistoryResponse.transaction[index].amount))}",
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2
                                            .copyWith(
                                                color: Palette
                                                    .transactionDetailsCardTextSecondary))
                                  ],
                                ),
                                Text(
                                    transactonState.transactionHistoryResponse
                                        .transaction[index].dateCreated,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2
                                        .copyWith(
                                            color: Palette
                                                .transactionDetailsCardTextSecondary)),
                                Divider(),
                                Text(tr('message'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle2
                                        .copyWith(
                                            color: Palette
                                                .transactionDetailsCardTextSecondary)),
                                Text(
                                    transactonState.transactionHistoryResponse
                                        .transaction[index].message,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2
                                        .copyWith(
                                            color: Palette
                                                .transactionDetailsCardTextSecondary)),
                              ],
                            ),
                          ),
                        );
                      },
                              childCount: transactonState
                                  .transactionHistoryResponse
                                  .transaction
                                  .length));
                    } else if (transactonState is TransactionFailed) {
                      return SliverToBoxAdapter(
                        child: Container(
                          height: MediaQuery.of(context).size.height / 1.3,
                          child: CustomErrorWidget(
                            msg: transactonState.message,
                          ),
                        ),
                      );
                    }
                    return SliverToBoxAdapter(
                      child: Container(
                        height: MediaQuery.of(context).size.height / 1.3,
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    );
                  },
                )
              ],
            ),
          ),
        ));
  }
}
