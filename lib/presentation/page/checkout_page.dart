import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/cart.dart';
import 'package:grobay/api/model/location.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/cart/cart_bloc.dart';
import 'package:grobay/presentation/bloc/user_address/useraddress_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/widget/cart_footer_widget.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class CheckoutPage extends StatelessWidget {
  const CheckoutPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CartBloc.steps = STEPS.CHECKOUT;
    TextEditingController textEditingController = TextEditingController();
    UseraddressSuccess useraddressSuccess =
        BlocProvider.of<UseraddressBloc>(context).state;

    BlocProvider.of<CartBloc>(context).add(FetchUserCartData(
      address: useraddressSuccess.selectedAddress,
      pincode: Location(
          id: useraddressSuccess.selectedAddress.pincodeId,
          pincode: useraddressSuccess.selectedAddress.pincode),
      cartCubit: BlocProvider.of<CartCubit>(context),
    ));

    return Scaffold(
      bottomNavigationBar: _buildCheckoutCard(context),
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: true,
              backgroundColor: Palette.primary,
              // iconTheme: Theme.of(context).accentIconTheme,
              expandedHeight: 200,
              collapsedHeight: 60,
              title: Text(
               tr('checkout'),
                style: Theme.of(context)
                    .accentTextTheme
                    .headline6
                    .copyWith(color: Palette.onSecondary),
              ),
              flexibleSpace: SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                child: _buildCouponCode(context, textEditingController),
              ),
            ),
            _buildBody()
          ],
        ),
      ),
    );
  }

  Widget _buildCouponCode(
      BuildContext context, TextEditingController textEditingController) {
    return BlocConsumer<CartBloc, CartState>(
      listener: (context, cartState) {
        if (cartState is CartSuccess &&
            cartState.promoCodeFailedMessage != null &&
            cartState.promoCode != null) {
          UIUtilWidget.showSnackBar(cartState.promoCodeFailedMessage);
        }
      },
      builder: (context, cartState) {
        if (cartState is CartSuccess &&
            cartState.promoCodeFailedMessage == null &&
            cartState.promoCode != null) {
          return Container(
              margin: EdgeInsets.only(top: 60),
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    tr('promoCodeApplied'),
                    style: Theme.of(context).accentTextTheme.subtitle2,
                  ),
                  UIUtilWidget.verticalSpaceLarge(),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        border: Border.all(color: Colors.white)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: Text(
                            "${cartState.promoCode}",
                            style: Theme.of(context).accentTextTheme.headline5,
                          ),
                        ),
                        IconButton(
                            onPressed: () {
                              textEditingController.clear();
                              BlocProvider.of<CartBloc>(context)
                                  .add(CartRemovePromoCode());
                            },
                            icon: Icon(Icons.close))
                      ],
                    ),
                  )
                ],
              ));
        }
        return Container(
          margin: EdgeInsets.only(top: 60),
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                tr('applyCouponCode'),
                style: Theme.of(context)
                    .accentTextTheme
                    .subtitle2
                    .copyWith(color: Palette.onSecondary),
              ),
              UIUtilWidget.verticalSpaceLarge(),
              Row(
                children: [
                  Expanded(
                      child: TextField(
                    controller: textEditingController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 5.0, horizontal: 8.0),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15.0),
                                bottomLeft: Radius.circular(15.0)),
                            borderSide: BorderSide(color: Colors.white)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15.0),
                                bottomLeft: Radius.circular(15.0)),
                            borderSide: BorderSide(color: Colors.white)),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15.0),
                                bottomLeft: Radius.circular(15.0)),
                            borderSide: BorderSide(color: Colors.white)),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15.0),
                                bottomLeft: Radius.circular(15.0)),
                            borderSide: BorderSide(color: Colors.white))),
                  )),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        onPrimary: Palette.onSecondary,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(15.0),
                                bottomRight: Radius.circular(15.0))),
                        padding: EdgeInsets.symmetric(vertical: 17.0),
                        primary: Theme.of(context).highlightColor),
                    onPressed: () {
                      BlocProvider.of<CartBloc>(context).add(CartApplyPromoCode(
                          promoCode: textEditingController.text ?? ""));
                    },
                    child: Text(tr('apply')),
                  )
                ],
              )
            ],
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return SliverPadding(
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
      sliver: BlocConsumer<CartBloc, CartState>(
        listener: (context, cartState) {
          if (cartState is CartSuccess && !cartState.isDeliverable) {
            UIUtilWidget.clearAllSnackBars();
            UIUtilWidget.showSnackBar(tr('notDeliverableToThisAddress'));
          }
        },
        builder: (context, cartState) {
          if (cartState is CartSuccess) {
            return SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
              return Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: _buildCartProductCard(context, cartState, index),
              );
            }, childCount: cartState.cartResponse.cart.data.length));
          }
          return SliverToBoxAdapter(
            child: Container(
              height: MediaQuery.of(context).size.height / 2,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildCheckoutCard(BuildContext context) {
    return CartFooterWidget(
      cartFooterType: CartFooterType.CHECKOUT,
    );
  }

  double calculateTaxPrice(CartItem cartItem) {
    double price = double.parse(cartItem.item[0].discountedPrice);
    return (price +
        (price * double.parse(cartItem.item[0].taxPercentage)) / 100);
  }

  Widget _buildCartProductCard(
      BuildContext context, CartSuccess cartState, int index) {
    return Container(
      decoration: BoxDecoration(
          color: Palette.productCartCardColor,
          borderRadius: BorderRadius.circular(15)),
      child: Row(
        children: [
          SizedBox(
            width: 88,
            child: AspectRatio(
              aspectRatio: 0.88,
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Palette.productCartCardColor,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: CachedNetworkImage(
                  imageUrl:
                      cartState.cartResponse.cart.data[index].item[0].image,
                  placeholder: (context, url) => Container(
                    decoration: BoxDecoration(
                        color: Palette.imageCacheColor,
                        borderRadius: BorderRadius.circular(50.0)),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ),
          ),
          UIUtilWidget.horizontalSpaceLarge(),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                UIUtilWidget.verticalSpaceMedium(),
                Padding(
                  padding: const EdgeInsets.only(right: 15.0),
                  child: Text(
                    cartState.cartResponse.cart.data[index].item[0].name,
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    maxLines: 2,
                  ),
                ),
                UIUtilWidget.verticalSpaceMedium(),
                if (!BlocProvider.of<CartCubit>(context)
                    .checkProductAvailabelInPincode(
                        cartState
                            .cartResponse.cart.data[index].item[0].pincodes,
                        cartState.pincode.id))
                  Text(
                    tr('notDeliverable'),
                    style: Theme.of(context)
                        .textTheme
                        .subtitle2
                        .copyWith(color: Colors.red),
                  ),
                if (cartState.cartResponse.cart.data[index].item[0].stock ==
                    "0")
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 0.0),
                    child: Text(
                      tr('soldOut'),
                      style: Theme.of(context)
                          .textTheme
                          .subtitle2
                          .copyWith(color: Colors.red),
                    ),
                  ),
                UIUtilWidget.verticalSpaceMedium(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    UIUtilWidget.verticalSpaceMedium(),
                    Text(
                      '${cartState.cartResponse.cart.data[index].item[0].measurement}  ${cartState.cartResponse.cart.data[index].item[0].unit}  ',
                      style: Theme.of(context).textTheme.bodyText1.copyWith(
                          fontWeight: FontWeight.w600,
                          color: Theme.of(context).primaryColor),
                    ),
                    UIUtilWidget.horizontalSpaceMedium(),
                    BlocBuilder<CartCubit, CartCubitState>(
                      builder: (context, state) {
                        return Text.rich(
                          TextSpan(
                              text:
                                  '${CurrencyWidget.getFixedPriceCurrencyString(calculateTaxPrice(cartState.cartResponse.cart.data[index]))}',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(),
                              children: [
                                TextSpan(
                                    text:
                                        " x ${BlocProvider.of<CartCubit>(context).getVariantsCount(cartState.cartResponse.cart.data[index].productVariantId).toString()}",
                                    style:
                                        Theme.of(context).textTheme.bodyText1),
                              ]),
                        );
                      },
                    ),
                  ],
                ),
                UIUtilWidget.verticalSpaceMedium(),
                Padding(
                  padding: EdgeInsets.only(right: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          "${tr('tax')} ${cartState.cartResponse.cart.data[index].item[0].taxTitle} ${cartState.cartResponse.cart.data[index].item[0].taxPercentage} %"),
                      Text(
                        '${CurrencyWidget.getFixedPriceCurrencyString((BlocProvider.of<CartCubit>(context).getVariantsCount(cartState.cartResponse.cart.data[index].productVariantId) * calculateTaxPrice(cartState.cartResponse.cart.data[index])))}',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
