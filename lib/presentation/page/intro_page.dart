import 'package:flutter/material.dart';
import 'package:grobay/core/constant/assets.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:intro_slider/intro_slider.dart';

class IntroPage extends StatefulWidget {
  IntroPage({Key key}) : super(key: key);

  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  List<Slide> slides = [];

  @override
  void initState() {
    super.initState();

    slides.add(this.buildSlide(Assets.SPLASH_IMAGE_THREE,
        description: "Look for things around you", title: "Discover"));
    slides.add(this.buildSlide(Assets.SPLASH_IMAGE_ONE,
        description: "Get the best deals & Coupons", title: "Shop"));
    slides.add(this.buildSlide(Assets.SPLASH_IMAGE_THREE,
        description: "Find Great Offers while you discover", title: "Offer"));
  }

  Slide buildSlide(String image,
      {String title, String description, Color backgroundColor}) {
    return Slide(
      backgroundColor: backgroundColor ?? Palette.onPrimary,
      // title: title ?? "",
      // maxLineTitle: 2,
      styleTitle: TextStyle(
          color: Palette.onSecondary,
          fontSize: 30.0,
          fontWeight: FontWeight.bold,
          fontFamily: 'RobotoMono'),
      // description: description ?? "",
      styleDescription: TextStyle(
          color: Palette.onSecondary,
          fontSize: 20.0,
          fontStyle: FontStyle.italic,
          fontFamily: 'Raleway'),
      marginDescription:
          EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 70.0),
      centerWidget: _buildSlideBody(image, title, description),
      directionColorBegin: Alignment.topLeft,
      directionColorEnd: Alignment.bottomRight,
      onCenterItemPress: () {},
    );
  }

  Container _buildSlideBody(String image, String title, String description) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
      child: Column(
        children: [
          Image.asset(
            image,
            width: 300,
            height: 300,
          ),
          Text(title ?? "",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Palette.onSecondary,
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold)),
          UIUtilWidget.verticalSpaceLarge(),
          Text(description ?? "",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black, fontSize: 25.0)),
        ],
      ),
    );
  }

  void onDonePress() {
    Navigator.pushNamedAndRemoveUntil(context, "home", (route) => false);
  }

  Widget renderNextBtn() {
    return Icon(
      Icons.navigate_next,
      color: Palette.buttonText,
    );
  }

  Widget renderDoneBtn() {
    return Icon(
      Icons.done,
      color: Palette.buttonText,
    );
  }

  Widget renderSkipBtn() {
    return Icon(
      Icons.skip_next,
      color: Palette.buttonText,
    );
  }

  ButtonStyle myButtonStyle() {
    return ButtonStyle(
      shape: MaterialStateProperty.all<OutlinedBorder>(StadiumBorder()),
      backgroundColor: MaterialStateProperty.all<Color>(Palette.buttonBg),
      overlayColor: MaterialStateProperty.all<Color>(Palette.buttonText),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Palette.primary,
      body: IntroSlider(
        // List slides
        slides: this.slides,

        // Skip button
        renderSkipBtn: this.renderSkipBtn(),
        skipButtonStyle: myButtonStyle(),

        // Next button
        renderNextBtn: this.renderNextBtn(),
        nextButtonStyle: myButtonStyle(),

        // Done button
        renderDoneBtn: this.renderDoneBtn(),
        onDonePress: this.onDonePress,
        doneButtonStyle: myButtonStyle(),

        // Dot indicator
        colorDot: Palette.highlightColor.withOpacity(0.5),
        colorActiveDot: Palette.highlightColor,
        sizeDot: 13.0,

        // Show or hide status bar

        backgroundColorAllSlides: Palette.onPrimary,
      ),
    );
  }
}
