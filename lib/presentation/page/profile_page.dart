import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/core/platform/camera_service.dart';
import 'package:grobay/presentation/bloc/favourite/favourite_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/widget/change_password_widget.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController _nameController = TextEditingController();
    TextEditingController _phoneNumberController = TextEditingController();
    TextEditingController _emailController = TextEditingController();
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: true,
              backgroundColor: Palette.secondary,
              collapsedHeight: 60,
              title: Text(
                tr('myProfile'),
                style: Theme.of(context).textTheme.subtitle2,
              ),
              actions: [
                IconButton(
                    icon: Icon(Icons.logout),
                    onPressed: () {
                      BlocProvider.of<CartCubit>(context).clearAll();
                      BlocProvider.of<CartCubit>(context).clear();
                      BlocProvider.of<UserloginBloc>(context).clear();
                      BlocProvider.of<FavouriteBloc>(context).clear();
                      BlocProvider.of<FavouriteBloc>(context)
                          .add(ResetFavouriteEvent());
                      BlocProvider.of<UserloginBloc>(context)
                          .add(FetchLogoutEvent());
                      Navigator.pushNamedAndRemoveUntil(
                          context, "home", (route) => false);
                    })
              ],
            ),
            SliverToBoxAdapter(
              child: BlocConsumer<UserloginBloc, UserloginState>(
                listener: (context, userLoginState) {
                  if (userLoginState is UserLoginSuccess &&
                      !userLoginState.userUpdateLoading &&
                      !userLoginState.userImageUpdateLoading &&
                      !userLoginState.userUpdateFailed) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(userLoginState.userUpdateMsg ??
                            userLoginState.userImageUpdateMsg)));
                  }
                },
                builder: (context, userLoginState) {
                  if (userLoginState is UserLoginSuccess &&
                          userLoginState.userUpdateLoading ||
                      (userLoginState is UserLoginSuccess &&
                          userLoginState.userImageUpdateLoading)) {
                    return Container(
                      height: MediaQuery.of(context).size.height / 2,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                  if (userLoginState is UserLoginSuccess &&
                      userLoginState.userUpdateFailed) {
                    return Container(
                        height: MediaQuery.of(context).size.height / 1.5,
                        child: CustomErrorWidget(
                          msg: userLoginState.userUpdateMsg,
                          onNetworkError: () {
                            BlocProvider.of<UserloginBloc>(context)
                                .add(ReloadUserDetails());
                          },
                        ));
                  }
                  if (userLoginState is UserLoginSuccess) {
                    _nameController.text =
                        userLoginState.userResponse.user.name;
                    _phoneNumberController.text =
                        userLoginState.userResponse.user.mobile;
                    _emailController.text =
                        userLoginState.userResponse.user.email;
                    return Container(
                      padding: EdgeInsets.all(20.0),
                      margin: EdgeInsets.only(top: 25.0),
                      child: Column(
                        children: [
                          _buildProfileImage(userLoginState, context),
                          _buildTextField(context, "Name",
                              editingController: _nameController),
                          _buildTextField(context, "Phone no",
                              enabled: false,
                              editingController: _phoneNumberController),
                          _buildTextField(context, "Email",
                              editingController: _emailController),
                          SizedBox(
                            height: 70.0,
                          ),
                          InkWell(
                            onTap: () {
                              BlocProvider.of<UserloginBloc>(context)
                                  .add(UpdateUserDetailsEvent(
                                name: _nameController.text,
                                email: _emailController.text,
                                mobile: _phoneNumberController.text,
                              ));
                            },
                            child: Container(
                              height: 50,
                              margin: EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 5.0),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Palette.primary),
                              child: Center(
                                child: Text(
                                  tr('updateDetails'),
                                  style: TextStyle(
                                      color: Palette.onPrimary,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          InkWell(
                            onTap: () {
                              showBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return BottomSheet(
                                        onClosing: () {},
                                        builder: (context) {
                                          return ChangePassword();
                                        });
                                  });
                            },
                            child: Container(
                              height: 50,
                              margin: EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 0.0),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Palette.buttonBg),
                              child: Center(
                                child: Text(
                                  tr('changePassword'),
                                  style: TextStyle(
                                      color: Palette.buttonText,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                  return SizedBox();
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Stack _buildProfileImage(
      UserLoginSuccess userLoginState, BuildContext context) {
    return Stack(
      children: [
        Align(
          alignment: Alignment.center,
          child: userLoginState.userResponse.user.profile.isNotEmpty
              ? CachedNetworkImage(
                height: 200,
                width: 200,
                  imageUrl: userLoginState.userResponse.user.profile,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(200),
                      image: DecorationImage(
                          image: NetworkImage(
                              userLoginState.userResponse.user.profile),
                          fit: BoxFit.cover,
                          colorFilter: ColorFilter.mode(
                              Colors.red, BlendMode.colorBurn)),
                    ),
                  ),
                  placeholder: (context, url) => CircularProgressIndicator(),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                )
              : Container(
                  height: 200,
                  width: 200,
                  clipBehavior: Clip.hardEdge,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: Icon(Icons.person),
                ),
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                height: 200,
                child: IconButton(
                  icon: Icon(
                    Icons.image,
                    color: Palette.onPrimary,
                  ),
                  onPressed: () {
                    showBottomSheet(
                        context: context,
                        builder: (context) {
                          return BottomSheet(
                            builder: (context) {
                              return Container(
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                child: Row(
                                  children: [
                                    InkWell(
                                        onTap: () {
                                          GetIt.I
                                              .get<CameraService>()
                                              .openCamera()
                                              .then((value) {
                                            GetIt.I
                                                .get<CameraService>()
                                                .cropImage(value.image)
                                                .then((data) {
                                              BlocProvider.of<UserloginBloc>(
                                                      context)
                                                  .add(UpdateUserImageEvent(
                                                      image: data));
                                              Navigator.pop(context);
                                            });
                                          });
                                        },
                                        child: Icon(
                                          Icons.camera,
                                          color: Palette.profilePageImageUploadIconColor,
                                          size: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2,
                                        )),
                                    InkWell(
                                        onTap: () {
                                          GetIt.I
                                              .get<CameraService>()
                                              .openGallery()
                                              .then((value) {
                                            GetIt.I
                                                .get<CameraService>()
                                                .cropImage(value.image)
                                                .then((data) {
                                              BlocProvider.of<UserloginBloc>(
                                                      context)
                                                  .add(UpdateUserImageEvent(
                                                      image: data));
                                              Navigator.pop(context);
                                            });
                                          });
                                        },
                                        child: Icon(
                                          
                                          Icons.image,
                                                 color: Palette.profilePageImageUploadIconColor,
                                          size: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2,
                                        ))
                                  ],
                                ),
                              );
                            },
                            onClosing: () {},
                          );
                        });
                  },
                )))
      ],
    );
  }

  Container _buildTextField(BuildContext context, String label,
      {TextEditingController editingController,
      TextInputType textInputType,
      bool enabled = true}) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey[200]))),
      child: TextField(
        enabled: enabled,
        style: enabled
            ? Theme.of(context).textTheme.subtitle2
            : Theme.of(context)
                .textTheme
                .subtitle2
                .copyWith(color: Colors.grey),
        keyboardType: textInputType ?? TextInputType.name,
        controller: editingController ?? TextEditingController(),
        decoration: InputDecoration(
            hintText: label,
            hintStyle: TextStyle(color: Colors.grey),
            border: InputBorder.none),
      ),
    );
  }
}
