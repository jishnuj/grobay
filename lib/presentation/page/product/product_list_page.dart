import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/category.dart';
import 'package:grobay/presentation/bloc/product/product_bloc.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/product_card_widget.dart';

class ProductListPage extends StatelessWidget {
  final Category parentCategory;
  final Category subCategory;
  final bool useLocalBloc;
  const ProductListPage(
      {Key key,
      this.parentCategory,
      this.subCategory,
      this.useLocalBloc = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            CustomAppbar(
              showBanner: false,
              showLocation: false,
              title: subCategory.name,
              showTitle: true,
              showSearchField: false,
            ),
            SliverPadding(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                sliver: this.useLocalBloc
                    ? this._buildLocalBloc()
                    : this._buildGlobalBloc(context))
          ],
        ),
      ),
    );
  }

  Widget _buildLocalBloc() {
    ProductBloc productBloc = ProductBloc();
    productBloc.add(FetchProductByCategoryEvent(
        parentCatId: parentCategory.id, catId: subCategory.id));
    return BlocBuilder<ProductBloc, ProductState>(
      bloc: productBloc,
      builder: (context, productState) {
        if (productState is ProductSuccess) {
          return _buildList(productState);
        } else if (productState is ProductFailed) {
          return _buildErrorBox(productState, context);
        }
        return _buildLoading();
      },
    );
  }

  Widget _buildGlobalBloc(BuildContext context) {
    BlocProvider.of<ProductBloc>(context).add(FetchProductByCategoryEvent(
        parentCatId: parentCategory.id, catId: subCategory.id));
    return BlocBuilder<ProductBloc, ProductState>(
      builder: (context, productState) {
        if (productState is ProductSuccess) {
          return _buildList(productState);
        } else if (productState is ProductFailed) {
          return _buildErrorBox(productState, context);
        }
        return _buildLoading();
      },
    );
  }

  SliverToBoxAdapter _buildLoading() {
    return SliverToBoxAdapter(
        child: Container(
      height: 300,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [CircularProgressIndicator()],
      ),
    ));
  }

  SliverToBoxAdapter _buildErrorBox(
      ProductFailed productState, BuildContext context) {
    return SliverToBoxAdapter(
        child: Container(
      height: MediaQuery.of(context).size.height / 1.5,
      child: CustomErrorWidget(
        callerName: "product_list_widget",
        msg: productState.message,
        onNetworkError: () {},
      ),
    ));
  }

  SliverList _buildList(ProductSuccess productState) {
    return SliverList(
        delegate: SliverChildBuilderDelegate((context, index) {
      return ProductCardWidget(
        product: productState.productResposne.products[index],
      );
    }, childCount: productState.productResposne.products.length));
  }
}
