import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:grobay/api/model/product.dart';
import 'package:grobay/api/model/variant.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/location/location_bloc.dart';
import 'package:grobay/presentation/bloc/pincheck/pincheck_bloc.dart';
import 'package:grobay/presentation/bloc/product_detail/productdetail_bloc.dart';
import 'package:grobay/presentation/bloc/seller/seller_bloc.dart';
import 'package:grobay/presentation/bloc/similar/similarproducts_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/page/seller/seller_products_page.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/similar_product_widget.dart';
import 'package:grobay/presentation/widget/pincode_list_widget.dart';
import 'package:grobay/presentation/widget/skelton_loaders.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:grobay/presentation/widget/wishlist_icon_widget.dart';
import 'package:share/share.dart';

// ignore: must_be_immutable
class ProductDetailsPage extends StatelessWidget {
  final String productId;
  ProductDetailsPage({Key key, this.productId, this.product}) : super(key: key);

  Product product;
  Variants selectedVariant;
  @override
  Widget build(BuildContext context) {
    ProductdetailBloc productdetailBloc = ProductdetailBloc();
    SimilarproductsBloc similarproductsBloc = SimilarproductsBloc();
    productdetailBloc.add(FetchProductDetailEvent(productId: product.id));
    // If routed to here from notificationPage then there will only
    // product id and no category id and variants
    if (this.product.variants != null) {
      this.selectedVariant = this.product.variants[0];
    }
    // If routed to here from notificationPage thene there will only
    // product id and no category id and variants
    // so we have to wait unit the product details to load
    // then only load similar products
    if (this.product.categoryId != null) {
      similarproductsBloc.add(FetchSimilarProduct(
        categoryId: this.product.categoryId,
        productId: this.productId,
      ));
    }
    return BlocProvider(
      create: (context) => PincheckBloc(),
      child: Scaffold(
          bottomNavigationBar: _buildBottomCart(context),
          body: BlocConsumer<ProductdetailBloc, ProductdetailState>(
            bloc: productdetailBloc,
            listener: (context, productState) {
              if (productState is ProductdetailSuccess &&
                  this.product.id == productState.product.id) {
                //Load Similar Products
                if (this.product.categoryId == null) {
                  similarproductsBloc.add(FetchSimilarProduct(
                    categoryId: productState.product.categoryId,
                    productId: this.productId,
                  ));
                }
                product = productState.product;
                selectedVariant = productState.selectedvariant;
              }
            },
            builder: (context, productState) {
              if (productState is ProductdetailSuccess &&
                  productState.product.id == product.id) {
                return _buildBody(context, productState, similarproductsBloc,
                    productdetailBloc);
              } else if (productState is ProductdetailFailed) {
                return CustomErrorWidget(
                  msg: productState.message,
                );
              }
              return ProductDetailsSkeltonloader(product: this.product);
            },
          )),
    );
  }

  Widget _buildBody(
      BuildContext context,
      ProductdetailState productState,
      SimilarproductsBloc similarproductsBloc,
      ProductdetailBloc productdetailBloc) {
    return SafeArea(
      child: CustomScrollView(
        slivers: [
          _buildAppBar(context),
          _buildPriceAndQuantity(context, productdetailBloc),
          SliverToBoxAdapter(
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                child: Row(
                  children: [
                    UIUtilWidget.buildBorderedContainer(
                      child: Text("${product.returnDays} ${tr('daysReturnable')}"),
                    ),
                    UIUtilWidget.horizontalSpaceMedium(),
                    UIUtilWidget.buildBorderedContainer(
                      child: Text(product.cancelableStatus != "0"
                          ? "${tr('cancellableTill')} ${product.tillStatus}"
                          : tr('notCancellable')),
                    ),
                  ],
                )),
          ),
          selectedVariant.stock != "0"
              ? SliverToBoxAdapter(
                  child: _buildAddRemoveButton(context),
                )
              : SliverToBoxAdapter(
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
                    child: Text(
                      selectedVariant.serveFor,
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: Colors.red),
                    ),
                  ),
                ),
          _buildLocationAvailabilityStatus(),
          _buildProductDescription(),
          SliverToBoxAdapter(
            child: Divider(),
          ),
          _buildManufactureDetails(context),
          _buildSellerName(context),
          SliverToBoxAdapter(
            child: Divider(),
          ),
          if (productState is ProductdetailSuccess &&
              this.product.id == productState.product.id)
            SimilarProductWidget(
              similarproductsBloc: similarproductsBloc,
              categoryId: product.categoryId,
              productId: product.id,
              title: tr('similarProducts'),
            )
          else
            SliverToBoxAdapter(
              child: UIUtilWidget.buildCircularProgressIndicator(),
            ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 10.0,
            ),
          ),
        ],
      ),
    );
  }

  SliverAppBar _buildAppBar(BuildContext context) {
    return SliverAppBar(
        pinned: true,
        actions: [
          IconButton(
            icon: const Icon(Icons.share),
            tooltip: tr('shareProduct'),
            onPressed: () {
              // https: //grobay.in/levis/itemdetail/tomato-1
              Share.share(AppConfig.PRODUCT_SHARE_URL + product.slug);
            },
          ),
          IconButton(
            icon: const Icon(Icons.search),
            tooltip: tr('search'),
            onPressed: () {
              Navigator.pushNamed(context, "search");
            },
          ),
        ],
        title: Text(
          product.name,
          style: Theme.of(context)
              .textTheme
              .headline6
              .copyWith(color: Palette.onSecondary),
        ),
        collapsedHeight: 60.0,
        expandedHeight: 360.0,
        backgroundColor: Palette.secondary,
        flexibleSpace: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: _buildBanner(context),
        ));
  }

  Container _buildBanner(context) {
    return Container(
      margin: EdgeInsets.only(top: 60.0),
      child: CarouselSlider(
        options: CarouselOptions(
            height: 300.0,
            disableCenter: true,
            viewportFraction: 1.0,
            autoPlay: true),
        items: product.otherImages.length == 0
            ? [
                Container(
                    padding: EdgeInsets.symmetric(vertical: 20.0),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(),
                    child: Center(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.image,
                          size: 100,
                        ),
                        Text(
                          tr('noImage'),
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ],
                    )))
              ]
            : product.otherImages.toList().map((i) {
                return Builder(
                  builder: (BuildContext context) {
                    return Container(
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(),
                      child: CachedNetworkImage(
                        fit: BoxFit.fitHeight,
                        imageUrl: i,
                        placeholder: (context, url) => Container(
                          color: Palette.imageCacheColor,
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    );
                  },
                );
              }).toList(),
      ),
    );
  }

  SliverToBoxAdapter _buildPriceAndQuantity(
      BuildContext context, ProductdetailBloc productdetailBloc) {
    return SliverToBoxAdapter(
      child: Container(
          height: 60,
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: Row(
            children: [
              Expanded(
                child: RichText(
                    text: TextSpan(
                        text:
                            '${CurrencyWidget.getFixedPriceCurrencyString(calculateTaxPrice(double.parse(selectedVariant.discountedPrice)))}  ',
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline5
                            .copyWith(
                              color: Palette.onSecondary,
                            ),
                        children: [
                      TextSpan(
                          text:
                              '${CurrencyWidget.getFixedPriceCurrencyString(calculateTaxPrice(double.parse(selectedVariant.price)))}',
                          style: Theme.of(context)
                              .primaryTextTheme
                              .headline6
                              .copyWith(
                                  decoration: TextDecoration.lineThrough,
                                  color: Theme.of(context).disabledColor))
                    ])),
              ),
              _buildSizeDropdown(context, productdetailBloc),
            ],
          )),
    );
  }

  Container _buildSizeDropdown(
      BuildContext context, ProductdetailBloc productdetailBloc) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Row(
        children: [
          Text(
            tr('size'),
            style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                color: Palette.onSecondary, fontWeight: FontWeight.bold),
          ),
          UIUtilWidget.horizontalSpaceMedium(),
          DropdownButton(
              icon: Icon(
                Icons.arrow_drop_down,
                size: 30.0,
              ),
              underline: UIUtilWidget.verticalSpaceCustom(0),
              onChanged: (variant) {
                productdetailBloc
                    .add(SelectVariantEvent(selectedvariant: variant));
              },
              // isExpanded: true,
              value: selectedVariant,
              items: product.variants
                  .map(
                    (e) => DropdownMenuItem(
                      child: Text(
                        "${e.measurement} ${e.measurementUnitName}",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                                color: Palette.onSecondary,
                                fontWeight: FontWeight.bold),
                      ),
                      value: e,
                    ),
                  )
                  .toList()),
        ],
      ),
    );
  }

  Widget _buildAddRemoveButton(BuildContext context) {
    return BlocConsumer<CartCubit, CartCubitState>(
      listener: (context, cartCubitState) {
        if (BlocProvider.of<CartCubit>(context).state.lastUpdatedVariantId ==
            this.selectedVariant.id) if ((BlocProvider.of<CartCubit>(context)
                        .getVariantsCount(selectedVariant.id)
                        .toDouble() *
                    double.parse(selectedVariant.measurement)) >=
                double.parse(selectedVariant.stock) &&
            !cartCubitState.isFromCart) {
          UIUtilWidget.clearAllSnackBars();
          UIUtilWidget.showSnackBar(
              "${tr('only')} ${BlocProvider.of<CartCubit>(context).getVariantsCount(selectedVariant.id)} ${tr('isLeft')}");
        } else if (BlocProvider.of<CartCubit>(context)
                    .getVariantsCount(selectedVariant.id) ==
                10 &&
            !cartCubitState.isFromCart) {
          UIUtilWidget.clearAllSnackBars();
          UIUtilWidget.showSnackBar(
              tr('apologiesMaxProdQtyReached'));
        }
      },
      builder: (context, cartCubitState) {
        return BlocBuilder<UserloginBloc, UserloginState>(
          builder: (context, userLoginState) {
            return Container(
              padding: EdgeInsets.all(5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  WishListIconWidget(product: product),
                  Container(
                    width: 150,
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () {
                            BlocProvider.of<CartCubit>(context)
                                .removeProductVariant(
                                    product.id, selectedVariant.id,
                                    userId: userLoginState is UserLoginSuccess
                                        ? userLoginState
                                            .userResponse.user.userId
                                        : null);
                          },
                          child: Container(
                            padding: EdgeInsets.all(4.0),
                            decoration: BoxDecoration(
                                color: Palette.buttonBg,
                                border: Border.all(
                                  color: Palette.buttonBg,
                                ),
                                borderRadius: BorderRadius.circular(10.0)),
                            child: Icon(
                              Icons.remove,
                              size: 15.0,
                              color: Palette.buttonText,
                            ),
                          ),
                        ),
                        UIUtilWidget.horizontalSpaceCustom(15.0),
                        Text(
                          BlocProvider.of<CartCubit>(context)
                                      .getVariantsCount(selectedVariant.id) !=
                                  null
                              ? BlocProvider.of<CartCubit>(context)
                                  .getVariantsCount(selectedVariant.id)
                                  .toString()
                              : 0.toString(),
                          textAlign: TextAlign.center,
                        ),
                        UIUtilWidget.horizontalSpaceCustom(15.0),
                        InkWell(
                          onTap: () {
                            BlocProvider.of<CartCubit>(context)
                                .addProductVariant(
                                    product.id,
                                    selectedVariant.id,
                                    double.parse(selectedVariant.stock),
                                    double.parse(selectedVariant.measurement),
                                    userId: userLoginState is UserLoginSuccess
                                        ? userLoginState
                                            .userResponse.user.userId
                                        : null);
                          },
                          child: Container(
                            padding: EdgeInsets.all(4.0),
                            decoration: BoxDecoration(
                                color: Palette.buttonBg,
                                border: Border.all(
                                  color: Palette.buttonBg,
                                ),
                                borderRadius: BorderRadius.circular(10.0)),
                            child: Icon(
                              Icons.add,
                              size: 15.0,
                              color: Palette.buttonText,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  SliverToBoxAdapter _buildLocationAvailabilityStatus() {
    return SliverToBoxAdapter(
      child: Container(
        child: BlocBuilder<LocationBloc, LocationState>(
          builder: (context, locationState) {
            if (locationState is LocationSuccess) {
              BlocProvider.of<PincheckBloc>(context).add(CheckifAvailableEvent(
                  product: product,
                  currentlySelected: locationState.selectedLocation));
              if (locationState.selectedLocation.pincode == "All") {
                return _buildDeliverText(
                    context, locationState, tr('pleaseSelecetLocation'));
              } else {
                return BlocBuilder<PincheckBloc, PincheckState>(
                  builder: (context, pincheckState) {
                    if (pincheckState is PincheckInitial) {}
                    if (pincheckState is PincheckCurrentState) {
                      if (pincheckState.isUnavailable &&
                          pincheckState.unavailableProductId == product.id) {
                        return _buildDeliverText(context, locationState,
                            "${tr('productNotAvailableThisLocaion')} ${locationState.selectedLocation.pincode}",
                            showError: true);
                      }
                    }
                    return _buildDeliverText(context, locationState,
                        "${tr('deliverableTo')} ${locationState.selectedLocation.pincode}");
                  },
                );
              }
            }
            return UIUtilWidget.verticalSpaceCustom(0.0);
          },
        ),
      ),
    );
  }

  Widget _buildDeliverText(
      BuildContext context, LocationSuccess locationState, String text,
      {bool showError = false}) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: Palette.pleaseSelectLocationBorderColor)),
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
      child: InkWell(
        onTap: () {
          Scaffold.of(context).showBottomSheet(
            (context) => PincodeList(),
          );
        },
        child: Row(
          children: [
            Icon(
              Icons.location_pin,
              size: 15,
              color: Palette.pleaseSelectLocationTextIcon,
            ),
            UIUtilWidget.horizontalSpaceCustom(5.0),
            Text(
              text,
              style: Theme.of(context).textTheme.subtitle2.copyWith(
                    color: showError
                        ? Colors.red
                        : Palette.pleaseSelectLocationTextColor,
                  ),
            ),
          ],
        ),
      ),
    );
  }

  SliverToBoxAdapter _buildProductDescription() {
    return SliverToBoxAdapter(
      child: Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Html(
                data: product.description,
              ),
            ],
          )),
    );
  }

  SliverToBoxAdapter _buildManufactureDetails(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
          padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RichText(
                text: TextSpan(children: [
                  if (product.manufacturer.isNotEmpty)
                    TextSpan(
                        text: tr('manufacturedBy'),
                        style: Theme.of(context).textTheme.subtitle1),
                  TextSpan(
                      text: product.manufacturer,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1
                          .copyWith(color: Palette.onSecondary)),
                ]),
              ),
              if (product.madeIn.isNotEmpty)
                RichText(
                  text: TextSpan(
                      text: tr('madeIn'),
                      children: [
                        TextSpan(
                            text: product.madeIn,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1
                                .copyWith(color: Palette.onSecondary))
                      ],
                      style: Theme.of(context).textTheme.subtitle2),
                )
            ],
          )),
    );
  }

  SliverToBoxAdapter _buildSellerName(BuildContext context) {
    return SliverToBoxAdapter(
      child: InkWell(
        onTap: () {
          BlocProvider.of<SellerBloc>(context)
              .add(FetchSeller(sellerId: product.sellerId));
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return SellerProductsPage();
          }));
        },
        child: Container(
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            child: RichText(
              text: TextSpan(
                  text: tr('soldBy'),
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1
                      .copyWith(color: Palette.onSecondary),
                  children: [
                    TextSpan(
                      text: product.sellerName,
                      style: Theme.of(context)
                          .primaryTextTheme
                          .subtitle1
                          .copyWith(
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.bold,
                              color: Palette.onSecondary),
                    )
                  ]),
            )),
      ),
    );
  }

  Widget _buildBottomCart(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Palette.bottomCartBarBg,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20.0))),
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      height: 60,
      child: BlocBuilder<CartCubit, CartCubitState>(
        builder: (context, cartCubitState) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: Text(
                  BlocProvider.of<CartCubit>(context)
                          .getVaraintNonDuplicateArray()
                          .length
                          .toString() +
                      (BlocProvider.of<CartCubit>(context)
                                  .getVaraintNonDuplicateArray()
                                  .length ==
                              1
                          ? tr('item')
                          : tr('items')),
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2
                      .copyWith(color: Palette.bottomCartBarText),
                ),
              ),
              OutlinedButton(
                style: OutlinedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    side: BorderSide(color: Palette.bottomCartBarButtonBorder),
                    primary: Palette.bottomCartBarButtonText),
                onPressed: () {
                  Navigator.pushNamed(context, "cart");
                },
                child: Text(
                  tr('goToCart'),
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2
                      .copyWith(color: Palette.bottomCartBarButtonText),
                ),
              )
            ],
          );
        },
      ),
    );
  }

  double calculateTaxPrice(double price) {
    return (price + (price * double.parse(product.taxPercentage)) / 100);
  }
}
