import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/assets.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:share/share.dart';

class ReferPage extends StatelessWidget {
  const ReferPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: CustomScrollView(
        slivers: [
          CustomAppbar(
            title: tr('referAndEarn'),
            showBanner: false,
            showSearchField: false,
            showTitle: true,
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 30.0),
              child: Image.asset(
                Assets.ICON,
                height: 160,
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 30.0, horizontal: 20.0),
              child: Text(
                """${tr('referMsg')} ${AppConfig.CURRENCY + AppConfig.MINIMUM_REFER_AND_ORDER_AMOUNT.toString()}.Which allows user to earn upto  ${AppConfig.CURRENCY + AppConfig.MAXIMUM_REFER_AND_EARN_AMOUNT.toString()}
                  """,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(
                      color: Palette.sharePageTextColorPrimary),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              child: Column(
                children: [
                  Text(
                    tr('yourReferralCode'),
                    style: Theme.of(context)
                        .textTheme
                        .headline6
                        .copyWith(color: Palette.sharePageTextColorSecondary),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  BlocBuilder<UserloginBloc, UserloginState>(
                    builder: (context, userLoginState) {
                      if (userLoginState is UserLoginSuccess) {
                        return InkWell(
                          onTap: () {
                            Clipboard.setData(ClipboardData(
                              text:
                                  userLoginState.userResponse.user.referralCode,
                            )).then((value) {
                              UIUtilWidget.clearAllSnackBars();
                              UIUtilWidget.showSnackBar(tr('referralCodeCopied'));
                            });
                          },
                          child: Column(
                            children: [
                              Text(
                                tr('tapToCopy'),
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .copyWith(
                                        color: Palette
                                            .sharePageTextColorSecondary),
                              ),
                              Text(
                                userLoginState.userResponse.user.referralCode,
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6
                                    .copyWith(
                                        color: Palette
                                            .sharePageTextColorTeritiary),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        );
                      }
                      return Container();
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: BlocBuilder<UserloginBloc, UserloginState>(
              builder: (context, userLoginState) {
                if (userLoginState is UserLoginSuccess) {
                  return Container(
                    margin:
                        EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0),
                    child: OutlinedButton(
                      style: OutlinedButton.styleFrom(
                          primary: Palette.buttonBg,
                          backgroundColor: Palette.buttonBg.withOpacity(0.2),
                          padding: EdgeInsets.symmetric(vertical: 15.0,horizontal: 15.0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0),
                          )),
                      child: Text(tr('referNow')),
                      onPressed: () {
                        Share.share("""
                            ${tr('referSuccessMsg')}
                           ${AppConfig.REFERRAL_URL}${userLoginState.userResponse.user.referralCode}""");
                      },
                    ),
                  );
                }
                return SizedBox();
              },
            ),
          )
        ],
      )),
    );
  }
}
