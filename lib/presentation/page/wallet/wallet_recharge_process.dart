import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/presentation/bloc/add_wallet_balance/add_wallet_balance_bloc.dart';
import 'package:grobay/presentation/bloc/settings/settings_bloc.dart';
import 'package:grobay/presentation/bloc/transaction/transaction_bloc.dart';
import 'package:grobay/presentation/bloc/user_address/useraddress_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/settings_cubit.dart';
import 'package:grobay/presentation/widget/payment_widgets/payment_widget.dart';

class WalletRechargeProcess extends StatelessWidget {
  final String amount;
  final String message;
  const WalletRechargeProcess({Key key, this.amount, this.message})
      : super(key: key);

  Map<String, dynamic> _createRazorpayConfig(SettingsSuccess settingsState) {
    return {
      "key": settingsState.paymentSettingsResponse.paymentMethods.razorpayKey,
      "key_secret": settingsState
          .paymentSettingsResponse.paymentMethods.razorpaySecretKey,
      "currency": AppConfig.CURRENCY,
      "razorpayMode":
          settingsState.paymentSettingsResponse.paymentMethods.isProduction,
    };
  }

  Map<String, dynamic> _createPayStackConfig(SettingsSuccess settingsState) {
    return {
      "public_key": settingsState
          .paymentSettingsResponse.paymentMethods.paystackPublicKey,
      "private_key": settingsState
          .paymentSettingsResponse.paymentMethods.paystackSecretKey,
      "currency": AppConfig.CURRENCY,
      "paystackMode":
          settingsState.paymentSettingsResponse.paymentMethods.isProduction
    };
  }

  Map<String, dynamic> _createFlutterwaveConfig(SettingsSuccess settingsState) {
    return {
      "public_key": settingsState
          .paymentSettingsResponse.paymentMethods.flutterwavePublicKey,
      "private_key": settingsState
          .paymentSettingsResponse.paymentMethods.flutterwaveSecretKey,
      "enc_key": settingsState
          .paymentSettingsResponse.paymentMethods.flutterwaveEncryptionKey,
      "flutterWaveMode":
          settingsState.paymentSettingsResponse.paymentMethods.isProduction,
      "currency_code": settingsState
          .paymentSettingsResponse.paymentMethods.flutterwaveCurrencyCode
    };
  }

  Map<String, dynamic> _createPaypalConfig(
      context, SettingsSuccess settingsState) {
    return {
      "currency_code": settingsState
          .paymentSettingsResponse.paymentMethods.paypalCurrencyCode,
      "paypalMode":
          settingsState.paymentSettingsResponse.paymentMethods.paypalMode,
      "email": settingsState
          .paymentSettingsResponse.paymentMethods.paypalBusinessEmail,
      "itemName":
          "${AppConfig.APP_NAME}  Recharge Wallet for ${AppConfig.CURRENCY} ${this.amount}",
    };
  }

  createStripeConfig(BuildContext context, SettingsSuccess settingsState) {
    return {
      "stripeMode":
          settingsState.paymentSettingsResponse.paymentMethods.isProduction
    };
  }

  Map<String, dynamic> _createPayUConfig(
      context, SettingsSuccess settingsState) {
    return {
      "mearchantKey": settingsState
          .paymentSettingsResponse.paymentMethods.payumoneyMerchantKey,
      "mearchantId": settingsState
          .paymentSettingsResponse.paymentMethods.payumoneyMerchantId,
      "salt":
          settingsState.paymentSettingsResponse.paymentMethods.payumoneySalt,
      "payuMoneyMode":
          settingsState.paymentSettingsResponse.paymentMethods.payumoneyMode,
      "currency": AppConfig.CURRENCY,
      "itemName":
          "${AppConfig.APP_NAME}  Recharge Wallet for ${AppConfig.CURRENCY} ${this.amount}",
    };
  }

  Map<String, dynamic> _createMidtransConfig(SettingsSuccess settingsState) {
    return {
      "client_key": settingsState
          .paymentSettingsResponse.paymentMethods.midtransClientKey,
      "server_key": settingsState
          .paymentSettingsResponse.paymentMethods.midtransServerKey,
      "merchant_id": settingsState
          .paymentSettingsResponse.paymentMethods.midtransMerchantId,
      "currency": AppConfig.CURRENCY,
      "midtransMode":
          settingsState.paymentSettingsResponse.paymentMethods.isProduction
    };
  }

  Map<String, dynamic> _createPaytmConfig(SettingsSuccess settingsState) {
    return {
      "mearchantId":
          settingsState.paymentSettingsResponse.paymentMethods.paytmMerchantId,
      "mearchantKey":
          settingsState.paymentSettingsResponse.paymentMethods.paytmMerchantKey,
      "paytmMode":
          settingsState.paymentSettingsResponse.paymentMethods.paytmMode,
    };
  }

  Map<String, dynamic> _createSSLCommerzConfig(SettingsSuccess settingsState) {
    return {
      "secret": settingsState
          .paymentSettingsResponse.paymentMethods.sslCommereceSecretKey,
      "storeId": settingsState
          .paymentSettingsResponse.paymentMethods.sslCommereceStoreId,
      "mode":
          settingsState.paymentSettingsResponse.paymentMethods.sslCommereceMode,
    };
  }

  void _onPaymentCompleted(
    context,
    options,
  ) async {
    BlocProvider.of<AddWalletBalanceBloc>(context).add(
        FetchAddWalletbalanceEvent(amount: this.amount, message: this.message));
  }

  void _onPaymentCancelled(BuildContext context, String msg) {
    Navigator.pop(context);
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(msg ?? tr('paymentFailed')),
    ));
  }

  @override
  Widget build(BuildContext context) {
    UseraddressState useraddressState =
        BlocProvider.of<UseraddressBloc>(context).state;
    UserLoginSuccess userLoginSuccess =
        BlocProvider.of<UserloginBloc>(context).state;
    SettingsCubitState settingsCubitState =
        BlocProvider.of<SettingsCubit>(context).state;

    return Scaffold(
      body: BlocConsumer<AddWalletBalanceBloc, AddWalletBalanceState>(
        listener: (context, walletState) {
          if (walletState is AddWalletBalanceSuccess) {
            BlocProvider.of<TransactionBloc>(context).add(AddTransactionEvent(
                status: walletState.addWalletResponse.data.status,
                amount: walletState.addWalletResponse.data.amount,
                message: walletState.addWalletResponse.data.message,
                dateCreated: walletState.addWalletResponse.data.dateCreated,
                id: walletState.addWalletResponse.data.id,
                type: walletState.addWalletResponse.data.type));
            BlocProvider.of<UserloginBloc>(context).add(ReloadUserDetails());
            Navigator.pop(context);
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(tr('rechargeSuccessfull'))));
          }
        },
        builder: (context, walletState) {
          if (walletState is AddWalletBalanceSuccess) {
            return Text(tr('success'));
          } else if (walletState is AddWalletBalanceLoading) {
            return Container(
              height: MediaQuery.of(context).size.height,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          } else if (walletState is AddWalletBalanceFailed) {
            return Text(tr('failed'));
          }
          return BlocBuilder<SettingsBloc, SettingsState>(
            builder: (context, settingsState) {
              if (settingsState is SettingsSuccess) {
                return PaymentWidget(
                  onPaymentComplete: (context, options) =>
                      this._onPaymentCompleted(
                    context,
                    options,
                  ),
                  onPaymentCancelled: (msg) =>
                      this._onPaymentCancelled(context, msg),
                  options: {
                    'razorpay': this._createRazorpayConfig(settingsState),
                    'paystack': this._createPayStackConfig(settingsState),
                    'flutterwave': this._createFlutterwaveConfig(settingsState),
                    'paypal': this._createPaypalConfig(context, settingsState),
                    "stripe": this.createStripeConfig(context, settingsState),
                    'payu': this._createPayUConfig(context, settingsState),
                    'midtrans': this._createMidtransConfig(settingsState),
                    'paytm': this._createPaytmConfig(settingsState),
                    'sslcommerz': this._createSSLCommerzConfig(settingsState),
                    //OTHER DETAILS
                    "username": userLoginSuccess.userResponse.user.email,
                    "address": useraddressState is UseraddressSuccess
                        ? useraddressState.selectedAddress.address
                        : "",
                    "city": useraddressState is UseraddressSuccess
                        ? useraddressState.selectedAddress.city
                        : "",
                    "pincode": useraddressState is UseraddressSuccess
                        ? useraddressState.selectedAddress.pincode
                        : "",
                    "state": useraddressState is UseraddressSuccess
                        ? useraddressState.selectedAddress.state
                        : "",
                    "country": useraddressState is UseraddressSuccess
                        ? useraddressState.selectedAddress.country
                        : "",

                    'amount': double.parse(this.amount),
                    'name': userLoginSuccess.userResponse.user.name,
                    'description':
                        "${AppConfig.APP_NAME} Wallet Recharge for ${this.amount}",
                    'contact': userLoginSuccess.userResponse.user.mobile,
                    'email': userLoginSuccess.userResponse.user.email
                  },
                  selectedMethod: settingsCubitState.paymentMethod,
                );
              }

              return Container(
                height: MediaQuery.of(context).size.height,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
