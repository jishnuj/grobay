import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/core/constant/payment_methods.dart';
import 'package:grobay/presentation/bloc/add_wallet_balance/add_wallet_balance_bloc.dart';
import 'package:grobay/presentation/bloc/settings/settings_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/settings_cubit.dart';
import 'package:grobay/presentation/page/wallet/wallet_recharge_process.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/payment_widgets/payment_select_widget.dart';
import 'package:grobay/presentation/widget/ui_helper.dart';
import 'package:logger/logger.dart';

class WalletRechargePage extends StatelessWidget {
  const WalletRechargePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<SettingsBloc>(context).add(FetchpaymentSettings());
    BlocProvider.of<AddWalletBalanceBloc>(context).add(ResetStateEvent());
    var format = NumberFormat.simpleCurrency(
        locale: Platform.localeName, name: AppConfig.CURRENCY);
    TextEditingController amountController = TextEditingController();
    TextEditingController messageController = TextEditingController();
    return Scaffold(
        bottomNavigationBar: BlocBuilder<UserloginBloc, UserloginState>(
          builder: (context, userLoginSuccess) {
            if (userLoginSuccess is UserLoginSuccess) {
              return BlocBuilder<SettingsBloc, SettingsState>(
                builder: (context, settingState) {
                  if (settingState is SettingsSuccess) {
                    return Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      child: ElevatedButton(
                          onPressed: () {
                            if (BlocProvider.of<SettingsCubit>(context)
                                    .state
                                    .paymentMethod
                                    .name !=
                                PAYMENT_METHODS.COD.name) {
                              if (amountController.text.isNotEmpty) {
                                if (double.parse(amountController.text) <=
                                    AppConfig.MAXIMUM_WALLET_REFILL_AMOUNT) {
                                  Navigator.pop(context);
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) {
                                    return WalletRechargeProcess(
                                      amount: amountController.text,
                                      message: messageController.text,
                                    );
                                  }));
                                } else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                          content: Text(
                                              "${tr('maximumRefillAMountIs')} ${AppConfig.MAXIMUM_WALLET_REFILL_AMOUNT}")));
                                }
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                        content:
                                            Text(tr('pleaseEnterAmount'))));
                              }
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                      content: Text(
                                         tr('pleaseSelectPaymentType'))));
                            }
                          },
                          child: Text(tr("recharge"))),
                    );
                  }
                  return SizedBox();
                },
              );
            }
            return LinearProgressIndicator();
          },
        ),
        body: SafeArea(
          child: CustomScrollView(
            slivers: [
              CustomAppbar(
                title: tr('walletRecharge'),
                showBanner: false,
                showSearchField: false,
                showTitle: true,
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: 450,
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextField(
                          controller: amountController,
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.headline1,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              hintStyle: Theme.of(context).textTheme.headline2,
                              hintText: "${AppConfig.CURRENCY} 0",
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none)),
                        ),
                        Text(tr('enterAmount'),
                            style: Theme.of(context)
                                .textTheme
                                .headline6
                                .copyWith(
                                    color:
                                        Palette.walletRechargeTextSecondary)),
                        UIHelper.verticalSpace(30.0),
                        Container(
                          width: 200,
                          child: TextField(
                            minLines: 1,
                            maxLines: 5,
                            controller: messageController,
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                                hintText: tr('enterRemarks'),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                    borderSide:
                                        BorderSide(color: Colors.black))),
                          ),
                        ),
                        UIHelper.verticalSpaceLarge(),
                        Divider(),
                        UIHelper.verticalSpaceLarge(),
                        InkWell(
                          onTap: () {
                            _buildPaymentSelectDialog(context);
                          },
                          child: BlocBuilder<SettingsCubit, SettingsCubitState>(
                            builder: (context, settingsCubitState) {
                              return Text(
                                settingsCubitState.paymentMethod != null
                                    ? settingsCubitState.paymentMethod
                                        .toString()
                                        .split('.')
                                        .last
                                        .toString()
                                    : tr('selectPaymentType'),
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6
                                    .copyWith(
                                        decoration: TextDecoration.underline,
                                        color: settingsCubitState.paymentMethod !=
                                                null
                                            ? Palette
                                                .walletDetailsCardTextPrimary
                                            : Palette
                                                .walletRechargeTextTeritiary),
                              );
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              // _buildPaymentTypes()
            ],
          ),
        ));
  }

  Future<dynamic> _buildPaymentSelectDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            child: SingleChildScrollView(
                child: Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    child: Column(
                      children: [
                        _buildPaymentTypes(),
                        UIHelper.verticalSpaceMedium(),
                        Divider(),
                        Row(
                          children: [
                            Expanded(
                                child: ElevatedButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text(tr('cancel')))),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                                child: ElevatedButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text(tr('ok')))),
                          ],
                        )
                      ],
                    ))),
          );
        });
  }

  Widget _buildPaymentTypes() {
    return BlocBuilder<SettingsCubit, SettingsCubitState>(
      builder: (context, state) {
        return BlocBuilder<SettingsBloc, SettingsState>(
          builder: (context, settingsState) {
            if (settingsState is SettingsSuccess) {
              Logger().i(settingsState.toString());
              return PaymentSelectWidget(
                enableCod: false,
                enablePayumoney: settingsState.paymentSettingsResponse
                        .paymentMethods.payumoneyPaymentMethod
                        .trim() ==
                    "1",
                enableMidtrans: settingsState.paymentSettingsResponse
                        .paymentMethods.midtransPaymentMethod
                        .trim() ==
                    "1",
                enablePaystack: settingsState.paymentSettingsResponse
                        .paymentMethods.paystackPaymentMethod
                        .trim() ==
                    "1",
                enablePaypal: settingsState.paymentSettingsResponse
                        .paymentMethods.paypalPaymentMethod
                        .trim() ==
                    "1",
                enableRazorpay: settingsState.paymentSettingsResponse
                        .paymentMethods.razorpayPaymentMethod
                        .trim() ==
                    "1",
                enableFlutterWave: settingsState.paymentSettingsResponse
                        .paymentMethods.flutterwavePaymentMethod
                        .trim() ==
                    "1",
                enableStripe: settingsState.paymentSettingsResponse
                        .paymentMethods.stripePaymentMethod
                        .trim() ==
                    "1",
                enablePaytm: settingsState.paymentSettingsResponse
                        .paymentMethods.paytmPaymentMethod
                        .trim() ==
                    "1",
                enableSslEcommerce: settingsState.paymentSettingsResponse
                        .paymentMethods.paytmPaymentMethod
                        .trim() ==
                    "1",
              );
            } else if (settingsState is SettingsLoading) {
              return Container(
                height: 200,
                child: Center(child: CircularProgressIndicator()),
              );
            }
            return SizedBox();
          },
        );
      },
    );
  }
}
