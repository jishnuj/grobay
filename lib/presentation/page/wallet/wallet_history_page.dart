import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/api/model/transaction.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/app_constants.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/transaction/transaction_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/page/wallet/wallet_recharge_page.dart';
import 'package:grobay/presentation/widget/currency_widget.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/refersh_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class WalletHistoryPage extends StatelessWidget {
  const WalletHistoryPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<TransactionBloc>(context).add(FetchTransaction(
        type: AppConstants.WALLET_TRANSACTION_HISTORY,
        limit: AppConfig.PAGE_LIMIT.toString()));
    ScrollController _scrollController = new ScrollController();
    _scrollController
      ..addListener(() {
        // var triggerFetchMoreSize =
        //     0.9 * ;

        if (_scrollController.offset >=
                _scrollController.position.maxScrollExtent - 100 &&
            !_scrollController.position.outOfRange) {
          // call fetch more method here
          TransactionState transactionState =
              BlocProvider.of<TransactionBloc>(context).state;
          if (transactionState is TransactionSuccess &&
              !transactionState.isPageLoading) {
            BlocProvider.of<TransactionBloc>(context).add(FetchTransaction(
                type: AppConstants.WALLET_TRANSACTION_HISTORY,
                offset: TransactionBloc.lastOffset.toString(),
                limit: AppConfig.PAGE_LIMIT.toString()));
          }
        }
      });
    return Scaffold(
        bottomNavigationBar: BlocBuilder<TransactionBloc, TransactionState>(
          builder: (context, transactonState) {
            if (transactonState is TransactionSuccess &&
                transactonState.isPageLoading) {
              return LinearProgressIndicator();
            } else if (transactonState is TransactionSuccess &&
                transactonState.isPageFailed) {
              return UIUtilWidget.buildRetryBox("retry", () {
                BlocProvider.of<TransactionBloc>(context).add(FetchTransaction(
                    type: AppConstants.WALLET_TRANSACTION_HISTORY,
                    offset: TransactionBloc.lastOffset.toString(),
                    limit: AppConfig.PAGE_LIMIT.toString()));
              });
            }
            return SizedBox();
          },
        ),
        body: SafeArea(
          child: CustomRefreshWidget(
            onRefresh: () {
              BlocProvider.of<UserloginBloc>(context).add(ReloadUserDetails());
              BlocProvider.of<TransactionBloc>(context).add(FetchTransaction(
                  type: AppConstants.WALLET_TRANSACTION_HISTORY,
                  limit: AppConfig.PAGE_LIMIT.toString()));
            },
            child: CustomScrollView(
              controller: _scrollController,
              slivers: [
                CustomAppbar(
                  title: tr('walletHistory'),
                  showBanner: false,
                  showSearchField: false,
                  showTitle: true,
                ),
                BlocBuilder<UserloginBloc, UserloginState>(
                  builder: (context, userLoginState) {
                    if (userLoginState is UserLoginSuccess) {
                      return SliverToBoxAdapter(
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Palette.teritaryColor)),
                          height: 200,
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                tr('balance'),
                                style: Theme.of(context).textTheme.headline5,
                              ),
                              Text(
                                CurrencyWidget.getFixedPriceCurrencyString(
                                    double.parse(userLoginState
                                        .userResponse.user.balance)),
                                style: Theme.of(context).textTheme.headline3,
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return WalletRechargePage();
                                    }));
                                  },
                                  child: Text(
                                    tr('rechargeYourWallet'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .copyWith(color: Colors.white),
                                  ))
                            ],
                          ),
                        ),
                      );
                    }
                    return SizedBox();
                  },
                ),
                BlocBuilder<TransactionBloc, TransactionState>(
                  builder: (context, transactonState) {
                    if (transactonState is TransactionSuccess) {
                      return SliverList(
                          delegate: SliverChildBuilderDelegate(
                              (context, index) {
                        return _buildWalletTransactionDetailsCard(
                            transactonState
                                .transactionHistoryResponse.transaction[index],
                            index,
                            context);
                      },
                              childCount: transactonState
                                  .transactionHistoryResponse
                                  .transaction
                                  .length));
                    } else if (transactonState is TransactionFailed) {
                      return SliverToBoxAdapter(
                        child: Container(
                          height: MediaQuery.of(context).size.height / 1.3,
                          child: CustomErrorWidget(
                            msg: transactonState.message,
                          ),
                        ),
                      );
                    }
                    return SliverToBoxAdapter(
                      child: Container(
                        height: MediaQuery.of(context).size.height / 1.3,
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ));
  }

  Card _buildWalletTransactionDetailsCard(
      Transaction transaction, int index, BuildContext context) {
    return Card(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${tr('id')} #" + transaction.id.toString(),
              style: Theme.of(context)
                  .textTheme
                  .headline6
                  .copyWith(color: Palette.walletDetailsCardTextPrimary),
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "${tr('via')} ${transaction.type}",
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2
                      .copyWith(color: Palette.walletDetailsCardTextSecondary),
                ),
                Container(
                  child: Text(transaction.status,
                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                          color: Palette.walletDetailsCardTextSecondary)),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(tr('dateAndTime'),
                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                        color: Palette.walletDetailsCardTextSecondary)),
                Text(
                    "${tr('amount')} ${CurrencyWidget.getFixedPriceCurrencyString(double.parse(transaction.amount))}",
                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                        color: Palette.walletDetailsCardTextSecondary))
              ],
            ),
            Text(transaction.dateCreated, style: Theme.of(context).textTheme.bodyText2.copyWith(
                color: Palette.walletDetailsCardTextSecondary
              )),
            Divider(),
            Text(tr('message'), style: Theme.of(context).textTheme.subtitle2.copyWith(
                color: Palette.walletDetailsCardTextSecondary
              )),
            Text(transaction.message, style: Theme.of(context).textTheme.bodyText2.copyWith(
                color: Palette.walletDetailsCardTextSecondary
              )),
          ],
        ),
      ),
    );
  }
}
