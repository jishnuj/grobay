import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:grobay/api/model/address.dart' as MyAddress;
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/cities/cities_bloc.dart';
import 'package:grobay/presentation/bloc/user_address/useraddress_bloc.dart';
import 'package:grobay/presentation/bloc/user_location/user_location_bloc.dart';
import 'package:grobay/presentation/page/map_page.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';

class AddAddressPage extends StatelessWidget {
  AddAddressPage({Key key, this.isUpdate = false, this.updateAddress})
      : super(key: key);
  Map<String, dynamic> formvalues = Map();
  final bool isUpdate;
  final MyAddress.Address updateAddress;
  @override
  Widget build(BuildContext context) {
    formvalues['isDefault'] = false;
    final _formKey = GlobalKey<FormState>();

    if (this.isUpdate) {
      formvalues['address'] = updateAddress.address;
      formvalues['name'] = updateAddress.name;
      formvalues['mobile'] = updateAddress.mobile;
      formvalues['alternate'] = updateAddress.alternateMobile;
      formvalues['type'] = updateAddress.type;
      formvalues['state'] = updateAddress.state;
      formvalues['country'] = updateAddress.country;
      formvalues['isDefault'] = updateAddress.isDefault;
      formvalues['landmark'] = updateAddress.landmark;
      double lat = double.parse(updateAddress.latitude);
      double lng = double.parse(updateAddress.longitude);
      if (lat > 0.0 && lng > 0.0) {
        BlocProvider.of<UserLocationBloc>(context)
            .add(SetCustomUserLocation(lat: lat, long: lng));
      } else {
        BlocProvider.of<UserLocationBloc>(context).add(SetUserLocation());
      }
    } else {
      BlocProvider.of<UserLocationBloc>(context).add(SetUserLocation());
    }

    return Scaffold(
      bottomNavigationBar: BlocConsumer<UseraddressBloc, UseraddressState>(
        listener: (context, userAddressState) {
          if (userAddressState is UseraddressSuccess &&
              userAddressState.lastEvent is SaveAddressEvent) {
            _onUpdateAddAddress(context);
          }
        },
        builder: (context, userAddressState) {
          if (userAddressState is UseraddressLoading) {
            return UIUtilWidget.buildCircularProgressIndicator();
          }
          return _buildSaveButton(_formKey, context);
        },
      ),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
              Palette.primary,
              Palette.primary.withOpacity(0.8),
              Palette.primary,
            ])),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            UIUtilWidget.verticalSpaceCustom(80.0),
            Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      BackButton(),
                      Text(
                        this.isUpdate == true ? tr('editAddress') : tr('addAddress'),
                        style:
                            TextStyle(color: Palette.onSecondary, fontSize: 40),
                      ),
                    ],
                  ),
                  UIUtilWidget.verticalSpaceMedium(),
                  Row(
                    children: [
                      UIUtilWidget.horizontalSpaceCustom(50.0),
                      Text(
                        this.isUpdate == true
                            ? tr('updateAddress')
                            : tr('createNewAddress'),
                        style:
                            TextStyle(color: Palette.onSecondary, fontSize: 18),
                      ),
                    ],
                  )
                ],
              ),
            ),
            UIUtilWidget.verticalSpaceLarge(),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.all(15),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          UIUtilWidget.verticalSpaceMedium(),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color.fromRGBO(225, 95, 27, .3),
                                      blurRadius: 20,
                                      offset: Offset(0, 10))
                                ]),
                            child: BlocBuilder<CitiesBloc, CitiesState>(
                              builder: (context, citiesState) {
                                if (citiesState is CitiesSuccess) {
                                  return Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      _buildType(context, citiesState),
                                      _buildTextField(tr('name'),
                                          prefixIcon: Icon(Icons.person),
                                          validator: (val) {
                                            if (val.isEmpty)
                                              return tr('thisFieldIsRequired');
                                            return null;
                                          },
                                          defaultValue: formvalues['name'],
                                          onChanged: (val) {
                                            formvalues['name'] = val;
                                          }),
                                      _buildTextField(tr('mobileNo'),
                                          prefixIcon: Icon(Icons.phone_android),
                                          textInputType: TextInputType.number,
                                          validator: (val) {
                                            if (val.isEmpty)
                                               return tr('thisFieldIsRequired');
                                            return null;
                                          },
                                          defaultValue: formvalues['mobile'],
                                          onChanged: (val) {
                                            formvalues['mobile'] = val;
                                          }),
                                      _buildTextField(tr('alternateMobNo'),
                                          prefixIcon: Icon(Icons.phone),
                                          textInputType: TextInputType.number,
                                          defaultValue: formvalues['alternate'],
                                          onChanged: (val) {
                                        formvalues['alternate'] = val;
                                      }),
                                      _buildTextField(tr('address'),
                                          prefixIcon: Icon(Icons.location_city),
                                          defaultValue: formvalues['address'],
                                          validator: (val) {
                                        if (val.isEmpty)
                                        return tr('thisFieldIsRequired');
                                        return null;
                                      }, onChanged: (val) {
                                        formvalues['address'] = val;
                                      }),
                                      _buildTextField(tr('landmark'),
                                          prefixIcon: Icon(Icons.landscape),
                                          defaultValue: formvalues['landmark'],
                                          validator: (val) {
                                        if (val.isEmpty)
                                        return tr('thisFieldIsRequired');
                                        return null;
                                      }, onChanged: (val) {
                                        formvalues['landmark'] = val;
                                      }),
                                      Flexible(
                                        child: Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.symmetric(
                                              vertical: 18.0, horizontal: 15.0),
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  bottom: BorderSide(
                                                      color:
                                                          Colors.grey[200]))),
                                          child: Row(
                                            children: [
                                              UIUtilWidget
                                                  .horizontalSpaceCustom(8.0),
                                              Icon(Icons.pin),
                                              UIUtilWidget
                                                  .horizontalSpaceCustom(12.0),
                                              Text(
                                                citiesState
                                                    .selectedAreas.pincode,
                                                textAlign: TextAlign.start,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .subtitle1
                                                    .copyWith(
                                                        color: Colors.grey),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Expanded(
                                            child: _buildTextField(tr('state'),
                                                prefixIcon: Icon(Icons
                                                    .location_city_rounded),
                                                defaultValue:
                                                    formvalues['state'],
                                                validator: (val) {
                                              if (val.isEmpty)
                                                return tr('thisFieldIsRequired');
                                              return null;
                                            }, onChanged: (val) {
                                              formvalues['state'] = val;
                                            }),
                                          ),
                                          UIUtilWidget.horizontalSpaceLarge(),
                                          Expanded(
                                            child: _buildTextField(tr('country'),
                                                prefixIcon: Icon(Icons
                                                    .location_city_outlined),
                                                defaultValue:
                                                    formvalues['country'],
                                                validator: (val) {
                                              if (val.isEmpty)
                                                       return tr('thisFieldIsRequired');
                                              return null;
                                            }, onChanged: (val) {
                                              formvalues['country'] = val;
                                            }),
                                          )
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Expanded(
                                              child: _buildCities(
                                                  context, citiesState)),
                                          Expanded(
                                              child: _buildAreas(
                                                  context, citiesState))
                                        ],
                                      ),
                                      SaveAsDefaultCheckBox(
                                        enabled: formvalues['isDefault'] == "1"
                                            ? true
                                            : false,
                                        onChanged: (val) {
                                          formvalues['isDefault'] = val;
                                        },
                                      ),
                                      BlocBuilder<UserLocationBloc,
                                              UserLocationState>(
                                          builder:
                                              (context, userLocationState) {
                                        if (userLocationState
                                            is UserLocationSuccess) {
                                          return Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10.0),
                                            child: Row(
                                              children: [
                                                Icon(Icons.location_pin),
                                                UIUtilWidget.horizontalSpaceSmall(),
                                                Flexible(
                                                  child: Text(
                                                      userLocationState
                                                              .address.name +
                                                          "," +
                                                          userLocationState
                                                              .address.street +
                                                          "," +
                                                          userLocationState
                                                              .address
                                                              .subAdministrativeArea +
                                                          "," +
                                                          userLocationState
                                                              .address
                                                              .subLocality,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .subtitle2),
                                                ),
                                              ],
                                            ),
                                          );
                                        }
                                        return UIUtilWidget.verticalSpaceCustom(
                                            0.0);
                                      }),
                                      UIUtilWidget.verticalSpaceLarge(),
                                      _buildGoogleMap(),
                                      Container(
                                        child: Center(
                                          child: OutlinedButton(
                                            child: Text(tr('updateYourLocation')),
                                            onPressed: () {
                                              Navigator.push(context,
                                                  MaterialPageRoute(
                                                      builder: (context) {
                                                return MapPage();
                                              }));
                                            },
                                          ),
                                        ),
                                      )
                                    ],
                                  );
                                } else if (citiesState is CitiesFailed) {
                                  return Container(
                                    height: MediaQuery.of(context).size.height /
                                        1.5,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [Text(citiesState.message)],
                                    ),
                                  );
                                }
                                return UIUtilWidget
                                    .buildCircularProgressIndicator();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  InkWell _buildSaveButton(
      GlobalKey<FormState> _formKey, BuildContext context) {
    return InkWell(
      onTap: () {
        _onAddressSave(_formKey, context);
      },
      child: Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: Theme.of(context).highlightColor),
        child: Center(
          child: Text(
            tr('save'),
            style: TextStyle(
                color: Palette.onSecondary, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }

  void _onAddressSave(GlobalKey<FormState> _formKey, BuildContext context) {
    if (_formKey.currentState.validate()) {
      UserLocationState userLocationState =
          BlocProvider.of<UserLocationBloc>(context).state;
      CitiesSuccess citiesSuccess = BlocProvider.of<CitiesBloc>(context).state;
      BlocProvider.of<UseraddressBloc>(context).add(SaveAddressEvent(
          id: updateAddress != null ? this.updateAddress.id : "",
          address: formvalues['address'],
          name: formvalues['name'],
          mobile: formvalues['mobile'],
          alternateMobile: formvalues['alternate'],
          pincodeId: citiesSuccess.selectedAreas.pincodeId,
          areaId: citiesSuccess.selectedAreas.id,
          cityId: citiesSuccess.selectedAreas.cityId,
          type: formvalues['type'],
          state: formvalues['state'],
          latitude: (userLocationState is UserLocationSuccess)
              ? userLocationState.coordinates.latitude
              : 0.0,
          longitude: (userLocationState is UserLocationSuccess)
              ? userLocationState.coordinates.longitude
              : 0.0,
          country: formvalues['country'],
          isUpdate: this.isUpdate ?? false,
          isDefault: formvalues['isDefault'].toString(),
          landmark: formvalues['landmark']));
    } else {
      UIUtilWidget.showSnackBar(tr('pleaseEnterAllFields'));
    }
  }

  void _onUpdateAddAddress(BuildContext context) {
    String msg = this.isUpdate
        ? tr('addressUpdatedSuccessfully')
        : tr('addressAddedSuccessfully');

    UIUtilWidget.showSnackBar(msg);
    Navigator.pop(context);
  }

  Widget _buildGoogleMap() {
    GoogleMapController _controller;
    return BlocBuilder<UserLocationBloc, UserLocationState>(
      builder: (context, userLocationState) {
        if (userLocationState is UserLocationSuccess) {
          var markerIdVal = UniqueKey().toString();
          final MarkerId markerId = MarkerId(markerIdVal);
          // creating a new MARKER
          final Marker marker = Marker(
            markerId: markerId,
            position: LatLng(userLocationState.coordinates.latitude,
                userLocationState.coordinates.longitude),
            infoWindow: InfoWindow(title: markerIdVal, snippet: '*'),
            onTap: () {},
          );

          CameraPosition _myCurrentLocation = CameraPosition(
            target: LatLng(userLocationState.coordinates.latitude,
                userLocationState.coordinates.longitude),
          );
          if (_controller != null) {
            _controller
                .moveCamera(CameraUpdate.newCameraPosition(_myCurrentLocation));
          }
          return Container(
              height: 150,
              child: Stack(
                children: [
                  GoogleMap(
                    markers: Set<Marker>.of([marker]),
                    mapType: MapType.normal,
                    initialCameraPosition: _myCurrentLocation,
                    onMapCreated: (GoogleMapController controller) {
                      _controller = controller;
                    },
                    onCameraMove: (cameraPosition) {
                      MapCameraSave.cameraPosition = cameraPosition;
                    },
                  ),
                ],
              ));
        } else if (userLocationState is UserLocationFailed) {
          return CustomErrorWidget(
            msg: userLocationState.message,
          );
        }
        return CircularProgressIndicator();
      },
    );
  }

  Container _buildTextField(String label,
      {String defaultValue,
      bool isEnabled = true,
      Widget prefixIcon,
      String Function(String) validator,
      TextInputType textInputType,
      Function onChanged}) {
    InputDecoration inputDecoration = InputDecoration(
        hintText: label,
        hintStyle: TextStyle(color: Colors.grey),
        border: InputBorder.none);
    if (prefixIcon != null) {
      inputDecoration = InputDecoration(
          hintText: label,
          prefixIcon: prefixIcon,
          hintStyle: TextStyle(color: Colors.grey),
          border: InputBorder.none);
    }
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey[200]))),
      child: TextFormField(
        enabled: isEnabled,
        onChanged: onChanged,
        validator: validator,
        keyboardType: textInputType ?? TextInputType.name,
        initialValue: defaultValue,
        decoration: inputDecoration,
      ),
    );
  }

  Container _buildAreas(BuildContext context, CitiesSuccess citiesSuccess) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 10.0),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Palette.teritaryColor))),
        child: DropdownButton(
          underline: UIUtilWidget.verticalSpaceCustom(0.0),
          hint: Text(tr('areas')),
          isExpanded: true,
          value: citiesSuccess.selectedAreas,
          onChanged: (value) {
            BlocProvider.of<CitiesBloc>(context)
                .add(SelectAreaEvent(selectedAreas: value));
          },
          items: citiesSuccess.selectedCity.areas
              .map((e) => DropdownMenuItem(
                    value: e,
                    child: Text("${e.areaName}"),
                  ))
              .toList(),
        ));
  }

  Container _buildCities(BuildContext context, CitiesSuccess citiesSuccess) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 10.0),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Palette.teritaryColor))),
        child: DropdownButton(
          underline: UIUtilWidget.verticalSpaceCustom(0.0),
          hint: Text(tr('cities')),
          isExpanded: true,
          value: citiesSuccess.selectedCity,
          onChanged: (value) {
            BlocProvider.of<CitiesBloc>(context)
                .add(SelectCityevent(selectedCity: value));
          },
          items: citiesSuccess.citiesResponse.data
              .map((e) => DropdownMenuItem(
                    value: e,
                    child: Text("${e.cityName}"),
                  ))
              .toList(),
        ));
  }

  Container _buildType(BuildContext context, CitiesSuccess citiesSuccess) {
    if (!this.isUpdate) {
      formvalues['type'] = "Home";
    }
    return Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Palette.teritaryColor))),
        child: DropdownButtonFormField(
          hint: Text(tr('type')),
          value: this.isUpdate == true ? formvalues['type'] : "Home",
          isExpanded: true,
          onChanged: (val) {
            formvalues['type'] = val;
          },
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.location_city_rounded),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent)),
            disabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent)),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent)),
          ),
          items: [
            DropdownMenuItem(
              value: "Home",
              child: Text(tr('addHome'),
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1
                      .copyWith(color: Palette.disabledColor)),
            ),
            DropdownMenuItem(
              value: "Office",
              child: Text(tr("addOffice")),
            )
          ],
        ));
  }
}

class SaveAsDefaultCheckBox extends StatefulWidget {
  Function onChanged;
  bool enabled;
  SaveAsDefaultCheckBox({Key key, this.onChanged, this.enabled})
      : super(key: key);

  @override
  _SaveAsDefaultCheckBoxState createState() => _SaveAsDefaultCheckBoxState();
}

class _SaveAsDefaultCheckBoxState extends State<SaveAsDefaultCheckBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: CheckboxListTile(
                title: Text(tr('setAsDefaultAddress')),
                value: widget.enabled,
                onChanged: (val) {
                  setState(() {
                    widget.onChanged(val ? "1" : "0");
                    widget.enabled = val;
                  });
                }),
          ),
          Divider(),
          UIUtilWidget.verticalSpaceCustom(30.0)
        ],
      ),
    );
  }
}

class MapCameraSave {
  static CameraPosition cameraPosition;
}
