import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/cart/cart_bloc.dart';
import 'package:grobay/presentation/bloc/user_address/useraddress_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/widget/address_list_widget.dart';
import 'package:grobay/presentation/widget/cart_footer_widget.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/ui_util_widget.dart';
import 'package:visibility_detector/visibility_detector.dart';

class AddressSelectpage extends StatelessWidget {
  final bool viewMode;
  const AddressSelectpage({Key key, this.viewMode = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!this.viewMode) {
      CartBloc.steps = STEPS.ADDRESS;
    }
    BlocProvider.of<UseraddressBloc>(context).add(FetchUserAddressEvent());
    return VisibilityDetector(
      key: UniqueKey(),
      onVisibilityChanged: (visibilityInfo) {
        var visiblePercentage = visibilityInfo.visibleFraction * 100;
        if (visiblePercentage == 100) {}
      },
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: Palette.buttonBg,
          foregroundColor: Palette.buttonText,
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.pushNamed(context, "addaddress");
          },
        ),
        body: SafeArea(
          child: CustomScrollView(
            slivers: [
              CustomAppbar(
                title: tr('address'),
                showBanner: false,
                showLogo: false,
                showTitle: true,
                showLocation: false,
                showSearchField: false,
                showAction: false,
              ),
             
              AddressListWidget(),
            ],
          ),
        ),
        bottomNavigationBar: this.viewMode
            ? Container(
                height: 10,
              )
            : _buildCheckoutCard(context),
      ),
    );
  }

  Widget _buildCheckoutCard(BuildContext context) {
    return CartFooterWidget(
      onPressed: (CartSuccess cartState) {
        _onAdressCheckout(context, cartState);
      },
    );
  }

  void _onAdressCheckout(BuildContext context, CartSuccess cartState) {
    UseraddressState useraddressState =
        BlocProvider.of<UseraddressBloc>(context).state;
    bool isDeliverableToThisAddress = true;

    if (useraddressState is UseraddressSuccess &&
        useraddressState.selectedAddress != null) {
      for (int i = 0; i < cartState.cartResponse.cart.data.length; i++) {
        isDeliverableToThisAddress = BlocProvider.of<CartCubit>(context)
            .checkProductAvailabelInPincode(
                cartState.cartResponse.cart.data[i].item[0].pincodes,
                useraddressState.selectedAddress.pincodeId);
        if (isDeliverableToThisAddress == false) {
          break;
        }
      }
      if (isDeliverableToThisAddress) {
        Navigator.pushNamed(context, "checkout");
      } else {
        UIUtilWidget.showSnackBar(tr('notDeliverableToThisAddress'));
      }
    } else if (useraddressState is UseraddressSuccess &&
        useraddressState.userAddressResponse.data.length == 0) {
      UIUtilWidget.showSnackBar(tr('pleaseAddAnAddress'));
    } else {
      UIUtilWidget.showSnackBar(tr('selectDeliveryAddress'));
    }
  }
}
