import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/presentation/bloc/cart/cart_bloc.dart';
import 'package:grobay/presentation/bloc/cities/cities_bloc.dart';
import 'package:grobay/presentation/bloc/search/search_bloc.dart';
import 'package:grobay/presentation/bloc/startup/startup_bloc.dart';
import 'package:grobay/presentation/bloc/user_address/useraddress_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/page/category/categories_list_page.dart';
import 'package:grobay/presentation/page/order/order_list_page.dart';
import 'package:grobay/presentation/page/wishlist_page.dart';
import 'package:grobay/presentation/widget/category_lsit_widget.dart';
import 'package:grobay/presentation/widget/custom_appbar.dart';
import 'package:grobay/presentation/widget/custom_error_widget.dart';
import 'package:grobay/presentation/widget/featured_section_widget.dart';
import 'package:grobay/presentation/widget/refersh_widget.dart';
import 'package:grobay/presentation/widget/seller_widget.dart';
import 'package:grobay/presentation/widget/side_menu_widget.dart';
import 'package:grobay/presentation/widget/skelton_loaders.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:visibility_detector/visibility_detector.dart';

class HomePage extends StatefulWidget {
  int index;
  HomePage({Key key, this.index = 0}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {
    CartBloc.steps = STEPS.CART;
    _initStartupAPIS(context);

    // Dont Know if this is needed here?
    // BlocProvider.of<OrderBloc>(context).add(FetchOrderEvent());

    return VisibilityDetector(
      key: UniqueKey(),
      onVisibilityChanged: onVisibilityChanged,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: SideMenuWidget(),
        backgroundColor: Palette.secondary,
        bottomNavigationBar: _buildBottomNavigationBar(context),
        body: SafeArea(child: BlocBuilder<StartupBloc, StartupState>(
          builder: (context, appState) {
            if (appState is StartupSuccess) {
              return _buildPage();
            } else if (appState is StartupFailed) {
              return _buildErrorBox(appState);
            }
            return HomePageSkeltonloader();
          },
        )),
      ),
    );
  }

  void _initStartupAPIS(BuildContext context) {
    if (BlocProvider.of<UserloginBloc>(context).state is UserLoginSuccess) {
      BlocProvider.of<UserloginBloc>(context).add(ReloadUserDetails());
      BlocProvider.of<UseraddressBloc>(context).add(FetchUserAddressEvent());
    }
  }

  onVisibilityChanged(visibilityInfo) {
    var visiblePercentage = visibilityInfo.visibleFraction * 100;
    if (visiblePercentage == 100) {
      BlocProvider.of<SearchBloc>(context).add(ResetSearchEvent());
      BlocProvider.of<CitiesBloc>(context).add(FetchCitiesevent());
    }
  }

  Widget _buildErrorBox(StartupFailed startupFailed) {
    return Container(
      child: CustomErrorWidget(
        callerName: "home_page",
        msg: startupFailed.message,
        onNetworkError: () {
          BlocProvider.of<StartupBloc>(context).add(FetchStartupEvent());
        },
      ),
    );
  }

  BottomNavigationBar _buildBottomNavigationBar(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      backgroundColor: Palette.onPrimary,
      selectedIconTheme: Theme.of(context).primaryIconTheme.copyWith(
            color: Palette.buttonBg,
          ),
      selectedItemColor: Palette.buttonBg,
      unselectedItemColor: Palette.disabledColor,
      unselectedIconTheme: Theme.of(context)
          .primaryIconTheme
          .copyWith(color: Palette.disabledColor),
      currentIndex: widget.index, // this will be set when a new tab is tapped
      onTap: (i) {
        setState(() {
          widget.index = i;
        });
      },
      items: _buildBottomNavigationItem(),
    );
  }

  List<BottomNavigationBarItem> _buildBottomNavigationItem() {
    return [
      BottomNavigationBarItem(
        icon: new Icon(Icons.home),
        label: tr('home'),
      ),
      BottomNavigationBarItem(
        icon: new Icon(Icons.category),
        label: tr('category'),
      ),
      BottomNavigationBarItem(
          icon: Icon(Icons.favorite), label: tr('favourite')),
      BottomNavigationBarItem(
          icon: Icon(Icons.bike_scooter), label: tr('orders'))
    ];
  }

  _onHomePageRefresh(BuildContext context) {
    if (widget.index == 0) {
      BlocProvider.of<SearchBloc>(context).add(ResetSearchEvent());
      BlocProvider.of<CitiesBloc>(context).add(FetchCitiesevent());
      BlocProvider.of<StartupBloc>(context).add(FetchStartupEvent());
      _initStartupAPIS(context);
    }
  }

  _buildPage() {
    if (widget.index == 0) {
      return _buildHomePage();
    } else if (widget.index == 1) {
      return _buildCategoriesListPage();
    } else if (widget.index == 2) {
      return _buildWishListPage();
    } else if (widget.index == 3) {
      return _buildOrderPage();
    }
  }

  Widget _buildHomePage() {
    return CustomRefreshWidget(
      onRefresh: () {
        this._onHomePageRefresh(context);
      },
      child: CustomScrollView(
        slivers: [
          //Banner is inside AppBar
          CustomAppbar(
            title: tr('hiThere'),
            showLocation: true,
          ),
          // Category Horizontal List
          CategoryListWidget(onViewAllTap: (i) {
            setState(() {
              widget.index = i;
            });
          }),
          // Featured Section
          FeaturedSectionWidget(),
          // Seller List
          SellerWidget(),
        ],
      ),
    );
  }

  Widget _buildCategoriesListPage() {
    return CategoriesListPage();
  }

  Widget _buildWishListPage() {
    return WishListPage();
  }

  Widget _buildOrderPage() {
    return OrderListPage();
  }
}
