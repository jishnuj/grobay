import 'package:grobay/api/request/base_request.dart';

class OrderFetchRequest extends BaseRequest {
  String userId;
  String orderId;
  String limit;
  String offset;
  OrderFetchRequest({
    this.userId,
    this.offset,
    this.limit,
    this.orderId
  }) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    if (this.orderId != null) {
      json.addAll({"order_id": this.orderId});
    }
    if (this.limit != null) {
      json.addAll({"limit": this.limit});
    }
    if (this.offset != null) {
      json.addAll({"offset": this.offset});
    }
    json.addAll({
      "get_orders": "1",
      "user_id": this.userId,
    });
    return json;
  }
}
