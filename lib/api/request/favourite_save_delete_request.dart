import 'package:flutter/widgets.dart';
import 'package:grobay/api/request/base_request.dart';

class FavouriteSaveDeleteRequest extends BaseRequest {
  String userId;
  String productId;
  String removeFromFavourites;
  FavouriteSaveDeleteRequest(
      {@required this.userId, this.productId, this.removeFromFavourites})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    if (this.removeFromFavourites != null) {
      json.addAll({"remove_from_favorites": "1"});
    } else {
      json.addAll({"add_to_favorites": "1"});
    }
    json.addAll({"product_id": this.productId, "user_id": this.userId});
    return json;
  }
}
