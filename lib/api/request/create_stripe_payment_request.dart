import 'package:grobay/api/request/base_request.dart';

class CreateStripePaymentRequest extends BaseRequest {
  final String username;
  final String addresss;
  final String postalCode;
  final String city;
  final String amount;
  final String orderId;
  CreateStripePaymentRequest(
      {this.username,
      this.addresss,
      this.postalCode,
      this.city,
      this.amount,
      this.orderId})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "name": username,
      "address_line1": addresss,
      "postal_code": postalCode,
      "city": city,
      "amount": amount,
      "order_id": orderId
    });
    return json;
  }
}
