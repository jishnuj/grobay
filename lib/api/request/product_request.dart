import 'package:grobay/api/request/base_request.dart';

class ProductRequest extends BaseRequest {
  String offset;
  int getAllProducts;
  int getSimilarProducts;
  String search;
  String userId;
  String categoryId;
  String subcategoryId;
  String productId;
  String sellerId;
  String pincodeId;
  ProductRequest(
      {this.offset,
      this.getAllProducts,
      this.getSimilarProducts,
      this.search,
      this.userId,
      this.categoryId,
      this.subcategoryId,
      this.pincodeId,
      this.productId,
      this.sellerId})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();

    if (this.getSimilarProducts != null) {
      json.addAll({"get_similar_products": 1.toString()});
    } else {
      json.addAll({"get_all_products": 1.toString()});
    }
    json.addAll({
      "offset": this.offset != null ? this.offset : "",
      "search": this.search ?? "",
      "user_id": this.userId != null ? this.userId.toString() : "",
      "category_id": this.categoryId != null ? this.categoryId.toString() : "",
      "product_id": this.productId != null ? this.productId.toString() : "",
      "pincode_id": this.pincodeId != null ? this.pincodeId.toString() : "",
      "subcategory_id":
          this.subcategoryId != null ? this.subcategoryId.toString() : "",
      "seller_id": this.sellerId != null ? this.sellerId.toString() : "",
    });
    return json;
  }
}
