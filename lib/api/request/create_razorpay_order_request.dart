import 'package:grobay/api/request/base_request.dart';

class CreateRazorpayOrderRequest extends BaseRequest {
  final String amount;
  CreateRazorpayOrderRequest({
    this.amount,
  }) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "amount": amount,
    });
    return json;
  }
}
