import 'package:grobay/api/request/base_request.dart';

class Citiesrequest extends BaseRequest {
  Citiesrequest() : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();

    json.addAll({
      "get_cities": "1",
    });
    return json;
  }
}
