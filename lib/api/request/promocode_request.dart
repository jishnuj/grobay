// validate_promo_code: 1
// total: 118.0
// accesskey: 90336
// user_id: 9
// promo_code: GROBAY10

import 'package:flutter/widgets.dart';
import 'package:grobay/api/request/base_request.dart';

class PromoCodeRequest extends BaseRequest {
  String userId;
  String total;
  String promoCode;
  PromoCodeRequest({this.total, @required this.userId, this.promoCode})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();

    json.addAll({
      "user_id": this.userId,
      "validate_promo_code": "1",
      "total": this.total,
      "promo_code": this.promoCode
    });
    return json;
  }
}
