import 'package:grobay/api/request/base_request.dart';

class TransactionHistoryRequest extends BaseRequest {
  String userId;
  String type;
  String offset;
  String limit;

  TransactionHistoryRequest({this.type, this.userId, this.limit, this.offset})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "get_user_transactions": "1",
      "user_id": this.userId,
      "type": this.type,
    });
    if (this.offset != null) {
      json.addAll({"offset": this.offset.toString()});
    }
    if (this.offset != null) {
      json.addAll({"limit": this.limit.toString()});
    }

    return json;
  }
}
