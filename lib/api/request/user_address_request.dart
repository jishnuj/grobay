import 'package:grobay/api/request/base_request.dart';

class UserAddressRequest extends BaseRequest {
  String getAddress;
  String userId;
  int limit;
  int offset;
  UserAddressRequest({
    this.userId,
    this.limit,
    this.offset
  }) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "get_addresses": this.getAddress ?? "1",
      "user_id": this.userId,
    });
    if (this.limit != null) {
      json.addAll({
        "offset": this.limit.toString(), // {optional}
      });
    }
    if (this.offset != null) {
      json.addAll({"limit": this.offset.toString()});
    }
    return json;
  }
}
