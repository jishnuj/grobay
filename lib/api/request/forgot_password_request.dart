import 'package:grobay/api/request/base_request.dart';

class ForgotPasswordRequest extends BaseRequest {
  String password;
  String mobile;

  ForgotPasswordRequest({this.password, this.mobile});

  ForgotPasswordRequest.fromJson(Map<String, dynamic> json) {
    password = json['password'];
    mobile = json['mobile'];
  }

  Map<String, String> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data['password'] = this.password;
    data['mobile'] = this.mobile;
    data['type'] = "forgot-password-mobile";
    return data;
  }
}
