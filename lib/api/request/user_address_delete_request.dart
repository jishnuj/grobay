import 'package:grobay/api/request/base_request.dart';

class UserAddressDeleteRequest extends BaseRequest {
  String deleteAddress;
  String id;
  UserAddressDeleteRequest({this.deleteAddress, this.id}) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "delete_address": this.deleteAddress ?? "1",
      "id": this.id.toString(),
    });

    return json;
  }
}
