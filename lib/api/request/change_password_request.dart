import 'package:grobay/api/request/base_request.dart';

class ChangePasswordRequest extends BaseRequest {
  String userId;
  String password;
  ChangePasswordRequest({this.userId, this.password}) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();

    json.addAll({
      "user_id": this.userId,
      "type": "change-password",
      "password": this.password
    });
    return json;
  }
}
