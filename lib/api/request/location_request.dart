import 'package:grobay/api/request/base_request.dart';

class LocationRequest extends BaseRequest {
  int getpincodes;
  LocationRequest({this.getpincodes=1}) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
     "get_pincodes":this.getpincodes.toString()
    });
    return json;
  }
}
