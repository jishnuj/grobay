import 'package:grobay/api/request/base_request.dart';

class Verifypaystackrequest extends BaseRequest {
  final String email;
  final String amount;
  final String reference;
  Verifypaystackrequest({this.email, this.amount, this.reference}) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "verify_paystack_transaction": "1",
      "email": this.email,
      "amount": this.amount,
      "reference": this.reference
    });
    return json;
  }
}
