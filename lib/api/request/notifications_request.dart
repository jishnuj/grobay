import 'package:grobay/api/request/base_request.dart';

class NotificationRequest extends BaseRequest {
  NotificationRequest() : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "offset": "0",
      "accesskey": "90336",
      "get-notifications": "1",
      "limit": "1010"
    });
    return json;
  }
}
