import 'package:grobay/api/request/base_request.dart';

class CreateTransactionRequest extends BaseRequest {
  String txnId;
  String transactiondate;
  String amount;
  String userId;
  String taxPercentage;
  String type;
  String orderId;
  String status;
  CreateTransactionRequest(
      {this.txnId,
      this.transactiondate,
      this.amount,

      this.userId,
      this.taxPercentage,
      this.type,
      this.orderId,
      this.status})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "txn_id": this.txnId,
      "transaction_date": this.transactiondate,
      "add_transaction": "1",
      "amount": this.amount,
      "user_id": this.userId,
      "tax_percentage": this.taxPercentage ?? "0.0",
      "type": this.type,
      "message": "Order+Successfully+Placed",
      "order_id": this.orderId,
      "status": this.status
    });
    return json;
  }
}
