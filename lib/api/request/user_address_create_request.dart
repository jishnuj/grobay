import 'package:grobay/api/request/base_request.dart';

class UsserAddressCreateRequest extends BaseRequest {
  String address;
  String id;
  String userId;
  String name;
  String mobile;
  String alternateMobile;
  String pincodeId;
  String type;
  String state;
  String country;
  String areaId;
  String cityId;
  String landmark;
  String isDefault;
  double latitude;
  double longitude;
  bool isUpdate;
  UsserAddressCreateRequest(
      {this.address,
      this.id,
      this.userId,
      this.name,
      this.mobile,
      this.alternateMobile,
      this.pincodeId,
      this.type,
      this.state,
      this.country,
      this.areaId,
      this.isDefault,
      this.isUpdate = false,
      this.cityId,
      this.latitude,
      this.longitude,
      this.landmark})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    if (this.isUpdate) {
      json.addAll({"update_address": "1", "id": this.id});
    } else {
      json.addAll({
        "add_address": "1",
      });
    }

    json.addAll({
      "address": this.address ?? "",
      "user_id": this.userId ?? "",
      "name": this.name ?? "",
      "mobile": this.mobile ?? "",
      "pincode_id": this.pincodeId ?? "",
      "type": this.type ?? "",
      "state": this.state ?? "",
      "country": this.country ?? "",
      "area_id": this.areaId ?? "",
      "city_id": this.cityId ?? "",
      "landmark": this.landmark ?? "",
      "alternate_mobile": this.alternateMobile ?? "",
      "is_default": this.isDefault ?? "0",
      "latitude": this.latitude != null ? this.latitude.toString() : "0.0",
      "longitude": this.longitude != null ? this.longitude.toString() : "0.0",
    });

    return json;
  }
}
