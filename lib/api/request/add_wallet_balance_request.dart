import 'package:grobay/api/request/base_request.dart';

class AddWalletBalanceRequest extends BaseRequest {
  String userId;
  String amount;
  String message;
  AddWalletBalanceRequest({this.userId, this.amount, this.message}) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "add_wallet_balance": "1",
      "type": "credit",
      "amount": this.amount,
      "message": this.message??"",
      "user_id":this.userId
    });

    return json;
  }
}
