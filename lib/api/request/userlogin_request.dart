import 'package:grobay/api/request/base_request.dart';

class UserLoginrequest extends BaseRequest {
  String mobile;
  String password;
  String fcmId;
  String login;
  UserLoginrequest({this.mobile, this.password, this.fcmId, this.login})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "mobile": this.mobile.toString(),
      "password": this.password.toString(),
      "fcm_id": this.fcmId.toString(),
      "login": this.login ?? "1"
    });
    return json;
  }
}
