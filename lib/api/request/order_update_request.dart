import 'package:grobay/api/request/base_request.dart';

class OrderUpdateRequest extends BaseRequest {
  String orderItemId;
  String orderId;
  String status;
  OrderUpdateRequest({this.orderItemId, this.orderId, this.status}) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "order_item_id": this.orderItemId,
      "update_order_status": "1",
      "order_id": this.orderId,
      "status": this.status
    });
    return json;
  }
}
