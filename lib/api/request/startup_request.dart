import 'package:flutter/widgets.dart';
import 'package:grobay/api/request/base_request.dart';

class StartupRequest extends BaseRequest {
  int settings;
  int gettimezone;
  String pincodeId;
  String userId;
  StartupRequest({
    this.gettimezone=1,
    this.settings=1,
    @required this.userId,
    this.pincodeId
  }) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "settings": this.settings.toString(),
      "get_timezone": this.gettimezone.toString(),
      "pincode_id":this.pincodeId??"",
      "userId":this.userId??""
    });
    return json;
  }
}
