import 'package:flutter/widgets.dart';
import 'package:grobay/api/request/base_request.dart';

class CartSaverequest extends BaseRequest {
  String userId;
  String productId;
  String productvriantId;
  String qty;
  bool addMultipleItems;
  bool removeAllFromcart;
  CartSaverequest(
      {@required this.userId,
      this.removeAllFromcart=false,
      this.productId,
      this.productvriantId,
      this.qty,
      this.addMultipleItems = false})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    if (this.removeAllFromcart) {
      json.addAll({"user_id": this.userId, "remove_from_cart": "1"});
    } else {
      json.addAll({
        "user_id": this.userId,
        "product_variant_id": this.productvriantId,
        "qty": qty
      });
      if (this.addMultipleItems) {
        json.addAll({"add_multiple_items": "1"});
      } else {
        json.addAll({
          "add_to_cart": "1",
          "product_id": this.productId,
        });
      }
    }
    return json;
  }
}
