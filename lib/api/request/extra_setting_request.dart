import 'package:grobay/api/request/base_request.dart';

class ExtraSettingsRequest extends BaseRequest {
  final String getContact;
  final String getAboutUs;
  final String getTerms;
  final String getPrivacy;
  ExtraSettingsRequest(
      {this.getContact, this.getAboutUs, this.getTerms, this.getPrivacy})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    if (this.getContact != null) {
      json.addAll({
        "get_contact": "1",
      });
    }
    if (this.getAboutUs != null) {
      json.addAll({
        "get_about_us": "1",
      });
    }
    if (this.getTerms != null) {
      json.addAll({
        "get_terms": "1",
      });
    }
    if (this.getPrivacy != null) {
      json.addAll({
        "get_privacy": "1",
      });
    }
    json.addAll({
      "settings": "1",
    });

    return json;
  }
}
