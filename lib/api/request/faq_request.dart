import 'package:grobay/api/request/base_request.dart';

class Faqrequest extends BaseRequest {
  Faqrequest() : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();

    json.addAll({"get_faqs": "1"});
    return json;
  }
}
