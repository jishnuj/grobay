import 'package:grobay/api/request/base_request.dart';

class UpdateProfileRequest extends BaseRequest {
  String userId;
  String name;
  String mobile;
  String email;
  String latitude;
  String longitude;
  String fcmId;
  UpdateProfileRequest(
      {this.userId,
      this.name,
      this.mobile,
      this.email,
      this.latitude,
      this.longitude,
      this.fcmId})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();

    json.addAll({
      "user_id": this.userId,
      "latitude": this.latitude??"0",
      "longitude": this.longitude??"0",
      "name": this.name,
      "mobile": this.mobile,
      "type": "edit-profile",
      "email": this.email,
      "fcm_id": this.fcmId??"0"
    });
    return json;
  }
}
