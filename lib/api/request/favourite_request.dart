import 'package:flutter/widgets.dart';
import 'package:grobay/api/request/base_request.dart';

class FavouriteRequest extends BaseRequest {
  String userId;
  FavouriteRequest({@required this.userId}) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();

    json.addAll({"get_favorites": "1", "user_id": this.userId});
    return json;
  }
}
