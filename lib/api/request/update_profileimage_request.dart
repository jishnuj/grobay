import 'dart:io';
import 'package:grobay/api/request/base_request.dart';

class UpdateProfileImageRequest extends BaseRequest {
  String userId;
  File file;
  UpdateProfileImageRequest(
      {this.userId,
      this.file})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();

    json.addAll({
      "user_id": this.userId,
      "type":"upload_profile"
    });
    return json;
  }
}
