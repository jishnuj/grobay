import 'package:grobay/api/request/base_request.dart';

class KeywordRequest extends BaseRequest {
  int getAllProductsName;
  KeywordRequest(
      {this.getAllProductsName})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
    
      "get_all_products_name": this.getAllProductsName != null
          ? this.getAllProductsName.toString()
          : 1.toString(),
    
    });
    return json;
  }
}
