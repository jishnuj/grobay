import 'package:grobay/api/request/base_request.dart';

class SubCategoryRequest extends BaseRequest {
  int categoryId;
  SubCategoryRequest({this.categoryId}) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "category_id":this.categoryId.toString()
    });
    return json;
  }
}
