import 'package:grobay/api/request/base_request.dart';

class UserProfileRequest extends BaseRequest {
  final String userId;
  UserProfileRequest({
    this.userId,
  }) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
     "get_user_data": "1",
     "user_id":this.userId
    });
   
    return json;
  }
}
