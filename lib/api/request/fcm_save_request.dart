import 'package:grobay/api/request/base_request.dart';

class FCMSaveRequest extends BaseRequest {
  String userId;
  String fcmId;
  FCMSaveRequest({this.userId, this.fcmId}) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    if (this.userId != null) {
      json.addAll({
        "user_id": this.userId,
      });
    }
    json.addAll({
      "fcm_id": this.fcmId,
      "store_fcm_id":"1"
    });
    return json;
  }
}
