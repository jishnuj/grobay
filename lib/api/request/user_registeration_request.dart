// {  "country_code":91,
// "password":"1234",
// "friends_code":"",
// "accesskey":"90336",
// "name":"jishnutest",
// "mobile":"8129528847",
// "type":"register" || "verify-user",
// "email":"jishnut01%40gmail.com"
// "fcm_id":" "
// }

import 'package:grobay/api/request/base_request.dart';

class UserRegisterationRequest extends BaseRequest {
  bool isRegisterUser;
  int countryCode;
  String password;
  String friendsCode;
  String name;
  String mobile;
  String email;
  String fcmId;

  UserRegisterationRequest(
      {this.countryCode,
      this.password,
      this.friendsCode,
      this.isRegisterUser = true,
      this.name,
      this.mobile,
      this.email,
      this.fcmId});

  UserRegisterationRequest.fromJson(Map<String, dynamic> json) {
    countryCode = json['country_code'];
    password = json['password'];
    friendsCode = json['friends_code'];
    name = json['name'];
    mobile = json['mobile'];
    email = json['email'];
    fcmId = json['fcm_id'];
  }

  Map<String, String> toJson() {
    final Map<String, String> data = super.toJson();
    if (this.isRegisterUser) {
      data['country_code'] = this.countryCode.toString();
      data['password'] = this.password;
      data['friends_code'] = this.friendsCode??"";
      data['name'] = this.name;
      data['mobile'] = this.mobile;
      data['type'] = "register";
      data['email'] = this.email;
      data['fcm_id'] = this.fcmId??"";
    } else {
      data['mobile'] = this.mobile;
      data['type'] = "verify-user";
    }
    return data;
  }
}
