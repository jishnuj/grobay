import 'package:grobay/api/request/base_request.dart';

class PaytmValidationRequest extends BaseRequest {
  final String order_id;
  PaytmValidationRequest({this.order_id});

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "order_id": this.order_id,
    });
    return json;
  }
}
