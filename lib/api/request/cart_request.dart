import 'package:flutter/widgets.dart';
import 'package:grobay/api/request/base_request.dart';

class Cartrequest extends BaseRequest {
  String userId;
  String pincodeId;
  String addressId;
  String getUserCart;
  Cartrequest(
      {this.addressId, @required this.userId, this.pincodeId, this.getUserCart})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    if (this.addressId != "") {
      json.addAll({
        "address_id": this.addressId ?? "",
      });
    } else {
      json.addAll({
        "pincode_id": this.pincodeId ?? "",
      });
    }
    json.addAll(
        {"user_id": this.userId, "get_user_cart": this.getUserCart ?? "1"});
    return json;
  }
}
