import 'package:grobay/api/request/base_request.dart';

class SettingsRequest extends BaseRequest {
  final String getTimeSlots;
  final String getTimeSlotsConfig;
  final String getPaymentSetting;
  final String getTimeZone;
  final String settings;
  SettingsRequest(
      {this.getPaymentSetting,
      this.getTimeSlots,
      this.getTimeSlotsConfig,
      this.getTimeZone,
      this.settings})
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    if (this.getTimeSlots != null) {
      json.addAll({
        "get_time_slots": "1",
      });
    }
    if (this.getTimeSlotsConfig != null) {
      json.addAll({
        "get_time_slot_config": "1",
      });
    }
    if (this.getPaymentSetting != null) {
      json.addAll({"get_payment_methods": "1"});
    }
    if (this.getTimeZone != null) {
      json.addAll({"get_timezone": "1"});
    }
    if (this.settings != null) {
      json.addAll({
        "settings": "1",
      });
    }
    return json;
  }
}
