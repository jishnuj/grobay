import 'package:flutter/widgets.dart';
import 'package:grobay/api/request/base_request.dart';

class VariantRequest extends BaseRequest {
  String varainatIds;
  String pincodeId;
  int getVariantsOffline;
  String userId;
  VariantRequest({
    this.varainatIds,
    this.pincodeId,
    this.getVariantsOffline,
    @required this.userId,
  }) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "get_variants_offline": this.getVariantsOffline.toString(),
      "pincode_id": this.pincodeId.toString(),
      "getVaraintsOffline": this.getVariantsOffline.toString(),
      "variant_ids": this.varainatIds
    });
    return json;
  }
}
