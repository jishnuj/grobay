import 'package:grobay/api/request/base_request.dart';

class PaytmChecksumRequest extends BaseRequest {
  final String cust_id;
  final String channel_id;
  final String order_id;
  final String txn_amount;
  final String industry_type_id;
  final String website;

  PaytmChecksumRequest(
      {this.cust_id,
      this.channel_id,
      this.order_id,
      this.txn_amount,
      this.industry_type_id,
      this.website});

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "CUST_ID": this.cust_id,
      "CHANNEL_ID": this.channel_id,
      "ORDER_ID": this.order_id,
      "TXN_AMOUNT": this.txn_amount,
      "INDUSTRY_TYPE_ID": this.industry_type_id,
      "WEBSITE": this.website
    });
    return json;
  }
}
