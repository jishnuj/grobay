import 'package:grobay/api/request/base_request.dart';

class OrderSaveRequest extends BaseRequest {
  String quantity;
  String addressId;
  String walletBalanceUsed;
  String deliveryTime;
  String orderNote;
  String total;
  String deliveryCharge;
  String userId;
  String finalTotal;
  String placeOrder;
  String walletused;
  String productVariantId;
  String paymentMethod;
  String promoCode;
  String promoCodeDiscount;
  OrderSaveRequest(
      {this.quantity,
      this.addressId,
      this.walletBalanceUsed,
      this.deliveryCharge,
      this.deliveryTime,
      this.orderNote,
      this.total,
      this.userId,
      this.finalTotal,
      this.placeOrder,
      this.walletused,
      this.paymentMethod,
      this.productVariantId,
      this.promoCode,
      this.promoCodeDiscount
      })
      : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "quantity": '[${this.quantity}]',
      "address_id": this.addressId,
      "wallet_balance": this.walletBalanceUsed ?? 0.0,
      "delivery_time": this.deliveryTime,
      "order_note": this.orderNote ?? "",
      "total": this.total,
      "delivery_charge": this.deliveryCharge,
      "user_id": this.userId,
      "final_total": this.finalTotal,
      "place_order": this.placeOrder ?? "1",
      "wallet_used": this.walletused ?? "false",
      "product_variant_id": "[${this.productVariantId}]",
      "payment_method": this.paymentMethod,
      "promo_code":this.promoCode??"",
      "promo_discount":this.promoCodeDiscount??""
    });
    return json;
  }
}
