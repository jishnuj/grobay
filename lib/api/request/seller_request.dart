import 'package:grobay/api/request/base_request.dart';

class SellerRequest extends BaseRequest {
  int getSellerData;
  String sellerId;
  SellerRequest({this.getSellerData,this.sellerId}) : super();

  Map<String, String> toJson() {
    Map<String, String> json = super.toJson();
    json.addAll({
      "get_seller_data":"1",
      "seller_id":this.sellerId,
    });
    return json;
  }
}
