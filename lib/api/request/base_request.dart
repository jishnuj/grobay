class BaseRequest {
  String accessKey;
  String token;
  BaseRequest({this.accessKey});

   Map<String, String> toJson() {
    return {
      "accesskey":this.accessKey
    };
  }
}