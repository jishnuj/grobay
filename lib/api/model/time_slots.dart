
class TimeSlots {
  String id;
  String title;
  String fromTime;
  String toTime;
  String lastOrderTime;
  String status;

  TimeSlots(
      {this.id,
      this.title,
      this.fromTime,
      this.toTime,
      this.lastOrderTime,
      this.status});

  TimeSlots.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    fromTime = json['from_time'];
    toTime = json['to_time'];
    lastOrderTime = json['last_order_time'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['from_time'] = this.fromTime;
    data['to_time'] = this.toTime;
    data['last_order_time'] = this.lastOrderTime;
    data['status'] = this.status;
    return data;
  }
}
