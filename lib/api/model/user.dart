class User {
  bool error;
  String message;
  String userId;
  String name;
  String email;
  String profile;
  String countryCode;
  String mobile;
  String balance;
  String referralCode;
  String friendsCode;
  String fcmId;
  String password;
  String status;
  String createdAt;
  List<Data> data;

  User(
      {this.error,
      this.message,
      this.userId,
      this.name,
      this.email,
      this.profile,
      this.countryCode,
      this.mobile,
      this.balance,
      this.referralCode,
      this.friendsCode,
      this.fcmId,
      this.password,
      this.status,
      this.createdAt,
      this.data});

  User.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    userId = json['user_id'];
    name = json['name'];
    email = json['email'];
    profile = json['profile'];
    countryCode = json['country_code'];
    mobile = json['mobile'];
    balance = json['balance'];
    referralCode = json['referral_code'];
    friendsCode = json['friends_code'];
    fcmId = json['fcm_id'];
    password = json['password'];
    status = json['status'];
    createdAt = json['created_at'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['email'] = this.email;
    data['profile'] = this.profile;
    data['country_code'] = this.countryCode;
    data['mobile'] = this.mobile;
    data['balance'] = this.balance;
    data['referral_code'] = this.referralCode;
    data['friends_code'] = this.friendsCode;
    data['fcm_id'] = this.fcmId;
    data['password'] = this.password;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String userId;
  String name;
  String email;
  String profile;
  String countryCode;
  String mobile;
  String balance;
  String referralCode;
  String friendsCode;
  String fcmId;
  String password;
  String status;
  String createdAt;

  Data(
      {this.userId,
      this.name,
      this.email,
      this.profile,
      this.countryCode,
      this.mobile,
      this.balance,
      this.referralCode,
      this.friendsCode,
      this.fcmId,
      this.password,
      this.status,
      this.createdAt});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    name = json['name'];
    email = json['email'];
    profile = json['profile'];
    countryCode = json['country_code'];
    mobile = json['mobile'];
    balance = json['balance'];
    referralCode = json['referral_code'];
    friendsCode = json['friends_code'];
    fcmId = json['fcm_id'];
    password = json['password'];
    status = json['status'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['email'] = this.email;
    data['profile'] = this.profile;
    data['country_code'] = this.countryCode;
    data['mobile'] = this.mobile;
    data['balance'] = this.balance;
    data['referral_code'] = this.referralCode;
    data['friends_code'] = this.friendsCode;
    data['fcm_id'] = this.fcmId;
    data['password'] = this.password;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    return data;
  }
}