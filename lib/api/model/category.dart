
class Category {
  String id;
  String rowOrder;
  String name;
  String subtitle;
  String image;
  String status;
  String productRating;
  String webImage;

  Category(
      {this.id,
      this.rowOrder,
      this.name,
      this.subtitle,
      this.image,
      this.status,
      this.productRating,
      this.webImage});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    rowOrder = json['row_order'];
    name = json['name'];
    subtitle = json['subtitle'];
    image = json['image'];
    status = json['status'];
    productRating = json['product_rating'];
    webImage = json['web_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['row_order'] = this.rowOrder;
    data['name'] = this.name;
    data['subtitle'] = this.subtitle;
    data['image'] = this.image;
    data['status'] = this.status;
    data['product_rating'] = this.productRating;
    data['web_image'] = this.webImage;
    return data;
  }
}