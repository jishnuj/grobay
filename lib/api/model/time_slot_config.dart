class TimeSlotConfig {
  String timeSlotConfig;
  String isTimeSlotsEnabled;
  String deliveryStartsFrom;
  String allowedDays;

  TimeSlotConfig(
      {this.timeSlotConfig,
      this.isTimeSlotsEnabled,
      this.deliveryStartsFrom,
      this.allowedDays});

  TimeSlotConfig.fromJson(Map<String, dynamic> json) {
    timeSlotConfig = json['time_slot_config'];
    isTimeSlotsEnabled = json['is_time_slots_enabled'];
    deliveryStartsFrom = json['delivery_starts_from'];
    allowedDays = json['allowed_days'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['time_slot_config'] = this.timeSlotConfig;
    data['is_time_slots_enabled'] = this.isTimeSlotsEnabled;
    data['delivery_starts_from'] = this.deliveryStartsFrom;
    data['allowed_days'] = this.allowedDays;
    return data;
  }
}
