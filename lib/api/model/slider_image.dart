
class SliderImage {
  String type;
  String typeId;
  String name;
  String image;

  SliderImage({this.type, this.typeId, this.name, this.image});

  SliderImage.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    typeId = json['type_id'];
    name = json['name'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['type_id'] = this.typeId;
    data['name'] = this.name;
    data['image'] = this.image;
    return data;
  }
}