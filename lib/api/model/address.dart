
class Address {
  String id;
  String userId;
  String type;
  String name;
  String mobile;
  String alternateMobile;
  String address;
  String landmark;
  String areaId;
  String pincodeId;
  String cityId;
  String state;
  String country;
  String latitude;
  String longitude;
  String isDefault;
  String dateCreated;
  String userName;
  String areaName;
  String pincode;
  String city;
  String minimumFreeDeliveryOrderAmount;
  String deliveryCharges;

  Address(
      {this.id,
      this.userId,
      this.type,
      this.name,
      this.mobile,
      this.alternateMobile,
      this.address,
      this.landmark,
      this.areaId,
      this.pincodeId,
      this.cityId,
      this.state,
      this.country,
      this.latitude,
      this.longitude,
      this.isDefault,
      this.dateCreated,
      this.userName,
      this.areaName,
      this.pincode,
      this.city,
      this.minimumFreeDeliveryOrderAmount,
      this.deliveryCharges});

  Address.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    type = json['type'];
    name = json['name'];
    mobile = json['mobile'];
    alternateMobile = json['alternate_mobile'];
    address = json['address'];
    landmark = json['landmark'];
    areaId = json['area_id'];
    pincodeId = json['pincode_id'];
    cityId = json['city_id'];
    state = json['state'];
    country = json['country'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    isDefault = json['is_default'];
    dateCreated = json['date_created'];
    userName = json['user_name'];
    areaName = json['area_name'];
    pincode = json['pincode'];
    city = json['city'];
    minimumFreeDeliveryOrderAmount = json['minimum_free_delivery_order_amount'];
    deliveryCharges = json['delivery_charges'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['type'] = this.type;
    data['name'] = this.name;
    data['mobile'] = this.mobile;
    data['alternate_mobile'] = this.alternateMobile;
    data['address'] = this.address;
    data['landmark'] = this.landmark;
    data['area_id'] = this.areaId;
    data['pincode_id'] = this.pincodeId;
    data['city_id'] = this.cityId;
    data['state'] = this.state;
    data['country'] = this.country;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['is_default'] = this.isDefault;
    data['date_created'] = this.dateCreated;
    data['user_name'] = this.userName;
    data['area_name'] = this.areaName;
    data['pincode'] = this.pincode;
    data['city'] = this.city;
    data['minimum_free_delivery_order_amount'] =
        this.minimumFreeDeliveryOrderAmount;
    data['delivery_charges'] = this.deliveryCharges;
    return data;
  }
}