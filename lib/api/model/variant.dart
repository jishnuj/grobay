
class Variants {
  String id;
  String productId;
  String type;
  String measurement;
  String measurementUnitId;
  String price;
  String discountedPrice;
  String serveFor;
  String stock;
  String stockUnitId;
  String measurementUnitName;
  String stockUnitName;
  String cartCount;

  Variants(
      {this.id,
      this.productId,
      this.type,
      this.measurement,
      this.measurementUnitId,
      this.price,
      this.discountedPrice,
      this.serveFor,
      this.stock,
      this.stockUnitId,
      this.measurementUnitName,
      this.stockUnitName,
      this.cartCount});

  Variants.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    type = json['type'];
    measurement = json['measurement'];
    measurementUnitId = json['measurement_unit_id'];
    price = json['price'];
    discountedPrice = json['discounted_price'];
    serveFor = json['serve_for'];
    stock = json['stock'];
    stockUnitId = json['stock_unit_id'];
    measurementUnitName = json['measurement_unit_name'];
    stockUnitName = json['stock_unit_name'];
    cartCount = json['cart_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['product_id'] = this.productId;
    data['type'] = this.type;
    data['measurement'] = this.measurement;
    data['measurement_unit_id'] = this.measurementUnitId;
    data['price'] = this.price;
    data['discounted_price'] = this.discountedPrice;
    data['serve_for'] = this.serveFor;
    data['stock'] = this.stock;
    data['stock_unit_id'] = this.stockUnitId;
    data['measurement_unit_name'] = this.measurementUnitName;
    data['stock_unit_name'] = this.stockUnitName;
    data['cart_count'] = this.cartCount;
    return data;
  }
}