
import 'package:grobay/api/model/variant.dart';

class Favourite {
  String id;
  String userId;
  String productId;
  String rowOrder;
  String name;
  String slug;
  String categoryId;
  String subcategoryId;
  String indicator;
  String manufacturer;
  String madeIn;
  String returnStatus;
  String cancelableStatus;
  String tillStatus;
  String image;
  List<String> otherImages;
  String description;
  String status;
  String dateAdded;
  bool isFavorite;
  List<Variants> variants;

  Favourite(
      {this.id,
      this.userId,
      this.productId,
      this.rowOrder,
      this.name,
      this.slug,
      this.categoryId,
      this.subcategoryId,
      this.indicator,
      this.manufacturer,
      this.madeIn,
      this.returnStatus,
      this.cancelableStatus,
      this.tillStatus,
      this.image,
      this.otherImages,
      this.description,
      this.status,
      this.dateAdded,
      this.isFavorite,
      this.variants});

  Favourite.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    productId = json['product_id'];
    rowOrder = json['row_order'];
    name = json['name'];
    slug = json['slug'];
    categoryId = json['category_id'];
    subcategoryId = json['subcategory_id'];
    indicator = json['indicator'];
    manufacturer = json['manufacturer'];
    madeIn = json['made_in'];
    returnStatus = json['return_status'];
    cancelableStatus = json['cancelable_status'];
    tillStatus = json['till_status'];
    image = json['image'];
    if (json['other_images'] != null) {
      otherImages = new List<String>();
      json['other_images'].forEach((v) {
        otherImages.add(v);
      });
    }
    description = json['description'];
    status = json['status'];
    dateAdded = json['date_added'];
    isFavorite = json['is_favorite'];
    if (json['variants'] != null) {
      variants = new List<Variants>();
      json['variants'].forEach((v) {
        variants.add(new Variants.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['product_id'] = this.productId;
    data['row_order'] = this.rowOrder;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['category_id'] = this.categoryId;
    data['subcategory_id'] = this.subcategoryId;
    data['indicator'] = this.indicator;
    data['manufacturer'] = this.manufacturer;
    data['made_in'] = this.madeIn;
    data['return_status'] = this.returnStatus;
    data['cancelable_status'] = this.cancelableStatus;
    data['till_status'] = this.tillStatus;
    data['image'] = this.image;
    if (this.otherImages != null) {
      data['other_images'] = this.otherImages.map((v) => v).toList();
    }
    data['description'] = this.description;
    data['status'] = this.status;
    data['date_added'] = this.dateAdded;
    data['is_favorite'] = this.isFavorite;
    if (this.variants != null) {
      data['variants'] = this.variants.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
