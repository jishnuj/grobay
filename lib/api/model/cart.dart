class Cart {
  bool error;
  String total;
  bool readyToCart;
  bool readyToCheckout;
  String totalAmount;
  List<CartItem> data;

  Cart(
      {this.error,
      this.total,
      this.readyToCart,
      this.readyToCheckout,
      this.totalAmount,
      this.data});

  Cart.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    total = json['total'];
    readyToCart = json['ready_to_cart'];
    readyToCheckout = json['ready_to_checkout'];
    totalAmount = json['total_amount'];
    if (json['data'] != null) {
      data = new List<CartItem>();
      json['data'].forEach((v) {
        data.add(new CartItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['total'] = this.total;
    data['ready_to_cart'] = this.readyToCart;
    data['ready_to_checkout'] = this.readyToCheckout;
    data['total_amount'] = this.totalAmount;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CartItem {
  String id;
  String userId;
  String productId;
  String productVariantId;
  String qty;
  String saveForLater;
  String dateCreated;
  List<CartItemVariant> item;

  CartItem(
      {this.id,
      this.userId,
      this.productId,
      this.productVariantId,
      this.qty,
      this.saveForLater,
      this.dateCreated,
      this.item});

  CartItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    productId = json['product_id'];
    productVariantId = json['product_variant_id'];
    qty = json['qty'];
    saveForLater = json['save_for_later'];
    dateCreated = json['date_created'];
    if (json['item'] != null) {
      item = new List<CartItemVariant>();
      json['item'].forEach((v) {
        item.add(new CartItemVariant.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['product_id'] = this.productId;
    data['product_variant_id'] = this.productVariantId;
    data['qty'] = this.qty;
    data['save_for_later'] = this.saveForLater;
    data['date_created'] = this.dateCreated;
    if (this.item != null) {
      data['item'] = this.item.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CartItemVariant {
  String id;
  String productId;
  String type;
  String measurement;
  String measurementUnitId;
  String price;
  String discountedPrice;
  String serveFor;
  String stock;
  String stockUnitId;
  String name;
  String dType;
  String pincodes;
  String slug;
  String image;
  List<String> otherImages;
  String codAllowed;
  String totalAllowedQuantity;
  String taxPercentage;
  String taxTitle;
  String unit;
  bool isItemDeliverable;
  bool isAvailable;

  CartItemVariant(
      {this.id,
      this.productId,
      this.type,
      this.measurement,
      this.measurementUnitId,
      this.price,
      this.discountedPrice,
      this.serveFor,
      this.stock,
      this.stockUnitId,
      this.name,
      this.dType,
      this.pincodes,
      this.slug,
      this.image,
      this.otherImages,
      this.codAllowed,
      this.totalAllowedQuantity,
      this.taxPercentage,
      this.taxTitle,
      this.unit,
      this.isItemDeliverable,
      this.isAvailable});

  CartItemVariant.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    type = json['type'];
    measurement = json['measurement'];
    measurementUnitId = json['measurement_unit_id'];
    price = json['price'];
    discountedPrice = json['discounted_price'];
    serveFor = json['serve_for'];
    stock = json['stock'];
    stockUnitId = json['stock_unit_id'];
    name = json['name'];
    dType = json['d_type'];
    pincodes = json['pincodes'];
    slug = json['slug'];
    image = json['image'];
    if (json['other_images'] != null) {
      otherImages = new List<String>();
      json['other_images'].forEach((v) {
        otherImages.add(v);
      });
    }
    codAllowed = json['cod_allowed'];
    totalAllowedQuantity = json['total_allowed_quantity'];
    taxPercentage = json['tax_percentage'];
    taxTitle = json['tax_title'];
    unit = json['unit'];
    isItemDeliverable = json['is_item_deliverable'];
    isAvailable = json['isAvailable'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['product_id'] = this.productId;
    data['type'] = this.type;
    data['measurement'] = this.measurement;
    data['measurement_unit_id'] = this.measurementUnitId;
    data['price'] = this.price;
    data['discounted_price'] = this.discountedPrice;
    data['serve_for'] = this.serveFor;
    data['stock'] = this.stock;
    data['stock_unit_id'] = this.stockUnitId;
    data['name'] = this.name;
    data['d_type'] = this.dType;
    data['pincodes'] = this.pincodes;
    data['slug'] = this.slug;
    data['image'] = this.image;
    if (this.otherImages != null) {
      data['other_images'] = this.otherImages.map((v) => v).toList();
    }
    data['cod_allowed'] = this.codAllowed;
    data['total_allowed_quantity'] = this.totalAllowedQuantity;
    data['tax_percentage'] = this.taxPercentage;
    data['tax_title'] = this.taxTitle;
    data['unit'] = this.unit;
    data['is_item_deliverable'] = this.isItemDeliverable;
    data['isAvailable'] = this.isAvailable;
    return data;
  }
}