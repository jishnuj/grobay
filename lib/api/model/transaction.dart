

class Transaction {
  String id;
  String userId;
  String orderId;
  String type;
  String txnId;
  String payuTxnId;
  String amount;
  String status;
  String message;
  String transactionDate;
  String dateCreated;
  String lastUpdated;
  String orderItemId;

  Transaction(
      {this.id,
      this.userId,
      this.orderId,
      this.type,
      this.txnId,
      this.payuTxnId,
      this.amount,
      this.status,
      this.message,
      this.transactionDate,
      this.dateCreated,
      this.lastUpdated,
      this.orderItemId});

  Transaction.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    orderId = json['order_id'];
    type = json['type'];
    txnId = json['txn_id'];
    payuTxnId = json['payu_txn_id'];
    amount = json['amount'];
    status = json['status'];
    message = json['message'];
    transactionDate = json['transaction_date'];
    dateCreated = json['date_created'];
    lastUpdated = json['last_updated'];
    orderItemId = json['order_item_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['order_id'] = this.orderId;
    data['type'] = this.type;
    data['txn_id'] = this.txnId;
    data['payu_txn_id'] = this.payuTxnId;
    data['amount'] = this.amount;
    data['status'] = this.status;
    data['message'] = this.message;
    data['transaction_date'] = this.transactionDate;
    data['date_created'] = this.dateCreated;
    data['last_updated'] = this.lastUpdated;
    data['order_item_id'] = this.orderItemId;
    return data;
  }
}