
class Areas {
  String id;
  String pincode;
  String status;
  String dateCreated;
  String cityName;
  String areaName;
  String cityId;
  String pincodeId;
  String minimumFreeDeliveryOrderAmount;
  String deliveryCharges;

  Areas(
      {this.id,
      this.pincode,
      this.status,
      this.dateCreated,
      this.cityName,
      this.areaName,
      this.cityId,
      this.pincodeId,
      this.minimumFreeDeliveryOrderAmount,
      this.deliveryCharges});

  Areas.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    pincode = json['pincode'];
    status = json['status'];
    dateCreated = json['date_created'];
    cityName = json['city_name'];
    areaName = json['area_name'];
    cityId = json['city_id'];
    pincodeId = json['pincode_id'];
    minimumFreeDeliveryOrderAmount = json['minimum_free_delivery_order_amount'];
    deliveryCharges = json['delivery_charges'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['pincode'] = this.pincode;
    data['status'] = this.status;
    data['date_created'] = this.dateCreated;
    data['city_name'] = this.cityName;
    data['area_name'] = this.areaName;
    data['city_id'] = this.cityId;
    data['pincode_id'] = this.pincodeId;
    data['minimum_free_delivery_order_amount'] =
        this.minimumFreeDeliveryOrderAmount;
    data['delivery_charges'] = this.deliveryCharges;
    return data;
  }
}
