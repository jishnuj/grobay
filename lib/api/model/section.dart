import 'package:grobay/api/model/product.dart';

class Section {
  String id;
  String title;
  String shortDescription;
  String style;
  List<String> productIds;
  List<Product> products;

  Section(
      {this.id,
      this.title,
      this.shortDescription,
      this.style,
      this.productIds,
      this.products});

  Section.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    shortDescription = json['short_description'];
    style = json['style'];
    productIds = json['product_ids'].cast<String>();
    if (json['products'] != null) {
      products = new List<Product>();
      json['products'].forEach((v) {
        products.add(new Product.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['short_description'] = this.shortDescription;
    data['style'] = this.style;
    data['product_ids'] = this.productIds;
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}