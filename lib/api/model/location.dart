
class Location {
  String id;
  String pincode;
  String status;
  String dateCreated;

  Location({this.id, this.pincode, this.status, this.dateCreated});

  Location.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    pincode = json['pincode'];
    status = json['status'];
    dateCreated = json['date_created'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['pincode'] = this.pincode;
    data['status'] = this.status;
    data['date_created'] = this.dateCreated;
    return data;
  }
}