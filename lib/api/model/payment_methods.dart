
class PaymentMethods {
  String paymentMethodSettings;
  String codPaymentMethod;
  String paypalPaymentMethod;
  String paypalMode;
  String paypalCurrencyCode;
  String paypalBusinessEmail;
  String payumoneyPaymentMethod;
  String payumoneyMode;
  String payumoneyMerchantKey;
  String payumoneyMerchantId;
  String payumoneySalt;
  String razorpayPaymentMethod;
  String razorpayKey;
  String razorpaySecretKey;
  String paystackPaymentMethod;
  String paystackPublicKey;
  String paystackSecretKey;
  String flutterwavePaymentMethod;
  String flutterwavePublicKey;
  String flutterwaveSecretKey;
  String flutterwaveEncryptionKey;
  String flutterwaveCurrencyCode;
  String midtransPaymentMethod;
  String isProduction;
  String midtransMerchantId;
  String midtransClientKey;
  String midtransServerKey;
  String stripePaymentMethod;
  String stripePublishableKey;
  String stripeSecretKey;
  String stripeWebhookSecretKey;
  String stripeCurrencyCode;
  String paytmPaymentMethod;
  String paytmMode;
  String paytmMerchantKey;
  String paytmMerchantId;
  String sslCommercePaymentMethod;
  String sslCommereceMode;
  String sslCommereceStoreId;
  String sslCommereceSecretKey;

  PaymentMethods(
      {this.paymentMethodSettings,
      this.codPaymentMethod,
      this.paypalPaymentMethod,
      this.paypalMode,
      this.paypalCurrencyCode,
      this.paypalBusinessEmail,
      this.payumoneyPaymentMethod,
      this.payumoneyMode,
      this.payumoneyMerchantKey,
      this.payumoneyMerchantId,
      this.payumoneySalt,
      this.razorpayPaymentMethod,
      this.razorpayKey,
      this.razorpaySecretKey,
      this.paystackPaymentMethod,
      this.paystackPublicKey,
      this.paystackSecretKey,
      this.flutterwavePaymentMethod,
      this.flutterwavePublicKey,
      this.flutterwaveSecretKey,
      this.flutterwaveEncryptionKey,
      this.flutterwaveCurrencyCode,
      this.midtransPaymentMethod,
      this.isProduction,
      this.midtransMerchantId,
      this.midtransClientKey,
      this.midtransServerKey,
      this.stripePaymentMethod,
      this.stripePublishableKey,
      this.stripeSecretKey,
      this.stripeWebhookSecretKey,
      this.stripeCurrencyCode,
      this.paytmPaymentMethod,
      this.paytmMode,
      this.paytmMerchantKey,
      this.paytmMerchantId,
      this.sslCommercePaymentMethod,
      this.sslCommereceMode,
      this.sslCommereceStoreId,
      this.sslCommereceSecretKey});

  PaymentMethods.fromJson(Map<String, dynamic> json) {
    paymentMethodSettings = json['payment_method_settings'];
    codPaymentMethod = json['cod_payment_method'];
    paypalPaymentMethod = json['paypal_payment_method'];
    paypalMode = json['paypal_mode'];
    paypalCurrencyCode = json['paypal_currency_code'];
    paypalBusinessEmail = json['paypal_business_email'];
    payumoneyPaymentMethod = json['payumoney_payment_method'];
    payumoneyMode = json['payumoney_mode'];
    payumoneyMerchantKey = json['payumoney_merchant_key'];
    payumoneyMerchantId = json['payumoney_merchant_id'];
    payumoneySalt = json['payumoney_salt'];
    razorpayPaymentMethod = json['razorpay_payment_method'];
    razorpayKey = json['razorpay_key'];
    razorpaySecretKey = json['razorpay_secret_key'];
    paystackPaymentMethod = json['paystack_payment_method'];
    paystackPublicKey = json['paystack_public_key'];
    paystackSecretKey = json['paystack_secret_key'];
    flutterwavePaymentMethod = json['flutterwave_payment_method'];
    flutterwavePublicKey = json['flutterwave_public_key'];
    flutterwaveSecretKey = json['flutterwave_secret_key'];
    flutterwaveEncryptionKey = json['flutterwave_encryption_key'];
    flutterwaveCurrencyCode = json['flutterwave_currency_code'];
    midtransPaymentMethod = json['midtrans_payment_method'];
    isProduction = json['is_production'];
    midtransMerchantId = json['midtrans_merchant_id'];
    midtransClientKey = json['midtrans_client_key'];
    midtransServerKey = json['midtrans_server_key'];
    stripePaymentMethod = json['stripe_payment_method'];
    stripePublishableKey = json['stripe_publishable_key'];
    stripeSecretKey = json['stripe_secret_key'];
    stripeWebhookSecretKey = json['stripe_webhook_secret_key'];
    stripeCurrencyCode = json['stripe_currency_code'];
    paytmPaymentMethod = json['paytm_payment_method'];
    paytmMode = json['paytm_mode'];
    paytmMerchantKey = json['paytm_merchant_key'];
    paytmMerchantId = json['paytm_merchant_id'];
    sslCommercePaymentMethod = json['ssl_commerce_payment_method'];
    sslCommereceMode = json['ssl_commerece_mode'];
    sslCommereceStoreId = json['ssl_commerece_store_id'];
    sslCommereceSecretKey = json['ssl_commerece_secret_key'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['payment_method_settings'] = this.paymentMethodSettings;
    data['cod_payment_method'] = this.codPaymentMethod;
    data['paypal_payment_method'] = this.paypalPaymentMethod;
    data['paypal_mode'] = this.paypalMode;
    data['paypal_currency_code'] = this.paypalCurrencyCode;
    data['paypal_business_email'] = this.paypalBusinessEmail;
    data['payumoney_payment_method'] = this.payumoneyPaymentMethod;
    data['payumoney_mode'] = this.payumoneyMode;
    data['payumoney_merchant_key'] = this.payumoneyMerchantKey;
    data['payumoney_merchant_id'] = this.payumoneyMerchantId;
    data['payumoney_salt'] = this.payumoneySalt;
    data['razorpay_payment_method'] = this.razorpayPaymentMethod;
    data['razorpay_key'] = this.razorpayKey;
    data['razorpay_secret_key'] = this.razorpaySecretKey;
    data['paystack_payment_method'] = this.paystackPaymentMethod;
    data['paystack_public_key'] = this.paystackPublicKey;
    data['paystack_secret_key'] = this.paystackSecretKey;
    data['flutterwave_payment_method'] = this.flutterwavePaymentMethod;
    data['flutterwave_public_key'] = this.flutterwavePublicKey;
    data['flutterwave_secret_key'] = this.flutterwaveSecretKey;
    data['flutterwave_encryption_key'] = this.flutterwaveEncryptionKey;
    data['flutterwave_currency_code'] = this.flutterwaveCurrencyCode;
    data['midtrans_payment_method'] = this.midtransPaymentMethod;
    data['is_production'] = this.isProduction;
    data['midtrans_merchant_id'] = this.midtransMerchantId;
    data['midtrans_client_key'] = this.midtransClientKey;
    data['midtrans_server_key'] = this.midtransServerKey;
    data['stripe_payment_method'] = this.stripePaymentMethod;
    data['stripe_publishable_key'] = this.stripePublishableKey;
    data['stripe_secret_key'] = this.stripeSecretKey;
    data['stripe_webhook_secret_key'] = this.stripeWebhookSecretKey;
    data['stripe_currency_code'] = this.stripeCurrencyCode;
    data['paytm_payment_method'] = this.paytmPaymentMethod;
    data['paytm_mode'] = this.paytmMode;
    data['paytm_merchant_key'] = this.paytmMerchantKey;
    data['paytm_merchant_id'] = this.paytmMerchantId;
    data['ssl_commerce_payment_method'] = this.sslCommercePaymentMethod;
    data['ssl_commerece_mode'] = this.sslCommereceMode;
    data['ssl_commerece_store_id'] = this.sslCommereceStoreId;
    data['ssl_commerece_secret_key'] = this.sslCommereceSecretKey;
    return data;
  }
}