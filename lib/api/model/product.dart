
import 'package:grobay/api/model/variant.dart';

class Product {
  String id;
  String sellerId;
  String rowOrder;
  String name;
  String taxId;
  String slug;
  String categoryId;
  String subcategoryId;
  String indicator;
  String manufacturer;
  String madeIn;
  String returnStatus;
  String cancelableStatus;
  String tillStatus;
  String image;
  List<String> otherImages;
  String description;
  String status;
  String dateAdded;
  String isApproved;
  String returnDays;
  String type;
  String pincodes;
  String sellerName;
  String sellerStatus;
  String taxTitle;
  String taxPercentage;
  bool isFavorite;
  List<Variants> variants;

  Product(
      {this.id,
      this.sellerId,
      this.rowOrder,
      this.name,
      this.taxId,
      this.slug,
      this.categoryId,
      this.subcategoryId,
      this.indicator,
      this.manufacturer,
      this.madeIn,
      this.returnStatus,
      this.cancelableStatus,
      this.tillStatus,
      this.image,
      this.otherImages,
      this.description,
      this.status,
      this.dateAdded,
      this.isApproved,
      this.returnDays,
      this.type,
      this.pincodes,
      this.sellerName,
      this.sellerStatus,
      this.taxTitle,
      this.taxPercentage,
      this.isFavorite,
      this.variants});

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sellerId = json['seller_id'];
    rowOrder = json['row_order'];
    name = json['name'];
    taxId = json['tax_id'];
    slug = json['slug'];
    categoryId = json['category_id'];
    subcategoryId = json['subcategory_id'];
    indicator = json['indicator'];
    manufacturer = json['manufacturer'];
    madeIn = json['made_in'];
    returnStatus = json['return_status'];
    cancelableStatus = json['cancelable_status'];
    tillStatus = json['till_status'];
    image = json['image'];
    otherImages = json['other_images'].cast<String>();
    description = json['description'];
    status = json['status'];
    dateAdded = json['date_added'];
    isApproved = json['is_approved'];
    returnDays = json['return_days'];
    type = json['type'];
    pincodes = json['pincodes'];
    sellerName = json['seller_name'];
    sellerStatus = json['seller_status'];
    taxTitle = json['tax_title'];
    taxPercentage = json['tax_percentage'];
    isFavorite = json['is_favorite'];
    if (json['variants'] != null) {
      variants = new List<Variants>();
      json['variants'].forEach((v) {
        variants.add(new Variants.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['seller_id'] = this.sellerId;
    data['row_order'] = this.rowOrder;
    data['name'] = this.name;
    data['tax_id'] = this.taxId;
    data['slug'] = this.slug;
    data['category_id'] = this.categoryId;
    data['subcategory_id'] = this.subcategoryId;
    data['indicator'] = this.indicator;
    data['manufacturer'] = this.manufacturer;
    data['made_in'] = this.madeIn;
    data['return_status'] = this.returnStatus;
    data['cancelable_status'] = this.cancelableStatus;
    data['till_status'] = this.tillStatus;
    data['image'] = this.image;
    data['other_images'] = this.otherImages;
    data['description'] = this.description;
    data['status'] = this.status;
    data['date_added'] = this.dateAdded;
    data['is_approved'] = this.isApproved;
    data['return_days'] = this.returnDays;
    data['type'] = this.type;
    data['pincodes'] = this.pincodes;
    data['seller_name'] = this.sellerName;
    data['seller_status'] = this.sellerStatus;
    data['tax_title'] = this.taxTitle;
    data['tax_percentage'] = this.taxPercentage;
    data['is_favorite'] = this.isFavorite;
    if (this.variants != null) {
      data['variants'] = this.variants.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
