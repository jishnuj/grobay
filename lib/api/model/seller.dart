class Seller {
  String id;
  String name;
  String storeName;
  String email;
  String mobile;
  String balance;
  String storeUrl;
  String storeDescription;
  String street;
  String pincodeId;
  String state;
  String categories;
  String accountNumber;
  String bankIfscCode;
  String bankName;
  String accountName;
  String logo;
  String nationalIdentityCard;
  String addressProof;
  String panNumber;
  String taxName;
  String taxNumber;
  String longitude;
  String latitude;
  String sellerAddress;

  Seller(
      {this.id,
      this.name,
      this.storeName,
      this.email,
      this.mobile,
      this.balance,
      this.storeUrl,
      this.storeDescription,
      this.street,
      this.pincodeId,
      this.state,
      this.categories,
      this.accountNumber,
      this.bankIfscCode,
      this.bankName,
      this.accountName,
      this.logo,
      this.nationalIdentityCard,
      this.addressProof,
      this.panNumber,
      this.taxName,
      this.taxNumber,
      this.longitude,
      this.latitude,
      this.sellerAddress});

  Seller.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    storeName = json['store_name'];
    email = json['email'];
    mobile = json['mobile'];
    balance = json['balance'];
    storeUrl = json['store_url'];
    storeDescription = json['store_description'];
    street = json['street'];
    pincodeId = json['pincode_id'];
    state = json['state'];
    categories = json['categories'];
    accountNumber = json['account_number'];
    bankIfscCode = json['bank_ifsc_code'];
    bankName = json['bank_name'];
    accountName = json['account_name'];
    logo = json['logo'];
    nationalIdentityCard = json['national_identity_card'];
    addressProof = json['address_proof'];
    panNumber = json['pan_number'];
    taxName = json['tax_name'];
    taxNumber = json['tax_number'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    sellerAddress =
        json['seller_address'] == false ? "" : json['seller_address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['store_name'] = this.storeName;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['balance'] = this.balance;
    data['store_url'] = this.storeUrl;
    data['store_description'] = this.storeDescription;
    data['street'] = this.street;
    data['pincode_id'] = this.pincodeId;
    data['state'] = this.state;
    data['categories'] = this.categories;
    data['account_number'] = this.accountNumber;
    data['bank_ifsc_code'] = this.bankIfscCode;
    data['bank_name'] = this.bankName;
    data['account_name'] = this.accountName;
    data['logo'] = this.logo;
    data['national_identity_card'] = this.nationalIdentityCard;
    data['address_proof'] = this.addressProof;
    data['pan_number'] = this.panNumber;
    data['tax_name'] = this.taxName;
    data['tax_number'] = this.taxNumber;
    data['longitude'] = this.longitude;
    data['latitude'] = this.latitude;
    data['seller_address'] = this.sellerAddress;
    return data;
  }
}
