class VariantProduct {
  String id;
  String productId;
  String type;
  String measurement;
  String measurementUnitId;
  String price;
  String discountedPrice;
  String serveFor;
  String stock;
  String stockUnitId;
  String taxId;
  List<Item> item;

  VariantProduct(
      {this.id,
      this.productId,
      this.type,
      this.measurement,
      this.measurementUnitId,
      this.price,
      this.discountedPrice,
      this.serveFor,
      this.stock,
      this.stockUnitId,
      this.taxId,
      this.item});

  VariantProduct.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    type = json['type'];
    measurement = json['measurement'];
    measurementUnitId = json['measurement_unit_id'];
    price = json['price'];
    discountedPrice = json['discounted_price'];
    serveFor = json['serve_for'];
    stock = json['stock'];
    stockUnitId = json['stock_unit_id'];
    taxId = json['tax_id'];
    if (json['item'] != null) {
      item = new List<Item>();
      json['item'].forEach((v) {
        item.add(new Item.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['product_id'] = this.productId;
    data['type'] = this.type;
    data['measurement'] = this.measurement;
    data['measurement_unit_id'] = this.measurementUnitId;
    data['price'] = this.price;
    data['discounted_price'] = this.discountedPrice;
    data['serve_for'] = this.serveFor;
    data['stock'] = this.stock;
    data['stock_unit_id'] = this.stockUnitId;
    data['tax_id'] = this.taxId;
    if (this.item != null) {
      data['item'] = this.item.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Item {
  String id;
  String productId;
  String type;
  String measurement;
  String measurementUnitId;
  String price;
  String discountedPrice;
  String serveFor;
  String stock;
  String stockUnitId;
  String sellerId;
  String rowOrder;
  String name;
  String taxId;
  String slug;
  String categoryId;
  String subcategoryId;
  String indicator;
  String manufacturer;
  String madeIn;
  String returnStatus;
  String cancelableStatus;
  String tillStatus;
  String image;
  List<String> otherImages;
  String description;
  String status;
  String dateAdded;
  String isApproved;
  String returnDays;
  String pincodes;
  String sellerName;
  String dType;
  String sellerStatus;
  String unit;
  bool isItemDeliverable;
  String cartCount;
  String taxTitle;
  String taxPercentage;

  Item(
      {this.id,
      this.productId,
      this.type,
      this.measurement,
      this.measurementUnitId,
      this.price,
      this.discountedPrice,
      this.serveFor,
      this.stock,
      this.stockUnitId,
      this.sellerId,
      this.rowOrder,
      this.name,
      this.taxId,
      this.slug,
      this.categoryId,
      this.subcategoryId,
      this.indicator,
      this.manufacturer,
      this.madeIn,
      this.returnStatus,
      this.cancelableStatus,
      this.tillStatus,
      this.image,
      this.otherImages,
      this.description,
      this.status,
      this.dateAdded,
      this.isApproved,
      this.returnDays,
      this.pincodes,
      this.sellerName,
      this.dType,
      this.sellerStatus,
      this.unit,
      this.isItemDeliverable,
      this.cartCount,
      this.taxTitle,
      this.taxPercentage});

  Item.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    type = json['type'];
    measurement = json['measurement'];
    measurementUnitId = json['measurement_unit_id'];
    price = json['price'];
    discountedPrice = json['discounted_price'];
    serveFor = json['serve_for'];
    stock = json['stock'];
    stockUnitId = json['stock_unit_id'];
    sellerId = json['seller_id'];
    rowOrder = json['row_order'];
    name = json['name'];
    taxId = json['tax_id'];
    slug = json['slug'];
    categoryId = json['category_id'];
    subcategoryId = json['subcategory_id'];
    indicator = json['indicator'];
    manufacturer = json['manufacturer'];
    madeIn = json['made_in'];
    returnStatus = json['return_status'];
    cancelableStatus = json['cancelable_status'];
    tillStatus = json['till_status'];
    image = json['image'];
    otherImages = json['other_images'].cast<String>();
    description = json['description'];
    status = json['status'];
    dateAdded = json['date_added'];
    isApproved = json['is_approved'];
    returnDays = json['return_days'];
    pincodes = json['pincodes'];
    sellerName = json['seller_name'];
    dType = json['d_type'];
    sellerStatus = json['seller_status'];
    unit = json['unit'];
    isItemDeliverable = json['is_item_deliverable'];
    cartCount = json['cart_count'];
    taxTitle = json['tax_title'];
    taxPercentage = json['tax_percentage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['product_id'] = this.productId;
    data['type'] = this.type;
    data['measurement'] = this.measurement;
    data['measurement_unit_id'] = this.measurementUnitId;
    data['price'] = this.price;
    data['discounted_price'] = this.discountedPrice;
    data['serve_for'] = this.serveFor;
    data['stock'] = this.stock;
    data['stock_unit_id'] = this.stockUnitId;
    data['seller_id'] = this.sellerId;
    data['row_order'] = this.rowOrder;
    data['name'] = this.name;
    data['tax_id'] = this.taxId;
    data['slug'] = this.slug;
    data['category_id'] = this.categoryId;
    data['subcategory_id'] = this.subcategoryId;
    data['indicator'] = this.indicator;
    data['manufacturer'] = this.manufacturer;
    data['made_in'] = this.madeIn;
    data['return_status'] = this.returnStatus;
    data['cancelable_status'] = this.cancelableStatus;
    data['till_status'] = this.tillStatus;
    data['image'] = this.image;
    data['other_images'] = this.otherImages;
    data['description'] = this.description;
    data['status'] = this.status;
    data['date_added'] = this.dateAdded;
    data['is_approved'] = this.isApproved;
    data['return_days'] = this.returnDays;
    data['pincodes'] = this.pincodes;
    data['seller_name'] = this.sellerName;
    data['d_type'] = this.dType;
    data['seller_status'] = this.sellerStatus;
    data['unit'] = this.unit;
    data['is_item_deliverable'] = this.isItemDeliverable;
    data['cart_count'] = this.cartCount;
    data['tax_title'] = this.taxTitle;
    data['tax_percentage'] = this.taxPercentage;
    return data;
  }
}