import 'package:grobay/core/config/app_config.dart';

class ApiConfig {
  static String appApiUrl = AppConfig.API_URL;
  static String appImageUrl = AppConfig.IMAGE_URL;
}
