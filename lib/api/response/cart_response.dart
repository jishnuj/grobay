
import 'package:grobay/api/model/cart.dart';

class CartResponse {
  Cart cart;
  CartResponse({this.cart});

  CartResponse.fromJson(Map<String, dynamic> json) {
   cart= Cart.fromJson(json);
  }

  Map<String, dynamic> toJson() {
    return cart.toJson();
  }
}
