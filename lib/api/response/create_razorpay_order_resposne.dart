class CreateRazorpayOrderResponse {
  bool error;
  String message;
  String id;
  int amount;
  String entity;
  int amountPaid;
  int amountDue;
  String currency;
  String receipt;
  Null offerId;
  String status;
  int attempts;
  int createdAt;

  CreateRazorpayOrderResponse(
      {this.error,
      this.message,
      this.id,
      this.amount,
      this.entity,
      this.amountPaid,
      this.amountDue,
      this.currency,
      this.receipt,
      this.offerId,
      this.status,
      this.attempts,
      this.createdAt});

  CreateRazorpayOrderResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    id = json['id'];
    amount = json['amount'];
    entity = json['entity'];
    amountPaid = json['amount_paid'];
    amountDue = json['amount_due'];
    currency = json['currency'];
    receipt = json['receipt'];
    offerId = json['offer_id'];
    status = json['status'];
    attempts = json['attempts'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['id'] = this.id;
    data['amount'] = this.amount;
    data['entity'] = this.entity;
    data['amount_paid'] = this.amountPaid;
    data['amount_due'] = this.amountDue;
    data['currency'] = this.currency;
    data['receipt'] = this.receipt;
    data['offer_id'] = this.offerId;
    data['status'] = this.status;
    data['attempts'] = this.attempts;
    data['created_at'] = this.createdAt;
    return data;
  }
}