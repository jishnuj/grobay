class UpdateProfileImageResponse {
  bool error;
  String profile;
  String message;

  UpdateProfileImageResponse({this.error, this.profile, this.message});

  UpdateProfileImageResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    profile = json['profile'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['profile'] = this.profile;
    data['message'] = this.message;
    return data;
  }
}