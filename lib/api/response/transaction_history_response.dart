import 'package:grobay/api/model/transaction.dart';

class TransactionHistoryResponse {
  bool error;
  String total;
  List<Transaction> transaction;

  TransactionHistoryResponse({this.error, this.total, this.transaction});

  TransactionHistoryResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    total = json['total'];
    if (json['data'] != null) {
      transaction = new List<Transaction>();
      json['data'].forEach((v) {
        transaction.add(new Transaction.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['total'] = this.total;
    if (this.transaction != null) {
      data['data'] = this.transaction.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
