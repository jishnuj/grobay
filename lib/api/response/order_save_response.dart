class OrderSaveResponse {
  bool error;
  String message;
  String orderId;

  OrderSaveResponse({this.error, this.message, this.orderId});

  OrderSaveResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    orderId = json['order_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['order_id'] = this.orderId;
    return data;
  }
}
