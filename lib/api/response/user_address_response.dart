import 'package:grobay/api/model/address.dart';

class UserAddressResponse {
  bool error;
  String message;
  String total;
  List<Address> data;

  UserAddressResponse({this.error, this.message, this.total, this.data});

  UserAddressResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    total = json['total'];
    if (json['data'] != null) {
      data = new List<Address>();
      json['data'].forEach((v) {
        data.add(new Address.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['total'] = this.total;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
