import 'package:grobay/api/model/favourite.dart';

class FavouriteResponse {
  bool error;
  String total;
  List<Favourite> favourites;

  FavouriteResponse({this.error, this.total, this.favourites});

  FavouriteResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    total = json['total'];
    if (json['data'] != null) {
      favourites = new List<Favourite>();
      json['data'].forEach((v) {
        favourites.add(new Favourite.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['total'] = this.total;
    if (this.favourites != null) {
      data['data'] = this.favourites.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
