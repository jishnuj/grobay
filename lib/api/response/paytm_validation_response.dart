class PaytmValidationResponse {
  Head head;
  Body body;

  PaytmValidationResponse({this.head, this.body});

  PaytmValidationResponse.fromJson(Map<String, dynamic> json) {
    head = json['head'] != null ? new Head.fromJson(json['head']) : null;
    body = json['body'] != null ? new Body.fromJson(json['body']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.head != null) {
      data['head'] = this.head.toJson();
    }
    if (this.body != null) {
      data['body'] = this.body.toJson();
    }
    return data;
  }
}

class Head {
  String responseTimestamp;
  String version;
  String signature;

  Head({this.responseTimestamp, this.version, this.signature});

  Head.fromJson(Map<String, dynamic> json) {
    responseTimestamp = json['responseTimestamp'];
    version = json['version'];
    signature = json['signature'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseTimestamp'] = this.responseTimestamp;
    data['version'] = this.version;
    data['signature'] = this.signature;
    return data;
  }
}

class Body {
  ResultInfo resultInfo;
  String txnId;
  String bankTxnId;
  String orderId;
  String txnAmount;
  String txnType;
  String gatewayName;
  String bankName;
  String mid;
  String paymentMode;
  String refundAmt;
  String txnDate;

  Body(
      {this.resultInfo,
      this.txnId,
      this.bankTxnId,
      this.orderId,
      this.txnAmount,
      this.txnType,
      this.gatewayName,
      this.bankName,
      this.mid,
      this.paymentMode,
      this.refundAmt,
      this.txnDate});

  Body.fromJson(Map<String, dynamic> json) {
    resultInfo = json['resultInfo'] != null
        ? new ResultInfo.fromJson(json['resultInfo'])
        : null;
    txnId = json['txnId'];
    bankTxnId = json['bankTxnId'];
    orderId = json['orderId'];
    txnAmount = json['txnAmount'];
    txnType = json['txnType'];
    gatewayName = json['gatewayName'];
    bankName = json['bankName'];
    mid = json['mid'];
    paymentMode = json['paymentMode'];
    refundAmt = json['refundAmt'];
    txnDate = json['txnDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resultInfo != null) {
      data['resultInfo'] = this.resultInfo.toJson();
    }
    data['txnId'] = this.txnId;
    data['bankTxnId'] = this.bankTxnId;
    data['orderId'] = this.orderId;
    data['txnAmount'] = this.txnAmount;
    data['txnType'] = this.txnType;
    data['gatewayName'] = this.gatewayName;
    data['bankName'] = this.bankName;
    data['mid'] = this.mid;
    data['paymentMode'] = this.paymentMode;
    data['refundAmt'] = this.refundAmt;
    data['txnDate'] = this.txnDate;
    return data;
  }
}

class ResultInfo {
  String resultStatus;
  String resultCode;
  String resultMsg;

  ResultInfo({this.resultStatus, this.resultCode, this.resultMsg});

  ResultInfo.fromJson(Map<String, dynamic> json) {
    resultStatus = json['resultStatus'];
    resultCode = json['resultCode'];
    resultMsg = json['resultMsg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['resultStatus'] = this.resultStatus;
    data['resultCode'] = this.resultCode;
    data['resultMsg'] = this.resultMsg;
    return data;
  }
}
