import 'package:grobay/api/model/variant_product.dart';

class VariantsResponse {
  bool error;
  String message;
  String total;
  List<VariantProduct> variantProducts;

  VariantsResponse({this.error, this.message, this.total, this.variantProducts});

  VariantsResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    total = json['total'];
    if (json['data'] != null) {
      variantProducts = new List<VariantProduct>();
      json['data'].forEach((v) {
        variantProducts.add(new VariantProduct.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['total'] = this.total;
    if (this.variantProducts != null) {
      data['data'] = this.variantProducts.map((v) => v.toJson()).toList();
    }
    return data;
  }
}