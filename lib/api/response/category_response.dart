import 'package:grobay/api/model/category.dart';

class CategoryResponse {
  bool error;
  String message;
  List<Category> categories;

  CategoryResponse({this.error, this.message, this.categories});

  CategoryResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    categories=[];
    if (json['data'] != null) {
      categories = new List<Category>();
      json['data'].forEach((v) {
        categories.add(new Category.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    if (this.categories != null) {
      data['data'] = this.categories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}