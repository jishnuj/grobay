import 'package:grobay/api/model/category.dart';
import 'package:grobay/api/model/section.dart';
import 'package:grobay/api/model/seller.dart';
import 'package:grobay/api/model/slider_image.dart';

class StartupResposne {
  bool error;
  String message;
  String style;
  String visibleCount;
  String columnCount;
  List<Category> categories;
  List<SliderImage> sliderImages;
  List<Section> sections;
  List<OfferImages> offerImages;
  List<Seller> seller;

  StartupResposne(
      {this.error,
      this.message,
      this.style,
      this.visibleCount,
      this.columnCount,
      this.categories,
      this.sliderImages,
      this.sections,
      this.offerImages,
      this.seller});

  StartupResposne.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    style = json['style'];
    visibleCount = json['visible_count'];
    columnCount = json['column_count'];
    if (json['categories'] != null) {
      categories = new List<Category>();
      json['categories'].forEach((v) {
        categories.add(new Category.fromJson(v));
      });
    }
    if (json['slider_images'] != null) {
      sliderImages = new List<SliderImage>();
      json['slider_images'].forEach((v) {
        sliderImages.add(new SliderImage.fromJson(v));
      });
    }
    if (json['sections'] != null) {
      sections = new List<Section>();
      json['sections'].forEach((v) {
        sections.add(new Section.fromJson(v));
      });
    }
    if (json['offer_images'] != null) {
      offerImages = new List<OfferImages>();
      json['offer_images'].forEach((v) {
        offerImages.add(new OfferImages.fromJson(v));
      });
    }
    if (json['seller'] != null) {
      seller = new List<Seller>();
      json['seller'].forEach((v) {
        seller.add(new Seller.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['style'] = this.style;
    data['visible_count'] = this.visibleCount;
    data['column_count'] = this.columnCount;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    if (this.sliderImages != null) {
      data['slider_images'] = this.sliderImages.map((v) => v.toJson()).toList();
    }
    if (this.sections != null) {
      data['sections'] = this.sections.map((v) => v.toJson()).toList();
    }
    if (this.offerImages != null) {
      data['offer_images'] = this.offerImages.map((v) => v.toJson()).toList();
    }
    if (this.seller != null) {
      data['seller'] = this.seller.map((v) => v.toJson()).toList();
    }
    return data;
  }
}


class OfferImages {
  String image;

  OfferImages({this.image});

  OfferImages.fromJson(Map<String, dynamic> json) {
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    return data;
  }
}
