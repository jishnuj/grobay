import 'package:grobay/api/model/address.dart';

class UserAddressSaveResponse {
   Address address;
  UserAddressSaveResponse({this.address});

  UserAddressSaveResponse.fromJson(Map<String,dynamic> json){
    address = Address.fromJson(json);
  }
}