class ExtraSettingResponse {
  bool error;
  String message;
  String data;

  ExtraSettingResponse({this.error, this.message, this.data});

  ExtraSettingResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    data = json['contact']??json['privacy']??json['terms']??json['about'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['data'] = this.data;
    return data;
  }
}