class AddWalletResponse {
  bool error;
  String message;
  String newBalance;
  Data data;

  AddWalletResponse({this.error, this.message, this.newBalance, this.data});

  AddWalletResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    newBalance = json['new_balance'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['new_balance'] = this.newBalance;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String id;
  String orderId;
  String orderItemId;
  String userId;
  String type;
  String amount;
  String message;
  String status;
  String dateCreated;
  String lastUpdated;

  Data(
      {this.id,
      this.orderId,
      this.orderItemId,
      this.userId,
      this.type,
      this.amount,
      this.message,
      this.status,
      this.dateCreated,
      this.lastUpdated});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderId = json['order_id'];
    orderItemId = json['order_item_id'];
    userId = json['user_id'];
    type = json['type'];
    amount = json['amount'];
    message = json['message'];
    status = json['status'];
    dateCreated = json['date_created'];
    lastUpdated = json['last_updated'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_id'] = this.orderId;
    data['order_item_id'] = this.orderItemId;
    data['user_id'] = this.userId;
    data['type'] = this.type;
    data['amount'] = this.amount;
    data['message'] = this.message;
    data['status'] = this.status;
    data['date_created'] = this.dateCreated;
    data['last_updated'] = this.lastUpdated;
    return data;
  }
}
