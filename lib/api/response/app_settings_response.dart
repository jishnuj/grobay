class AppSettingsResponse {
  bool error;
  String message;
  Settings settings;

  AppSettingsResponse({this.error, this.message, this.settings});

  AppSettingsResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    settings = json['settings'] != null
        ? new Settings.fromJson(json['settings'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    if (this.settings != null) {
      data['settings'] = this.settings.toJson();
    }
    return data;
  }
}

class Settings {
  String systemConfigurations;
  String systemTimezoneGmt;
  String systemConfigurationsId;
  String appName;
  String supportNumber;
  String supportEmail;
  String currentVersion;
  String minimumVersionRequired;
  String isVersionSystemOn;
  String storeAddress;
  String mapLatitude;
  String mapLongitude;
  String currency;
  String systemTimezone;
  String maxCartItemsCount;
  String minOrderAmount;
  String areaWiseDeliveryCharge;
  String minAmount;
  String deliveryCharge;
  String isReferEarnOn;
  String minReferEarnOrderAmount;
  String referEarnBonus;
  String referEarnMethod;
  String maxReferEarnAmount;
  String minimumWithdrawalAmount;
  String maxProductReturnDays;
  String deliveryBoyBonusPercentage;
  String lowStockLimit;
  String userWalletRefillLimit;
  String fromMail;
  String replyTo;
  String generateOtp;
  String smtpFromMail;
  String smtpReplyTo;
  String smtpEmailPassword;
  String smtpHost;
  String smtpPort;
  String smtpContentType;
  String smtpEncryptionType;

  Settings(
      {this.systemConfigurations,
      this.systemTimezoneGmt,
      this.systemConfigurationsId,
      this.appName,
      this.supportNumber,
      this.supportEmail,
      this.currentVersion,
      this.minimumVersionRequired,
      this.isVersionSystemOn,
      this.storeAddress,
      this.mapLatitude,
      this.mapLongitude,
      this.currency,
      this.systemTimezone,
      this.maxCartItemsCount,
      this.minOrderAmount,
      this.areaWiseDeliveryCharge,
      this.minAmount,
      this.deliveryCharge,
      this.isReferEarnOn,
      this.minReferEarnOrderAmount,
      this.referEarnBonus,
      this.referEarnMethod,
      this.maxReferEarnAmount,
      this.minimumWithdrawalAmount,
      this.maxProductReturnDays,
      this.deliveryBoyBonusPercentage,
      this.lowStockLimit,
      this.userWalletRefillLimit,
      this.fromMail,
      this.replyTo,
      this.generateOtp,
      this.smtpFromMail,
      this.smtpReplyTo,
      this.smtpEmailPassword,
      this.smtpHost,
      this.smtpPort,
      this.smtpContentType,
      this.smtpEncryptionType});

  Settings.fromJson(Map<String, dynamic> json) {
    systemConfigurations = json['system_configurations'];
    systemTimezoneGmt = json['system_timezone_gmt'];
    systemConfigurationsId = json['system_configurations_id'];
    appName = json['app_name'];
    supportNumber = json['support_number'];
    supportEmail = json['support_email'];
    currentVersion = json['current_version'];
    minimumVersionRequired = json['minimum_version_required'];
    isVersionSystemOn = json['is_version_system_on'];
    storeAddress = json['store_address'];
    mapLatitude = json['map_latitude'];
    mapLongitude = json['map_longitude'];
    currency = json['currency'];
    systemTimezone = json['system_timezone'];
    maxCartItemsCount = json['max_cart_items_count'];
    minOrderAmount = json['min_order_amount'];
    areaWiseDeliveryCharge = json['area_wise_delivery_charge'];
    minAmount = json['min_amount'];
    deliveryCharge = json['delivery_charge'];
    isReferEarnOn = json['is_refer_earn_on'];
    minReferEarnOrderAmount = json['min_refer_earn_order_amount'];
    referEarnBonus = json['refer_earn_bonus'];
    referEarnMethod = json['refer_earn_method'];
    maxReferEarnAmount = json['max_refer_earn_amount'];
    minimumWithdrawalAmount = json['minimum_withdrawal_amount'];
    maxProductReturnDays = json['max_product_return_days'];
    deliveryBoyBonusPercentage = json['delivery_boy_bonus_percentage'];
    lowStockLimit = json['low_stock_limit'];
    userWalletRefillLimit = json['user_wallet_refill_limit'];
    fromMail = json['from_mail'];
    replyTo = json['reply_to'];
    generateOtp = json['generate_otp'];
    smtpFromMail = json['smtp_from_mail'];
    smtpReplyTo = json['smtp_reply_to'];
    smtpEmailPassword = json['smtp_email_password'];
    smtpHost = json['smtp_host'];
    smtpPort = json['smtp_port'];
    smtpContentType = json['smtp_content_type'];
    smtpEncryptionType = json['smtp_encryption_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['system_configurations'] = this.systemConfigurations;
    data['system_timezone_gmt'] = this.systemTimezoneGmt;
    data['system_configurations_id'] = this.systemConfigurationsId;
    data['app_name'] = this.appName;
    data['support_number'] = this.supportNumber;
    data['support_email'] = this.supportEmail;
    data['current_version'] = this.currentVersion;
    data['minimum_version_required'] = this.minimumVersionRequired;
    data['is_version_system_on'] = this.isVersionSystemOn;
    data['store_address'] = this.storeAddress;
    data['map_latitude'] = this.mapLatitude;
    data['map_longitude'] = this.mapLongitude;
    data['currency'] = this.currency;
    data['system_timezone'] = this.systemTimezone;
    data['max_cart_items_count'] = this.maxCartItemsCount;
    data['min_order_amount'] = this.minOrderAmount;
    data['area_wise_delivery_charge'] = this.areaWiseDeliveryCharge;
    data['min_amount'] = this.minAmount;
    data['delivery_charge'] = this.deliveryCharge;
    data['is_refer_earn_on'] = this.isReferEarnOn;
    data['min_refer_earn_order_amount'] = this.minReferEarnOrderAmount;
    data['refer_earn_bonus'] = this.referEarnBonus;
    data['refer_earn_method'] = this.referEarnMethod;
    data['max_refer_earn_amount'] = this.maxReferEarnAmount;
    data['minimum_withdrawal_amount'] = this.minimumWithdrawalAmount;
    data['max_product_return_days'] = this.maxProductReturnDays;
    data['delivery_boy_bonus_percentage'] = this.deliveryBoyBonusPercentage;
    data['low_stock_limit'] = this.lowStockLimit;
    data['user_wallet_refill_limit'] = this.userWalletRefillLimit;
    data['from_mail'] = this.fromMail;
    data['reply_to'] = this.replyTo;
    data['generate_otp'] = this.generateOtp;
    data['smtp_from_mail'] = this.smtpFromMail;
    data['smtp_reply_to'] = this.smtpReplyTo;
    data['smtp_email_password'] = this.smtpEmailPassword;
    data['smtp_host'] = this.smtpHost;
    data['smtp_port'] = this.smtpPort;
    data['smtp_content_type'] = this.smtpContentType;
    data['smtp_encryption_type'] = this.smtpEncryptionType;
    return data;
  }
}
