import 'package:grobay/api/model/time_slot_config.dart';

class TimeSlotsConfigResponse {
  bool error;
  String message;
  TimeSlotConfig timeSlotConfig;

  TimeSlotsConfigResponse({this.error, this.message, this.timeSlotConfig});

  TimeSlotsConfigResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    timeSlotConfig = json['time_slot_config'] != null
        ? new TimeSlotConfig.fromJson(json['time_slot_config'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    if (this.timeSlotConfig != null) {
      data['time_slot_config'] = this.timeSlotConfig.toJson();
    }
    return data;
  }
}
