import 'package:grobay/api/model/time_slots.dart';

class TimeSlotsResponse {
  bool error;
  String message;
  List<TimeSlots> timeSlots;

  TimeSlotsResponse({this.error, this.message, this.timeSlots});

  TimeSlotsResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    if (json['time_slots'] != null) {
      timeSlots = new List<TimeSlots>();
      json['time_slots'].forEach((v) {
        timeSlots.add(new TimeSlots.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    if (this.timeSlots != null) {
      data['time_slots'] = this.timeSlots.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
