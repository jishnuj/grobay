import 'package:grobay/api/model/category.dart';

class SubCategoryResponse {
  bool error;
  String message;
  String total;
  List<Category> categories;

  SubCategoryResponse({this.error, this.message, this.total, this.categories});

  SubCategoryResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    total = json['total'];
    if (json['data'] != null) {
      categories = new List<Category>();
      json['data'].forEach((v) {
        categories.add(new Category.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['total'] = this.total;
    if (this.categories != null) {
      data['data'] = this.categories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
