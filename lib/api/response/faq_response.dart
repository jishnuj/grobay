import 'package:grobay/api/model/faq.dart';

class FaqResponse {
  bool error;
  String total;
  List<Faq> faq;

  FaqResponse({this.error, this.total, this.faq});

  FaqResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    total = json['total'];
    if (json['data'] != null) {
      faq = new List<Faq>();
      json['data'].forEach((v) {
        faq.add(new Faq.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['total'] = this.total;
    if (this.faq != null) {
      data['data'] = this.faq.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
