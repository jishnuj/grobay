class CreateTransactionResponse {
  bool error;
  int transactionId;
  String message;

  CreateTransactionResponse({this.error, this.transactionId, this.message});

  CreateTransactionResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    transactionId = json['transaction_id'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['transaction_id'] = this.transactionId;
    data['message'] = this.message;
    return data;
  }
}