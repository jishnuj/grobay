import 'package:grobay/api/model/user.dart';

class UserResponse {
  User user;
  UserResponse({this.user});

  UserResponse.fromJson(Map<String, dynamic> json) {
   user= User.fromJson(json);
  }

  Map<String, dynamic> toJson() {
    return user.toJson();
  }
}
