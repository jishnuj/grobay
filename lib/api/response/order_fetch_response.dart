class OrderFetchResponse {
  bool error;
  String total;
  List<Order> data;

  OrderFetchResponse({this.error, this.total, this.data});

  OrderFetchResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    total = json['total'];
    if (json['data'] != null) {
      data = new List<Order>();
      json['data'].forEach((v) {
        data.add(new Order.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['total'] = this.total;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Order {
  String id;
  String userId;
  String otp;
  String mobile;
  String orderNote;
  String total;
  String deliveryCharge;
  String taxAmount;
  String taxPercentage;
  String walletBalance;
  String discount;
  String promoCode;
  String promoDiscount;
  String finalTotal;
  String paymentMethod;
  String address;
  String latitude;
  String longitude;
  String deliveryTime;
  String dateAdded;
  String orderFrom;
  String pincodeId;
  String areaId;
  String userName;
  String discountRupees;
  List<OrderedProduct> items;

  Order(
      {this.id,
      this.userId,
      this.otp,
      this.mobile,
      this.orderNote,
      this.total,
      this.deliveryCharge,
      this.taxAmount,
      this.taxPercentage,
      this.walletBalance,
      this.discount,
      this.promoCode,
      this.promoDiscount,
      this.finalTotal,
      this.paymentMethod,
      this.address,
      this.latitude,
      this.longitude,
      this.deliveryTime,
      this.dateAdded,
      this.orderFrom,
      this.pincodeId,
      this.areaId,
      this.userName,
      this.discountRupees,
      this.items});

  Order.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    otp = json['otp'];
    mobile = json['mobile'];
    orderNote = json['order_note'];
    total = json['total'];
    deliveryCharge = json['delivery_charge'];
    taxAmount = json['tax_amount'];
    taxPercentage = json['tax_percentage'];
    walletBalance = json['wallet_balance'];
    discount = json['discount'];
    promoCode = json['promo_code'];
    promoDiscount = json['promo_discount'];
    finalTotal = json['final_total'];
    paymentMethod = json['payment_method'];
    address = json['address'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    deliveryTime = json['delivery_time'];
    dateAdded = json['date_added'];
    orderFrom = json['order_from'];
    pincodeId = json['pincode_id'];
    areaId = json['area_id'];
    userName = json['user_name'];
    discountRupees = json['discount_rupees'];
    if (json['items'] != null) {
      items = new List<OrderedProduct>();
      json['items'].forEach((v) {
        items.add(new OrderedProduct.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['otp'] = this.otp;
    data['mobile'] = this.mobile;
    data['order_note'] = this.orderNote;
    data['total'] = this.total;
    data['delivery_charge'] = this.deliveryCharge;
    data['tax_amount'] = this.taxAmount;
    data['tax_percentage'] = this.taxPercentage;
    data['wallet_balance'] = this.walletBalance;
    data['discount'] = this.discount;
    data['promo_code'] = this.promoCode;
    data['promo_discount'] = this.promoDiscount;
    data['final_total'] = this.finalTotal;
    data['payment_method'] = this.paymentMethod;
    data['address'] = this.address;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['delivery_time'] = this.deliveryTime;
    data['date_added'] = this.dateAdded;
    data['order_from'] = this.orderFrom;
    data['pincode_id'] = this.pincodeId;
    data['area_id'] = this.areaId;
    data['user_name'] = this.userName;
    data['discount_rupees'] = this.discountRupees;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderedProduct {
  String id;
  String userId;
  String orderId;
  String productName;
  String variantName;
  String productVariantId;
  String deliveryBoyId;
  String quantity;
  String price;
  String discountedPrice;
  String taxAmount;
  String taxPercentage;
  String discount;
  String subTotal;
  List<List<dynamic>> status;
  String activeStatus;
  String dateAdded;
  String sellerId;
  String isCredited;
  String variantId;
  String name;
  String image;
  String manufacturer;
  String madeIn;
  String returnStatus;
  String cancelableStatus;
  String tillStatus;
  String measurement;
  String unit;
  String sellerName;
  String sellerStoreName;
  String returnDays;
  bool appliedForReturn;

  OrderedProduct(
      {this.id,
      this.userId,
      this.orderId,
      this.productName,
      this.variantName,
      this.productVariantId,
      this.deliveryBoyId,
      this.quantity,
      this.price,
      this.discountedPrice,
      this.taxAmount,
      this.taxPercentage,
      this.discount,
      this.subTotal,
      this.status,
      this.activeStatus,
      this.dateAdded,
      this.sellerId,
      this.isCredited,
      this.variantId,
      this.name,
      this.image,
      this.manufacturer,
      this.madeIn,
      this.returnStatus,
      this.cancelableStatus,
      this.tillStatus,
      this.measurement,
      this.unit,
      this.sellerName,
      this.sellerStoreName,
      this.returnDays,
      this.appliedForReturn});

  OrderedProduct.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    orderId = json['order_id'];
    productName = json['product_name'];
    variantName = json['variant_name'];
    productVariantId = json['product_variant_id'];
    deliveryBoyId = json['delivery_boy_id'];
    quantity = json['quantity'];
    price = json['price'];
    discountedPrice = json['discounted_price'];
    taxAmount = json['tax_amount'];
    taxPercentage = json['tax_percentage'];
    discount = json['discount'];
    subTotal = json['sub_total'];
    if (json['status'] != null) {
      status = new List<List<dynamic>>();
      json['status'].forEach((v) {
        status.add(v);
      });
    }
    activeStatus = json['active_status'];
    dateAdded = json['date_added'];
    sellerId = json['seller_id'];
    isCredited = json['is_credited'];
    variantId = json['variant_id'];
    name = json['name'];
    image = json['image'];
    manufacturer = json['manufacturer'];
    madeIn = json['made_in'];
    returnStatus = json['return_status'];
    cancelableStatus = json['cancelable_status'];
    tillStatus = json['till_status'];
    measurement = json['measurement'];
    unit = json['unit'];
    sellerName = json['seller_name'];
    sellerStoreName = json['seller_store_name'];
    returnDays = json['return_days'];
    appliedForReturn = json['applied_for_return'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['order_id'] = this.orderId;
    data['product_name'] = this.productName;
    data['variant_name'] = this.variantName;
    data['product_variant_id'] = this.productVariantId;
    data['delivery_boy_id'] = this.deliveryBoyId;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    data['discounted_price'] = this.discountedPrice;
    data['tax_amount'] = this.taxAmount;
    data['tax_percentage'] = this.taxPercentage;
    data['discount'] = this.discount;
    data['sub_total'] = this.subTotal;
    if (this.status != null) {
      data['status'] = this.status.map((v) => v).toList();
    }
    data['active_status'] = this.activeStatus;
    data['date_added'] = this.dateAdded;
    data['seller_id'] = this.sellerId;
    data['is_credited'] = this.isCredited;
    data['variant_id'] = this.variantId;
    data['name'] = this.name;
    data['image'] = this.image;
    data['manufacturer'] = this.manufacturer;
    data['made_in'] = this.madeIn;
    data['return_status'] = this.returnStatus;
    data['cancelable_status'] = this.cancelableStatus;
    data['till_status'] = this.tillStatus;
    data['measurement'] = this.measurement;
    data['unit'] = this.unit;
    data['seller_name'] = this.sellerName;
    data['seller_store_name'] = this.sellerStoreName;
    data['return_days'] = this.returnDays;
    data['applied_for_return'] = this.appliedForReturn;
    return data;
  }
}
