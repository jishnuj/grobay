import 'package:grobay/api/model/product.dart';

class ProductResponse {
  bool error;
  String message;
  String total;
  List<Product> products;

  ProductResponse({this.error, this.message, this.total, this.products});

  ProductResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    total = json['total'];
    if (json['data'] != null) {
      products = new List<Product>();
      json['data'].forEach((v) {
        products.add(new Product.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['total'] = this.total;
    if (this.products != null) {
      data['data'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}