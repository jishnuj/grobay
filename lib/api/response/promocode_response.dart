class PromoCodeResponse {
  bool error;
  String message;
  String promoCode;
  String promoCodeMessage;
  String total;
  String discount;
  String discountedAmount;

  PromoCodeResponse(
      {this.error,
      this.message,
      this.promoCode,
      this.promoCodeMessage,
      this.total,
      this.discount,
      this.discountedAmount});

  PromoCodeResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    promoCode = json['promo_code'];
    promoCodeMessage = json['promo_code_message'];
    total = json['total'];
    discount = json['discount'];
    discountedAmount = json['discounted_amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['promo_code'] = this.promoCode;
    data['promo_code_message'] = this.promoCodeMessage;
    data['total'] = this.total;
    data['discount'] = this.discount;
    data['discounted_amount'] = this.discountedAmount;
    return data;
  }
}