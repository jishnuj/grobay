class UserRegisterationResponse {
  bool error;
  String message;
  String password;
  String userId;
  String name;
  String email;
  String profile;
  String mobile;
  String balance;
  String countryCode;
  String referralCode;
  String friendsCode;
  String fcmId;
  String status;
  String createdAt;

  UserRegisterationResponse(
      {this.error,
      this.message,
      this.password,
      this.userId,
      this.name,
      this.email,
      this.profile,
      this.mobile,
      this.balance,
      this.countryCode,
      this.referralCode,
      this.friendsCode,
      this.fcmId,
      this.status,
      this.createdAt});

  UserRegisterationResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    password = json['password'];
    userId = json['user_id'];
    name = json['name'];
    email = json['email'];
    profile = json['profile'];
    mobile = json['mobile'];
    balance = json['balance'];
    countryCode = json['country_code'];
    referralCode = json['referral_code'];
    friendsCode = json['friends_code'];
    fcmId = json['fcm_id'];
    status = json['status'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['password'] = this.password;
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['email'] = this.email;
    data['profile'] = this.profile;
    data['mobile'] = this.mobile;
    data['balance'] = this.balance;
    data['country_code'] = this.countryCode;
    data['referral_code'] = this.referralCode;
    data['friends_code'] = this.friendsCode;
    data['fcm_id'] = this.fcmId;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    return data;
  }
}