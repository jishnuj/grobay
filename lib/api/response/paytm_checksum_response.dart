class PaytmChecksumResponse {
  bool error;
  String message;
  String orderId;
  Data data;
  String signature;

  PaytmChecksumResponse(
      {this.error, this.message, this.orderId, this.data, this.signature});

  PaytmChecksumResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    orderId = json['order id'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    signature = json['signature'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['order id'] = this.orderId;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['signature'] = this.signature;
    return data;
  }
}

class Data {
  String mID;
  String oRDERID;
  String cUSTID;
  String iNDUSTRYTYPEID;
  String cHANNELID;
  String tXNAMOUNT;
  String wEBSITE;
  String cALLBACKURL;

  Data(
      {this.mID,
      this.oRDERID,
      this.cUSTID,
      this.iNDUSTRYTYPEID,
      this.cHANNELID,
      this.tXNAMOUNT,
      this.wEBSITE,
      this.cALLBACKURL});

  Data.fromJson(Map<String, dynamic> json) {
    mID = json['MID'];
    oRDERID = json['ORDER_ID'];
    cUSTID = json['CUST_ID'];
    iNDUSTRYTYPEID = json['INDUSTRY_TYPE_ID'];
    cHANNELID = json['CHANNEL_ID'];
    tXNAMOUNT = json['TXN_AMOUNT'];
    wEBSITE = json['WEBSITE'];
    cALLBACKURL = json['CALLBACK_URL'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MID'] = this.mID;
    data['ORDER_ID'] = this.oRDERID;
    data['CUST_ID'] = this.cUSTID;
    data['INDUSTRY_TYPE_ID'] = this.iNDUSTRYTYPEID;
    data['CHANNEL_ID'] = this.cHANNELID;
    data['TXN_AMOUNT'] = this.tXNAMOUNT;
    data['WEBSITE'] = this.wEBSITE;
    data['CALLBACK_URL'] = this.cALLBACKURL;
    return data;
  }
}