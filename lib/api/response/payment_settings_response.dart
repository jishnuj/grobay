import 'package:grobay/api/model/payment_methods.dart';

class PaymentSettingsResponse {
  bool error;
  String message;
  PaymentMethods paymentMethods;

  PaymentSettingsResponse({this.error, this.message, this.paymentMethods});

  PaymentSettingsResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    paymentMethods = json['payment_methods'] != null
        ? new PaymentMethods.fromJson(json['payment_methods'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    if (this.paymentMethods != null) {
      data['payment_methods'] = this.paymentMethods.toJson();
    }
    return data;
  }
}
