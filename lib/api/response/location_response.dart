import 'package:grobay/api/model/location.dart';

class LocationResponse {
  bool error;
  String message;
  String total;
  List<Location> locations;

  LocationResponse({this.error, this.message, this.total, this.locations});

  LocationResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    total = json['total'];
    if (json['data'] != null) {
      locations = new List<Location>();
      json['data'].forEach((v) {
        locations.add(new Location.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['total'] = this.total;
    if (this.locations != null) {
      data['data'] = this.locations.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
