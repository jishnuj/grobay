import 'package:grobay/api/model/seller.dart';

class SellerResponse {
  bool error;
  String message;
  List<Seller> sellers;

  SellerResponse({this.error, this.message, this.sellers});

  SellerResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    if (json['data'] != null) {
      sellers = new List<Seller>();
      json['data'].forEach((v) {
        sellers.add(new Seller.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    if (this.sellers != null) {
      data['data'] = this.sellers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}