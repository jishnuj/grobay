import 'dart:convert';
import 'dart:io';
import 'package:grobay/api/api_client.dart';
import 'package:grobay/api/api_config.dart';
import 'package:grobay/api/api_exception.dart';
import 'package:grobay/api/request/add_wallet_balance_request.dart';
import 'package:grobay/api/request/cart_request.dart';
import 'package:grobay/api/request/cart_save_request.dart';
import 'package:grobay/api/request/change_password_request.dart';
import 'package:grobay/api/request/create_razorpay_order_request.dart';
import 'package:grobay/api/request/create_stripe_payment_request.dart';
import 'package:grobay/api/request/create_transaction_request.dart';
import 'package:grobay/api/request/favourite_request.dart';
import 'package:grobay/api/request/favourite_save_delete_request.dart';
import 'package:grobay/api/request/forgot_password_request.dart';
import 'package:grobay/api/request/order_update_request.dart';
import 'package:grobay/api/request/order_fetch_request.dart';
import 'package:grobay/api/request/order_save_request.dart';
import 'package:grobay/api/request/paytm_checksum_request.dart';
import 'package:grobay/api/request/paytm_validation_request.dart';
import 'package:grobay/api/request/promocode_request.dart';
import 'package:grobay/api/request/transaction_history_request.dart';
import 'package:grobay/api/request/update_profile_request.dart';
import 'package:grobay/api/request/update_profileimage_request.dart';
import 'package:grobay/api/request/user_address_create_request.dart';
import 'package:grobay/api/request/user_address_delete_request.dart';
import 'package:grobay/api/request/user_address_request.dart';
import 'package:grobay/api/request/user_profile_request.dart';
import 'package:grobay/api/request/user_registeration_request.dart';
import 'package:grobay/api/request/userlogin_request.dart';
import 'package:grobay/api/request/verify_paystack_request.dart';
import 'package:grobay/api/response/add_wallet_response.dart';
import 'package:grobay/api/response/cart_response.dart';
import 'package:grobay/api/response/cart_save_response.dart';
import 'package:grobay/api/response/create_razorpay_order_resposne.dart';
import 'package:grobay/api/response/create_transaction_response.dart';
import 'package:grobay/api/response/favourite_response.dart';
import 'package:grobay/api/response/order_fetch_response.dart';
import 'package:grobay/api/response/order_save_response.dart';
import 'package:grobay/api/response/paytm_checksum_response.dart';
import 'package:grobay/api/response/paytm_validation_response.dart';
import 'package:grobay/api/response/promocode_response.dart';
import 'package:grobay/api/response/transaction_history_response.dart';
import 'package:grobay/api/response/update_profileimage_response.dart';
import 'package:grobay/api/response/user_address_response.dart';
import 'package:grobay/api/response/user_address_save_response.dart';
import 'package:grobay/api/response/user_registeration_response.dart';
import 'package:grobay/api/response/user_response.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';

/// This Class Provides User Related Methods
class UserResourceApi {
  ApiClient apiClient = ApiClient();
  final _log = Logger();
  final _dateFormatter = DateFormat(
      '${AppConfig.DEFAULT_DATE_FORMAT_INTL} ${AppConfig.DEFAULT_TIME_FORMAT_INTL}');

  UserResourceApi();

  /// Takes a object of [UserLoginrequest] and
  /// Fetch the UserResponse After Logging in
  /// Throws an [ApiException] if the response error is true
  /// Return [UserResponse]
  Future<UserResponse> login(UserLoginrequest userLoginrequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/login.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${userLoginrequest.token}");

      request.fields.addAll(userLoginrequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          userLoginrequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return UserResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [UserRegisterationRequest] and
  /// Verify and register user
  /// Throws an [ApiException] if the response error is true
  /// Return [UserRegisterationResponse]
  Future<UserRegisterationResponse> register(
      UserRegisterationRequest userRegisterationRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/user-registration.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${userRegisterationRequest.token}");

      request.fields.addAll(userRegisterationRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          userRegisterationRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      return UserRegisterationResponse.fromJson(_responseMap);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [UserProfileRequest] and
  /// Fetch the User Profile
  /// Throws an [ApiException] if the response error is true
  /// Return [UserResponse]
  Future<UserResponse> getUserDetails(
      UserProfileRequest userProfileRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-user-data.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${userProfileRequest.token}");

      request.fields.addAll(userProfileRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          userProfileRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return UserResponse.fromJson(_responseMap['data'][0]);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [UpdateProfileRequest] and
  /// Update Profile Details
  /// Throws an [ApiException] if the response error is true
  /// Return [TransactionHistoryResponse]
  Future<String> updateProfile(
      UpdateProfileRequest updateProfileRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/user-registration.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${updateProfileRequest.token}");

      request.fields.addAll(updateProfileRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          updateProfileRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return _responseMap['message'];
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [UpdateProfileImageRequest] and
  /// Update Profile Image
  /// Throws an [ApiException] if the response error is true
  /// Return [UpdateProfileImageResponse]
  Future<UpdateProfileImageResponse> updateProfileImage(
      UpdateProfileImageRequest updateProfileImageRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/user-registration.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${updateProfileImageRequest.token}");
      request.fields.addAll(updateProfileImageRequest.toJson());
      request.files.add(new http.MultipartFile.fromBytes(
          'profile',
          await File.fromUri(Uri.parse(updateProfileImageRequest.file.path))
              .readAsBytes(),
          filename: updateProfileImageRequest.file.path.substring(
            updateProfileImageRequest.file.path.lastIndexOf("/") + 1,
          )));
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          updateProfileImageRequest.toJson().toString() +
          "\n" +
          updateProfileImageRequest.file.path.substring(
            updateProfileImageRequest.file.path.lastIndexOf("/") + 1,
          ) +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return UpdateProfileImageResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [ChangePasswordRequest] and
  /// Update User Password
  /// Throws an [ApiException] if the response error is true
  /// Return [UserResponse]
  Future<String> updatePassword(
      ChangePasswordRequest changePasswordRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/user-registration.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${changePasswordRequest.token}");

      request.fields.addAll(changePasswordRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          changePasswordRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return _responseMap["message"];
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  Future<String> forgotPassword(
      ForgotPasswordRequest forgotPasswordRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/user-registration.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${forgotPasswordRequest.token}");

      request.fields.addAll(forgotPasswordRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          forgotPasswordRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return _responseMap['message'];
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [UsserAddressCreateRequest] and
  /// Saves/ Update User Address
  /// Throws an [ApiException] if the response error is true
  /// Return [UserAddressSaveResponse]
  Future<UserAddressSaveResponse> saveAddress(
      UsserAddressCreateRequest usserAddressCreateRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/user-addresses.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${usserAddressCreateRequest.token}");

      request.fields.addAll(usserAddressCreateRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          usserAddressCreateRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return UserAddressSaveResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [userAddressRequest] and
  ///Fetch All user Address
  /// Throws an [ApiException] if the response error is true
  /// Return [UserAddressResponse]
  Future<UserAddressResponse> fetchAddress(
      UserAddressRequest userAddressRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/user-addresses.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${userAddressRequest.token}");

      request.fields.addAll(userAddressRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          userAddressRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return UserAddressResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [userAddressRequest] and
  ///Delete a user Address
  /// Throws an [ApiException] if the response error is true
  /// Return [String]
  Future<String> deleteAddress(
      UserAddressDeleteRequest userAddressDeleteRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/user-addresses.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${userAddressDeleteRequest.token}");

      request.fields.addAll(userAddressDeleteRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          userAddressDeleteRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return _responseMap['message'];
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [favouriteRequest] and
  ///Fetch All user favourites
  /// Throws an [ApiException] if the response error is true
  /// Return [FavouriteResponse]
  Future<FavouriteResponse> fetchfavorites(
      FavouriteRequest favouriteRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/favorites.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${favouriteRequest.token}");

      request.fields.addAll(favouriteRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          favouriteRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return FavouriteResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [FavouriteSaveRequest] and
  /// Save a product as favourites
  /// Throws an [ApiException] if the response error is true
  /// Return [CartSaveResponse]
  Future<String> addRemovefavorites(
      FavouriteSaveDeleteRequest favouriteSaveDeleteRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/favorites.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${favouriteSaveDeleteRequest.token}");

      request.fields.addAll(favouriteSaveDeleteRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          favouriteSaveDeleteRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return _responseMap['message'];
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [PromoCodeRequest] and
  ///Check if the promocode is valid
  /// Throws an [ApiException] if the error is true
  /// Return [PromoCodeResponse]
  Future<PromoCodeResponse> validatePromoCode(
      PromoCodeRequest promoCodeRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/validate-promo-code.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${promoCodeRequest.token}");
      request.fields.addAll(promoCodeRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          promoCodeRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return PromoCodeResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [Cartrequest] and
  /// Fetch the Users cart
  /// Throws an [ApiException] if the response error is true
  /// Return [CartResponse]
  Future<CartResponse> getUserCart(Cartrequest cartRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/cart.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers
          .putIfAbsent("Authorization", () => "Bearer ${cartRequest.token}");

      request.fields.addAll(cartRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          cartRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return CartResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [cartSaveRequest] and
  /// Saves an Item / Items to Users cart
  /// Throws an [ApiException] if the response error is true
  /// Return [CartSaveResponse]
  Future<CartSaveResponse> saveItemToCart(
      CartSaverequest cartSaverequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/cart.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${cartSaverequest.token}");

      request.fields.addAll(cartSaverequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          cartSaverequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return CartSaveResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [OrderSaveRequest] and
  /// Creates a new order
  /// Throws an [ApiException] if the response error is true
  /// Return [OrderSaveResponse]
  Future<OrderSaveResponse> createOrder(
      OrderSaveRequest orderSaveRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/order-process.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${orderSaveRequest.token}");

      request.fields.addAll(orderSaveRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          orderSaveRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return OrderSaveResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [OrderFetchRequest] and
  /// Fetch all user Orders
  /// Throws an [ApiException] if the response error is true
  /// Return [OrderFetchResponse]
  Future<OrderFetchResponse> fetchOrders(
      OrderFetchRequest orderFetchRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/order-process.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${orderFetchRequest.token}");

      request.fields.addAll(orderFetchRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          orderFetchRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return OrderFetchResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [OrderCancellRequest] and
  /// Update Order Status
  /// Throws an [ApiException] if the response error is true
  /// Return [String]
  Future<String> updateOrderRequest(
      OrderUpdateRequest orderUpdateRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/order-process.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${orderUpdateRequest.token}");

      request.fields.addAll(orderUpdateRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          orderUpdateRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return _responseMap['message'];
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  //Transcation Related API's

  /// Takes a object of [OrderFetchRequest] and
  /// Fetch all Wallet or user Transaction history
  /// Throws an [ApiException] if the response error is true
  /// Return [TransactionHistoryResponse]
  Future<TransactionHistoryResponse> fetchTransactionHistory(
      TransactionHistoryRequest transactionHistoryRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-user-transactions.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${transactionHistoryRequest.token}");

      request.fields.addAll(transactionHistoryRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          transactionHistoryRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);

      if (_responseMap['error'] == false) {
        return TransactionHistoryResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [CreateTransactionRequest] and
  /// Creates a new transaction
  /// Throws an [ApiException] if the response error is true
  /// Return [CreateTransactionResponse]
  Future<CreateTransactionResponse> createTransaction(
      CreateTransactionRequest createTransactionRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/order-process.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${createTransactionRequest.token}");

      request.fields.addAll(createTransactionRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          createTransactionRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return CreateTransactionResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [AddWalletBalanceRequest] and
  /// Add Money to Wallet
  /// Throws an [ApiException] if the response error is true
  /// Return [CreateTransactionResponse]
  Future<AddWalletResponse> createWalletTransaction(
      AddWalletBalanceRequest addWalletBalanceRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-user-transactions.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${addWalletBalanceRequest.token}");

      request.fields.addAll(addWalletBalanceRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          addWalletBalanceRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return AddWalletResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [CreateStripePaymentRequest] and
  /// Create a Stripe payment
  /// Throws an [ApiException] if the response error is true
  /// Return [Map<dynamic, dynamic>]
  Future<Map<dynamic, dynamic>> createStripePayment(
      CreateStripePaymentRequest createStripePaymentRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/../stripe/create-payment.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${createStripePaymentRequest.token}");

      request.fields.addAll(createStripePaymentRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          createStripePaymentRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['publishableKey'].toString().isNotEmpty) {
        return _responseMap;
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [PaytmChecksumRequest] and
  /// Create a paytm checksum request
  /// Throws an [ApiException] if the response error is true
  /// Return [PaytmChecksumResponse]
  Future<PaytmChecksumResponse> createPaytmChecksum(
      PaytmChecksumRequest paytmChecksumRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/../paytm/generate-checksum.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${paytmChecksumRequest.token}");

      request.fields.addAll(paytmChecksumRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          paytmChecksumRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return PaytmChecksumResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [PaytmValidationRequest] and
  /// Verify a paytm transaction
  /// Throws an [ApiException] if the response error is true
  /// Return [PaytmValidationResponse]
  Future<PaytmValidationResponse> verifyPaytmValidation(
      PaytmValidationRequest paytmValidationRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/../paytm/valid-transction.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${paytmValidationRequest.token}");

      request.fields.addAll(paytmValidationRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          paytmValidationRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return PaytmValidationResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [Verifypaystackrequest] and
  /// Verify a Paystack transaction
  /// Throws an [ApiException] if the response error is true
  /// Return [String]
  Future<String> verifyPaystackTransaction(
      Verifypaystackrequest verifypaystackrequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/payment-request.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${verifypaystackrequest.token}");

      request.fields.addAll(verifypaystackrequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          verifypaystackrequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return "";
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [CreateRazorpayOrderRequest] and
  /// Create a Razorpay order
  /// Throws an [ApiException] if the response error is true
  /// Return [CategoryResponse]
  Future<CreateRazorpayOrderResponse> createRazorpayOrder(
      CreateRazorpayOrderRequest createRazorpayOrderRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/create-razorpay-order.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${createRazorpayOrderRequest.token}");

      request.fields.addAll(createRazorpayOrderRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          createRazorpayOrderRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return CreateRazorpayOrderResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }
}
