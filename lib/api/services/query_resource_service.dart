import 'dart:convert';

import 'package:grobay/api/api_client.dart';
import 'package:grobay/api/api_config.dart';
import 'package:grobay/api/api_exception.dart';
import 'package:grobay/api/request/category_request.dart';
import 'package:grobay/api/request/cities_request.dart';
import 'package:grobay/api/request/extra_setting_request.dart';
import 'package:grobay/api/request/faq_request.dart';
import 'package:grobay/api/request/fcm_save_request.dart';
import 'package:grobay/api/request/keyword_request.dart';
import 'package:grobay/api/request/location_request.dart';
import 'package:grobay/api/request/notifications_request.dart';
import 'package:grobay/api/request/product_request.dart';
import 'package:grobay/api/request/seller_request.dart';
import 'package:grobay/api/request/settings_request.dart';
import 'package:grobay/api/request/subcategory_request.dart';
import 'package:grobay/api/request/variant_request.dart';
import 'package:grobay/api/response/app_settings_response.dart';
import 'package:grobay/api/response/category_response.dart';
import 'package:grobay/api/response/city_response.dart';
import 'package:grobay/api/response/extra_setting_response.dart';
import 'package:grobay/api/response/faq_response.dart';
import 'package:grobay/api/response/keyword_response.dart';
import 'package:grobay/api/response/location_response.dart';
import 'package:grobay/api/response/notifications_response.dart';
import 'package:grobay/api/response/payment_settings_response.dart';
import 'package:grobay/api/response/product_response.dart';
import 'package:grobay/api/response/seller_response.dart';
import 'package:grobay/api/response/startup_response.dart';
import 'package:grobay/api/request/startup_request.dart';
import 'package:grobay/api/response/subcategory_response.dart';
import 'package:grobay/api/response/time_slots_config_response.dart';
import 'package:grobay/api/response/time_slots_response.dart';
import 'package:grobay/api/response/varaint_response.dart';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';

/// This Class Provides Methods To Fetch Data From Server
class QueryResourceApi {
  ApiClient apiClient = ApiClient();
  final _log = Logger();

  /// Takes a object of [StartupRequest] and
  /// Fetch the App meta data required for drawing screen options
  /// Throws an [ApiException] if the error is true
  /// Return [StartupResposne]
  Future<StartupResposne> getStartupData(StartupRequest startupRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-all-data.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers
          .putIfAbsent("Authorization", () => "Bearer ${startupRequest.token}");
      request.fields.addAll(startupRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          startupRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return StartupResposne.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [FCMSaveRequest] and
  /// Save the fcmid
  /// Throws an [ApiException] if the response error is true
  /// Return [bool]
  Future<bool> storeFCMId(FCMSaveRequest fcmSaveRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/store-fcm-id.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers
          .putIfAbsent("Authorization", () => "Bearer ${fcmSaveRequest.token}");
      request.fields.addAll(fcmSaveRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          fcmSaveRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      return true;
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [ExtraSettingsRequest] and
  /// Fetch about / contact / terms / privacy
  /// Throws an [ApiException] if the response error is true
  /// Return [ExtraSettingResponse]
  Future<ExtraSettingResponse> getExtraSettings(
      ExtraSettingsRequest extraSettingsRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/settings.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${extraSettingsRequest.token}");
      request.fields.addAll(extraSettingsRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          extraSettingsRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return ExtraSettingResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [SettingsRequest] and
  /// Fetch the Payments Settings
  /// Throws an [ApiException] if the response error is true
  /// Return [PaymentSettingsResponse]
  Future<PaymentSettingsResponse> getPaymentSettings(
      SettingsRequest settingsRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/settings.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${settingsRequest.token}");
      request.fields.addAll(settingsRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          settingsRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return PaymentSettingsResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [SettingsRequest] and
  /// Fetch the Time Slots
  /// Throws an [ApiException] if the response error is true
  /// Return [TimeSlotsResponse]
  Future<TimeSlotsResponse> getTimeSlots(
      SettingsRequest settingsRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/settings.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${settingsRequest.token}");
      request.fields.addAll(settingsRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          settingsRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return TimeSlotsResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [SettingsRequest] and
  /// Fetch the Time Slots Config
  /// Throws an [ApiException] if the response error is true
  /// Return [TimeSlotsConfigResponse]
  Future<TimeSlotsConfigResponse> getTimeSlotConfig(
      SettingsRequest settingsRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/settings.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${settingsRequest.token}");
      request.fields.addAll(settingsRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          settingsRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return TimeSlotsConfigResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [SettingsRequest] and
  /// Fetch the App Settings
  /// Throws an [ApiException] if the response error is true
  /// Return [AppSettingsResponse]
  Future<AppSettingsResponse> getAppSettings(
      SettingsRequest settingsRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/settings.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${settingsRequest.token}");
      request.fields.addAll(settingsRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          settingsRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return AppSettingsResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [Faqrequest] and
  /// Fetch faq response
  /// Throws an [ApiException] if the response error is true
  /// Return [FaqResponse]
  Future<FaqResponse> getFaq(Faqrequest faqrequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-faqs.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers
          .putIfAbsent("Authorization", () => "Bearer ${faqrequest.token}");
      request.fields.addAll(faqrequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          faqrequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return FaqResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [LocationRequest] and
  /// Fetch the Locations List
  /// Throws an [ApiException] if the response error is true
  /// Return [LocationResponse]
  Future<LocationResponse> getLocations(LocationRequest locationRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-locations.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${locationRequest.token}");
      request.fields.addAll(locationRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          locationRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return LocationResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [Citiesrequest] and
  /// Fetch the Available Cities List
  /// Throws an [ApiException] if the response error is true
  /// Return [CitiesResponse]
  Future<CitiesResponse> getCities(Citiesrequest citiesrequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-locations.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers
          .putIfAbsent("Authorization", () => "Bearer ${citiesrequest.token}");
      request.fields.addAll(citiesrequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          citiesrequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return CitiesResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [KeywordRequest] and
  /// Fetch the Product keyword base on request param
  /// Throws an [ApiException] if the response error is true
  /// Return [KeywordResponse]
  Future<KeywordResponse> getKeyword(KeywordRequest keywordRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-products.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers
          .putIfAbsent("Authorization", () => "Bearer ${keywordRequest.token}");

      request.fields.addAll(keywordRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          keywordRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false || _responseMap['data'].length == 0) {
        return KeywordResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [ProductRequest] and
  /// Fetch the Product List base on request param
  /// Throws an [ApiException] if the response error is true
  /// Return [ProductResponse]
  Future<ProductResponse> getProducts(ProductRequest productRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-products.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers
          .putIfAbsent("Authorization", () => "Bearer ${productRequest.token}");

      request.fields.addAll(productRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          productRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return ProductResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [CategoryRequest] and
  /// Fetch All Categories
  /// Throws an [ApiException] if the response error is true
  /// Return [CategoryResponse]
  Future<CategoryResponse> getCategories(
      CategoryRequest categoryRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-categories.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${categoryRequest.token}");

      request.fields.addAll(categoryRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          categoryRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false || _responseMap['data'].length == 0) {
        return CategoryResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [SubCategoryRequest] and
  /// Fetch All Sub Categories
  /// Throws an [ApiException] if the response error is true
  /// Return [SubCategoryResponse]
  Future<SubCategoryResponse> getSubCategories(
      SubCategoryRequest subCategoryRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-subcategories.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${subCategoryRequest.token}");

      request.fields.addAll(subCategoryRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          subCategoryRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false || _responseMap['data'].length == 0) {
        return SubCategoryResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [SellerRequest] and
  /// Fetch All Seller / a Single Seller
  /// Throws an [ApiException] if the response error is true
  /// Return [SellerResponse]
  Future<SellerResponse> getSellers(SellerRequest sellerRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-seller-data.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers
          .putIfAbsent("Authorization", () => "Bearer ${sellerRequest.token}");

      request.fields.addAll(sellerRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          sellerRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false || _responseMap['data'].length == 0) {
        return SellerResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [NotificationRequest] and
  /// Fetch All Notifications
  /// Throws an [ApiException] if the response error is true
  /// Return [NotificationResponse]
  Future<NotificationResponse> getNotifications(
      NotificationRequest notificationRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/sections.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers.putIfAbsent(
          "Authorization", () => "Bearer ${notificationRequest.token}");

      request.fields.addAll(notificationRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          notificationRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false || _responseMap['data'].length == 0) {
        return NotificationResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }

  /// Takes a object of [VariantRequest] and
  /// Fetch the cart List base on request param
  /// Throws an [ApiException] if the response error is true
  /// Return [VariantsResponse]
  Future<VariantsResponse> getCartVaraints(
      VariantRequest variantRequest) async {
    try {
      final path = ApiConfig.appApiUrl + '/get-products.php';
      var request = http.MultipartRequest("POST", Uri.parse(path));
      request.headers
          .putIfAbsent("Authorization", () => "Bearer ${variantRequest.token}");

      request.fields.addAll(variantRequest.toJson());
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      _log.i(path +
          "\n" +
          variantRequest.toJson().toString() +
          "\n" +
          response.body.toString());
      Map<dynamic, dynamic> _responseMap = json.decode(response.body);
      if (_responseMap['error'] == false) {
        return VariantsResponse.fromJson(_responseMap);
      } else
        throw ApiException(
            500, _responseMap['data'] ?? _responseMap['message']);
    } catch (e) {
      throw ApiException(e.code ?? 500, e.message ?? "Unknown Error");
    }
  }
}
