import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:image_cropper/image_cropper.dart';

class CameraImage {
  String base64;
  String type;
  String name;
  File image;

  CameraImage({
    this.base64,
  });
}

class CameraService {
  final picker = ImagePicker();
  File _image;

  Future<File> cropImage(File imageFile) async {
    File croppedFile = await ImageCropper().cropImage(
      sourcePath: imageFile.path,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
        CropAspectRatioPreset.ratio3x2,
        CropAspectRatioPreset.original,
        CropAspectRatioPreset.ratio4x3,
        CropAspectRatioPreset.ratio16x9
      ],
      androidUiSettings: AndroidUiSettings(
          toolbarTitle: 'Cropper',
          toolbarColor: Colors.deepOrange,
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.original,
          lockAspectRatio: false),
      iosUiSettings: IOSUiSettings(
        minimumAspectRatio: 1.0,
      )
    );
    return croppedFile;
  }

  Future<CameraImage> _openImageFromSource(ImageSource source) async {
    final pickedFile = await picker.getImage(source: source);
       CameraImage cameraImage = CameraImage(base64: "");
    if (pickedFile != null) {
      _image = File(pickedFile.path);
      List<int> imageBytes = _image.readAsBytesSync();
      String imageB64 = base64Encode(imageBytes);
      File file = File(pickedFile.path);
      final fileName = file.path.split('/').last;
      String type = lookupMimeType(fileName);
      cameraImage.type = "data:$type;base64,";
      cameraImage.base64 =  imageB64;
      cameraImage.name = fileName;
      cameraImage.image = _image;

      return cameraImage;
    } else {
    }
    return cameraImage;
  }

  Future<CameraImage> openCamera() async {
    return await this._openImageFromSource(ImageSource.camera);
  }

  Future<CameraImage> openGallery() async {
    return await this._openImageFromSource(ImageSource.gallery);
  }
}
