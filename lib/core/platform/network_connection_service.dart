import 'dart:io';
import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:logger/logger.dart';

/// The NetworkConnectionService class provides methods
/// to check whether the device is connected to the internet
class NetworkConnectionService {
  //This creates the single instance by calling the `_internal` constructor specified below
  static final NetworkConnectionService _singleton =
      new NetworkConnectionService._internal();
  final log = Logger();
  NetworkConnectionService._internal();

  //This is what's used to retrieve the instance through the app
  static NetworkConnectionService getInstance() => _singleton;

  //This tracks the current connection status
  bool hasConnection = false;

  //Flag to check if the network connection check is running first time
  bool isFirstTimeChecking = true;

  //This is how we'll allow subscribing to connection changes
  StreamController connectionChangeController = new StreamController.broadcast();

  //flutter_connectivity
  final Connectivity _connectivity = Connectivity();

  //Hook into flutter_connectivity's Stream to listen for changes
  //And check the connection status out of the gate
  void initialize() {
    _connectivity.onConnectivityChanged.listen(_connectionChange);
    checkConnection();
  }

  Stream get connectionChange => connectionChangeController.stream;

  //A clean up method to close our StreamController
  //Because this is meant to exist through the entire application life cycle this isn't really an issue
  void dispose() {
    connectionChangeController.close();
  }

  //flutter_connectivity's listener
  void _connectionChange(ConnectivityResult result) {
    checkConnection();
  }

  //The test to actually see if there is a connection
  Future<bool> checkConnection() async {
    log.i("Checking Network Connection");
    // bool previousConnection = hasConnection;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        hasConnection = true;
      } else {
        hasConnection = false;
      }
    } on SocketException catch (_) {
      hasConnection = false;
    }
    //The connection status changed send out an update to all listeners
    log.i("Network Connection State Changed==>" + this.hasConnection.toString());
    connectionChangeController.add(hasConnection);
    // if (previousConnection != hasConnection || isFirstTimeChecking) {
    //   this.isFirstTimeChecking = false;
    //   log.i("Network Connection State Changed==>" + this.isFirstTimeChecking.toString());
    //   connectionChangeController.add(hasConnection);
    // }
    return hasConnection;
  }
}
