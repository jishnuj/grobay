import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:logger/logger.dart';

class LocationService {
  Future<String> getCountryCode() async {
    try {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      Logger().i('location: ${position.latitude}');
      List<Placemark> placemarks =
          await placemarkFromCoordinates(position.latitude, position.longitude);
      var first = placemarks.first;
      return first.isoCountryCode; // this will return country code
    } catch (e) {
      Logger().e(e);
    }
  }

  Future<Placemark> getCustomPosition(double lat, double long) async {
    List<Placemark> placemarks = await placemarkFromCoordinates(lat, long);
    var first = placemarks.first;
    return first;
  }

  Future<Placemark> getCurrentPosition() async {
    Coordinates coordinates = await this.getLatLon();
    List<Placemark> placemarks = await placemarkFromCoordinates(
        coordinates.latitude, coordinates.longitude);
    var first = placemarks.first;
    return first;
  }

  Future<Coordinates> getLatLon() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    Logger().i('location: ${position.latitude}');
    final coordinates = new Coordinates(position.latitude, position.longitude);
    return coordinates;
  }
}

class Coordinates {
  final double latitude;
  final double longitude;
  Coordinates(this.latitude, this.longitude);
}
