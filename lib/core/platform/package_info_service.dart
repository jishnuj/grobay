import 'package:package_info_plus/package_info_plus.dart';

class PackageInfoService {
  /// Fetches the current App version
 
  Future<String> getVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    return packageInfo.version + '.' + packageInfo.buildNumber;
    // return packageInfo.version + '.' + "1";
  }
}
