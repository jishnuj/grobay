import 'package:flutter_secure_storage/flutter_secure_storage.dart';

/// The LocalStorageService class provides wrapper methods
/// for flutter_secure_storage methods
class LocalStorageService {
  FlutterSecureStorage flutterSecureStorage = FlutterSecureStorage();
  

  Future<void> setItem(String key, String value) async {
    return await this.flutterSecureStorage.write(key: key, value: value);
  }

  Future<String> getItem(String key) async {
    return await this.flutterSecureStorage.read(key: key);
  }

  Future<void> deleteItem(String key) async{
    return this.flutterSecureStorage.delete(key: key);
  }

  Future<void> deleteAll() async {
    return await this.flutterSecureStorage.deleteAll();
  }

  Future<Map<String,String>> readAll() async {
    return this.flutterSecureStorage.readAll();
  }
}
