import 'dart:convert';
import 'dart:io';
import 'package:get/get.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:grobay/api/model/category.dart';
import 'package:grobay/api/model/product.dart';
import 'package:grobay/presentation/page/category/category_products_page.dart';
import 'package:grobay/presentation/page/notifications_page.dart';
import 'package:grobay/presentation/page/product/product_detail_page.dart';
import 'package:logger/logger.dart';

String topic = 'notifications';

AwesomeNotifications awesomeNotifications = new AwesomeNotifications();

//Handle background messages
Future<void> _handleBackgroundNativeNotification(message) async {
  final logger = Logger();
  logger.i(
      'Background Message also contained a notification: ${message.data.toString()}');
  Map<String, dynamic> data = jsonDecode(message.data["data"]);
  awesomeNotifications.createNotification(
      content: NotificationContent(
          id: 10,
          payload: {"type": data['type'], "id": data['id']},
          bigPicture: data['image'],
          channelKey: 'basic_channel',
          notificationLayout: data['image'] != null
              ? NotificationLayout.BigPicture
              : NotificationLayout.Default,
          title: data['title'],
          body: data['message']));
  return Future<void>.value();
}

/// The FireBaseService class provides methods
/// to access firebase functionalities
class FireBaseService {
  FirebaseMessaging firebaseMessaging;
  final logger = Logger();

  FireBaseService();

  // Initialize Firebase
  Future<void> initialize() async {
    WidgetsFlutterBinding.ensureInitialized();
    await initializeAwseomeNotification();
    await Firebase.initializeApp();
    this.firebaseMessaging = FirebaseMessaging.instance;
    FirebaseMessaging.onBackgroundMessage(_handleBackgroundNativeNotification);
    this.listenForegroundMessages();
    await firebaseMessaging.requestPermission(
      alert: true,
      announcement: true,
      badge: true,
      carPlay: true,
      criticalAlert: true,
      provisional: true,
      sound: true,
    );
    awesomeNotifications.actionStream.listen((receivedNotifiction) {
      if (receivedNotifiction.payload["type"] == "product") {
        Get.to(ProductDetailsPage(
          productId: receivedNotifiction.payload["id"],
          product: Product(id: receivedNotifiction.payload["id"],name: "..."),
        ));
      } else if (receivedNotifiction.payload["type"] == "category") {
        logger.i("Navigate to category", receivedNotifiction.payload["id"]);
        Get.to(CategoryProductsPage(
          parentCategory: Category(
              id: receivedNotifiction.payload["id"],
              name: receivedNotifiction.payload["title"]),
        ));
      } else {
        logger.i(
            "Navigate to notification page", receivedNotifiction.payload["id"]);
        Get.to(NotificationsPage());
      }
    });
  }

  // Listen foreground messages
  void listenForegroundMessages() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      logger.i(
          'Foreground Message also contained a notification: ${message.data.toString()}');
      Map<String, dynamic> data = jsonDecode(message.data["data"]);
      this.createNativeNotification(data);
    });
  }

  void createNativeNotification(Map<String, dynamic> data) {
    awesomeNotifications.createNotification(
        content: NotificationContent(
            id: 10,
            payload: {
              "type": data['type'],
              "id": data['id'],
              "title": data['title']
            },
            bigPicture: data['image'],
            channelKey: 'basic_channel',
            notificationLayout: data['image'] != null
                ? NotificationLayout.BigPicture
                : NotificationLayout.BigText,
            title: data['title'],
            body: data['message']));
  }

  initializeAwseomeNotification() async {
    await awesomeNotifications.initialize(
        // set the icon to null if you want to use the default app icon
        "resource://drawable/launcher_icon",
        [
          NotificationChannel(
              channelGroupKey: 'basic_channel_group',
              channelKey: 'basic_channel',
              channelName: 'Basic notifications',
              channelDescription: 'Notification channel for basic tests',
              defaultColor: Color(0xFF9D50DD),
              ledColor: Colors.white)
        ],
        // Channel groups are only visual and are not required
        channelGroups: [
          NotificationChannelGroup(
              channelGroupkey: 'basic_channel_group',
              channelGroupName: 'Basic group')
        ],
        debug: true);
    await AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
      if (!isAllowed) {
        // This is just a basic example. For real apps, you must show some
        // friendly dialog box before call the request method.
        // This is very important to not harm the user experience
        AwesomeNotifications().requestPermissionToSendNotifications();
      }
    });
  }

  // Returns FCM Token for android /
  // APNs Token for IOS
  Future<String> getToken() async {
    try {
      if (Platform.isAndroid) {
        String token = await this.firebaseMessaging.getToken();
        this.logger.i("message" + "_$token");
        return token;
      } else if (Platform.isIOS) {
        // String token = await this.firebaseMessaging.getAPNSToken();
        String token = await this.firebaseMessaging.getToken();
        this.logger.i("message" + "_$token");
        return token;
      }
      return "";
    } catch (e) {
      logger.i('Getting Firebase Token Issue ${e.toString()}');
      return "";
    }
  }

  Future<void> deleteToken() async {
    return await FirebaseMessaging.instance.deleteToken();
  }

  Future<void> unsubscribe() async {
    return await FirebaseMessaging.instance.unsubscribeFromTopic(topic);
  }

  Future<void> subscribe() async {
    return await FirebaseMessaging.instance.subscribeToTopic(topic);
  }
}
