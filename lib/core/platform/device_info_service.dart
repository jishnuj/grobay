import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';

class DeviceInfoModel {
  String deviceModel;
  String deviceManufacturer;
  String platform;
  bool isPhysicalDevice;

  DeviceInfoModel(
      {this.deviceModel,
      this.isPhysicalDevice = true,
      this.deviceManufacturer,
      this.platform});

  @override
  String toString() {
    return """{model:${this.deviceModel},
    'manufacturer':${this.deviceManufacturer},'platform':${this.platform}}""";
  }
}

/// The DeviceInfoService class provides methods to access
/// Device Details
class DeviceInfoService {
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};
  DeviceInfoService();

  /// Gets The Manufacturer,Model & Platform of The Device
  /// Return [DeviceInfoModel]
  Future<DeviceInfoModel> getDeviceInfo() async {
    bool isPhysical = true;
    try {
      if (Platform.isAndroid) {
        _deviceData =
            _readAndroidDeviceInfo(await deviceInfoPlugin.androidInfo);
        _deviceData['platform'] = "android";
      } else if (Platform.isIOS) {
        _deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
        _deviceData['platform'] = "ios";
        IosDeviceInfo data = await deviceInfoPlugin.iosInfo;
        isPhysical = data.isPhysicalDevice;
      }

      return DeviceInfoModel(
          deviceModel: _deviceData['model'],
          deviceManufacturer: _deviceData['manufacturer'],
          platform: _deviceData['platform'],
          isPhysicalDevice: isPhysical);
    } on PlatformException {
      _deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }
  }

  /// Takes a object of [AndroidDeviceInfo] and
  /// Gets The Manufacturer,Model,Product Details of Android Device
  /// Return [Map<String, dynamic>]
  Map<String, dynamic> _readAndroidDeviceInfo(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
    };
  }

  /// Takes a object of [IosDeviceInfo] and
  /// Gets The Manufacturer , Model Details of IOS Device
  /// Return [Map<String, dynamic>]
  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'manufacturer': 'Apple',
      'model:': data.utsname.machine,
    };
  }
}
