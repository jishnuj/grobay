import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:url_launcher/url_launcher.dart';
import 'config/app_config.dart';
import 'platform/package_info_service.dart';

class Util {
  PackageInfoService _packageInfoService = GetIt.I.get<PackageInfoService>();
  final logger = Logger();
  Map<String, Color> colorMap = Map<String, Color>();
  // Util Methods

  /// Open Play Store if the new App
  /// Update is Available
  Future<bool> openUpdateApp() async {
    logger.i("Update App");
    bool value = await canLaunch(AppConfig.PLAY_STORE_URL);
    if (value) {
      return launch(AppConfig.PLAY_STORE_URL);
    } else {
      throw 'Could not launch ${AppConfig.PLAY_STORE_URL}';
    }
  }

  /// Opens the EmailClient
  Future<bool> openEmail(String email, {bool isUri = false}) async {
    logger.i("Opening email $email");
    if (isUri) {
      logger.i("Opening Via URL Scheme");
      if (await canLaunch(email)) {
        return await launch(email);
      } else {
        throw 'Could not launch $email';
      }
    } else {
      if (await canLaunch("mailto:" + email)) {
        return await launch("mailto:" + email);
      } else {
        throw 'Could not launch $email';
      }
    }
  }

  /// Opens the Dialer with the specified number
  Future<bool> openPhone(String number) async {
    logger.i("Making a Call $number");
    if (await canLaunch('tel:$number')) {
      return await launch('tel:$number');
    } else {
      throw 'Could not launch $number';
    }
  }

  /// Opens the Browser with the specified url
  Future<bool> openBrowser(String url, {bool isWebView = false}) async {
    logger.i("Opening browser $url");
    if (await canLaunch(url)) {
      if (isWebView) {
        return await launch(url,
            forceWebView: true, enableJavaScript: true, enableDomStorage: true);
      } else {
        return await launch(url);
      }
    } else {
      throw 'Could not launch $url';
    }
  }

  /// Gets the app version
  Future<String> appVersion() async {
    return await this._packageInfoService.getVersion();
  }

  /// Gets the appId
  String appAccessKey() {
    return AppConfig.ACCESS_KEY;
  }

  // /// Gets the company details
  // String compantDetails() {
  //   return AppConfig.companyDetails;
  // }

  Color makeRandomColors({String key}) {
    // Define all colors you want here
    // Get Same Color Base on a String key
    const predefinedColors = [
      Colors.red,
      Colors.green,
      Colors.blue,
      Colors.orangeAccent,
      Colors.pink
    ];
    Random random = Random();
    if (key != null) {
      if (this.colorMap.containsKey(key)) {
        return this.colorMap[key];
      } else {
        this.colorMap[key] =
            predefinedColors[random.nextInt(predefinedColors.length)];
        return this.colorMap[key];
      }
    }
    return predefinedColors[random.nextInt(predefinedColors.length)];
  }

  String validateURL(String link) {
    if (link.indexOf("http://") == 0 || link.indexOf("https://") == 0) {
      return link;
    } else {
      return 'https://' + link;
    }
  }
}
