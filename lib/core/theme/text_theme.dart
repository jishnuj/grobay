import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

TextTheme textTheme = TextTheme(
    headline3: GoogleFonts.montserrat(
        fontSize: 42.0, fontWeight: FontWeight.bold, color: Colors.grey[700]),
    headline4: GoogleFonts.montserrat(
      fontSize: 25.0,
      fontWeight: FontWeight.bold,
    ),
    headline5: GoogleFonts.montserrat(
        fontSize: 24.0, fontWeight: FontWeight.w400, color: Colors.grey[700]),
    headline6: GoogleFonts.montserrat(
      fontSize: 20.0,
      fontWeight: FontWeight.w500,
    ),
    subtitle1: GoogleFonts.montserrat(
        fontSize: 16.0, fontWeight: FontWeight.w400, color: Colors.grey[700]),
    subtitle2: GoogleFonts.montserrat(
        fontSize: 14.0, fontWeight: FontWeight.w500, color: Colors.grey[700]),
    bodyText1: GoogleFonts.montserrat(
        fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.grey[700]),
    bodyText2: GoogleFonts.montserrat(
        fontSize: 12.0, fontWeight: FontWeight.w400, color: Colors.grey[700]));
