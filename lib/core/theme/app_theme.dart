import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/core/theme/text_theme.dart';

enum AppThemes { PrimaryTheme }

final appThemes = {
  AppThemes.PrimaryTheme: ThemeData(
    brightness: Brightness.light,
    appBarTheme:
        AppBarTheme(iconTheme: IconThemeData(color: Palette.onSecondary)),
    textTheme: textTheme,
    highlightColor: Colors.yellow,
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        textStyle: GoogleFonts.montserrat(
          fontSize: 14.0,
          fontWeight: FontWeight.w600,
          color: Colors.white,
        ),
        primary: Palette.buttonBg,
        onPrimary: Palette.buttonText,
        elevation: 5.0,
        padding: const EdgeInsets.symmetric(vertical: 18.0, horizontal: 20.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
      ),
    ),
  )
};
