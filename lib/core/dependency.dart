import 'package:get_it/get_it.dart';
import 'package:grobay/api/services/query_resource_service.dart';
import 'package:grobay/api/services/user_resource_api.dart';
import 'package:grobay/core/api_bridge.dart';
import 'package:grobay/core/platform/camera_service.dart';
import 'package:grobay/core/platform/device_info_service.dart';
import 'package:grobay/core/platform/firebase_service.dart';
import 'package:grobay/core/platform/local_storage_service.dart';
import 'package:grobay/core/platform/location_service.dart';
import 'package:grobay/core/platform/network_connection_service.dart';
import 'package:grobay/core/platform/package_info_service.dart';
import 'package:grobay/core/util.dart';

final getIt = GetIt.instance;

/// Dependency Injection
///
/// This Function Registeres All The dependencies required by the app
/// This should be called before initializing the app [runApp()]
///
/// Example
/// -------
/// Use the Registered Dependencies in any file
/// ExampleService exampleService = GetIt.I.get<ExampleService>();
///
Future<void> setupDependencies() async {
  FireBaseService fireBaseService = FireBaseService();
  await fireBaseService.initialize();
  NetworkConnectionService networkConnectionService =
      NetworkConnectionService.getInstance();

  getIt.registerSingleton<FireBaseService>(fireBaseService);
  getIt.registerSingleton<DeviceInfoService>(DeviceInfoService());
  getIt.registerSingleton<PackageInfoService>(PackageInfoService());
  getIt.registerSingleton<LocalStorageService>(LocalStorageService());
  getIt.registerSingleton<LocationService>(LocationService());
  getIt.registerSingleton<CameraService>(CameraService());
  getIt.registerSingleton<Util>(Util());
  getIt.registerSingleton<NetworkConnectionService>(networkConnectionService);
  getIt.registerSingleton<QueryResourceApi>(QueryResourceApi());
  getIt.registerSingleton<UserResourceApi>(UserResourceApi());
  getIt.registerSingleton<ApiBridge>(ApiBridge());
}
