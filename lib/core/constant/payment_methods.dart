enum PAYMENT_METHODS {
  WALLET,
  COD,
  PAYSTACK,
  PAYPAL,
  RAZORPAY,
  FLUTTERWAVE,
  STRIPE,
  MIDTRANS,
  PAYUMONEY,
  //
  PAYTM,
  SSLCOMMERZ,
}

extension PAYMENT_METHODS_EXTENSION on PAYMENT_METHODS {
  String get name {
    switch (this) {
      case PAYMENT_METHODS.COD:
        return 'cod';
      case PAYMENT_METHODS.PAYUMONEY:
        return 'payumoney';
      case PAYMENT_METHODS.MIDTRANS:
        return 'midtrans';
      case PAYMENT_METHODS.PAYPAL:
        return 'paypal';
      case PAYMENT_METHODS.RAZORPAY:
        return 'razorpay';
      case PAYMENT_METHODS.FLUTTERWAVE:
        return 'flutterwave';
      case PAYMENT_METHODS.PAYSTACK:
        return 'paystack';
      case PAYMENT_METHODS.STRIPE:
        return 'stripe';
      case PAYMENT_METHODS.PAYTM:
        return 'paytm';
      case PAYMENT_METHODS.SSLCOMMERZ:
        return 'SSLCOMMERZ';
      case PAYMENT_METHODS.WALLET:
        return 'wallet';
      default:
        return "cod";
    }
  }
}
