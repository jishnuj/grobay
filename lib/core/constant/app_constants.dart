class AppConstants {
  static String ORDER_RECIEVED = "received";
  static String ORDER_PROCESSED = "processed";
  static String ORDER_SHIPPED = "shipped";
  static String ORDER_DELIVERED = "delivered";
  static String ORDER_CANCELLED = "cancelled";
  static String ORDER_RETURNED = "returned";
  static String USER_TRANSACTION_HISTORY = "transactions";
  static String WALLET_TRANSACTION_HISTORY = "wallet_transactions";
}
