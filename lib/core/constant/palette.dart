import 'package:flutter/material.dart';

class Palette {
  static const Color primary = Color(0xFFFFe100);
  static const Color secondary = Colors.white;
  static const Color onPrimary = Colors.white;
  static Color onSecondary = Colors.grey[800];
  static Color buttonBg = Color(0xFF88cb21);
  static Color buttonText = Colors.white;
  static Color highlightColor = Color(0xFFed1c24);
  static Color teritaryColor = Colors.grey[200];
  static Color disabledColor = Colors.grey[600];
  static Color bottomCartBarBg = Color(0xFFFFe100).withOpacity(0.3);
  static Color bottomCartBarText = Colors.black;
  static Color bottomCartBarButtonBorder = Colors.black;
  static Color bottomCartBarButtonText = Colors.black;
  static Color pleaseSelectLocationTextColor = Colors.black;
  static Color pleaseSelectLocationTextIcon = Color(0xFF88cb21);
  static Color pleaseSelectLocationBorderColor = Colors.grey[200];
  static Color homeCurrencyTextColor = Colors.grey[600];
  static Color productCartCardColor = Color(0xFFFFe100).withOpacity(0.1);
  static Color productCartCardSecondaryText = Colors.grey[800];
  static Color addressSelectedColor = Color(0xFF88cb21);
  static Color checkOutButtonBg = Color(0xFFFFe100);
  static Color checkOutButtonText = Colors.grey[700];
  static Color orderCardBackgroundColor = Colors.white;
  static Color orderCardTextColorPrimary = Colors.grey[800];
  static Color orderCardTextColorSecondary = Colors.grey[700];
  static Color orderDetailsPageOtpBoxColor = primary;
  static Color walletDetailsCardBg = secondary;
  static Color walletDetailsCardTextPrimary = onSecondary;
  static Color walletDetailsCardTextSecondary = Colors.grey[700];
  static Color transactionDetailsCardBg = secondary;
  static Color transactionDetailsCardTextPrimary = onSecondary;
  static Color transactionDetailsCardTextSecondary = Colors.grey[700];

  static Color walletRechargeTextPrimary = Colors.grey[700];
  static Color walletRechargeTextSecondary = Colors.grey[700];
  static Color walletRechargeTextTeritiary = Colors.grey[700];
  static Color sharePageIconColor = Color(0xFF88cb21);
  static Color sharePageTextColorPrimary = Colors.grey[800];
  static Color sharePageTextColorSecondary = Colors.grey[700];
  static Color sharePageTextColorTeritiary = Color(0xFF88cb21);
  static Color profilePageImageUploadIconColor = Colors.grey[700];
  static Color changePasswordBottomSheetBorder = Colors.grey[400];
  static Color bottomNavigationBarColor = Colors.white;

  static Color imageCacheColor = Color(0xFFFFe100).withOpacity(0.6);
}
