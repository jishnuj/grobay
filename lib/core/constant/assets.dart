class Assets {
  static final String LOGO = "assets/logo.png";
  static final String ICON = "assets/icon/icon.png";
  static final String SPLASH_IMAGE = "assets/icon/icon.png";
  static final String ICON_HEADER = "assets/icon/icon.png";
  static final String SPLASH_IMAGE_ONE = "assets/mocks/sl1.png";
  static final String SPLASH_IMAGE_TWO = "assets/mocks/sl2.png";
  static final String SPLASH_IMAGE_THREE = "assets/mocks/sl3.png";
}