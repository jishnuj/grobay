/// Keys Used to Save Data to LocalStorage
enum StorageKeys {
  isLoggedInKey,
  loginResponseKey,
  usernameKey,
  isFirstTime,
  Pincode,
  UserId
}

extension StorageKeysExtension on StorageKeys {
  String get key {
    switch (this) {
      case StorageKeys.isLoggedInKey:
        return 'isLoggedIn';
      case StorageKeys.loginResponseKey:
        return 'LoginResponse';
      case StorageKeys.usernameKey:
        return 'username';
      case StorageKeys.isFirstTime:
        return 'isFirstTime';
      case StorageKeys.Pincode:
        return 'pincode';
      case StorageKeys.UserId:
        return 'userid';
      default:
        return null;
    }
  }
}
