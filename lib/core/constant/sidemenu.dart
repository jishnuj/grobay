import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/presentation/bloc/favourite/favourite_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:share/share.dart';

List<Map<String, dynamic>> sidemenu = [
  {
    "icon": Icons.home_outlined,
    "name": tr('home'),
    "path": "home",
    "usermenu": 0,
    "onpressed": null
  },
  {
    "icon": Icons.shopping_cart_outlined,
    "name": tr('cart'),
    "path": "cart",
    "usermenu": 0,
    "onpressed": null
  },
  {
    "icon": Icons.notifications,
    "name": tr('notifications'),
    "path": "notification",
    "usermenu": 1,
    "onpressed": null
  },
  {
    "icon": Icons.wallet_travel_outlined,
    "name": tr('walletHistory'),
    "path": "wallet",
    "usermenu": 1,
    "onpressed": null
  },
  {
    "icon": Icons.money,
    "name": tr('transactionHistory'),
    "path": "transaction",
    "usermenu": 1,
    "onpressed": null
  },
  {
    "icon": Icons.ios_share,
    "name": tr('referAndEarn'),
    "path": "refer",
    "usermenu": 1,
    "onpressed": null
  },
  {
    "icon": Icons.home,
    "name": tr('manageAddress'),
    "path": "viewaddress",
    "usermenu": 1,
    "onpressed": null
  },
  {
    "icon": Icons.contact_mail,
    "name": tr('contactUs'),
    "path": "contact",
    "usermenu": 0,
    "onpressed": null
  },
  {
    "icon": Icons.info_outline_rounded,
    "name": tr('aboutUs'),
    "path": "about",
    "usermenu": 0,
    "onpressed": null
  },
  {
    "icon": Icons.rate_review_outlined,
    "name": tr('rateUs'),
    "path": "rate",
    "usermenu": 0,
    "onpressed": (context) async {
      final InAppReview inAppReview = InAppReview.instance;

      if (await inAppReview.isAvailable()) {
        inAppReview.requestReview();
      }
    }
  },
  {
    "icon": Icons.share_outlined,
    "name": tr('shareApp'),
    "path": null,
    "usermenu": 0,
    "onpressed": (context) {
      Share.share(AppConfig.PLAY_STORE_URL);
    }
  },
  {
    "icon": Icons.message_outlined,
    "name": tr('faq'),
    "path": "faq",
    "usermenu": 0,
    "onpressed": null
  },
  {
    "icon": Icons.calendar_today_outlined,
    "name": tr('termsAndConditions'),
    "path": "terms",
    "usermenu": 0,
    "onpressed": null
  },
  {
    "icon": Icons.privacy_tip_outlined,
    "name": tr('privacy'),
    "path": "privacy",
    "usermenu": 0,
    "onpressed": null
  },
  {
    "icon": Icons.logout,
    "name": tr('logout'),
    "path": null,
    "usermenu": 1,
    "onpressed": (context) {
      BlocProvider.of<UserloginBloc>(context).add(FetchLogoutEvent());
      BlocProvider.of<CartCubit>(context).clearAll();
      BlocProvider.of<CartCubit>(context).clear();
      BlocProvider.of<UserloginBloc>(context).clear();
      BlocProvider.of<FavouriteBloc>(context).clear();
      BlocProvider.of<FavouriteBloc>(context).add(ResetFavouriteEvent());
      Navigator.pushNamedAndRemoveUntil(context, "home", (route) => false);
    }
  },
];
