
class AppConfig {
  static const DEFAULT_TIME_FORMAT_INTL = "HH:mm";
  static const DEFAULT_DATE_FORMAT_INTL = "yyyy-MM-dd";

  static const String APP_NAME = "Grobay";
  static const String REFERRAL_URL = "https://grobay.in/levis/refer/";

  static const String API_URL = "https://grobay.in/levis/api-firebase";
  static const String PRODUCT_SHARE_URL = "https://grobay.in/levis/itemdetail/";
  static const String IMAGE_URL = "https://grobay.in/levis/";
  static const String ACCESS_KEY = "90336";
  static const String JWT_KEY =
      "eyJhbGciOiJIUzI1NiJ9.eyJTdWIiOiJHcm9iYXkgQXV0aGVudGljYXRpb24iLCJJc3N1ZXIiOiJTZWd1cm8iLCJleHAiOjI1NDcxMDAzMDQsImlhdCI6MTYzMTk1MTUwNH0.h6OQjcbtul2Dcg3cbnTC7aFVHEoV6nYPHubit8k7bXc";
  static const String JWT_TOKEN_ISSUER = "Seguro";
  static const String JWT_TOKEN_SUB = "Grobay Authentication";
  static const PLAY_STORE_URL =
      "https://play.google.com/store/apps/details?id=com.seguro.grobay";

  static const int PAGE_LIMIT = 10;

  static String CURRENCY = "₹";
  static String COUNTRY_CODE = "IN";
  static String COUNTRY_DIAL_CODE = "+91";
  static int MAXIMUM_ITEMS_ALLOWED_IN_CART = 10;
  static double MINIMUM_ORDER_AMOUNT = 10;
  static double MINIMUM_REFER_AND_ORDER_AMOUNT = 10;
  static double MAXIMUM_REFER_AND_EARN_AMOUNT = 10;
  static double MAXIMUM_WALLET_REFILL_AMOUNT = 10;
  static int MAX_DAYS_TO_RETURN_PRODUCT = 10;
  static int USER_WALLET_REFILL_LIMIT = 10000;

  static String SUPPORT_NUMBER = "";
  static String SUPPORT_EMAIL = "";
  static String CURRENT_APP_VERSION = "";
  static String MINIMUM_APP_VERSION = "";
  static String ADDRESS = "";
  static double LATITUDE = 0.0;
  static double LONGITUDE = 0.0;

  // GoogleMap
  static final String GOOGLE_MAP_KEY =
      "AIzaSyAZTRFqT51Mbg1z0ECRvcEAD61jKuaM_lM";

  //PAYTMCONFIG
  static final String PAYTM_WEBSITE_LIVE_VAL = "WEB";
  static final String PAYTM_INDUSTRY_TYPE_ID_LIVE_VAL = "Retail";
  static final String PAYTM_MOBILE_APP_CHANNEL_ID_LIVE_VAL = "WAP";
  static final String PAYTM_ORDER_PROCESS_LIVE_URL =
      "https://securegw.paytm.in/order/process";
  static final String PAYTM_WEBSITE_DEMO_VAL = "WEBSTAGING";
  static final String PAYTM_INDUSTRY_TYPE_ID_DEMO_VAL = "Retail";
  static final String PAYTM_MOBILE_APP_CHANNEL_ID_DEMO_VAL = "WAP";
  static final String PAYTM_ORDER_PROCESS_DEMO_VAL =
      "https://securegw-stage.paytm.in/order/process";

  //PAYPAL CONFIG
  static String PAYPAL_CLIENTID =
      'ATlzOuS_X4LW0ti3RsuF9eY-d7meQ5Ihr4A5iyaYYjEGAY0SoOwskb-403dSLjnr4pWM3uzCrII2tQrN';
  static String PAYPAL_SECRET =
      'EInEGM-SEtysm1nwS7KW6JlrPChkzELV5vvpVTi9xJKYANXAf7mx-PMGy76y1G9phXX5VExYdQQAocml';
  static String PAYPAL_RETURN_URL = 'return.example.com';
  static String PAYPAL_CANCEL_URL = 'cancel.example.com';
}
