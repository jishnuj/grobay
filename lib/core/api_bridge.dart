import 'dart:io';

import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:grobay/api/request/add_wallet_balance_request.dart';
import 'package:grobay/api/request/base_request.dart';
import 'package:grobay/api/request/cart_request.dart';
import 'package:grobay/api/request/cart_save_request.dart';
import 'package:grobay/api/request/category_request.dart';
import 'package:grobay/api/request/change_password_request.dart';
import 'package:grobay/api/request/cities_request.dart';
import 'package:grobay/api/request/create_razorpay_order_request.dart';
import 'package:grobay/api/request/create_stripe_payment_request.dart';
import 'package:grobay/api/request/create_transaction_request.dart';
import 'package:grobay/api/request/extra_setting_request.dart';
import 'package:grobay/api/request/faq_request.dart';
import 'package:grobay/api/request/favourite_request.dart';
import 'package:grobay/api/request/favourite_save_delete_request.dart';
import 'package:grobay/api/request/fcm_save_request.dart';
import 'package:grobay/api/request/forgot_password_request.dart';
import 'package:grobay/api/request/keyword_request.dart';
import 'package:grobay/api/request/location_request.dart';
import 'package:grobay/api/request/notifications_request.dart';
import 'package:grobay/api/request/order_fetch_request.dart';
import 'package:grobay/api/request/order_save_request.dart';
import 'package:grobay/api/request/order_update_request.dart';
import 'package:grobay/api/request/paytm_checksum_request.dart';
import 'package:grobay/api/request/paytm_validation_request.dart';
import 'package:grobay/api/request/product_request.dart';
import 'package:grobay/api/request/promocode_request.dart';
import 'package:grobay/api/request/seller_request.dart';
import 'package:grobay/api/request/settings_request.dart';
import 'package:grobay/api/request/startup_request.dart';
import 'package:grobay/api/request/subcategory_request.dart';
import 'package:grobay/api/request/transaction_history_request.dart';
import 'package:grobay/api/request/update_profile_request.dart';
import 'package:grobay/api/request/update_profileimage_request.dart';
import 'package:grobay/api/request/user_address_create_request.dart';
import 'package:grobay/api/request/user_address_delete_request.dart';
import 'package:grobay/api/request/user_address_request.dart';
import 'package:grobay/api/request/user_profile_request.dart';
import 'package:grobay/api/request/user_registeration_request.dart';
import 'package:grobay/api/request/userlogin_request.dart';
import 'package:grobay/api/request/variant_request.dart';
import 'package:grobay/api/request/verify_paystack_request.dart';
import 'package:grobay/api/response/add_wallet_response.dart';
import 'package:grobay/api/response/app_settings_response.dart';
import 'package:grobay/api/response/cart_response.dart';
import 'package:grobay/api/response/cart_save_response.dart';
import 'package:grobay/api/response/category_response.dart';
import 'package:grobay/api/response/city_response.dart';
import 'package:grobay/api/response/create_razorpay_order_resposne.dart';
import 'package:grobay/api/response/create_transaction_response.dart';
import 'package:grobay/api/response/extra_setting_response.dart';
import 'package:grobay/api/response/faq_response.dart';
import 'package:grobay/api/response/favourite_response.dart';
import 'package:grobay/api/response/keyword_response.dart';
import 'package:grobay/api/response/location_response.dart';
import 'package:grobay/api/response/notifications_response.dart';
import 'package:grobay/api/response/order_fetch_response.dart';
import 'package:grobay/api/response/order_save_response.dart';
import 'package:grobay/api/response/payment_settings_response.dart';
import 'package:grobay/api/response/paytm_checksum_response.dart';
import 'package:grobay/api/response/paytm_validation_response.dart';
import 'package:grobay/api/response/product_response.dart';
import 'package:grobay/api/response/promocode_response.dart';
import 'package:grobay/api/response/seller_response.dart';
import 'package:grobay/api/response/startup_response.dart';
import 'package:grobay/api/response/subcategory_response.dart';
import 'package:grobay/api/response/time_slots_config_response.dart';
import 'package:grobay/api/response/time_slots_response.dart';
import 'package:grobay/api/response/transaction_history_response.dart';
import 'package:grobay/api/response/update_profileimage_response.dart';
import 'package:grobay/api/response/user_address_response.dart';
import 'package:grobay/api/response/user_address_save_response.dart';
import 'package:grobay/api/response/user_registeration_response.dart';
import 'package:grobay/api/response/user_response.dart';
import 'package:grobay/api/response/varaint_response.dart';
import 'package:grobay/api/services/query_resource_service.dart';
import 'package:grobay/api/services/user_resource_api.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/storage_keys.dart';
import 'package:grobay/core/platform/firebase_service.dart';
import 'package:grobay/core/platform/local_storage_service.dart';
import 'package:logger/logger.dart';

/// This class Provides Common Interface for [CommandResourceApi]
/// , [QueryResourceApi] , [UserResourceApi] and some extra methods
/// NOTE - If this class grows to more than 500 lines
/// refactor by creating a new class and move related methods there
class ApiBridge {
  final logger = Logger();
  LocalStorageService _localStorageService = GetIt.I.get<LocalStorageService>();
  QueryResourceApi _queryResourceApi = GetIt.I.get<QueryResourceApi>();
  UserResourceApi _userResourceApi = GetIt.I.get<UserResourceApi>();
  FireBaseService _fireBaseService = GetIt.I.get<FireBaseService>();
  String _defaulttoken;

  ApiBridge() {
    this._createJwtToken();
  }

  /// Fetch the App meta data
  /// Return Future<AppMetaResponse>
  Future<StartupResposne> getStartupData() async {
    String pincodeId =
        await this._localStorageService.getItem(StorageKeys.Pincode.toString());
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    StartupRequest startupRequest =
        StartupRequest(userId: userId, pincodeId: pincodeId);
    await _addBaseRequestValues(startupRequest);
    return this._queryResourceApi.getStartupData(startupRequest);
  }

  /// Save fcmid
  /// Return Future<bool>
  Future<bool> storeFCMId() async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    String fcmId = await this._fireBaseService.getToken();
    FCMSaveRequest fcmSaveRequest =
        FCMSaveRequest(fcmId: fcmId, userId: userId);
    await _addBaseRequestValues(fcmSaveRequest);
    return this._queryResourceApi.storeFCMId(fcmSaveRequest);
  }

  /// Fetch Contact
  /// Return Future<ExtraSettingResponse>
  Future<ExtraSettingResponse> fetchContact() async {
    ExtraSettingsRequest extraSettingsRequest =
        ExtraSettingsRequest(getContact: "1");
    await _addBaseRequestValues(extraSettingsRequest);
    return this._queryResourceApi.getExtraSettings(extraSettingsRequest);
  }

  /// Fetch About
  /// Return Future<ExtraSettingResponse>
  Future<ExtraSettingResponse> fetchAbout() async {
    ExtraSettingsRequest extraSettingsRequest =
        ExtraSettingsRequest(getAboutUs: "1");
    await _addBaseRequestValues(extraSettingsRequest);
    return this._queryResourceApi.getExtraSettings(extraSettingsRequest);
  }

  /// Fetch Terms
  /// Return Future<ExtraSettingResponse>
  Future<ExtraSettingResponse> fetchTerms() async {
    ExtraSettingsRequest extraSettingsRequest =
        ExtraSettingsRequest(getTerms: "1");
    await _addBaseRequestValues(extraSettingsRequest);
    return this._queryResourceApi.getExtraSettings(extraSettingsRequest);
  }

  /// Fetch Privacy
  /// Return Future<ExtraSettingResponse>
  Future<ExtraSettingResponse> fetchPrivacy() async {
    ExtraSettingsRequest extraSettingsRequest =
        ExtraSettingsRequest(getPrivacy: "1");
    await _addBaseRequestValues(extraSettingsRequest);
    return this._queryResourceApi.getExtraSettings(extraSettingsRequest);
  }

  /// Gets The Payment Config
  /// Return Future<PaymentSettingsResponse>
  Future<PaymentSettingsResponse> getPaymentSettings() async {
    SettingsRequest settingsRequest =
        SettingsRequest(getPaymentSetting: "1", settings: "1");
    await _addBaseRequestValues(settingsRequest);
    return this._queryResourceApi.getPaymentSettings(settingsRequest);
  }

  /// Fetchs the Time Slots
  /// Return Future<TimeSlotsResponse>
  Future<TimeSlotsResponse> getTimeSlots() async {
    SettingsRequest settingsRequest = SettingsRequest(
      getTimeSlots: "1",
    );
    await _addBaseRequestValues(settingsRequest);
    return this._queryResourceApi.getTimeSlots(settingsRequest);
  }

  /// Fetchs the Time Slots Configs
  /// Return Future<TimeSlotsConfigResponse>
  Future<TimeSlotsConfigResponse> getTimeSlotsConfig() async {
    SettingsRequest settingsRequest =
        SettingsRequest(getTimeSlotsConfig: "1", settings: "1");
    await _addBaseRequestValues(settingsRequest);
    return this._queryResourceApi.getTimeSlotConfig(settingsRequest);
  }

  /// Fetchs the AppSettings
  /// Return Future<AppSettingsResponse>
  Future<AppSettingsResponse> getAppSettings() async {
    SettingsRequest settingsRequest =
        SettingsRequest(getTimeZone: "1", settings: "1");
    await _addBaseRequestValues(settingsRequest);
    return this._queryResourceApi.getAppSettings(settingsRequest);
  }

  /// Fetch FAQ
  /// Return Future<FaqResponse>
  Future<FaqResponse> fetchFaq() async {
    Faqrequest faqrequest = Faqrequest();
    await _addBaseRequestValues(faqrequest);
    return this._queryResourceApi.getFaq(faqrequest);
  }

  /// Fetch  All Locations
  /// Return Future<LocationResponse>
  Future<LocationResponse> getLocations({int getPincodes}) async {
    LocationRequest locationRequest =
        LocationRequest(getpincodes: getPincodes ?? 1);
    await _addBaseRequestValues(locationRequest);
    return this._queryResourceApi.getLocations(locationRequest);
  }

  /// Fetch  All Cities
  /// Return Future<CitiesResponse>
  Future<CitiesResponse> getCities() async {
    Citiesrequest citiesrequest = Citiesrequest();
    await _addBaseRequestValues(citiesrequest);
    return this._queryResourceApi.getCities(citiesrequest);
  }

  /// Fetch  All Keyword
  /// Return Future<KeywordResponse>
  Future<KeywordResponse> getKeywords() async {
    KeywordRequest keywordRequest = KeywordRequest();
    await _addBaseRequestValues(keywordRequest);
    return this._queryResourceApi.getKeyword(keywordRequest);
  }

  /// Fetch  All Products
  /// Return Future<ProductResponse>
  Future<ProductResponse> getProducts({
    String offset,
    int getAllProducts,
    int getSimilarProducts,
    String search,
    String categoryId,
    String productId,
    String subcategoryId,
    String sellerId,
  }) async {
    String pincodeId =
        await this._localStorageService.getItem(StorageKeys.Pincode.toString());
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    ProductRequest productRequest = ProductRequest(
        offset: offset,
        getAllProducts: getAllProducts,
        getSimilarProducts: getSimilarProducts,
        search: search,
        productId: productId,
        userId: userId,
        categoryId: categoryId,
        pincodeId: pincodeId,
        subcategoryId: subcategoryId,
        sellerId: sellerId);
    await _addBaseRequestValues(productRequest);
    return this._queryResourceApi.getProducts(productRequest);
  }

  /// Fetch  All Categories
  /// Return Future<CategoryResponse>
  Future<CategoryResponse> getCategories() async {
    CategoryRequest categoryRequest = CategoryRequest();
    await _addBaseRequestValues(categoryRequest);
    return this._queryResourceApi.getCategories(categoryRequest);
  }

  /// Fetch  All Sub Categories
  /// Return Future<SubCategoryResponse>
  Future<SubCategoryResponse> getSubCategories(int categoryId) async {
    SubCategoryRequest subCategoryRequest =
        SubCategoryRequest(categoryId: categoryId);
    await _addBaseRequestValues(subCategoryRequest);
    return this._queryResourceApi.getSubCategories(subCategoryRequest);
  }

  /// Fetch  All Sellers / a Single Seller
  /// Return  Future<SellerResponse>
  Future<SellerResponse> getSeller({String sellerId}) async {
    SellerRequest sellerRequest = SellerRequest(sellerId: sellerId ?? "");
    await _addBaseRequestValues(sellerRequest);
    return this._queryResourceApi.getSellers(sellerRequest);
  }

  /// Fetch all Notification
  /// Return Future<NotificationResponse>
  Future<NotificationResponse> getNotifications() async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    NotificationRequest notificationRequest = NotificationRequest();
    await _addBaseRequestValues(notificationRequest);
    return this._queryResourceApi.getNotifications(notificationRequest);
  }

  /// Fetch  Cart List
  /// Return Future<VariantsResponse>
  Future<VariantsResponse> getCartVaraints({
    List<String> variantIds,
    String pincodeId,
    String userId,
  }) async {
    String vids = variantIds.join(",");
    VariantRequest variantRequest = VariantRequest(
        getVariantsOffline: 1,
        varainatIds: vids,
        userId: userId ?? "",
        pincodeId: pincodeId);
    await _addBaseRequestValues(variantRequest);
    return this._queryResourceApi.getCartVaraints(variantRequest);
  }

  /// Fetch the UserResponse After Logging in
  /// Return Future<UserResponse>
  Future<UserResponse> login({String username, String password}) async {
    String token = await this._fireBaseService.getToken();
    UserLoginrequest userLoginrequest =
        UserLoginrequest(fcmId: token, password: password, mobile: username);
    await _addBaseRequestValues(userLoginrequest);
    return this._userResourceApi.login(userLoginrequest);
  }

  /// Register a user
  /// Return Future<UserRegisterationResponse>
  Future<UserRegisterationResponse> register(String mobileNo, bool isRegister,
      {int countryCode,
      String name,
      String email,
      String password,
      String friendsCode}) async {
    String token = await this._fireBaseService.getToken();
    UserRegisterationRequest userRegisterationRequest =
        UserRegisterationRequest(
      isRegisterUser: isRegister,
      mobile: mobileNo,
      name: name,
      countryCode: countryCode,
      email: email,
      password: password,
      fcmId: token,
      friendsCode: friendsCode ?? "",
    );
    await _addBaseRequestValues(userRegisterationRequest);
    return this._userResourceApi.register(userRegisterationRequest);
  }

  //Verify a user
  Future<Map<String, dynamic>> verifyUser(String mobileNo) async {
    UserRegisterationRequest userRegisterationRequest =
        UserRegisterationRequest(
      isRegisterUser: false,
      mobile: mobileNo,
    );
    await _addBaseRequestValues(userRegisterationRequest);
    UserRegisterationResponse userRegisterationResponse =
        await this._userResourceApi.register(userRegisterationRequest);
    return {
      "status": userRegisterationResponse.error,
      "msg": userRegisterationResponse.message
    };
  }

  /// Fetch the UserResponse After Logging in
  /// Return Future<UserResponse>
  Future<UserResponse> getUserDetails() async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    UserProfileRequest userProfileRequest = UserProfileRequest(userId: userId);
    await _addBaseRequestValues(userProfileRequest);
    return this._userResourceApi.getUserDetails(userProfileRequest);
  }

  /// Update Profile Details
  /// Return Future<String>
  Future<String> updateProfile(
      {String name, String email, String mobile, String fcmId}) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    UpdateProfileRequest updateProfileRequest = UpdateProfileRequest(
        userId: userId, name: name, email: email, mobile: mobile, fcmId: fcmId);
    await _addBaseRequestValues(updateProfileRequest);
    return this._userResourceApi.updateProfile(updateProfileRequest);
  }

  /// Update Profile Image
  /// Return Future<UpdateProfileImageResponse>
  Future<UpdateProfileImageResponse> updateProfileImage(File image) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    UpdateProfileImageRequest updateProfileImageRequest =
        UpdateProfileImageRequest(userId: userId, file: image);
    await _addBaseRequestValues(updateProfileImageRequest);
    return this._userResourceApi.updateProfileImage(updateProfileImageRequest);
  }

  /// Update Profile Password
  /// Return Future<String>
  Future<String> updatePassword({String password}) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    ChangePasswordRequest changePasswordRequest =
        ChangePasswordRequest(userId: userId, password: password);
    await _addBaseRequestValues(changePasswordRequest);
    return this._userResourceApi.updatePassword(changePasswordRequest);
  }

  /// Forgot Password reset
  /// Return Future<bool>
  Future<String> forgotPassword(String password, String mobileNo) async {
    ForgotPasswordRequest forgotPasswordRequest =
        ForgotPasswordRequest(password: password, mobile: mobileNo);
    await _addBaseRequestValues(forgotPasswordRequest);
    return this._userResourceApi.forgotPassword(forgotPasswordRequest);
  }

  /// Save a address
  /// Return Future<UserAddressSaveResponse>
  Future<UserAddressSaveResponse> saveAddress(
      {String address,
      String id,
      String userId,
      String name,
      String mobile,
      String alternateMobile,
      String pincodeId,
      double latitude,
      double longitude,
      String type,
      String state,
      String country,
      String areaId,
      String cityId,
      String landmark,
      String isDefault,
      bool isUpdate}) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    UsserAddressCreateRequest usserAddressCreateRequest =
        UsserAddressCreateRequest(
            id: id,
            userId: userId,
            address: address,
            name: name,
            mobile: mobile,
            alternateMobile: alternateMobile,
            pincodeId: pincodeId,
            isDefault: isDefault,
            isUpdate: isUpdate,
            areaId: areaId,
            cityId: cityId,
            latitude: latitude,
            longitude: longitude,
            type: type,
            state: state,
            country: country,
            landmark: landmark);
    await _addBaseRequestValues(usserAddressCreateRequest);
    return this._userResourceApi.saveAddress(usserAddressCreateRequest);
  }

  /// Fetch all user address
  /// Return Future<UserAddressResponse>
  Future<UserAddressResponse> fetchAddress({int limit, int offset}) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    UserAddressRequest userAddressRequest =
        UserAddressRequest(userId: userId, limit: limit, offset: offset);
    await _addBaseRequestValues(userAddressRequest);
    return this._userResourceApi.fetchAddress(userAddressRequest);
  }

  /// delete address
  /// Return Future<String>
  Future<String> deleteAddress(String addressId) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    UserAddressDeleteRequest userAddressDeleteRequest =
        UserAddressDeleteRequest(id: addressId);
    await _addBaseRequestValues(userAddressDeleteRequest);
    return this._userResourceApi.deleteAddress(userAddressDeleteRequest);
  }

  /// fetch all the favourites
  /// Return Future<FavouriteResponse>
  Future<FavouriteResponse> fetchfavourites() async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    FavouriteRequest favouriteRequest = FavouriteRequest(
      userId: userId,
    );
    await _addBaseRequestValues(favouriteRequest);
    return this._userResourceApi.fetchfavorites(favouriteRequest);
  }

  /// Add a product as favourite
  /// Return Future<String>
  Future<String> addTofavorites(String pid) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    FavouriteSaveDeleteRequest favouriteSaveRequest =
        FavouriteSaveDeleteRequest(userId: userId, productId: pid);
    await _addBaseRequestValues(favouriteSaveRequest);
    return this._userResourceApi.addRemovefavorites(favouriteSaveRequest);
  }

  /// Remove a product as favourite
  /// Return Future<String>
  Future<String> removefavorites(String pid) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    FavouriteSaveDeleteRequest favouriteSaveRequest =
        FavouriteSaveDeleteRequest(
            userId: userId, productId: pid, removeFromFavourites: "1");
    await _addBaseRequestValues(favouriteSaveRequest);
    return this._userResourceApi.addRemovefavorites(favouriteSaveRequest);
  }

  /// Validate Promocode
  /// Return Future<PromoCodeResponse>
  Future<PromoCodeResponse> validatePromoCode(
      String promoCode, String total) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    PromoCodeRequest promoCodeRequest =
        PromoCodeRequest(userId: userId, promoCode: promoCode, total: total);
    await _addBaseRequestValues(promoCodeRequest);
    return this._userResourceApi.validatePromoCode(promoCodeRequest);
  }

  /// Fetch User Cart
  /// Return Future<CartResponse>
  Future<CartResponse> getUserCart({
    String addressId,
    String pincodeId,
  }) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    Cartrequest cartrequest =
        Cartrequest(addressId: addressId, userId: userId, pincodeId: pincodeId);
    await _addBaseRequestValues(cartrequest);
    return this._userResourceApi.getUserCart(cartrequest);
  }

  /// Save a Item to Cart
  /// Return Future<CartSaveResponse>
  Future<CartSaveResponse> saveItemToCart(
      {String pid, String pvid, String qty}) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    CartSaverequest cartSaverequest = CartSaverequest(
        userId: userId, productId: pid, productvriantId: pvid, qty: qty);
    await _addBaseRequestValues(cartSaverequest);
    return this._userResourceApi.saveItemToCart(cartSaverequest);
  }

  /// Save Multiple Items to cart
  /// Return Future<CartSaveResponse>
  Future<CartSaveResponse> saveItemsToCart(
      {List<String> pid, List<String> pvid, List<String> qty}) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    CartSaverequest cartSaverequest = CartSaverequest(
        userId: userId,
        productvriantId: pvid.join(","),
        qty: qty.join(","),
        addMultipleItems: true);
    await _addBaseRequestValues(cartSaverequest);
    return this._userResourceApi.saveItemToCart(cartSaverequest);
  }

  /// Empty Cart
  /// Return Future<CartSaveResponse>
  Future<CartSaveResponse> removeAllFromCart(
      {List<String> pid, List<String> pvid, List<String> qty}) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    CartSaverequest cartSaverequest =
        CartSaverequest(userId: userId, removeAllFromcart: true);
    await _addBaseRequestValues(cartSaverequest);
    return this._userResourceApi.saveItemToCart(cartSaverequest);
  }

  /// Creates new order
  /// Return Future<CartSaveResponse>
  Future<OrderSaveResponse> createOrder({
    @required String quantity,
    @required String addressId,
    @required String walletBalanceUsed,
    @required String deliveryTime,
    @required String orderNote,
    @required String total,
    @required String deliveryCharge,
    @required String finalTotal,
    @required String placeOrder,
    @required String walletused,
    @required String productVariantId,
    @required String paymentMethod,
    @required String promoCode,
    @required String promoCodeDiscount,
  }) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    OrderSaveRequest orderRequest = OrderSaveRequest(
        userId: userId,
        quantity: quantity,
        addressId: addressId,
        walletBalanceUsed: walletBalanceUsed,
        deliveryTime: deliveryTime,
        orderNote: orderNote,
        total: total,
        deliveryCharge: deliveryCharge,
        finalTotal: finalTotal,
        placeOrder: placeOrder,
        walletused: walletused,
        productVariantId: productVariantId,
        paymentMethod: paymentMethod,
        promoCode: promoCode,
        promoCodeDiscount: promoCodeDiscount);
    await _addBaseRequestValues(orderRequest);
    return this._userResourceApi.createOrder(orderRequest);
  }

  /// Fetch All Orders
  /// Return Future<OrderFetchResponse>
  Future<OrderFetchResponse> fetchAllOrders(
      {String limit, String offset}) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    OrderFetchRequest orderFetchRequest =
        OrderFetchRequest(userId: userId, offset: offset, limit: limit);
    await _addBaseRequestValues(orderFetchRequest);
    return this._userResourceApi.fetchOrders(orderFetchRequest);
  }

  /// Fetch Order By Order Id
  /// Return Future<OrderFetchResponse>
  Future<OrderFetchResponse> fetchOrderById(String orderId) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    OrderFetchRequest orderFetchRequest =
        OrderFetchRequest(userId: userId, orderId: orderId);
    await _addBaseRequestValues(orderFetchRequest);
    return this._userResourceApi.fetchOrders(orderFetchRequest);
  }

  /// Update order status
  /// Return Future<String>
  Future<String> updateOrderRequest(
      String updateOrderStatus, String orderId, String orderItemId) async {
    OrderUpdateRequest orderUpdateRequest = OrderUpdateRequest(
        status: updateOrderStatus, orderId: orderId, orderItemId: orderItemId);
    await _addBaseRequestValues(orderUpdateRequest);
    return this._userResourceApi.updateOrderRequest(orderUpdateRequest);
  }

  /// Fetch all Wallet or user Transaction history
  /// Return Future<OrderFetchResponse>
  Future<TransactionHistoryResponse> fetchTransactionHistory(String type,
      {String limit, String offset}) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    TransactionHistoryRequest transactionHistoryRequest =
        TransactionHistoryRequest(
            userId: userId, type: type, offset: offset, limit: limit);
    await _addBaseRequestValues(transactionHistoryRequest);
    return this
        ._userResourceApi
        .fetchTransactionHistory(transactionHistoryRequest);
  }

  /// Create Transaction
  /// Return Future<CreateTransactionResponse>
  Future<CreateTransactionResponse> createTransaction({
    String txnId,
    String transactiondate,
    String amount,
    String taxPercentage,
    String type,
    String orderId,
    String status,
  }) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    CreateTransactionRequest createTransactionRequest =
        CreateTransactionRequest(
            userId: userId,
            txnId: txnId,
            transactiondate: transactiondate,
            amount: amount,
            taxPercentage: taxPercentage,
            type: type,
            orderId: orderId,
            status: status);
    await _addBaseRequestValues(createTransactionRequest);
    return this._userResourceApi.createTransaction(createTransactionRequest);
  }

  /// Create Transaction
  /// Return Future<CreateTransactionResponse>
  Future<AddWalletResponse> createWalletTransaction(
      {String amount, String message}) async {
    String userId =
        await this._localStorageService.getItem(StorageKeys.UserId.toString());
    AddWalletBalanceRequest addWalletBalanceRequest = AddWalletBalanceRequest(
        userId: userId, amount: amount, message: message);
    await _addBaseRequestValues(addWalletBalanceRequest);
    return this
        ._userResourceApi
        .createWalletTransaction(addWalletBalanceRequest);
  }

  /// Create Stripe Payment
  /// Return Future<String>
  Future<Map<dynamic, dynamic>> createStripePayment(
      {String username,
      String address,
      String postalCode,
      String city,
      String orderId,
      String amount}) async {
    CreateStripePaymentRequest createStripePaymentRequest =
        CreateStripePaymentRequest(
            amount: amount,
            username: username,
            addresss: address,
            postalCode: postalCode,
            city: city,
            orderId: orderId);
    await _addBaseRequestValues(createStripePaymentRequest);
    return this
        ._userResourceApi
        .createStripePayment(createStripePaymentRequest);
  }

  /// Create Paytm checksum
  /// Return Future<String>
  Future<PaytmChecksumResponse> createPaytmChecksum(
      {String cust_id,
      String channel_id,
      String order_id,
      String txn_amount,
      String industry_type_id,
      String website}) async {
    PaytmChecksumRequest paytmChecksumRequest = PaytmChecksumRequest(
        cust_id: cust_id,
        channel_id: channel_id,
        order_id: order_id,
        txn_amount: txn_amount,
        industry_type_id: industry_type_id,
        website: website);
    await _addBaseRequestValues(paytmChecksumRequest);
    return this._userResourceApi.createPaytmChecksum(paytmChecksumRequest);
  }

  /// Verify Paytm transaction
  /// Return Future<PaytmValidationResponse>
  Future<PaytmValidationResponse> verifyPaytmValidation(
      {String orderId}) async {
    PaytmValidationRequest paytmValidationRequest =
        PaytmValidationRequest(order_id: orderId);
    await _addBaseRequestValues(paytmValidationRequest);
    return this._userResourceApi.verifyPaytmValidation(paytmValidationRequest);
  }

  /// Verify Paystack Payment
  /// Return Future<String>
  Future<String> verifyPaystackTransaction(
      {String email, String reference, String amount}) async {
    Verifypaystackrequest verifypaystackrequest = Verifypaystackrequest(
        email: email,
        reference: reference,
        amount: (double.parse(amount) * 100).toInt().toString());
    await _addBaseRequestValues(verifypaystackrequest);
    return this
        ._userResourceApi
        .verifyPaystackTransaction(verifypaystackrequest);
  }

  /// Create Stripe Payment
  /// Return Future<String>
  Future<CreateRazorpayOrderResponse> createRazorpayOrder(
      {String amount}) async {
    CreateRazorpayOrderRequest createStripePaymentRequest =
        CreateRazorpayOrderRequest(
      amount: amount,
    );
    await _addBaseRequestValues(createStripePaymentRequest);
    return this
        ._userResourceApi
        .createRazorpayOrder(createStripePaymentRequest);
  }

  // Other Util Methods

  Future<void> logout() async {
    await this._localStorageService.deleteAll();
  }

  /// This methods Sets the Required BaseRequest Fields
  /// Return Future<String>
  /// If logged in then the value from [LoginResponse]
  /// is used else the fields are set to default values
  /// also returns token if logged in else empty string
  Future<String> _addBaseRequestValues(BaseRequest request) async {
    bool isLoggedIn = await checkIfAlreadyLoggedIn();
    request.accessKey = AppConfig.ACCESS_KEY;
    if (isLoggedIn) {
      // LoginResponse loginResponse = await this.getLoginResponse();
      // request.token = loginResponse.token;

    } else {
      request.token = this._defaulttoken ?? this._createJwtToken();
    }
    return null;
  }

  /// Check if a the User is Logged in
  /// Returns Future<bool>
  /// true if logged in else falsetoken
  Future<bool> checkIfAlreadyLoggedIn() async {
    String value =
        await this._localStorageService.getItem(StorageKeys.isLoggedInKey.key);
    bool isLoggedIn = value == 'true';
    logger.i("Checking if logged in =>" + isLoggedIn.toString());
    return isLoggedIn;
  }

  /// Check if a the App is Opened first Time
  /// Returns Future<bool>
  /// true if logged in else false
  Future<bool> checkIfFirstTimeOpeningApp() async {
    String value =
        await this._localStorageService.getItem(StorageKeys.isFirstTime.key);
    bool isFirstTime = (value == null);
    logger.i("Checking if App Openend FirstTime =>" + isFirstTime.toString());
    return isFirstTime;
  }

  String _createJwtToken() {
    // Create a json web token
    if (_defaulttoken == null) {
      final jwt = JWT(
        {},
        subject: AppConfig.JWT_TOKEN_SUB,
        issuer: AppConfig.JWT_TOKEN_ISSUER,
      );
      this._defaulttoken = jwt.sign(SecretKey(AppConfig.JWT_KEY));
    }
    Logger().i(this._defaulttoken);
    return _defaulttoken;
  }
}
