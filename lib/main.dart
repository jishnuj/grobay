import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:grobay/core/config/app_config.dart';
import 'package:grobay/core/constant/palette.dart';
import 'package:grobay/core/dependency.dart';
import 'package:grobay/core/theme/app_theme.dart';
import 'package:grobay/presentation/bloc/add_wallet_balance/add_wallet_balance_bloc.dart';
import 'package:grobay/presentation/bloc/app_settings/app_settings_bloc.dart';
import 'package:grobay/presentation/bloc/cart/cart_bloc.dart';
import 'package:grobay/presentation/bloc/category/category_bloc.dart';
import 'package:grobay/presentation/bloc/change_password/changepassword_bloc.dart';
import 'package:grobay/presentation/bloc/cities/cities_bloc.dart';
import 'package:grobay/presentation/bloc/extra_settings/extrasettings_bloc.dart';
import 'package:grobay/presentation/bloc/favourite/favourite_bloc.dart';
import 'package:grobay/presentation/bloc/forgot_password/forgot_password_bloc.dart';
import 'package:grobay/presentation/bloc/keyword/keyword_bloc.dart';
import 'package:grobay/presentation/bloc/location/location_bloc.dart';
import 'package:grobay/presentation/bloc/notification/notification_bloc.dart';
import 'package:grobay/presentation/bloc/order/order_bloc.dart';
import 'package:grobay/presentation/bloc/payment_confirm/payment_confirm_bloc.dart';
import 'package:grobay/presentation/bloc/product/product_bloc.dart';
import 'package:grobay/presentation/bloc/search/search_bloc.dart';
import 'package:grobay/presentation/bloc/seller/seller_bloc.dart';
import 'package:grobay/presentation/bloc/settings/settings_bloc.dart';
import 'package:grobay/presentation/bloc/startup/startup_bloc.dart';
import 'package:grobay/presentation/bloc/subcategory/subcategory_bloc.dart';
import 'package:grobay/presentation/bloc/transaction/transaction_bloc.dart';
import 'package:grobay/presentation/bloc/user_address/useraddress_bloc.dart';
import 'package:grobay/presentation/bloc/user_location/user_location_bloc.dart';
import 'package:grobay/presentation/bloc/user_login/userlogin_bloc.dart';
import 'package:grobay/presentation/bloc/user_register/user_register_bloc.dart';
import 'package:grobay/presentation/cubit/cart_cubit.dart';
import 'package:grobay/presentation/cubit/order_cubit.dart';
import 'package:grobay/presentation/cubit/settings_cubit.dart';
import 'package:grobay/presentation/cubit/wishlistchangenotifier_cubit.dart';
import 'package:grobay/presentation/page/extras/about_us_page.dart';
import 'package:grobay/presentation/page/address/add_address_page.dart';
import 'package:grobay/presentation/page/cart/cart_page.dart';
import 'package:grobay/presentation/page/checkout_page.dart';
import 'package:grobay/presentation/page/extras/contact_us_page.dart';
import 'package:grobay/presentation/page/extras/faq_page.dart';
import 'package:grobay/presentation/page/auth/forgot_password_page.dart';
import 'package:grobay/presentation/page/intro_page.dart';
import 'package:grobay/presentation/page/home_page.dart';
import 'package:grobay/presentation/page/auth/login_page.dart';
import 'package:grobay/presentation/page/notifications_page.dart';
import 'package:grobay/presentation/page/order/order_details_page.dart';
import 'package:grobay/presentation/page/extras/privacy_policy_page.dart';
import 'package:grobay/presentation/page/order/order_process_payment_page.dart';
import 'package:grobay/presentation/page/order/order_time_payment_select_page.dart';
import 'package:grobay/presentation/page/product/product_detail_page.dart';
import 'package:grobay/presentation/page/product/product_list_page.dart';
import 'package:grobay/presentation/page/profile_page.dart';
import 'package:grobay/presentation/page/auth/register_page.dart';
import 'package:grobay/presentation/page/refer_page.dart';
import 'package:grobay/presentation/page/search_page.dart';
import 'package:grobay/presentation/page/address/address_select_page.dart';
import 'package:grobay/presentation/page/seller/sellers_list_page.dart';
import 'package:grobay/presentation/page/splash_page.dart';
import 'package:grobay/presentation/page/extras/terms_condition_page.dart';
import 'package:grobay/presentation/page/transaction_history_page.dart';
import 'package:grobay/presentation/page/wallet/wallet_history_page.dart';
import 'package:grobay/presentation/widget/payment_widgets/blocs/create_paytm_order/create_paytm_bloc.dart';
import 'package:grobay/presentation/widget/payment_widgets/blocs/create_razorpay_order/create_razorpay_order_bloc.dart';
import 'package:grobay/presentation/widget/payment_widgets/blocs/create_stripe_payment/create_stripe_payment_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:easy_localization/easy_localization.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: await getTemporaryDirectory(),
  );
  // Logger.level = Level.verbose;
  Logger.level = Level.nothing;
  await setupDependencies();
  await EasyLocalization.ensureInitialized();
  runApp(
    EasyLocalization(
        supportedLocales: [Locale('en'), Locale('de', 'DE')],
        path: 'assets/translations',
        fallbackLocale: Locale('en'),
        child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    setNotificationAction(context);
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => StartupBloc()),
        BlocProvider(create: (context) => LocationBloc()),
        BlocProvider(create: (context) => SearchBloc()),
        BlocProvider(create: (context) => ProductBloc()),
        BlocProvider(create: (context) => KeywordBloc()),
        BlocProvider(create: (context) => CategoryBloc()),
        BlocProvider(create: (context) => SubcategoryBloc()),
        BlocProvider(create: (context) => SellerBloc()),
        BlocProvider(create: (context) => CartCubit()),
        BlocProvider(create: (context) => WishlistchangenotifierCubit()),
        BlocProvider(create: (context) => CartBloc()),
        BlocProvider(create: (context) => UserloginBloc()),
        BlocProvider(create: (context) => UserLocationBloc()),
        BlocProvider(create: (context) => UserRegisterBloc()),
        BlocProvider(create: (context) => FavouriteBloc()),
        BlocProvider(create: (context) => UseraddressBloc()),
        BlocProvider(create: (context) => CitiesBloc()),
        BlocProvider(create: (context) => SettingsBloc()),
        BlocProvider(create: (context) => AppSettingsBloc()),
        BlocProvider(create: (context) => SettingsCubit()),
        BlocProvider(create: (context) => ChangepasswordBloc()),
        BlocProvider(create: (context) => OrderBloc()),
        BlocProvider(create: (context) => OrderCubit()),
        BlocProvider(create: (context) => TransactionBloc()),
        BlocProvider(create: (context) => ExtrasettingsBloc()),
        BlocProvider(create: (context) => PaymentConfirmBloc()),
        BlocProvider(create: (context) => CreateStripePaymentBloc()),
        BlocProvider(create: (context) => AddWalletBalanceBloc()),
        BlocProvider(create: (context) => CreateRazorpayOrderBloc()),
        BlocProvider(create: (context) => NotificationBloc()),
        BlocProvider(create: (context) => ForgotPasswordBloc()),
        BlocProvider(create: (context) => CreatePaytmBloc()),
      ],
      child: RefreshConfiguration(
        footerTriggerDistance: 15,
        dragSpeedRatio: 0.91,
        headerBuilder: () => MaterialClassicHeader(),
        footerBuilder: () => ClassicFooter(),
        enableLoadingWhenNoData: false,
        enableRefreshVibrate: false,
        enableLoadMoreVibrate: false,
        shouldFooterFollowWhenNotFull: (state) {
          // If you want load more with noMoreData state ,may be you should return false
          return false;
        },
        child: GetMaterialApp(
          supportedLocales: [
            Locale("af"),
            Locale("am"),
            Locale("ar"),
            Locale("az"),
            Locale("be"),
            Locale("bg"),
            Locale("bn"),
            Locale("bs"),
            Locale("ca"),
            Locale("cs"),
            Locale("da"),
            Locale("de"),
            Locale("el"),
            Locale("en"),
            Locale("es"),
            Locale("et"),
            Locale("fa"),
            Locale("fi"),
            Locale("fr"),
            Locale("gl"),
            Locale("ha"),
            Locale("he"),
            Locale("hi"),
            Locale("hr"),
            Locale("hu"),
            Locale("hy"),
            Locale("id"),
            Locale("is"),
            Locale("it"),
            Locale("ja"),
            Locale("ka"),
            Locale("kk"),
            Locale("km"),
            Locale("ko"),
            Locale("ku"),
            Locale("ky"),
            Locale("lt"),
            Locale("lv"),
            Locale("mk"),
            Locale("ml"),
            Locale("mn"),
            Locale("ms"),
            Locale("nb"),
            Locale("nl"),
            Locale("nn"),
            Locale("no"),
            Locale("pl"),
            Locale("ps"),
            Locale("pt"),
            Locale("ro"),
            Locale("ru"),
            Locale("sd"),
            Locale("sk"),
            Locale("sl"),
            Locale("so"),
            Locale("sq"),
            Locale("sr"),
            Locale("sv"),
            Locale("ta"),
            Locale("tg"),
            Locale("th"),
            Locale("tk"),
            Locale("tr"),
            Locale("tt"),
            Locale("uk"),
            Locale("ug"),
            Locale("ur"),
            Locale("uz"),
            Locale("vi"),
            Locale("zh")
          ],
          localizationsDelegates: context.localizationDelegates,
          debugShowCheckedModeBanner: false,
          builder: (BuildContext context, Widget widget) {
            ErrorWidget.builder = (FlutterErrorDetails errorDetails) {
              return Container(
                child: Text("Error"),
              );
            };

            return widget;
          },
          title: AppConfig.APP_NAME,
          theme: appThemes[AppThemes.PrimaryTheme],
          initialRoute: "/",
          routes: {
            "/": (context) => SplashPage(),
            "intro": (context) => IntroPage(),
            "home": (context) => HomePage(),
            "sellers": (context) => SellersListPage(),
            "products": (context) => ProductListPage(),
            "search": (context) => SearchPage(),
            "login": (context) => LoginPage(),
            "register": (context) => RegisterPage(),
            "cart": (context) => CartPage(),
            "selectaddress": (context) => AddressSelectpage(),
            "viewaddress": (context) => AddressSelectpage(
                  viewMode: true,
                ),
            "addaddress": (context) => AddAddressPage(),
            "checkout": (context) => CheckoutPage(),
            "ordertimepaymentselect": (context) => OrderTimePaymentSelectPage(),
            "orderprocess": (context) => OrderProcessPaymentpage(),
            "product": (context) => ProductDetailsPage(),
            "orderdetails": (context) => OrderDetailsPage(),
            "notification": (context) => NotificationsPage(),
            "profile": (context) => ProfilePage(),
            "transaction": (context) => TransactionHistoryPage(),
            "wallet": (context) => WalletHistoryPage(),
            "refer": (context) => ReferPage(),
            //Other
            "about": (context) => AboutUsPage(),
            "contact": (context) => ContactUsPage(),
            "faq": (context) => FaqPage(),
            "terms": (context) => TermsAndConditionsPage(),
            "privacy": (context) => PrivacyPolicyPage(),
            "forgotpassword": (context) => ForgotPasswordPage(),
          },
        ),
      ),
    );
  }

  setNotificationAction(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.dark,
        statusBarBrightness: Brightness.light,
        systemNavigationBarColor: Palette.bottomNavigationBarColor,
        systemNavigationBarIconBrightness: Brightness.dark,
        statusBarColor: Palette.primary));
  }
}
